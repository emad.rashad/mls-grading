<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="" />
    <meta name="Classification" content="" />
    <meta name="RESOURCE-TYPE" content="DOCUMENT" />
    <meta name="DISTRIBUTION" content="GLOBAL" />
    <meta name="robots" content="all" />
    <meta name="revisit-after" content="5 days" />
    <meta name="rating" content="general" />
    <meta http-equiv="Content-Language" content="" />
    <meta name="author" content=""/>
  <?php
   $main = "";
  if(isset($_GET["alias"])){
    $node_alias = $_GET["alias"];
    if($opened_url_file == "post_details.php"){
         $post_info = $define_node->front_node_data(null,'post',$node_alias, null,$lang_info->id, null, null, null, null,  null,'one');
         if(!empty($post_info)){
           $main = $post_info;
           if(!empty($post_info->cover_image)){
               $image = $post_info->cover_image;
             $parts = explode('/',$image);
             $image_cover = $parts[count($parts)-1];
             $folder = $parts[count($parts)-2];
           }else{
             $folder = "";
             $image_cover = "";
           }
           $website_new_title = $post_info->title;
          echo "  <meta property='og:url' content='{$define_general_setting->site_url}post_details.php?lang=$lang;&alias=$post_info->alias' />
          <meta property'og:image' content='{$define_general_setting->site_url}/media-library-thumb/$folder/large/$image_cover '/>
          <meta property='og:description' content='".mb_substr(strip_tags($post_info->summary),0,110,'utf-8')."'/>
          <meta property='og:title' content='$post_info->title' />";
        }
          }else if($opened_url_file == "content.php"){
        $page_info =  $define_node->front_node_data(null,'page',$node_alias,null,$lang_info->id,null,null, null, null,null, 'one',null,null);
         if(!empty($page_info)){
           $main = $page_info;
           if($page_info->cover_image){
             $image = $page_info->cover_image;
             $parts = explode('/',$image);
             $image_cover = $parts[count($parts)-1];
             $folder = $parts[count($parts)-2];
           }else{
             $folder = "";
             $image_cover = "";
           }
             $website_new_title = $page_info->title;
          echo "  <meta property='og:url' content='{$define_general_setting->site_url}post_details.php?lang=$lang;&alias=$page_info->alias' />
            <meta property'og:image' content='{$define_general_setting->site_url}/media-library-thumb/$folder/large/$image_cover '/>
            <meta property='og:description' content='".mb_substr(strip_tags($page_info->summary),0,110,'utf-8')."'/>
            <meta property='og:title' content='$page_info->title' />";
         }
      }
  }else{
    $website_new_title = $website_title;
    ?>
        <meta property="og:type" content="website" />
        <meta property="og:title" content="<?php echo $website_new_title;?>" />
        <meta property="og:description" content="<?php echo $define_general_setting->description;?>" />
        <meta property="og:url" content="<?php echo $define_general_setting->site_url;?>" />
        <meta property="og:site_name" content="<?php  echo $website_new_title?>" />
  <?php }?>

    <title><?php echo $website_new_title ; ?></title>

    <!-- Bootstrap -->
    <link href='main/css/bootstrap.min.css' rel='stylesheet'>

    <!--font-awesome-->
    <link href='main/css/font-awesome.min.css' rel='stylesheet'>

    <link rel='stylesheet' href='main/css/jquery.datetimepicker.css'>


    <!--Custom-->
    <link href='main/css/style-en.css' rel='stylesheet'>
    <link href='main/css/responsive-en.css' rel='stylesheet'>

     
    <!-- angular Style  --> 
    <link href='main/css/angular-style.css' rel='stylesheet'>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src='https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js'></script>
      <script src='https://oss.maxcdn.com/respond/1.4.2/respond.min.js'></script>
    <![endif]-->
  </head>
  <body ng-app="useAngular">
    
    <header class='col-md-12 header'><!--start of header-->
      <nav class='clearfix header-top-nav'><!--start of header top nav-->
        <div class='container'><!--start of container-->

          <div class='navbar-header'><!--start of navbar header-->
            <button class='navbar-toggle' data-target='#header-collapse' data-toggle='collapse'>
              <span class='icon-bar'></span>
              <span class='icon-bar'></span>
              <span class='icon-bar'></span>
            </button>
          </div><!--end of navbar header-->

          <div class='collapse navbar-collapse header-collapse' id='header-collapse'> <!--start of header collapse-->
            <ul class='nav navbar-nav give-flex'>
              <li><a href='index.php'>Home</a></li>


              <?php 
              /*
                starting coding to get the main menu in dynamic way 
                                                                  */ 
                $mainMenu = $define_menu_link->menu_submenu_front_data('sorting', 'ASC', 'main_menu', 0, $lang_info->id); 
                // check if main menu exists or not : 
                if(count($mainMenu)){
                    //loop through main menu 
                 
                  foreach($mainMenu as $mainLink){
                          
                    // get sub menu : 
                    $sub_menu = $define_menu_link->menu_submenu_front_data('sorting', 'ASC' , 'main_menu' , $mainLink->id , $lang_info->id); 
                    if($sub_menu){

                          $get_path_assigned_to_link = $define_node->get_node_content($mainLink->path , $lang_info->id); 

                            echo "<li class='dropdown'>
                            <a href='' class='dropdown-toggle' data-toggle='dropdown'>$mainLink->title</a>
                            <ul class='dropdown-menu header-mega-menu'>"; 
                            echo " <li class='clearfix'>
                            <ul class='col-md-12 header-mega-list'>"; 
                           

                            foreach($sub_menu as $sub_link){
                              $path_link_data = $define_node->get_node_content($sub_link->path,$lang_info->id);
                              $path = "";
                              if($sub_link->path_type == "page"){
                                $path = "content.php";
                              }else if($sub_link->path_type == "post"){
                                $path = "post_details.php";
                              }else if($sub_link->path_type == "event"){
                                $path = "event_details.php";
                              }
                              if($sub_link->path_type == "external"){
                                echo "<li><a href='content.php?alias=$get_path_assigned_to_link->alias$sub_link->external_path'>$sub_link->title</a></li>";
                              }else{
                                echo "<li><a href='$path?alias=$path_link_data->alias'>$sub_link->title</a></li>";
                              }
                             
                            }
                              echo "</ul>"; 
                              // get info : 
                              $get_data = $define_node_content->get_from_content($get_path_assigned_to_link->alias , $lang_info->id); 
                              $page_id = $get_data->node_id; 
                              echo $page_id ; 
                              $page_info =  $define_node->front_node_data($page_id,'page',$get_path_assigned_to_link->alias,null,$lang_info->id,null,null, null, null,null, 'one',null,null);
                               
                             
                              echo " <div class='col-md-12 header-mega-image' style='background-image: url(media-library/$page_info->cover_image);'></div>"; 
                              echo "</li>"; 
                              echo "</ul></li>"; 
                    }else{



                    $get_path_assigned_to_link = $define_node->get_node_content($mainLink->path , $lang_info->id); 
                    //check if path is page or post or event  
                    $path = ''; 
                    //check if it is page  : 
                    if($mainLink->path_type == 'page'){
                      $path = 'content.php'; 
                    //check if it is post
                    }else if($mainLink->path_type == 'post'){
                      $path = "post_details.php";
                    //check if it is event    
                    }else if($mainLink->path_type == 'event'){
                      $path="event_details.php";
                    }

                    // check if link is external :
                    if($mainLink->path_type =='external'){
                      echo "<li><a href='$mainLink->external_path'>$mainLink->title</a></li>"; 
                    }else{
                      echo "<li><a href='$path?alias=$get_path_assigned_to_link->alias'>$mainLink->title</a></li>" ; 
                    }
                   }

                  }
                  echo "</ul>"; 
                }


              ?>
             
             
          </div><!--end of header collapse-->

        </div><!--end of container-->
      </nav><!--end of header top nav-->
     
      <div class='clearfix header-middle-nav'><!--start of header middle nav-->
        <div class='container'>
          <a href='index.php' class='header-logo'><img src='main/images/logo.png' alt=''></a>

          <?php 
          // under header links part  : 
          $info_header_id = 7 ; 
          $info_header_part = $define_node->get_node_content($info_header_id , $lang_info->id);

          $info_header_hotLine = strip_tags($info_header_part->summary); 
          $info_header_hotLine_limit = string_limit_words($info_header_hotLine , 5); 

          $info_header_message = strip_tags($info_header_part->body); 
          $info_header_message_limit = string_limit_words($info_header_message , 10);  

          ?>
          <ul class='col-md-12 header-contact-list'>
            <li><p class='fa fa-phone'></p><?php echo $info_header_hotLine_limit ;  ?></li>
            <li><a href=""><p class='fa fa-facebook'></p><?php echo $info_header_message_limit ;  ?></a></li>
          </ul>
        </div>
      </div><!--end of header middle nav-->
    </header><!--end of header-->
    
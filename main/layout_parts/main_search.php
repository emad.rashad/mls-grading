 <!--==============content Goes here==============================-->
    <section class='col-md-12 main-search' style='background-image: url(main/images/search_bg.jpg);'><!--start of main search-->
      <div class='container'>
        <form action='' class='col-md-12 primary-form main-search-form'>
          <h2 class='col-md-12 give-red-c'>Search</h2>
          <div class='form-group'>
            <input type='text' class='form-control' placeholder='From' id='datetimepicker'>
            <p class='fa fa-calendar primary-calendar'></p>
          </div>
          <div class='form-group'>
            <input type='text' class='form-control' placeholder='To' id='datetimepicker2'>
            <p class='fa fa-calendar primary-calendar'></p>
          </div>
           <div class='form-group text-right'>
            <input type='submit' class='btn' value='Search'>
          </div>
        </form>
      </div>
    </section><!--end of main search-->
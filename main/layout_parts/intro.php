    <?php 
    // intro page info : 
    $intro_page_id = 8 ; 
    $intro_page_info = $define_node->get_node_content($intro_page_id , $lang_info->id);

    //getting summary while stripping tags except br tag : 
    $intro_page_summary = strip_tags($intro_page_info->summary , '<br>'); 
    //limit the summary : 
    $intro_page_summary_limited = string_limit_words($intro_page_summary , 10);  
    //getting body text : 
    $intro_page_body = strip_tags($intro_page_info->body); 
    $intro_page_body_limit = string_limit_words($intro_page_body , 50); 
    ?>
    <section class='col-md-12 main-intro'><!--start of main intro-->
      <div class='container'>
        <h1 class='col-md-12 main-intro-title give-white-c'>
         <?php echo $intro_page_summary_limited ;  ?>
        </h1>
        <p class='col-md-12 main-intro-text give-white-c'><?php echo $intro_page_body_limit ; ?></p>
      </div>
    </section><!--end of main intro-->
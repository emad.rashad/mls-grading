 <footer class='col-md-12 footer' style='background-image: url(main/images/footer_bg.jpg);'><!--start-of footer-->
       <div class='container'>
         <div class='row clearfix'>
            


             <?php 
             $footer_info_id = 16 ; 
             $footer_info_page = $define_node->get_node_content($footer_info_id , $lang_info->id);


             echo $footer_info_page->summary;  
             echo $footer_info_page->body;  
             ?>

            <div class='col-md-4 footer-column' id="App1" ng-controller="valController">
              <h3 class='col-md-12 footer-column-title give-title-border'>Contact Us</h3>

              <form name="contactForm"  action='main/data_model/contact_us.php' method="post" id="contact_us" class='col-md-12 primary-form footer-contact-form' ng-submit="submitForm(contactForm.$valid)" novalidate>

                <div class='form-group' ng-class="{'has-error': contactForm.user_name.$invalid && !contactForm.user_name.$pristine}" >

                 <input type='text' class='form-control' name="user_name" ng-model="userName"  ng-minlength = "5" ng-maxlength="20" id="name" placeholder='Name' required>
                 <span id="userName" class="glyphicon glyphicon-ok  form-control-feedback "></span>
                 <p class="help-block" ng-show="contactForm.user_name.$invalid && !contactForm.user_name.$pristine">* Name Is Required</p>
                 <p class="help-block" ng-show="contactForm.user_name.$error.minlength">* Name Is Too Short (min 5)</p>
                 <p class="help-block" ng-show="contactForm.user_name.$error.maxlength">* Name Is Too Long (max 20)</p>
                 
                 
               </div>

               <div class='form-group' ng-class="{'has-error': contactForm.user_email.$invalid && !contactForm.user_email.$pristine}">
                
                 <input type='email' class='form-control' id="email" name="user_email"  ng-model="userEmail"  placeholder='Email' required>
                 <span id="userEmail" class="glyphicon glyphicon-ok  form-control-feedback "></span>

                 <p class="help-block" ng-show="contactForm.user_email.$invalid && !contactForm.user_email.$pristine">* Email Is Required</p>
                 <p class="help-block" ng-show="contactForm.user_email.$error.email">* This Isn't A Valid Email</p>
                
                  
                 
               </div>

                <div class='form-group' ng-class="{'has-error': contactForm.user_message.$invalid && !contactForm.user_message.$pristine}">

                 <textarea name='user_message' id='message' class='form-control'   ng-model = "userMessage"   placeholder='Message' required></textarea>
                 <span id="userMessage" class="glyphicon glyphicon-ok  form-control-feedback "></span>
                 
                 <p class="help-block" ng-show="contactForm.user_message.$invalid && !contactForm.user_message.$pristine">* Message Is Required</p>

               </div>

                 <div class='form-group' >
                  <input type='submit'  id="submit" class='btn' value='Send' ng-disabled="contactForm.$invalid">
                </div>

                <div id="loading_data">
                  
                </div>

              </form>
            </div>
         </div>
       </div>
       <div class='clearfix footer-copyright'>
          <div class='container'>
             <p class='pull-left'>Copyright © 2016 <span class='col-md-12 give-red-c'>Rodymar Shipping Line</span>. All rights reserved.</p>
             <p class='pull-right'>Powered By <a href='http://diva-lab.com' target='_blank' class='col-md-12 give-red-c'>Diva-lab</a></p>
          </div>
       </div>
    </footer><!--end-of footer-->
    
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src=' main/js/jquery-1.11.3.min.js'></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src='main/js/bootstrap.min.js'></script>
    <!--mobile friendly-->
    <script src='main/js/jquery.mobile.custom.min.js'></script>

   <script src="main/js/angular.js"></script>
    <!-- Custom angular script -->     
    <script src="main/js/myScript.js"></script>
    <!--mobile friendly-->
    <script src=' main/js/jquery.datetimepicker.js'></script>
    <!--custom-->
    <script src='main/js/script.js'></script>

    <!-- contact us js file -->
    <script src='main/js-crud/contact_us.js'></script>

     <script> 
      //load tabs by # 
      $(document).ready(function(){ 
          var hash = location.hash; 
          if (hash && hash != '#') { 
              hash=hash.substr(1); /* remove the "#" */ 
              $('.nav-tabs a').filter(function () { 
                  return $(this).attr('href').indexOf(hash) > -1; 
              }).click();/* click the matching tab */ 
          } 
      }); 
      </script>
    
  
    
  </body>
</html>
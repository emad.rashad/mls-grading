
   <?php 
   // get all maximum 4 posts 
   // posts here will be based on  category and  front page assign to it  . 

   $front_posts = $define_node->front_node_data(null,"post",null,null,$lang_info->id,"yes","no",1,null, null,'many',4);



   ?>


   <section class='col-md-12 main-posts'><!--start of main posts-->
      <div class='container'>
        <div class='row clearfix'>

          <?php 
          // we will loop through all posts to get wanted  . 
          foreach($front_posts as $post){

            echo " <article class='col-md-6 primary-post give-triangle'><!--post-->"; 
            echo " <a href='post_details.php?alias=$post->alias' class='col-md-6 primary-post-image' style='background-image: url(media-library/$post->cover_image);'></a>";
            echo "<div class='col-md-6 primary-post-text give-post-bg '>" ; 
            echo "<h1 class='col-md-12 give-red-c'><a href='post_details.php?alias=$post->alias'>$post->title</a></h1>"; 
            $post_summary = strip_tags($post->summary); 
            $post_summary_limit = string_limit_words($post_summary , 20); 
            echo "<p>$post_summary_limit</p>";
            echo "</div>"; 
            echo "</article><!--post-->";  

          }

          ?>
  
        </div>
      </div>
    </section><!--start of main posts-->
<?php 

require_once('../../classes/FrontSession.php'); 

require_once('../../classes/Customers.php'); 

require_once("../../classes/MysqlDatabase.php"); 


header('Content-Type: application/json'); 

if(isset($_POST["task"]) && $_POST["task"] == "login"){ 

	if(isset($_POST["email"]) && isset($_POST["password"])){ 

		$email = $_POST["email"]; 

		$password = $_POST["password"]; 
		
		$find_employee = Customers::find_by_useremail_pass($email, $password); 

		//send message 

		if($find_employee){ 
		
			 $front_session->Login($find_employee); 
			 $data  = array("status"=>"work"); 
			 echo json_encode($data); 

		}else{ 

			$data  = array("status"=>"error"); 

			echo json_encode($data); 

		}		 

	}

} 

//close connection 

if(isset($database)){ 

	$database->close_connection(); 

} 

?>	
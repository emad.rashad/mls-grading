<?php 
require_once('../../classes/FrontSession.php'); 
require_once('../../classes/Orders.php'); 
require_once('../../classes/Nodes.php');
require_once("../../classes/MysqlDatabase.php"); 
require_once("../../classes/Quantity_Distribution.php"); 
require_once("../../classes/NodesSelectedTaxonomies.php");
 
header('Content-Type: application/json'); 

if(isset($_POST["task"]) && $_POST["task"] == "reserve"){ 
   
   
   //get the product_id 
   $product_id = $_POST['prod_id'];
   //get the strore id : 
   $store_id = $_POST['store_id']; 
   // get the quantity employee has entered it  : 
   $quantity_wanted = $_POST['quantity']; 
   //now get the quantity upon selecetd option : 
   $check_quantity = Quantity_Distribution::find_exhibtions($product_id , $store_id); 
   //this is the selected quantity 
   $quantity_per_selected = $check_quantity->total_quantity ;
   
  

   if($quantity_wanted > $quantity_per_selected){   
	    $data  = array("status"=>"quantity_wrong"); 
		echo json_encode($data);	
  
	}else if($quantity_wanted == ""){
		
		$data  = array("status"=>"quantity_empty"); 
		echo json_encode($data);
		
	}else{ 
	    


	    $get_product = Nodes::find_by_custom_filed('id' , $product_id); 
		$total_quantity_for_product  =  $get_product->prod_qty ; 
		$final_product_qty =  $total_quantity_for_product - $quantity_wanted ; 
		$get_product->prod_qty = $final_product_qty;
		$get_product->update(); 
		
		// update old quantity in quantity distribution  : 
	    $check_quantity->total_quantity = $quantity_per_selected - $quantity_wanted ; 
		$done = $check_quantity->update(); 

		
		
	    $define_taxonomies = new NodesSelectedTaxonomies(); 
        $define_taxonomies->enable_relation() ; 
        $get_data_for_category = $define_taxonomies->return_taxonomy_nodes($store_id ,"page","category",'one',1);
		
			
		//beginning database working    
	    $add_to_orders = new Orders();
		
		$add_to_orders->customer_name  = $_POST['customer_name'];  
		$add_to_orders->customer_phone = $_POST['customer_phone']; 
		$add_to_orders->employee_code = $_POST['employee_code']; 
		$add_to_orders->employee_id = $_POST['employee_id'] ; 
		$add_to_orders->employee_name = $_POST['full_name']; 
		$add_to_orders->exhibtion_id = $_POST['store_id']; 
		$add_to_orders->product_id = $_POST['prod_id'] ; 
		$add_to_orders->sold_quantity = $_POST['quantity'] ; 
		$add_to_orders->invoice_date = date("Y-m-d H:i:s") ; 
		$add_to_orders->exhibtion_name = $get_data_for_category->node_title ; 
		$add_to_orders->product_name = $_POST['product_name'] ; 
		

		$fine_then_add = $add_to_orders->insert() ; 
		
		
		if($done && $fine_then_add){
		 
	
        $data  = array("status"=>"work"); 
		echo json_encode($data);
		}
	}
   
   }



//close connection 

if(isset($database)){ 

	$database->close_connection(); 

} 

?>	
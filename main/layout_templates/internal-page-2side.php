<?php
    $title = "";
	$summary = "";
	if($node_type == "page"){
		$node_id = $get_page_content->id;
		$node_layout = $page_layout_info->model;
		$title =  $get_page_content->title;
		
	 }else if($node_type == "post"){
		$node_id = $get_post_content->id;
		$node_layout = $post_layout_info->model;
		$title =  $get_post_content->title;
		$category = $define_node_tax->return_node_taxonomy($get_post_content->id,'post','category','one',$lang_info->id);
		 
		//$page_data = $define_node_tax->return_taxonomy_nodes($category->id,"post","category",'one',$lang_info->id);
	}else{
		$node_id = $get_event_content->id;
		$node_layout = $event_layout_info->model;
		$title =  $get_event_content->title;
		 
	}
	
?> 

<section class="col-md-12 main-search internal-cover" style="background-image: url(main/images/search_bg.jpg);"><!--start of main search-->
      <div class="container">
        <div class="col-md-12 internal-cover-links">
        	<h1 class="col-md-12 give-red-c"><?php echo $title ; ?></h1>
       
		        <ol class="breadcrumb internal-path-crumb">
		          <li><a href="index.php">Home</a></li>
		          <?php
				    if($node_type == "page"){

						
						  echo " <li class='active '><a onclick='return false'>$title</a></li>";
						
					}else if($node_type == "post"){
							if(isset($_GET['pg_id'])){
							$page_id = $_GET["pg_id"];
							$page_data = $define_node->get_node_content($page_id,$lang_info->id);
							echo "<li><a href='content.php?alias=$page_data->alias'>$page_data->title</a></li>";
							echo " <li class='active '><a onclick='return false'>$title</a></li>";
						}else{
							echo " <li class='active '><a onclick='return false'>$title</a></li>";
						}
					}else{
						
						  $page = $define_node_tax->return_taxonomy_nodes($category->id,"page","category", 'one',$lang_info->id);
						  if($page){
							  
							  echo "<li><a href='content.php?alias=$page->node_alias'>$page->node_title</a></li>";
							  echo " <li class='active'> $title</li>";
						  }
					}
				  
				   ?>
		         
		        </ol>
		        </div>
		        	<!--- Search Form inside breadcrumb --> 
		        	<form action='' class='col-md-12 primary-form main-search-form give-static give-flex'>
			          <h2 class='col-md-12 give-red-c'>Search</h2>
			          <div class='form-group'>
			            <input type='text' class='form-control' placeholder='From' id='datetimepicker'>
			            <p class='fa fa-calendar primary-calendar'></p>
			          </div>
			          <div class='form-group'>
			            <input type='text' class='form-control' placeholder='To' id='datetimepicker2'>
			            <p class='fa fa-calendar primary-calendar'></p>
			          </div>
			           <div class='form-group text-right'>
			            <input type='submit' class='btn' value='Search'>
			          </div>
			        </form>
		       </div>
    </section><!--end of main search-->
    
    
   <section class="col-md-12 main-intro main-internal"><!--start of main intro-->
      <div class="container">
     
 <?php 
 
 // this is the plugin value we put it here between our tags 
 //get left models by model ids 
				$define_theme_left_plugins = new ThemeLayoutModelPlugin();
				 $define_theme_left_plugins->enable_relation();
				 $left_plugins = $define_theme_left_plugins->front_get_model_plugin($node_layout, 'left');
				foreach($left_plugins as $plugin){
					require_once("plugins/".$plugin->plugin_source."/viewer.php");
					
				}
				
?>
</div>
</section>
 
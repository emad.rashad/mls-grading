$(document).ready(function (){

//get email val and trim it

var email = $('#employee_email');

//get pass val and trim it

var password = $('#employee_password');

//when button clicked call vaild email and password function

$("#submit").click(function (){

	//alert("test");

  if(valid_email() && valid_password()){

         //disable button

         $('#submit').attr("disabled", "true");

		 //show laoding 

         var type = $('#login_form').attr('method');

         var url = 'main/data_model/login_process.php';

		 		 //send json data

         var data = {	

		 				task: 'login',

                        email: email.val(), 

                        password: password.val()

                    };

		 $.ajax({

             type: type,

             url: url,

             data: data,

			 dataType:"JSON",

			 beforeSend: function(){

				  $('#email_wrong').text('');

				  $('#password_wrong').text(''); 

       			  $('#loading_data_error').html('<img src="main/images/loading.gif"/>&nbspLoading..');

			 },

             success: function(data){

				 if(data.status == "work"){

					window.location.href = "index.php";
			     }else{

					  $('#submit').removeAttr('disabled');

					 $('#loading_data_error').css("color","#000").html('البيانات المدخله غير صحيحه');

				 }

             }

         });

           return false;

      }else{

         valid_email();

         valid_password();

         return false;

      } 

 }) ;   

//function valid email

  function valid_email(){

       if(email.val() == ''){

           $('#email_wrong').css("color","#000").text('من فضلك قم بادخال بريدك الالكترونى');

           return false;

       }else{

           $('#email_wrong').text('');

           return true;

       }

   }

   

 //function valid password

   function valid_password(){

        if(password.val() ==''){

           $('#password_wrong').css("color","#000").text('من فضلك قم بادخال كلمه المرور');

           return false;

       }else{

           $('#password_wrong').text('');

           return true;

       }

   }  

})
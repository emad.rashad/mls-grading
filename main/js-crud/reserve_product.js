$(document).ready(function (){

$("#submit_reserve_form").click(function (){


$('#submit_reserve_form').attr("disabled", "true");

var type = $('#reserve_form').attr('method');

var url = 'main/data_model/reserve_product.php';

//send json data

var data = {	

  task: 'reserve',
  full_name :$('#full_name').val() , 
  employee_id : $('#employee_id').val() , 
  product_name :$('#product_name').val() , 
  quantity : $('#quantity').val() , 
  prod_id :$('#prod_id').val(), 
  customer_name:$('#customer_name').val() ,
  customer_phone:$('#customer_phone').val(), 
  employee_code:$('#employee_code').val()

};

$.ajax({

type: type,

url: url,

data: data,

dataType:"JSON",

beforeSend: function(){

$('#loading_data').html('<img src="main/images/loading.gif"/>&nbspLoading..');

},

success: function(data){

if(data.status == "quantity_wrong"){
$('#submit_reserve_form').removeAttr('disabled');
$('#loading_data').css("color","#fcfc58").html('الكميه المدخله غير متاحه');




}else if(data.status == "empty_quantity"){

$('#submit_reserve_form').removeAttr('disabled');
$('#loading_data').css("color","#fcfc58").html('يجب ادخال الكميه اولا قبل الضغط على زر التاكيد');
	
}else{
$('#reserve_form')[0].reset();
$('#submit_reserve_form').removeAttr("disabled");
$('#loading_data').html('<div class="alert alert-success alert-block fade in"><span style ="font-size:16px">تم الحجز بنجاح</span></div>');
window.setTimeout(function() {
    window.location.href = 'index.php';
}, 5000);

}

}

});

return false;


}) ;  


});
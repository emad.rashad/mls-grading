jQuery(document).ready(function($) { 
	// process the form
	$('#subscribe').submit(function(event) {
	var formData = {
					email:$('#sub_email').val(),
					
					
		};
		// process the form
		$.ajax({
			type 		: 'POST', // define the type of HTTP verb we want to use (POST for our form)
			url 		: 'main/data_model/add_email_subscription.php', // the url where we want to POST
			data 		: formData, // our data object
			dataType 	: 'json', // what type of data do we expect back from the server
                        encode          : true
		})
			// using the done promise callback
			.done(function(data) {
                 if(data.status == 'work'){
			       $("#fromEmailHolderMessage_sub").show();
				   
					   $("#fromEmailHolderMessage_sub").html('<div class="alert alert-success alert-block fade in"><span style ="font-size:16px">Thank you subscribe to our newsletter  </span></div>');
				  
				   $('#subscribe')[0].reset();
                    setTimeout(function() { $('#fromEmailHolderMessage_sub').hide();}, 5000);
				 
				    $('#submit').removeAttr('disabled');
				   
				}else if(data.status == 'Wrong_email'){
					
					$("#fromEmailHolderMessage_sub").show();
					
					  $("#fromEmailHolderMessage_sub").html('<div class="alert alert-block alert-danger fade in"><span style ="font-size:16px">E-mail the wrong formula </span></div>');
				  
					 $('#submit').removeAttr('disabled');
				}else if(data.status == 'Wrong_email_exist'){
					    $('#subscribe')[0].reset();
					
					$("#fromEmailHolderMessage_sub").show();
					
					
					$("#fromEmailHolderMessage_sub").html('<div class="alert alert-block alert-danger fade in"><span style ="font-size:16px">Email already exists</span></div>');
				
					 $('#submit').removeAttr('disabled');
					
				}else if(data.status == 'valid_error'){
					 $("#fromEmailHolderMessage_sub").show();
				 $('#fromEmailHolderMessage_sub').html('<div class="alert alert-block alert-danger fade in"><span style ="font-size:16px">'+data.fileds+'</span></div>');
				 
				 $('#submit').removeAttr('disabled');
				 
				}else{ 
					$("#fromEmailHolderMessage_sub").html('<div class="alert alert-block alert-danger fade in"><span style ="font-size:16px">Please enter your e-mail</span></div>');
						
					
			     $("#fromEmailHolderMessage_sub").show();
				 $('#subscribe')[0].reset();
			     }
			});
		// stop the form from submitting the normal way and refreshing the page
		event.preventDefault();
	});
});
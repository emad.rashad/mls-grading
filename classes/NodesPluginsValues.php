<?php 
require_once 'CRUD.php'; 
class NodesPluginsValues extends CRUD{ 
   //calss attributes 
   public $id; 
   public $plugin_id; 
   public $lang_id; 
   public $node_id; 
   public $content; 
   public $type; 
   //relation table attribute 
   //push attributes for relational tables 
   public function enable_relation(){ 
		array_push(static::$primary_fields , ''); 
   }    
    
   //define table name and fields 
	protected static $table_name = 'nodes_plugins_values'; 
	protected static $primary_fields = array('id','node_id','lang_id','plugin_id','content','type'); 
	 
	public static function get_plugin_values($plugin_id,$lang,$node_id,$type){ 
		$sql = "SELECT * FROM nodes_plugins_values WHERE plugin_id = '$plugin_id'  
		        AND node_id = '$node_id' AND  type = '$type' "; 
		if(!empty($lang)){ 
			$sql .= " AND lang_id = '{$lang}'"; 
		} 
        $result_array = static::find_by_sql($sql); 
		return !empty($result_array)? array_shift($result_array):false; 
	}  
  } 
?> 

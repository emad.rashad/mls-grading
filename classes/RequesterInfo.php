<?php 
require_once 'CRUD.php'; 
class RequesterInfo extends CRUD{ 
   //calss attributes 
   public $id; 
   public $user_name;  
   public $node_id; 
   public $email; 
   
   //push attributes for relational tables 
   public function enable_relation(){ 
		array_push(static::$primary_fields); 
   }    
   //define table name and fields 
	protected static $table_name = 'requester_info'; 
	protected static $primary_fields = array('id','user_name','node_id','email'); 
    public function requester_info_data($node_id){ 
		$sql = "SELECT requester_info.id AS id,requester_info.user_name AS user_name,requester_info.node_id AS node_id, 
		        requester_info.email AS email 
				FROM requester_info 
				AND requester_info.node_id = $node_id"; 
				$result_array = static::find_by_sql($sql); 
			    return !empty($result_array)? array_shift($result_array) : false;		 
	 
	}  
	 
	 
    
} 
?> 

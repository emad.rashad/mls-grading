<?php 
require_once 'CRUD.php'; 
class MidYearPrimarySecondThird extends CRUD{ 
   //class attributes 
   public $id; 
   public $student_code; 
   public $student_name; 
   public $class;
   public $stage;
   public $seat_number;
   public $max ;  
   public $year; 
   public $arabic;
   public $arabic_eval;
   public $maths;
   public $maths_eval;
   public $english_ol;
   public $english_ol_eval;
   public $religion;
   public $religion_eval;
   public $english_al;
   public $english_al_eval;
   public $french_max ; 
   public $french;
   public $french_eval;
   public $extra_curricular_one;
   public $extra_curricular_one_eval;
   public $extra_curricular_two;
   public $extra_curricular_two_eval;
   public $art;
   public $art_eval;
   public $p_e;
   public $p_e_eval;
 
   //push attributes for relational tables 
   public function enable_relation(){ 
      array_push(static::$primary_fields , ''); 
   }    
   //define table name and fields 
   protected static $table_name = 'midyear_primary_second_third'; 
   protected static $primary_fields = array('id','student_code','student_name','class','stage','seat_number','max' ,'year','arabic', 'arabic_eval','maths','maths_eval','english_ol','english_ol_eval' 
   ,'religion','religion_eval','english_al','english_al_eval','french_max', 'french','french_eval','extra_curricular_one','extra_curricular_one_eval','extra_curricular_two','extra_curricular_two_eval','art','art_eval','p_e','p_e_eval'); 
    
     public static function findMidYearPrimarySecondThird($student_code){

         global $database ; 

         $student_code = $database->escape_values($student_code) ; 
         $sql = " SELECT * FROM ".self::$table_name."  WHERE student_code = '{$student_code}' LIMIT 1 " ; 
         $result = self::find_by_sql($sql) ; 
         return !empty($result) ? array_shift($result) : false ; 
     }
} 
?> 
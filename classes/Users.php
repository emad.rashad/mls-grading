<?php 
require_once 'CRUD.php'; 
class Users extends CRUD{ 
   //calss attributes 
   public $id; 
   public $user_name; 
   public $first_name; 
   public $last_name; 
   public $email; 
   public $password; 
   public $inserted_by; 
   public $inserted_date; 
   public $update_by; 
   public $last_update; 
   public $user_profile; 
   //relation table attribute 
    public $user_profile_id; 
    
    //push attributes for relational tables 
   public function enable_relation(){ 
		array_push(static::$primary_fields , 'user_profile_id'); 
   }    
   //define table name and fields 
	protected static $table_name = 'users'; 
	protected static $primary_fields = array('id', 'user_name', 'first_name', 'last_name' , 'email', 'password', 
	'inserted_by','inserted_date', 'update_by', 'last_update', 'user_profile'); 
   //get by users email and pass 
   public static function find_by_username_pass($user_name, $pass){ 
	   global $database; 
	   $user_name = $database->escape_values($user_name); 
	   $password = $database->escape_values($pass); 
       $sql = "SELECT * FROM ".self::$table_name." WHERE user_name = '{$user_name}' AND password = '{$password}' limit 1"; 
       $result_set = self::find_by_sql($sql); 
       return !empty($result_set)? array_shift($result_set) : false; 
   } 
    
   public  function full_name(){ 
		if(isset($this->first_name) && isset($this->last_name)){ 
			return 	$this->first_name." ".$this->last_name; 
		}else{ 
			return false;	 
		} 
   } 
    
	//get user data 	 
	public function user_data($sort_filed = null, $order_by = null, $id = null , $profile = null , $date_from = null ,$date_to = null ,$inserted_by = null){ 
		$sql = "SELECT users.id AS id, users.user_name AS user_name, users.first_name AS first_name, users.last_name AS last_name, users.email AS email, 
				user3.user_name AS inserted_by, users.inserted_date AS inserted_date, user2.user_name AS update_by, users.last_update AS last_update,  
				profile.title as user_profile , users.user_profile AS user_profile_id 
				FROM users  
				LEFT JOIN users AS user3 ON users.inserted_by = user3.id 
				LEFT JOIN users AS user2 ON users.update_by = user2.id 
				LEFT JOIN profile on users.user_profile = profile.id  WHERE 1"; 
		if(!empty($id)){		 
			 $sql .= " AND users.id = $id "; 
			 $result_array = static::find_by_sql($sql); 
			 return !empty($result_array)? array_shift($result_array) : false; 
		}else{ 
			if(!empty($profile)){ 
			 $sql .="  AND users.user_profile = '$profile' ";	 
			} 
			 if(!empty($date_from) || !empty($date_to)){ 
			 $sql .=" AND CAST(users.inserted_date as DATE)  BETWEEN '$date_from' AND '$date_to' ";	 
			} 
			if(!empty($inserted_by)){ 
			 $inserted_by_string = implode(",",$inserted_by); 
			 $sql .=" AND  Users.inserted_by  in  ($inserted_by_string) ";	 
			}			 
			if(!empty($sort_filed) && !empty($order_by)){ 
				$sql .= " ORDER BY ".$sort_filed." ".$order_by;  
			 } 
			return self::find_by_sql($sql);   
		}				 
	} 	    
} 
?> 

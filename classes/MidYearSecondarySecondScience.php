<?php 
require_once 'CRUD.php'; 
class MidYearSecondarySecondScience extends CRUD{ 
   //class attributes 
   public $id; 
   public $student_code; 
   public $student_name; 
   public $class;
   public $stage;
   public $seat_number;  
   public $year; 
   public $arabic_max;
   public $arabic;
   public $english_ol_max;
   public $english_ol;
   public $french_ol_max;
   public $french_ol;
   public $algebra_max;
   public $algebra;
   public $calculus_trig_max;
   public $calculus_trig;
   public $physics_max;
   public $physics;
   public $chemistry_max;
   public $chemistry;
   public $biology_max;
   public $biology;
   public $mechanics_max;
   public $mechanics;
  
   public $religion_max;
   public $religion;
   public $civics_max;
   public $civics;
   public $english_al_max;
   public $english_al;
   public $french_al_max;
   public $french_al;
   public $computer_max;
   public $computer;

   public $agriculture_max;
   public $agriculture;
 
 
   //push attributes for relational tables 
   public function enable_relation(){ 
      array_push(static::$primary_fields , ''); 
   }    
   //define table name and fields 
   protected static $table_name = 'midyear_secondary_second_science'; 
   protected static $primary_fields = array('id','student_code','student_name','class','stage','seat_number' ,'year','arabic_max', 'arabic','english_ol_max','english_ol','french_ol_max','french_ol' 
   ,'algebra_max','algebra','calculus_trig_max','calculus_trig','physics_max','physics', 'chemistry_max','chemistry' , 'biology_max','biology' ,'mechanics_max','mechanics'
   ,'religion_max','religion','civics_max','civics','english_al_max','english_al','french_al_max','french_al','computer_max','computer' , 'agriculture_max','agriculture'); 
    
     public static function findMidYearSecondarySecondScience($student_code){

         global $database ; 

         $student_code = $database->escape_values($student_code) ; 
         $sql = " SELECT * FROM ".self::$table_name."  WHERE student_code = '{$student_code}' LIMIT 1 " ; 
         $result = self::find_by_sql($sql) ; 
         return !empty($result) ? array_shift($result) : false ; 
     }
} 
?> 
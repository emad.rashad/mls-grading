<?php
require_once 'CRUD.php';
class FormAttributes extends CRUD{
   //calss attributes
   public $id;
   public $attribute_label;
   public $sorting;
   public $form_id;
   public $required;
   public $attribute_id;
   public $attribute_values;
   public $inserted_by;
   public $inserted_date;
   public $update_by;
   public $last_update;
   //relation table attribute
   public $form_name;
   public $form_label;
   public $attribute_type;
   //push attributes for relational tables
   public function enable_relation(){
		array_push(static::$primary_fields,'form_name','form_label','attribute_type');
   }   
   //define table name and fields
	protected static $table_name = 'form_attributes';
	protected static $primary_fields = array('id','attribute_label','sorting','form_id','attribute_id','attribute_values',
	'required','inserted_by','inserted_date','last_update','update_by');
	
	
	public function front_form_attributes_data($form_id){
		$sql = "SELECT form_attributes.attribute_label AS attribute_label, attributes.type AS attribute_type,
		       form_attributes.attribute_values AS attribute_values 
				FROM form_attributes, attributes
				WHERE attributes.id = form_attributes.attribute_id AND form_attributes.form_id = {$form_id}  
				ORDER BY form_attributes.sorting ASC";	
		return self::find_by_sql($sql);  			
		
	}
	public function form_attribute_data($sort_filed = null, $order_by = null, $id = null,$form_id = null){
		$sql = "SELECT  DISTINCT form_attributes.id AS id, form_attributes.form_id AS form_id, form_attributes.attribute_label AS attribute_label,
				form_attributes.attribute_id AS attribute_id,form_attributes.attribute_values AS attribute_values,users.user_name AS inserted_by,
				forms.name AS form_name,form_attributes.inserted_date AS inserted_date,attributes.type AS attribute_type,
				user2.user_name AS update_by,form_attributes.last_update AS last_update,
				form_attributes.required AS required
				FROM form_attributes
				LEFT JOIN users ON form_attributes.inserted_by = users.id
				LEFT JOIN forms ON form_attributes.form_id = forms.id
				LEFT JOIN attributes ON form_attributes.attribute_id = attributes.id
				LEFT JOIN users AS user2 ON form_attributes.update_by = user2.id
				WHERE 1";
		if(!empty($id)){		
			 $sql .= " AND  form_attributes.id = $id ";
			 $result_array = static::find_by_sql($sql);
			 return !empty($result_array)? array_shift($result_array) : false;
		}else{
			if(!empty($form_id)){
				$sql .= " And  form_attributes.form_id = $form_id ";
			}
					
			if(!empty($sort_filed) && !empty($order_by)){
				$sql .= " ORDER BY ".$sort_filed." ".$order_by; 
			 }
			return self::find_by_sql($sql);  
		}
	}			
}
?>

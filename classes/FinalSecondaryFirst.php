<?php 
require_once 'CRUD.php'; 
class FinalSecondaryFirst extends CRUD{ 
   //class attributes 
   public $id; 
   public $student_code; 
   public $student_name; 
   public $class;
   public $stage;
   public $seat_number;
   public $year;
   public $arabic_max;
   public $arabic_min;
   public $arabic;
   public $arabic_st;
   public $english_ol_max;
   public $english_ol_min;
   public $english_ol;
   public $english_ol_st;
   public $french_ol_max;
   public $french_ol_min;
   public $french_ol;
   public $french_ol_st;
   public $algebra_max;
   public $algebra_min;
   public $algebra;
   public $algebra_st;
   public $geometry_max;
   public $geometry_min;
   public $geometry;
   public $geometry_st;
   public $physics_max;
   public $physics_min;
   public $physics;
   public $physics_st;
   public $chemistry_max;
   public $chemistry_min;
   public $chemistry;
   public $chemistry_st; 
   public $biology_max;
   public $biology_min;
   public $biology;
   public $biology_st; 
   public $history_max;
   public $history_min;
   public $history;
   public $history_st; 
   public $geography_max;
   public $geography_min;
   public $geography;
   public $geography_st; 
   public $philosophy_max;
   public $philosophy_min;
   public $philosophy;
   public $philosophy_st; 
   public $total;
   public $religion_max;
   public $religion_min;
   public $religion;
   public $religion_st;
   public $civics_max;
   public $civics_min;
   public $civics;
   public $civics_st;
   public $english_al_max;
   public $english_al_min;
   public $english_al;
   public $english_al_st;
   public $french_al_max;
   public $french_al_min;
   public $french_al;
   public $french_al_st;
   public $computer_max;
   public $computer_min;
   public $computer;
   public $computer_st;
   public $re_exam_start;
   
   //relation table attribute 
   
   //push attributes for relational tables 
   public function enable_relation(){ 
		array_push(static::$primary_fields , ''); 
   }    
   //define table name and fields 
	protected static $table_name = 'final_secondary_first'; 
	protected static $primary_fields = array('id','student_code','student_name','class','stage','seat_number','year', 'arabic_max','arabic_min','arabic','arabic_st','english_ol_max','english_ol_min','english_ol','english_ol_st','french_ol_max','french_ol_min','french_ol','french_ol_st','algebra_max','algebra_min','algebra','algebra_st','geometry_max','geometry_min','geometry','geometry_st','physics_max','physics_min','physics','physics_st','chemistry_max','chemistry_min','chemistry','chemistry_st','biology_max','biology_min','biology','biology_st','history_max','history_min','history','history_st','geography_max','geography_min','geography','geography_st','philosophy_max','philosophy_min','philosophy','philosophy_st','total','religion_max','religion_min','religion','religion_st' ,'civics_max','civics_min','civics','civics_st','english_al_max','english_al_min','english_al','english_al_st','french_al_max','french_al_min','french_al','french_al_st','computer_max','computer_min','computer','computer_st','re_exam_start'); 
	 
	  public static function findFinalSecondaryFirst($student_code){

         global $database ; 

         $student_code = $database->escape_values($student_code) ; 
         $sql = " SELECT * FROM ".self::$table_name."  WHERE student_code = '{$student_code}' LIMIT 1 " ; 
         $result = self::find_by_sql($sql) ; 
         return !empty($result) ? array_shift($result) : false ; 
     }
} 
?> 
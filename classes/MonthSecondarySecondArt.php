<?php 
require_once 'CRUD.php'; 
class MonthSecondarySecondArt extends CRUD{ 
   //class attributes 
   public $id; 
   public $student_code; 
   public $student_name; 
   public $class;
   public $stage;
   public $seat_number;  
   public $year;  
   public $month; 
   public $arabic_max;
   public $arabic_min;
   public $arabic;
   public $english_ol_max;
   public $english_ol_min;
   public $english_ol;
   public $french_ol_max;
   public $french_ol_min;
   public $french_ol;
   public $algebra_max;
   public $algebra_min;
   public $algebra;
   public $calculus_trig_max;
   public $calculus_trig_min;
   public $calculus_trig;
   public $history_max ; 
   public $history_min ; 
   public $history;
   public $philosophy_max;
   public $philosophy_min;
   public $philosophy;
   public $psychology_max;
   public $psychology_min;
   public $psychology;
   public $geography_max;
   public $geography_min;
   public $geography;

   public $total ; 
   public $religion_max;
   public $religion_min;
   public $religion;
   public $human_rights_max;
   public $human_rights_min;
   public $human_rights;
   public $computer_max;
   public $computer_min;
   public $computer;
   public $english_al_max;
   public $english_al_min;
   public $english_al;
   public $french_al_max;
   public $french_al_min;
   public $french_al;
   public $attendance_max;
   public $attendance_min;
   public $attendance;
  
 
 
   //push attributes for relational tables 
   public function enable_relation(){ 
      array_push(static::$primary_fields , ''); 
   }    
   //define table name and fields 
   protected static $table_name = 'months_secondary_second_art'; 
   protected static $primary_fields = array('id','student_code','student_name','class','stage','seat_number' ,'year', 'month','arabic_max','arabic_min', 'arabic','english_ol_max','english_ol_min','english_ol','french_ol_max','french_ol_min','french_ol' ,'algebra_max','algebra_min','algebra','calculus_trig_max','calculus_trig_min','calculus_trig','history_max','history_min', 'history','philosophy_max','philosophy_min','philosophy','psychology_max','psychology_min','psychology','geography_max','geography_min','geography','total','religion_max','religion_min','religion','human_rights_max','human_rights_min','human_rights','computer_max','computer_min','computer','english_al_max','english_al_min','english_al','french_al_max','french_al_min','french_al','attendance_max','attendance_min','attendance'); 
    
     public static function findMonthSecondarySecondArt($student_code){

         global $database ; 

         $student_code = $database->escape_values($student_code) ; 
         $sql = " SELECT * FROM ".self::$table_name."  WHERE student_code = '{$student_code}' LIMIT 1 " ; 
         $result = self::find_by_sql($sql) ; 
         return !empty($result) ? array_shift($result) : false ; 
     }
} 
?> 
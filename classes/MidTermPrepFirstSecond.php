<?php 
require_once 'CRUD.php'; 
class MidTermPrepFirstSecond extends CRUD{ 
   //class attributes 
   public $id; 
   public $student_code; 
   public $student_name; 
   public $class;
   public $stage;
   public $seat_number;
   public $mid_term_type;
   public $year;
   public $max; 
   public $arabic;
   public $maths;
   public $science	;
   public $social_studies;
   public $english_ol;
   public $religion;
   public $art;
   public $computer;
   public $english_al;
   public $french;
  


   

   //relation table attribute 
   
   //push attributes for relational tables 
   public function enable_relation(){ 
      array_push(static::$primary_fields , ''); 
   }    
   //define table name and fields 
   protected static $table_name = 'midterm_prep_first_second'; 
   protected static $primary_fields = array('id','student_code','student_name','class','stage','seat_number','mid_term_type', 'year', 'max', 'arabic','maths','science','social_studies','english_ol','religion','art','computer', 'english_al','french'); 
    
     public static function findMidTermPrepFirstSecond($student_code){

         global $database ; 

         $student_code = $database->escape_values($student_code) ; 
         $sql = " SELECT * FROM ".self::$table_name."  WHERE student_code = '{$student_code}' LIMIT 1 " ; 
         $result = self::find_by_sql($sql) ; 
         return !empty($result) ? array_shift($result) : false ; 
     }
} 
?> 
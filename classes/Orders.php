<?php 
require_once 'CRUD.php';
class Orders extends  CRUD{
	
	// database attributes  : 
	public $id ; 
	public $employee_name ; 
	public $employee_id ; 
	public $product_name;
	public $product_id ;
	public $invoice_date ;  
	public $sold_quantity ; 
	public $customer_name; 
	public $customer_phone ;
	public $exhibtion_name ;
	public $exhibtion_id ; 
	public $employee_code ; 

   //define table name and fields 
	protected static $table_name = 'orders'; 
	protected static $primary_fields = array('id','employee_name','employee_id', 'product_name', 'invoice_date' , 'sold_quantity','customer_name','customer_phone',        'exhibtion_name','exhibtion_id','product_id', 'employee_code'); 
	
	
	  public function return_exhibtions_ids($employee_id,$exhibtion_id = null ){ 
	   	$sql = "SELECT * FROM orders WHERE employee_id = {$employee_id}  ";
		if(!empty($exhibtion_id)){
			$sql .=" AND exhibtion_id = '$exhibtion_id' ";
		}
		return self::find_by_sql($sql);  
		
   }

   public static function get_invoice_data($invoice_id){
   	$sql = "SELECT * from orders WHERE id = '$invoice_id'" ; 
   	$result_array = static::find_by_sql($sql);
	return !empty($result_array)? array_shift($result_array) : false;

   }

 

}

?>
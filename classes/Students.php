<?php 
require_once 'CRUD.php'; 
class Students extends CRUD{ 
   //class attributes 
   public $id; 
   public $student_code; 
   public $student_name; 
   public $student_password; 
   public $student_email; 
   public $student_stage;
   public $student_class;
   public $seat_number;
   public $insert_date;
   public $last_update;

  
   //relation table attribute 
   
   //push attributes for relational tables 
   public function enable_relation(){ 
		array_push(static::$primary_fields , ''); 
   }    
   //define table name and fields 
	protected static $table_name = 'students'; 
	protected static $primary_fields = array('id','student_code','student_name','student_password','student_email','student_stage','student_class','seat_number','insert_date','last_update'); 
	 
     public  function find_by_code_password($code,$password)
     {
         global $database ; 
         $code     = $database->escape_values($code) ; 
         $password = $database->escape_values($password);  
         $sql = "SELECT * FROM ".self::$table_name."  WHERE student_code = '{$code}' AND student_password = '{$password}' LIMIT 1 " ; 
         $data = self::find_by_sql($sql); 
         return !empty($data)?array_shift($data) : false ; 
     }


     
	 
} 



?>
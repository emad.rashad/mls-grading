<?php 
require_once 'CRUD.php'; 
class MidYearPrepFirstSecond extends CRUD{ 
   //class attributes 
   public $id; 
   public $student_code; 
   public $student_name; 
   public $class;
   public $stage;
   public $seat_number;
   public $max ;  
   public $year; 
   public $mid_year_mark;
   public $year_work_mark; 

   public $arabic_mid_year;
   public $arabic_year_work;
   public $arabic_total;
   public $arabic_eval;
   public $maths_mid_year;
   public $maths_years_work;
   public $maths_total;
   public $maths_eval;
   public $english_ol_mid_year;
   public $english_ol_years_work; 
   public $english_ol_total;
   public $english_ol_eval;
   public $social_studies_mid_year;
   public $social_studies_years_work;
   public $social_studies_total ; 
   public $social_studies_eval;
   public $science_mid_year;
   public $science_years_work;
   public $science_total;
   public $science_eval;
   public $computer_mid_year	;
   public $computer_years_work;
   public $computer_total;
   public $computer_eval;
   public $art_mid_year;
   public $art_years_work;
   public $art_total;
   public $art_eval;
   public $religion_mid_year;
   public $religion_years_work;
   public $religion_total;
   public $religion_eval;
   public $english_al_mid_year;
   public $english_al_years_work;
   public $english_al_total;
   public $english_al_eval;
   public $french_mid_year;
   public $french_years_work;
   public $french_total;
   public $french_eval;
   public $extra_curricular_one_max;
   public $extra_curricular_one_total;
   public $extra_curricular_one_eval;
   public $extra_curricular_two_max;
   public $extra_curricular_two_total;
   public $extra_curricular_two_eval ; 


   


 
   //push attributes for relational tables 
   public function enable_relation(){ 
      array_push(static::$primary_fields , ''); 
   }    
   //define table name and fields 
   protected static $table_name = 'midyear_prep_first_second'; 
   protected static $primary_fields = array('id','student_code','student_name','class','stage','seat_number','max' ,'year','mid_year_mark','year_work_mark', 'arabic_mid_year', 'arabic_year_work','arabic_total','arabic_eval',
    'maths_mid_year','maths_years_work','maths_total','maths_eval','english_ol_mid_year','english_ol_years_work', 'english_ol_total','english_ol_eval','social_studies_mid_year','social_studies_years_work','social_studies_total', 
    'social_studies_eval','science_mid_year','science_years_work', 'science_total','science_eval','computer_mid_year','computer_years_work','computer_total','computer_eval' ,'art_mid_year' ,'art_years_work','art_total','art_eval',
    'religion_mid_year','religion_years_work','religion_total','religion_eval','english_al_mid_year','english_al_years_work','english_al_total','english_al_eval','french_mid_year','french_years_work','french_total'
    ,'french_eval','extra_curricular_one_max','extra_curricular_one_total','extra_curricular_one_eval','extra_curricular_two_max','extra_curricular_two_total','extra_curricular_two_eval');
    
     public static function findMidYearPrepFirstSecond($student_code){

         global $database ; 

         $student_code = $database->escape_values($student_code) ; 
         $sql = " SELECT * FROM ".self::$table_name."  WHERE student_code = '{$student_code}' LIMIT 1 " ; 
         $result = self::find_by_sql($sql) ; 
         return !empty($result) ? array_shift($result) : false ; 
     }
} 
?> 
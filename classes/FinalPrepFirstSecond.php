<?php 
require_once 'CRUD.php'; 
class FinalPrepFirstSecond extends CRUD{ 
   //class attributes 
   public $id; 
   public $student_code; 
   public $student_name; 
   public $max; 
   public $min; 
   public $class;
   public $stage;
   public $seat_number;
   public $year;
   public $arabic;
   public $arabic_st;
   public $maths;
   public $maths_st;
   public $english_ol;
   public $english_ol_st;
   public $social_studies;
   public $social_studies_st;
   public $science;
   public $science_st;
   public $computer;   
   public $computer_st;
   public $art;
   public $art_st;  
   public $extra_curricular_one;
   public $extra_curricular_one_st;
   public $extra_curricular_two;
   public $extra_curricular_two_st;
   public $total;
   public $religion;
   public $religion_st;
   public $english_al;
   public $english_al_st;
   public $french;
   public $french_st;
   public $re_exam_start ; 

   //push attributes for relational tables 
   public function enable_relation(){ 
		array_push(static::$primary_fields , ''); 
   }    
   //define table name and fields 
	protected static $table_name = 'final_prep_first_second'; 
	protected static $primary_fields = array('id','student_code','student_name','max','min','class','stage','seat_number','year', 'arabic','arabic_st','maths','maths_st','english_ol','english_ol_st','social_studies','social_studies_st','science','science_st', 'computer','computer_st','art','art_st',    'extra_curricular_one','extra_curricular_one_st','extra_curricular_two','extra_curricular_two_st','total','religion','religion_st','english_al','english_al_st','french','french_st' , 're_exam_start'); 
	 
	 
     public static function findFinalPrepFirstSecond($student_code){
         global $database ; 
         $student_code = $database->escape_values($student_code) ; 
         $sql = " SELECT * FROM ".self::$table_name."  WHERE student_code = '{$student_code}' LIMIT 1 " ; 
         $result = self::find_by_sql($sql) ; 
         return !empty($result) ? array_shift($result) : false ; 
     }
} 
?> 
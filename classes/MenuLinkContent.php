<?php 
require_once 'CRUD.php'; 
class MenuLinkContent extends CRUD{ 
   //calss attributes 
   public $id; 
   public $link_id;    
   public $lang_id; 
   public $title; 
   public $description; 
    
   
   //relation table attribute 
   
   
   //push attributes for relational tables 
   public function enable_relation(){ 
		array_push(static::$primary_fields , ''); 
   }    
	//define table name and fields 
	protected static $table_name = 'structure_menu_link_content'; 
	protected static $primary_fields = array('id', 'link_id', 'title','lang_id','description'); 
	 
	//get menu content 
	public static  function get_link_content($link_id = null,$lang = null){ 
		$sql = " SELECT * FROM structure_menu_link_content WHERE link_id = $link_id AND lang_id = $lang " ; 
		$result_array = static::find_by_sql($sql); 
	    return !empty($result_array)? array_shift($result_array) : false; 
		 
	} 
	 
	 
	 
} 
?> 

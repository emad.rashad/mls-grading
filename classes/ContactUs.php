<?php 
require_once 'CRUD.php'; 
class ContactUs extends CRUD{ 
   //class attributes 
   public $id; 
   public $user_name; 
   public $email; 
   public $body; 
   public $inserted_date; 
   public $phone;
   //relation table attribute 
   
   //push attributes for relational tables 
   public function enable_relation(){ 
		array_push(static::$primary_fields , ''); 
   }    
   //define table name and fields 
	protected static $table_name = 'contact_us'; 
	protected static $primary_fields = array('id','user_name','email','body','inserted_date'); 
	 
	 
} 
?> 
<?php 
require_once 'CRUD.php'; 
class MidTermPrimarySecondThird extends CRUD{ 
   //class attributes 
   public $id; 
   public $student_code; 
   public $student_name; 
   public $class;
   public $stage;
   public $seat_number;
   public $mid_term_type ; 
   public $year;
   public $max ; 
   public $arabic;
   public $maths;
   public $english_ol;
   public $english_al;
   public $religion;
  


   

   //relation table attribute 
   
   //push attributes for relational tables 
   public function enable_relation(){ 
      array_push(static::$primary_fields , ''); 
   }    
   //define table name and fields 
   protected static $table_name = 'midterm_primary_second_third'; 
   protected static $primary_fields = array('id','student_code','student_name','class','stage','seat_number','mid_term_type' ,   'year','max', 'arabic','maths','english_ol','english_al','religion'); 
    
     public static function findMidTermPrimarySecondThird($student_code){

         global $database ; 

         $student_code = $database->escape_values($student_code) ; 
         $sql = " SELECT * FROM ".self::$table_name."  WHERE student_code = '{$student_code}' LIMIT 1 " ; 
         $result = self::find_by_sql($sql) ; 
         return !empty($result) ? array_shift($result) : false ; 
     }
} 
?> 
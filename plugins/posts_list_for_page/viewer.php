
<?php
//get page category
//show posts
$define_page_categories = new NodesSelectedTaxonomies();
$define_page_categories->enable_relation();
$page_categories = $define_page_categories->return_node_taxonomy($get_page_content->id,'page','category','many',$lang_info->id);
$page_categories_array = array();
if(count($page_categories) > 0){
	
  foreach($page_categories as $category){
	  $page_categories_array[$category->id] = $category->taxonomy_name;
  }
  //get categories ids
  $page_categories_ids = array();
  foreach($page_categories_array as $key=>$value){
	  $page_categories_ids [] = $key;
  }
  
   $count = count($page_categories_ids);
   $index = 1;
  $categories = implode(",",$page_categories_ids);
   //count all posts in the page
   
  $page_all_post = $define_node->front_node_data(null,'post',null,null,$lang_info->id,null,null,$categories,null,null,'many');
  //total posts
  $total_posts = count($page_all_post);
  //get page id if exist
  $page_number = !empty($_GET['page']) ? (int)$_GET['page'] : 1;
 //number of posts in one page
  $per_page =4 ;
  $pagination = new Pagination($page_number , $per_page, $total_posts);
  //calculate the offset
  $offset = $pagination->offset();
  $List_posts_for_blog = $define_node->front_node_data(null,'post',null,null,$lang_info->id,null,null,$categories,null,null,'many',$per_page,$offset);	
    
	  foreach($List_posts_for_blog as $post){
			  
			  $get_date = arabic_date($post->start_publishing) ;
       	   $clean_summary = strip_tags($post->summary); 
           $limit_sum = string_limit_words($clean_summary , 20) ;
        echo "<article class='clearfix primary-post internal-post'><!--post-->";
        echo "<a href='post_details.php?alias=$post->alias&pg_id=$get_page_content->id ' class='col-md-3 primary-post-image' style='background-image: url(media-library/$post->cover_image);'></a>"; 
        echo "<div class='col-md-9 primary-post-text '>"; 
        echo "<h1 class='col-md-12 give-red-c'><a href='post_details.php?alias=$post->alias&pg_id=$get_page_content->id'>$post->title</a></h1>";
        echo "<p>$limit_sum</p>"; 
        echo "</div>";
        echo "</article><!--post-->"; 

			 
		  }//end of foreach
		  
 }//end of page_categories 
 ?>

 </div>
 
<?php 
echo "<nav class='col-md-12 pagination-wrap'>";
if($pagination->total_pages() > 1){
echo "<ul class='pagination'>";
if($pagination->has_previous_page()){
  $previous_page = $pagination->previous_page();
  
  echo "<li>
		  <a href='content.php?alias=$node_alias&page=$previous_page' aria-label='Previous'>
			<span class='fa fa-caret-left'></span> 
		  </a>
		</li>"; 	
} 
for($i=1; $i <= $pagination->total_pages(); $i++) {
	  if($i == $page_number){
	  $active = "  active";
	  }else{
	  $active = " ";
	  }
echo "<li class='$active'><a href='content.php?alias=$node_alias&page={$i}'>$i</a></li>";


} 

// check next page
if($pagination->has_next_page()) { 
$next_page = $pagination->next_page();
echo "<li><a href='content.php?alias=$node_alias&page=$next_page' aria-label='Next'>
      <span class='fa fa-chevron-right'></span></a></li>";
}
echo "</ul>";  	

} 
echo "</nav>"; 
?>








</section>


 <div class="col-md-12 internal-cover" style="background-image: url('main/images/int_cover2.jpg')"><!--start of internal-cover--></div>
  <article class="col-md-12 internal-hero"><!--start of internal-hero-->
            <div class="col-md-12 internal-hero-title">
              <h1 class="col-md-12 main-title give-color1-bg give-radius"><?php echo $title; ?></h1>
            </div>
            <?php 
			if($get_event_content->summary){
			?>
            <div class="col-md-12 internal-hero-text give-card-bg give-radius give-shadow">
              <?php echo $get_event_content->summary; ?>
            </div>
            <?php }?>
           </article><!--end of internal-hero-->
     <section class="col-md-12 internal-wrapper"><!--start of internal-wrapper-->
         <div class="container"><!--start of container-->
<?php 
//process body and separate between plugin name and value
$fullText = $get_event_content->body;
//add plugin
$add_plugin = preg_replace_callback("/\[.+\]/U", "plugin_callback", $fullText)."\n";
echo $add_plugin;
//print final
//echo $add_plugin;
function plugin_callback($match){
	global $node_id;
	global $node_type;
	global $define_image_gallery;
	$query = substr($match[0],1,-1);                           
	//delete [ and ]
	$text_manipulation = preg_replace('/[\[\]]/s', '', $match[0]);
	ob_start();
	$separate_text = explode('@',$text_manipulation);
	if($separate_text[0] == "cms_plugin"){
		$plugin_value = $separate_text[2];
		if(file_exists("plugins/$separate_text[1]/viewer.php")){
			include "plugins/".$separate_text[1]."/viewer.php";
		}else{
			echo "File Not Exist";	
		}
		$pluginContent = ob_get_contents();
		ob_end_clean();
		return $pluginContent;		
	}
}
  
 ?> 
</div>
</section>
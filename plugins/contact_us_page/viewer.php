<?php 

$employee_info = $define_employee->front_employees_data($front_session->user_id) ; 

?>


<form action="" class="row col-md-5 reg-form">
            <div class="form-group">
              <label for="" class="col-md-12">الإسم الأول</label>
              <input type="text" class="form-control" disabled="" id="first_name" value="<?php echo $employee_info->first_name ;  ?>">
            </div>

             <div class="form-group">
              <label for="" class="col-md-12">الإسم الأخير</label>
              <input type="text" class="form-control" disabled="" id="last_name" value="<?php echo $employee_info->last_name ;  ?>">
            </div>

           <div class="form-group">
              <label for="" class="col-md-12">كلمة المرور</label>
              <input type="password" class="form-control" disabled="" id="password" value="<?php echo $employee_info->password ;  ?>">
            </div>
            
            

           <div class="form-group">
              <label for="" class="col-md-12">البريد الإلكتروني</label>
              <input type="text" class="form-control" disabled="" id="email" value="<?php echo $employee_info->email ;  ?>">
            </div>

           <div class="form-group">
              <label for="" class="col-md-12">النوع</label>
              <select name="" id="gender_selected" class="form-control" disabled="">
                <option value="ذكر"<?php if($employee_info->gender == 'ذكر'){echo "selected" ; } ?>>
				<?php echo $employee_info->gender ;  ?></option>
                <option value="انثى"<?php if($employee_info->gender == 'انثى'){echo "selected" ; } ?>>
				<?php echo $employee_info->gender ;  ?></option>
              </select>
            </div>

           <div class="form-group">
              <label for="" class="col-md-12">المعرض</label>
              <select name="" id="exhibtion_selected" class="form-control" disabled="">
               <option value="معرض مدينتى"<?php if($employee_info->exhibtion == 'معرض مدينتى'){echo "selected" ; } ?>>
				<?php echo $employee_info->exhibtion;  ?></option>
                <option value="معرض الرحاب"<?php if($employee_info->exhibtion == 'معرض الرحاب'){echo "selected" ; } ?>>
				<?php echo $employee_info->exhibtion;  ?></option>
                <option value="معرض المستقبل"<?php if($employee_info->exhibtion == 'معرض المستقبل'){echo "selected" ; } ?>>
				<?php echo $employee_info->exhibtion;  ?></option>
                <option value="معرض الرمايه"<?php if($employee_info->exhibtion == 'معرض الرمايه'){echo "selected" ; } ?>>
				<?php echo $employee_info->exhibtion;  ?></option>
                <option value="معرض الميثاق"<?php if($employee_info->exhibtion == 'معرض الميثاق'){echo "selected" ; } ?>>
				<?php echo $employee_info->exhibtion;  ?></option>
                
              </select>
            </div>
            
           <div class="form-group">
              <label for="" class="col-md-12">المسمي الوظيفي</label>
              <input type="text" id="title" class="form-control" disabled="" 
              value="<?php echo $employee_info->title;  ?>">
            </div>

            <div class="form-group btn-area">
            <!--
              <input type="submit" id="save" class="btn give-red-bg save-form hide" value="حفظ">
              <input type="button" id="submit_update" class="btn give-red-bg edit-form" value="تعديل">
            -->
            </div>

         </form>
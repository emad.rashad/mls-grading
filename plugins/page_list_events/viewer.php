<?php 
$stage_path = "";
$class = "no-top";
if(empty($stage)){
	echo "<div class='col-md-12 internal-cover' style='background-image: url(main/images/int_cover2.jpg)'> </div><!--end of internal-cover-->";
	$class = "";
	
}else{
	$stage_path = "&stages=$stage";
}
?>
<article class="col-md-12 internal-hero <?php echo $class; ?>"><!--start of internal-hero-->
            <div class="col-md-12 internal-hero-title">
              <h1 class="col-md-12 main-title give-color1-bg give-radius"><?php  echo $get_page_content->title; ?></h1>
            </div>
            <?php 
			if($get_page_content->summary){
			?>
            <div class="col-md-12 internal-hero-text give-card-bg give-radius give-shadow">
              <?php echo $get_page_content->summary; ?>
            </div>
            <?php }?>
      </article><!--end of internal-hero-->
       <section class="col-md-12 internal-wrapper"><!--start of internal-wrapper-->
      <div class="container"><!--start of container-->
              <div class="row"><!--start of row-->
               
<?php
$page_all_events = $define_node->front_node_data(null,'event',null,null,$lang_info->id,'yes',null,null,null,null,'many');
   
    //total posts
  $total_posts = count($page_all_events);
  //get page id if exist
  $page_number = !empty($_GET['page']) ? (int)$_GET['page'] : 1;
 //number of posts in one page
  $per_page = 3 ;
  $pagination = new Pagination($page_number , $per_page, $total_posts);
  //calculate the offset
  $offset = $pagination->offset();
  $List_events = $define_node->front_node_data(null,'event',null,null,$lang_info->id,'yes',null,null,null,null,'many',$per_page,$offset);	
    foreach($List_events as $event){
		 $event_details = EventDetails::find_by_custom_filed("event_id",$event->id);
		 
			 echo "<article class='clearfix col-md-6 event'><!--post-->
                  <h1 class='col-md-12 give-color1-c'><a href='event_details.php?alias=$event->alias$stage_path'>$event->title</a></h1>
                  <p>".english_date($event_details->start_date)."  to ".english_date($event_details->end_date)."</p>
                  <a href='event_details.php?alias=$event->alias$stage_path' class='btn give-color2-bg'>Read more</a>
                </article><!--post-->
                ";
			 
		  }//end of foreach
		  ?>
          </div>
          </div>
 <?php echo "<nav class='col-md-12 pagination-wrap'>";
 if($pagination->total_pages() > 1) {
			echo "<ul class='pagination'>";
			if($pagination->has_previous_page()) { 
			   $previous = $pagination->previous_page();
			   echo "<li ><a href='content.php?alias=$node_alias&page=$previous$stage_path' aria-label='Previous'><span  class='fa fa-chevron-left'></span> </a></li>";
			  }
			  for($i=1; $i <= $pagination->total_pages(); $i++) {
				   if($i == $page_number){
					     $active = "  active";
				   }else{
					     $active = " ";
				   }
				   echo "<li class='$active' ><a href='content.php?alias=$node_alias&page={$i}$stage_path'>$i</a></li>";
				   
				   
			   }
			  
			  
			  if($pagination->has_next_page()) { 
			    $next = $pagination->next_page();
			    echo "<li><a href='content.php?alias=$node_alias&page=$next$stage_path' aria-label='Next'><span  class='fa fa-chevron-right'></span></a></li>";
			  }
			  echo "</ul>";
 }
 echo "</nav>";
 
 ?>
 </section>
 
             
 
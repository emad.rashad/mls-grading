<?php 
$stage_path = "";
$class = "no-top";
if(empty($stage)){
	echo "<div class='col-md-12 internal-cover' style='background-image: url(main/images/int_cover2.jpg)'> </div><!--end of internal-cover-->";
	$class = "";
	
}else{
	$stage_path = "&stages=$stage";
}
?>
<article class="col-md-12 internal-hero <?php echo $class; ?>"><!--start of internal-hero-->
            <div class="col-md-12 internal-hero-title">
              <h1 class="col-md-12 main-title give-color1-bg give-radius"><?php  echo $get_page_content->title; ?></h1>
            </div>
           <?php 
			if($get_page_content->summary){
			?>
            <div class="col-md-12 internal-hero-text give-card-bg give-radius give-shadow">
              <?php echo $get_page_content->summary; ?>
            </div>
            <?php }?>
      </article><!--end of internal-hero-->
       <section class="col-md-12 internal-wrapper"><!--start of internal-wrapper-->
      <div class="container"><!--start of container-->
             
              
<?php
//get page category
//show posts
$define_page_categories = new NodesSelectedTaxonomies();
$define_page_categories->enable_relation();
$page_categories = $define_page_categories->return_node_taxonomy($get_page_content->id,'page','category','one',$lang_info->id);
$page_categories_array = array();
//get sub cat 
$sub_cats =  $define_taxonomy_class->front_taxonomies_data(null,'category',null ,"inserted_date","ASC",'many',$lang_info->id,$page_categories->id);
if($sub_cats){
	foreach($sub_cats as $cat){
		echo "   <h1 class='col-md-12 give-color1-c'>$cat->name</h1>
                <div class='panel-group'  role='tablist' aria-multiselectable='true'>";
			$category_faq = $define_node->front_node_data(null,'post',null,null,$lang_info->id,null,null,$cat->id,null,null,'many');
			if($category_faq){
				foreach($category_faq as $faq){
					echo "<div class='panel panel-default'>
                    <div class='panel-heading' role='tab' id='heading$faq->id'>
                      <h3 class='panel-title'>
                        <a role='button' data-toggle='collapse' data-parent='#accordion' href='#collapse$faq->id' aria-expanded='true' aria-controls='collapse$faq->id'>
                          <p class='fa fa-play'></p>
                         $faq->title
                        </a>
                      </h3>
                    </div>
                    <div id='collapse$faq->id' class='panel-collapse collapse' role='tabpanel' aria-labelledby='heading$faq->id'>
                      <div class='panel-body'>".
                       $faq->body."
                      </div>
                    </div>
                  </div>";
				}
			
	        }
			echo "</div>";
     }
}
 
 
 ?>
 </div>
</section>
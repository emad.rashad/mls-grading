<?php
require_once("classes/Session.php");
require_once("classes/Users.php");
require_once("classes/FrontSession.php");
require_once("classes/Functions.php");
require_once("classes/MysqlDatabase.php");
require_once("classes/Localization.php");
require_once("classes/GeneralSettings.php");
require_once("classes/MenuLink.php");
require_once("classes/Advertisements.php");
require_once("classes/Nodes.php");
require_once("classes/NodesContent.php");
require_once("classes/EventDetails.php");
require_once("classes/Taxonomies.php");
require_once("classes/TaxonomiesContent.php");
require_once("classes/Plugins.php");
require_once("classes/NodesPluginsValues.php");
require_once("classes/ThemeLayoutModel.php");
require_once("classes/ThemeLayoutModelPlugin.php");
require_once("classes/MenuGroup.php");
require_once("classes/Forms.php");
require_once("classes/NodesSelectedTaxonomies.php");
require_once("classes/FormAttributes.php");
require_once("classes/FormInsertedData.php");
require_once("classes/Pagination.php");
require_once("classes/SocialComments.php");
require_once("classes/NodesImageGallery.php");
require_once("classes/Customers.php");
require_once('classes/Students.php'); 

require_once('classes/FinalPrepFirstSecond.php'); 
require_once('classes/FinalPrimaryFourthFifth.php'); 
require_once('classes/FinalPrimarySecondThird.php'); 
require_once('classes/FinalSecondaryFirst.php'); 
require_once('classes/FinalSecondarySecondArt.php'); 
require_once('classes/FinalSecondarySecondScience.php'); 
require_once('classes/MidTermPrepFirstSecond.php'); 
require_once('classes/MidTermPrimaryFourthFifth.php'); 
require_once('classes/MidTermPrimarySecondThird.php'); 
require_once('classes/MidYearPrepFirstSecond.php'); 
require_once('classes/MidYearPrimaryFourthFifth.php'); 
require_once('classes/MidYearPrimarySecondThird.php'); 
require_once('classes/MidYearSecondaryFirst.php'); 
require_once('classes/MidYearSecondarySecondArt.php'); 
require_once('classes/MidYearSecondarySecondScience.php'); 
require_once('classes/MonthSecondaryFirst.php'); 
require_once('classes/MonthSecondarySecondArt.php'); 
require_once('classes/MonthSecondarySecondScience.php'); 

 
ob_start();
//define classes
    $lang = "en";
    $date_now_method = date_now();
	//catch url and convert to utf8
	$catch_url = urldecode(basename($_SERVER['REQUEST_URI']));
	//get type of opened node content/event/post
	$current_opnened_url_file = $_SERVER["PHP_SELF"];
	$explode_opened_url_file = Explode('/', $current_opnened_url_file);
	$opened_url_file = $explode_opened_url_file[count($explode_opened_url_file)-1];   //give u file
	//check lang exist in db
	$lang_info = Localization::find_by_custom_filed('label',$lang);
	//define general setting
	$define_general_setting = GeneralSettings::find_by_id(1);
	$new_website_title = $define_general_setting->title;
	//define taxonomy class
	$define_taxonomy_class = new Taxonomies();
	$define_taxonomy_class->enable_relation();
	//define taxonomy content class
	$define_taxonomy_content_class = new TaxonomiesContent();
	$define_taxonomy_content_class->enable_relation();
    //define Node class
	$define_node = new Nodes();
	$define_node->enable_relation();
	//define employee class 
	$define_employee = new Customers(); 
	
	//define post class
	$define_node_tax = new NodesSelectedTaxonomies();
	$define_node_tax->enable_relation();
	//define Node content  class
	$define_node_content = new NodesContent();
	$define_node_content->enable_relation();
	//define menu link class
	$define_menu_link = new MenuLink();
	$define_menu_link->enable_relation();
	//define advertisements class
	$define_adv = new Advertisements();
	$define_adv->enable_relation();
	//define node selected taxonomy 
	$define_node_selected_taxonomy = new NodesSelectedTaxonomies();
	$define_node_selected_taxonomy->enable_relation();
	//get the main website title
	$website_new_title = "";
    $website_title = $define_general_setting->title;
	if(isset($_GET['alias'])){
		  $node_alias = urldecode($_GET['alias']);
	}else{
	//redirect_to('index.php');
    }
?>

-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 24, 2016 at 07:39 PM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rodymar`
--

-- --------------------------------------------------------

--
-- Table structure for table `advertisements`
--

CREATE TABLE `advertisements` (
  `id` int(11) NOT NULL,
  `image_cover` varchar(500) NOT NULL,
  `path` int(11) NOT NULL,
  `status` enum('active','disable') NOT NULL,
  `type` enum('post','page','event','external') NOT NULL,
  `external` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `advertisement_content`
--

CREATE TABLE `advertisement_content` (
  `id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `adv_id` int(11) NOT NULL,
  `title` varchar(500) NOT NULL,
  `content` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `attributes`
--

CREATE TABLE `attributes` (
  `id` int(11) NOT NULL,
  `type` varchar(500) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

--
-- Dumping data for table `attributes`
--

INSERT INTO `attributes` (`id`, `type`) VALUES
(1, 'text'),
(2, 'email'),
(3, 'url'),
(4, 'textarea'),
(5, 'radio'),
(6, 'checkbox'),
(7, 'select'),
(8, 'number'),
(9, 'password'),
(1, 'text'),
(2, 'email'),
(3, 'url'),
(4, 'textarea'),
(5, 'radio'),
(6, 'checkbox'),
(7, 'select'),
(8, 'number'),
(9, 'password'),
(1, 'text'),
(2, 'email'),
(3, 'url'),
(4, 'textarea'),
(5, 'radio'),
(6, 'checkbox'),
(7, 'select'),
(8, 'number'),
(9, 'password'),
(1, 'text'),
(2, 'email'),
(3, 'url'),
(4, 'textarea'),
(5, 'radio'),
(6, 'checkbox'),
(7, 'select'),
(8, 'number'),
(9, 'password');

-- --------------------------------------------------------

--
-- Table structure for table `cms_module_access`
--

CREATE TABLE `cms_module_access` (
  `id` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `icon` varchar(30) NOT NULL,
  `sorting` int(11) NOT NULL,
  `type` enum('module','page') NOT NULL,
  `shadow` enum('yes','no') NOT NULL,
  `file_source` text NOT NULL,
  `inserted_by` int(11) NOT NULL,
  `inserted_date` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `last_update` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cms_module_access`
--

INSERT INTO `cms_module_access` (`id`, `sid`, `title`, `icon`, `sorting`, `type`, `shadow`, `file_source`, `inserted_by`, `inserted_date`, `update_by`, `last_update`) VALUES
(5, 0, 'Media Library', 'icon-inbox', 2, 'module', '', 'media_library_directories', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(6, 0, 'Users', 'icon-user', 1, 'module', 'no', 'users', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(7, 0, 'Menus Group', ' icon-link', 3, 'module', '', 'menu_group,menu_link', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(8, 0, 'Pages', 'icon-file-text-alt', 4, 'module', 'no', 'pages', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(9, 0, 'Posts', ' icon-pushpin', 5, 'module', 'no', 'posts', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(10, 0, 'Events', 'icon-calendar', 6, 'module', 'no', 'events', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(11, 0, 'Taxonomy', 'icon-tags', 8, 'module', 'no', 'taxonomy_authors,taxonomy_category,taxonomy_countries,taxonomy_tag', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(12, 0, 'Setting', 'icon-cogs', 10, 'module', 'no', 'profiles,profile_pages,localization,cms_modules,general_settings,ecom_customers_groups,ecom_taxes,ecom_payment_methods', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(13, 5, 'Media Directories', '', 1, 'page', 'no', 'media_library_directories/view', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(16, 6, 'show all', '', 1, 'page', 'no', 'users/view', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(17, 6, 'add new', '', 2, 'page', 'no', 'users/insert', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(18, 8, 'show all', '', 0, 'page', 'no', 'pages/view', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(19, 8, 'add new', '', 2, 'page', 'no', 'pages/insert', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(20, 9, 'show all', '', 1, 'page', 'no', 'posts/view', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(21, 9, 'add new', '', 2, 'page', 'no', 'posts/insert', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(22, 10, 'show all', '', 1, 'page', 'no', 'events/view', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(23, 10, 'add new', '', 2, 'page', 'no', 'events/insert', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(24, 11, 'categories', '', 1, 'page', 'no', 'taxonomy_category/view', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(25, 11, 'tags', '', 6, 'page', 'no', 'taxonomy_tag/view', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(26, 12, 'profiles', '', 2, 'page', 'no', 'profiles/view', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(27, 12, 'Localization', '', 8, 'page', 'no', 'localization/view', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(28, 12, 'CPanel Menus Structure', '', 7, 'page', 'no', 'cms_modules/view', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(29, 7, 'show all ', '', 1, 'page', 'no', 'menu_group/view', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(30, 7, 'add new ', '', 2, 'page', 'no', 'menu_group/insert', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(31, 11, 'Authors', '', 11, 'page', 'no', 'taxonomy_authors/view', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(33, 0, 'Social Activity', ' icon-comments-alt', 7, 'module', 'no', 'social_suggestion_topics,social_comments,social_email_subscription,poll_questions_options,poll_questions,form_attributes,forms,advertisements', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(34, 33, 'Comments ', '', 1, 'page', 'no', 'social_comments/view', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(35, 33, 'Polls ', '', 5, 'page', 'no', 'poll_questions/view', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(36, 33, 'Poll Options  View', '', 10, 'page', 'yes', 'poll_questions_options/view', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(37, 33, 'Email Subscription ', '', 15, 'page', 'no', 'social_email_subscription/view', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(38, 33, 'suggestion topics ', '', 16, 'page', 'no', 'social_suggestion_topics/view', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(39, 12, 'General Settings', '', 1, 'page', 'no', 'general_settings/update', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(40, 275, 'Plugins', '', 1, 'page', 'no', 'plugins/view', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(42, 275, 'Themes & Layouts', '', 2, 'page', 'no', 'themes/view', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(43, 12, 'Profile Insert', '', 0, 'page', 'yes', 'profiles/insert', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(44, 12, 'Profile Update', '', 2, 'page', 'yes', 'profiles/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(45, 12, 'Profile Delete', '', 2, 'page', 'yes', 'profiles/delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(47, 12, 'CPanel Menus Structure Insert', '', 3, 'page', 'yes', 'cms_modules/insert', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(48, 12, 'CPanel Menus Structure Update', '', 3, 'page', 'yes', 'cms_modules/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(50, 12, 'Localization  Insert ', '', 8, 'page', 'yes', 'localization/insert', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(52, 12, 'Localization update', '', 8, 'page', 'yes', 'localization/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(53, 12, 'Localization Delete', '', 8, 'page', 'yes', 'localization/delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(54, 12, 'CPanel Menus Structure Delete', '', 8, 'page', 'yes', 'cms_modules/delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(55, 11, 'categories Insert', '', 2, 'page', 'yes', 'taxonomy_category/insert', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(56, 11, 'categories Update', '', 3, 'page', 'yes', 'taxonomy_category/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(58, 11, 'tags Insert', '', 7, 'page', 'yes', 'taxonomy_tag/insert', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(59, 11, 'tags update', '', 8, 'page', 'yes', 'taxonomy_tag/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(61, 11, 'Authors Insert', '', 12, 'page', 'yes', 'taxonomy_authors/insert', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(62, 11, 'Authors Update', '', 13, 'page', 'yes', 'taxonomy_authors/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(64, 10, 'update', '', 3, 'page', 'yes', 'events/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(65, 10, 'delete', '', 5, 'page', 'yes', 'events/delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(66, 9, 'update', '', 3, 'page', 'yes', 'posts/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(67, 9, 'delete', '', 5, 'page', 'yes', 'posts/delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(68, 8, 'update', '', 3, 'page', 'yes', 'pages/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(69, 8, 'delete', '', 5, 'page', 'yes', 'pages/delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(70, 6, 'update', '', 3, 'page', 'yes', 'users/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(71, 6, 'delete', '', 5, 'page', 'yes', 'users/delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(72, 7, 'group update ', '', 3, 'page', 'yes', 'menu_group/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(73, 7, 'group delete ', '', 5, 'page', 'yes', 'menu_group/delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(74, 33, 'Comments update ', '', 2, 'page', 'yes', 'social_comments/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(77, 33, ' Poll Insert', '', 6, 'page', 'yes', 'poll_questions/insert', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(78, 33, 'Poll Update ', '', 7, 'page', 'yes', 'poll_questions/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(79, 33, 'Poll full info', '', 8, 'page', 'yes', 'poll_questions/full_info', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(80, 33, ' Poll delete', '', 9, 'page', 'yes', 'poll_questions/delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(81, 33, 'Poll Options Insert', '', 11, 'page', 'yes', 'poll_questions_options/insert', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(83, 33, 'Poll Options update', '', 12, 'page', 'yes', 'poll_questions_options/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(84, 33, 'Poll Option full info', '', 13, 'page', 'yes', 'poll_questions_options/full_info', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(86, 33, 'Poll Options delete', '', 14, 'page', 'yes', 'poll_questions_options/delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(87, 33, 'Suggestion topics delete ', '', 17, 'page', 'yes', ' social_suggestion_topics /delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(89, 8, 'full info', '', 4, 'page', 'yes', 'pages/full_info', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(90, 9, 'full info', '', 4, 'page', 'yes', 'posts/full_info', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(91, 6, 'full info', '', 4, 'page', 'yes', 'users/full_info', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(92, 7, 'group full info ', '', 4, 'page', 'yes', 'menu_group/full_info', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(93, 10, 'full info', '', 4, 'page', 'yes', 'events/full_info', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(94, 11, 'categories Full info', '', 4, 'page', 'yes', 'taxonomy_category/full_info', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(95, 11, 'tags full info', '', 9, 'page', 'yes', 'tags/full_info', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(96, 11, 'Authors Full info', '', 14, 'page', 'yes', 'taxonomy_authors/full_info', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(97, 12, 'profile access updates', '', 2, 'page', 'yes', 'profile_pages/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(98, 7, 'menu show all', '', 6, 'page', 'yes', 'menu_link/view', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(99, 7, 'menu add new', '', 7, 'page', 'yes', 'menu_link/insert', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(101, 7, 'menu update', '', 7, 'page', 'yes', 'menu_link/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(102, 7, 'menu delete', '', 9, 'page', 'yes', 'menu_link/delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(103, 7, 'menu full info', '', 8, 'page', 'yes', 'menu_link/full_info', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(107, 12, 'localization label&message', '', 17, 'page', 'yes', 'localization/insert_content', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(108, 12, 'Profile Full Info', '', 2, 'page', 'yes', 'profiles/full_info', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(109, 33, 'suggestion topics full info', '', 16, 'page', 'yes', ' social_suggestion_topics /full_info', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(110, 33, 'comments full info', '', 3, 'page', 'yes', 'social_comments/full_info', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(111, 33, 'Email Subscription  delete', '', 15, 'page', 'yes', 'social_email_subscription/delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(112, 33, 'Comments delete', '', 4, 'page', 'yes', 'social_comments/delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(113, 11, 'Categoires delete', '', 5, 'page', 'yes', 'taxonomy_category/delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(114, 11, 'tags delete', '', 10, 'page', 'yes', 'taxonomy_tag/delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(115, 11, 'authors delete', '', 15, 'page', 'yes', 'taxonomy_authors/delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(116, 12, 'insert theme', '', 7, 'page', 'yes', 'themes/insert', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(117, 12, 'view layouts', '', 7, 'page', 'yes', 'themes/view_layouts', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(118, 12, 'plugins insert', '', 6, 'page', 'yes', 'plugins/insert', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(119, 5, 'media library Insert', '', 2, 'page', 'yes', 'media_library_directories/insert', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(120, 5, 'media library edit', '', 3, 'page', 'yes', 'media_library_directories/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(121, 5, 'media File Insert', '', 4, 'page', 'yes', 'media_library_files/insert', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(122, 12, 'customize layout plugin', '', 14, 'page', 'yes', 'themes/customize_layout_plugin', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(123, 12, 'link viwer', '', 17, 'page', 'yes', 'plugins/insert_link', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(124, 12, 'video viwer', '', 18, 'page', 'yes', 'plugins/insert_video', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(125, 9, 'insert translation', '', 3, 'page', 'yes', 'posts/translation', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(126, 8, 'insert content', '', 3, 'page', 'yes', 'pages/insert_update_content', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(127, 10, 'Insert content', '', 3, 'page', 'yes', 'events/insert_update_content', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(128, 8, 'page plugin option', '', 6, 'page', 'yes', 'pages/plugin_option', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(129, 7, 'Insert menu link content', '', 11, 'page', 'yes', 'menu_link/insert_content', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(130, 9, 'Post Plugin Option', '', 7, 'page', 'yes', 'posts/plugin_option', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(131, 10, 'Event Plugin Option', '', 7, 'page', 'yes', 'events/plugin_option', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(132, 33, 'Forms', '', 2, 'page', 'no', 'forms/view', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(133, 33, 'form insert ', '', 21, 'page', 'yes', 'forms/insert', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(134, 33, 'forms update', '', 22, 'page', 'yes', 'forms/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(135, 33, 'forms full info', '', 23, 'page', 'yes', 'forms/full_info', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(136, 33, 'forms delete', '', 24, 'page', 'yes', 'forms/delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(137, 33, 'form attributes view', '', 25, 'page', 'yes', 'form_attributes/view', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(138, 33, 'form attributes Insert', '', 26, 'page', 'yes', 'form_attributes/insert', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(139, 33, 'form attributes  update', '', 27, 'page', 'yes', 'form_attributes/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(140, 33, 'form attributes  full info', '', 28, 'page', 'yes', 'form_attributes/full_info', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(141, 33, 'form attributes delete', '', 29, 'page', 'yes', 'form_attributes/delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(142, 33, 'advertisements ', '', 30, 'page', 'no', 'advertisements/view', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(143, 33, 'advertisements insert', '', 31, 'page', 'yes', 'advertisements/insert', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(144, 33, 'advertisements update', '', 32, 'page', 'yes', 'advertisements/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(145, 33, 'advertisements full info', '', 33, 'page', 'yes', 'advertisements/full_info', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(146, 33, 'advertisements delete', '', 34, 'page', 'yes', 'advertisements/delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(149, 33, 'form content', '', 34, 'page', 'yes', 'forms/insert_content', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(150, 33, 'advertisements content', '', 36, 'page', 'yes', 'advertisements/insert_content', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(151, 33, 'view form table', '', 38, 'page', 'yes', 'form_attributes/view_table', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(152, 33, 'form view table', '', 4, 'page', 'yes', 'form_attributes/view_table', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(153, 33, 'form inserted data', '', 5, 'page', 'yes', 'form_attributes/inserted_data', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(154, 5, 'view files', '', 2, 'page', 'yes', 'media_library_files/view', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(155, 11, 'insert_taxonomy_content', '', 16, 'page', 'yes', 'taxonomy_category/insert_update_content', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(156, 11, 'full info tags', '', 17, 'page', 'yes', 'taxonomy_tag/full_info', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(157, 11, 'insert tags content', '', 18, 'page', 'yes', 'taxonomy_tag/insert_update_content', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(158, 11, 'insert author content', '', 19, 'page', 'yes', 'taxonomy_authors/insert_update_content', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(226, 12, 'taxes', '', 2, 'page', 'no', 'ecom_taxes/view', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(227, 12, 'taxes insert', '', 2, 'page', 'yes', 'ecom_taxes/insert', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(228, 12, 'taxes update', '', 2, 'page', 'yes', 'ecom_taxes/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(229, 12, 'taxes full info', '', 2, 'page', 'yes', 'ecom_taxes/full_info', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(234, 12, 'payment methods', '', 3, 'page', 'no', 'ecom_payment_methods/view', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(235, 12, 'payment method insert', '', 3, 'page', 'yes', 'ecom_payment_methods/insert', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(237, 12, 'payment methods update', '', 3, 'page', 'yes', 'ecom_payment_methods/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(238, 12, ' payment methods info', '', 3, 'page', 'yes', 'ecom_payment_methods/full_info', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(254, 12, 'Home layout ', '', 40, 'page', 'no', 'index_layout/view', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(255, 12, 'Home layout Insert', '', 41, 'page', 'yes', 'index_layout/insert', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(256, 12, 'home layout update', '', 41, 'page', 'yes', 'index_layout/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(257, 12, 'home layout full_info', '', 41, 'page', 'yes', 'index_layout/full_info', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(258, 12, 'home layout delete', '', 41, 'page', 'yes', 'index_layout/delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(267, 33, 'Contact US', '', 39, 'page', 'no', 'contact_us/view', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(268, 33, 'Contact us delete', '', 39, 'page', 'yes', 'contact_us/delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(275, 0, 'utilities ', ' icon-wrench', 11, 'module', 'no', 'plugins,themes,cities,options,ecom_order_statuses', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE `contact_us` (
  `id` int(11) NOT NULL,
  `user_name` varchar(256) NOT NULL,
  `phone` int(11) NOT NULL,
  `email` varchar(265) CHARACTER SET utf16 COLLATE utf16_esperanto_ci NOT NULL,
  `company` varchar(100) NOT NULL,
  `body` longtext NOT NULL,
  `inserted_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(11) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email` varchar(150) NOT NULL,
  `gender` varchar(100) NOT NULL,
  `exhibtion` varchar(100) NOT NULL,
  `title` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `events_details`
--

CREATE TABLE `events_details` (
  `id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `place` varchar(2580) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `forms`
--

CREATE TABLE `forms` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `label` varchar(250) NOT NULL,
  `enable` enum('yes','no') NOT NULL,
  `email_to` varchar(500) NOT NULL,
  `inserted_date` datetime NOT NULL,
  `inserted_by` int(11) NOT NULL,
  `last_update` datetime NOT NULL,
  `update_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `forms`
--

INSERT INTO `forms` (`id`, `name`, `label`, `enable`, `email_to`, `inserted_date`, `inserted_by`, `last_update`, `update_by`) VALUES
(1, 'dfgdfg', 'tret', 'no', 'ttrt', '2015-05-24 16:09:05', 1, '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `form_attributes`
--

CREATE TABLE `form_attributes` (
  `id` int(11) NOT NULL,
  `attribute_label` varchar(250) NOT NULL,
  `sorting` int(11) NOT NULL,
  `required` enum('yes','no') NOT NULL,
  `form_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `attribute_values` text NOT NULL,
  `inserted_date` datetime NOT NULL,
  `inserted_by` int(11) NOT NULL,
  `last_update` date NOT NULL,
  `update_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `form_attributes`
--

INSERT INTO `form_attributes` (`id`, `attribute_label`, `sorting`, `required`, `form_id`, `attribute_id`, `attribute_values`, `inserted_date`, `inserted_by`, `last_update`, `update_by`) VALUES
(1, 'Name', 1, 'no', 1, 1, '', '2015-05-24 16:10:14', 1, '0000-00-00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `form_inserted_data`
--

CREATE TABLE `form_inserted_data` (
  `id` int(11) NOT NULL,
  `form_id` int(11) NOT NULL,
  `inserted_date` datetime NOT NULL,
  `label` varchar(25) NOT NULL,
  `value` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `general_setting`
--

CREATE TABLE `general_setting` (
  `id` int(11) NOT NULL,
  `title` varchar(256) NOT NULL,
  `site_url` varchar(500) NOT NULL,
  `meta_key` text NOT NULL,
  `email` varchar(256) NOT NULL,
  `time_zone_id` int(11) NOT NULL,
  `front_lang_id` int(11) NOT NULL,
  `translate_lang_id` int(11) NOT NULL,
  `enable_website` enum('yes','no') NOT NULL,
  `offline_messages` longtext NOT NULL,
  `description` longtext NOT NULL,
  `google_analitic` longtext CHARACTER SET utf16 NOT NULL,
  `main_order_statues` int(11) NOT NULL,
  `enable_store` enum('yes','no') NOT NULL,
  `update_by` int(11) NOT NULL,
  `last_update` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `general_setting`
--

INSERT INTO `general_setting` (`id`, `title`, `site_url`, `meta_key`, `email`, `time_zone_id`, `front_lang_id`, `translate_lang_id`, `enable_website`, `offline_messages`, `description`, `google_analitic`, `main_order_statues`, `enable_store`, `update_by`, `last_update`) VALUES
(1, 'Welcome To Indoors', '', 'website design in Egypt, web development in egypt,Mobile solutions, web applications, web based applications, web marketing, web services, seo, search engine optimization', 'info@diva-lab.com', 35, 1, 1, 'yes', '', 'Diva company provides the latest technologies in web design and web development in Egypt & ME providing responsive and good user experience interface', '', 8, '', 1, '2016-04-23 19:27:15');

-- --------------------------------------------------------

--
-- Table structure for table `localization`
--

CREATE TABLE `localization` (
  `id` int(11) NOT NULL,
  `name` varchar(25) NOT NULL,
  `sorting` int(11) NOT NULL,
  `status` enum('active','disable') NOT NULL,
  `label` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `localization`
--

INSERT INTO `localization` (`id`, `name`, `sorting`, `status`, `label`) VALUES
(1, 'English', 1, 'active', 'en');

-- --------------------------------------------------------

--
-- Table structure for table `nodes`
--

CREATE TABLE `nodes` (
  `id` int(11) NOT NULL,
  `status` enum('draft','publish') NOT NULL,
  `enable_summary` enum('yes','no') NOT NULL,
  `inserted_date` datetime NOT NULL,
  `inserted_by` int(11) NOT NULL,
  `last_update` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `start_publishing` datetime NOT NULL,
  `end_publishing` datetime NOT NULL,
  `enable_comments` enum('yes','no') NOT NULL,
  `front_page` enum('yes','no') NOT NULL,
  `slide_show` enum('yes','no') NOT NULL,
  `cover_image` varchar(250) NOT NULL,
  `slider_cover` varchar(250) NOT NULL,
  `node_type` enum('post','page','event','faq','product') NOT NULL,
  `place` varchar(256) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `model` int(11) NOT NULL,
  `shortcut_link` varchar(35) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `nodes`
--

INSERT INTO `nodes` (`id`, `status`, `enable_summary`, `inserted_date`, `inserted_by`, `last_update`, `update_by`, `start_publishing`, `end_publishing`, `enable_comments`, `front_page`, `slide_show`, `cover_image`, `slider_cover`, `node_type`, `place`, `start_date`, `end_date`, `model`, `shortcut_link`) VALUES
(1, 'publish', '', '2016-05-24 12:15:20', 1, '0000-00-00 00:00:00', 0, '2016-05-24 12:15:20', '0000-00-00 00:00:00', 'no', 'no', 'no', '', '', 'page', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1464084920'),
(2, 'publish', '', '2016-05-24 12:16:33', 1, '0000-00-00 00:00:00', 0, '2016-05-24 12:16:33', '0000-00-00 00:00:00', 'no', 'no', 'no', '', '', 'page', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1464084993'),
(3, 'publish', '', '2016-05-24 12:16:59', 1, '0000-00-00 00:00:00', 0, '2016-05-24 12:16:59', '0000-00-00 00:00:00', 'no', 'no', 'no', '', '', 'page', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1464085019'),
(4, 'publish', '', '2016-05-24 12:17:21', 1, '0000-00-00 00:00:00', 0, '2016-05-24 12:17:21', '0000-00-00 00:00:00', 'no', 'no', 'no', '', '', 'page', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1464085041'),
(5, 'publish', '', '2016-05-24 12:17:40', 1, '0000-00-00 00:00:00', 0, '2016-05-24 12:17:40', '0000-00-00 00:00:00', 'no', 'no', 'no', '', '', 'page', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1464085060'),
(6, 'publish', '', '2016-05-24 12:17:58', 1, '0000-00-00 00:00:00', 0, '2016-05-24 12:17:58', '0000-00-00 00:00:00', 'no', 'no', 'no', '', '', 'page', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1464085078'),
(7, 'publish', '', '2016-05-24 13:12:12', 1, '0000-00-00 00:00:00', 0, '2016-05-24 13:12:12', '0000-00-00 00:00:00', 'no', 'no', 'no', '', '', 'page', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1464088332'),
(8, 'publish', '', '2016-05-24 13:33:55', 1, '2016-05-24 13:44:54', 1, '2016-05-24 13:33:55', '0000-00-00 00:00:00', 'no', 'no', 'no', '', '', 'page', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1464089635'),
(9, 'publish', 'no', '2016-05-24 14:36:24', 1, '0000-00-00 00:00:00', 0, '2016-05-24 14:36:24', '0000-00-00 00:00:00', 'no', 'yes', 'no', 'posts/post1.jpg', '', 'post', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 2, '1464093384'),
(10, 'publish', 'no', '2016-05-24 14:38:16', 1, '0000-00-00 00:00:00', 0, '2016-05-24 14:38:16', '0000-00-00 00:00:00', 'no', 'yes', 'no', 'posts/post1.jpg', '', 'post', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 2, '1464093496'),
(11, 'publish', 'no', '2016-05-24 14:39:46', 1, '0000-00-00 00:00:00', 0, '2016-05-24 14:39:46', '0000-00-00 00:00:00', 'no', 'yes', 'no', 'posts/post1.jpg', '', 'post', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 2, '1464093586'),
(13, 'publish', 'no', '2016-05-24 14:41:47', 1, '0000-00-00 00:00:00', 0, '2016-05-24 14:41:47', '0000-00-00 00:00:00', 'no', 'yes', 'no', 'posts/post1.jpg', '', 'post', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 2, '1464093707'),
(14, 'publish', 'no', '2016-05-24 14:43:24', 1, '2016-05-24 15:12:29', 1, '2016-05-24 14:43:24', '0000-00-00 00:00:00', 'no', 'yes', 'no', 'posts/post1.jpg ', '', 'post', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 2, '1464093804'),
(15, 'publish', '', '2016-05-24 16:07:09', 1, '2016-05-24 16:28:05', 1, '2016-05-24 16:07:09', '0000-00-00 00:00:00', 'no', 'no', 'no', 'estimate/lorry.png ', '', 'page', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1464098829'),
(16, 'publish', '', '2016-05-24 16:51:34', 1, '0000-00-00 00:00:00', 0, '2016-05-24 16:51:34', '0000-00-00 00:00:00', 'no', 'no', 'no', '', '', 'page', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1464101494');

-- --------------------------------------------------------

--
-- Table structure for table `nodes_content`
--

CREATE TABLE `nodes_content` (
  `id` int(11) NOT NULL,
  `node_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `title` varchar(250) NOT NULL,
  `alias` varchar(250) NOT NULL,
  `summary` text NOT NULL,
  `body` longtext NOT NULL,
  `meta_keys` varchar(250) NOT NULL,
  `meta_description` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `nodes_content`
--

INSERT INTO `nodes_content` (`id`, `node_id`, `lang_id`, `title`, `alias`, `summary`, `body`, `meta_keys`, `meta_description`) VALUES
(1, 1, 1, 'About', 'about', '', '', '', ''),
(2, 2, 1, 'Sailing Schedule', 'sailing_schedule', '', '', '', ''),
(3, 3, 1, 'Sea Services', 'sea_services', '', '', '', ''),
(4, 4, 1, 'Rodymar Agencies', 'rodymar_agencies', '', '', '', ''),
(5, 5, 1, 'Land Service', 'land_service', '', '', '', ''),
(6, 6, 1, 'Contact Us', 'contact_us', '', '', '', ''),
(7, 7, 1, 'Info Header', 'info_header', '<p>Hot Line: 002 066 3251946</p>', '<p>Reserve online and pay as you wish in person</p>', '', ''),
(8, 8, 1, 'Intro', 'intro', '<p>WE OFFER <br /> DIFFERENT SERVICES</p>', '<p>At Logiscargo, we Ð°rÐµ making research continuously Ð°nd improving Ð¾ur Ñ•ÐµrvÑ–ÑÐµÑ• to thÐµ hÑ–ghÐµÑ•t Ñ•tÐ°ndÐ°rdÑ•. WÐµ Ð¾ffÐµr dÑ–ffÐµrÐµnt Ñ•ÐµrvÑ–ÑÐµs rÐ°ngÑ–ng frÐ¾m logistics, warehousing, cargo, transport and other related services. Our ÑlÑ–Ðµnt''Ñ• Ñ–ntÐµrÐµÑ•t Ð¾ur priority.WÐµ are mÑ–ndful Ð¾f building a hÐµÐ°lthÑƒ rÐµlÐ°tÑ–Ð¾nÑ•hÑ–Ñ€ with Ð¾ur ÑuÑ•tÐ¾mÐµrÑ•.</p>', '', ''),
(9, 9, 1, 'First Post', 'first_post', '<p>RODYMAR Shipping line has been designed from the ground up to maximize efficiency and accountability of each employee .</p>', '', '', ''),
(10, 10, 1, 'Second Post', 'second_post', '<p>RODYMAR Shipping line has been designed from the ground up to maximize efficiency and accountability of each employee .</p>', '', '', ''),
(11, 11, 1, 'Third Post', 'third_post', '<p>RODYMAR Shipping line has been designed from the ground up to maximize efficiency and accountability of each employee .</p>', '', '', ''),
(13, 13, 1, 'Fourth Post', 'fourth_post', '<p>RODYMAR Shipping line has been designed from the ground up to maximize efficiency and accountability of each employee .</p>', '', '', ''),
(14, 14, 1, 'Fifth Post', 'fifth_post', '<p>RODYMAR Shipping line has been designed from the ground up to maximize efficiency and accountability of each employee .</p>', '', '', ''),
(15, 15, 1, 'SERVICE ESTIMATE', 'service_estimate', '<p><img src="media-library/estimate/lorry.png" alt="" /></p>', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex ab fugit molestias commodi dicta ratione! Labore doloribus accusamus deserunt, ea minus omnis officia? Praesentium illo natus delectus eos nam soluta!</p>', '', ''),
(16, 16, 1, 'Footer Info', 'footer_info', '<div class="col-md-4 footer-column">\n<h3 class="col-md-12 footer-column-title give-title-border">Rodymar Shipping Line( Sea- Services</h3>\n<ul class="col-md-12 footer-contact-list">\n<li>\n<div class="col-md-12 footer-contact-item give-footer-c">Address:</div>\n<div class="col-md-12 footer-contact-item give-white-c">\n<p>Zamzam Tower,El-Thalathiny and El-Shaheed Mokhtar Said Street, 7th Floor â€“ PORT SAID â€“ EGYPT</p>\n</div>\n</li>\n<li>\n<div class="col-md-12 footer-contact-item give-footer-c">Phone:</div>\n<div class="col-md-12 footer-contact-item give-white-c">\n<p>+2 066 3251946</p>\n<p>+2 066 3251946</p>\n<p>+2 066 3251946</p>\n</div>\n</li>\n<li>\n<div class="col-md-12 footer-contact-item give-footer-c">Fax:</div>\n<div class="col-md-12 footer-contact-item give-white-c">\n<p>+2 066 3251946</p>\n</div>\n</li>\n<li>\n<div class="col-md-12 footer-contact-item give-footer-c">Email:</div>\n<div class="col-md-12 footer-contact-item give-white-c">\n<p>info@rodymar.com</p>\n</div>\n</li>\n</ul>\n</div>', '<div class="col-md-4 footer-column">\n<h3 class="col-md-12 footer-column-title give-title-border">Rodymar Shipping Line( Sea- Services</h3>\n<ul class="col-md-12 footer-contact-list">\n<li>\n<div class="col-md-12 footer-contact-item give-footer-c">Address:</div>\n<div class="col-md-12 footer-contact-item give-white-c">\n<p>Zamzam Tower,El-Thalathiny and El-Shaheed Mokhtar Said Street, 7th Floor â€“ PORT SAID â€“ EGYPT</p>\n</div>\n</li>\n<li>\n<div class="col-md-12 footer-contact-item give-footer-c">Phone:</div>\n<div class="col-md-12 footer-contact-item give-white-c">\n<p>+2 066 3251946</p>\n<p>+2 066 3251946</p>\n<p>+2 066 3251946</p>\n</div>\n</li>\n<li>\n<div class="col-md-12 footer-contact-item give-footer-c">Fax:</div>\n<div class="col-md-12 footer-contact-item give-white-c">\n<p>+2 066 3251946</p>\n</div>\n</li>\n<li>\n<div class="col-md-12 footer-contact-item give-footer-c">Email:</div>\n<div class="col-md-12 footer-contact-item give-white-c">\n<p>info@rodymar.com</p>\n</div>\n</li>\n</ul>\n</div>', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `nodes_image_gallery`
--

CREATE TABLE `nodes_image_gallery` (
  `id` int(11) NOT NULL,
  `type` enum('node','propertie') NOT NULL,
  `related_id` int(11) NOT NULL,
  `image` varchar(50) NOT NULL,
  `sort` varchar(5) NOT NULL,
  `caption` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `nodes_plugins_values`
--

CREATE TABLE `nodes_plugins_values` (
  `id` int(11) NOT NULL,
  `type` enum('post','page','event') NOT NULL,
  `node_id` int(11) NOT NULL,
  `plugin_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `content` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `nodes_selected_taxonomies`
--

CREATE TABLE `nodes_selected_taxonomies` (
  `id` int(11) NOT NULL,
  `taxonomy_id` int(11) NOT NULL,
  `node_id` int(11) NOT NULL,
  `taxonomy_type` enum('tag','category','author','country') NOT NULL,
  `node_type` enum('post','page','event','faq','product') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `nodes_selected_taxonomies`
--

INSERT INTO `nodes_selected_taxonomies` (`id`, `taxonomy_id`, `node_id`, `taxonomy_type`, `node_type`) VALUES
(1, 1, 9, 'category', 'post'),
(2, 1, 10, 'category', 'post'),
(3, 1, 11, 'category', 'post'),
(4, 1, 13, 'category', 'post'),
(5, 1, 14, 'category', 'post');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `employee_name` varchar(150) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `product_name` varchar(150) NOT NULL,
  `invoice_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `sold_quantity` varchar(150) NOT NULL,
  `customer_name` varchar(150) NOT NULL,
  `customer_phone` varchar(150) NOT NULL,
  `exhibtion_name` varchar(150) NOT NULL,
  `exhibtion_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `employee_code` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `plugins`
--

CREATE TABLE `plugins` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `description` text NOT NULL,
  `version` varchar(50) NOT NULL,
  `author` varchar(150) NOT NULL,
  `uploaded_date` datetime NOT NULL,
  `uploaded_by` int(11) NOT NULL,
  `source` varchar(150) NOT NULL,
  `enable_options` enum('yes','no') NOT NULL,
  `enable_translate` enum('yes','no') NOT NULL,
  `enable_event` enum('no','yes') NOT NULL,
  `enable_product` enum('yes','no') NOT NULL,
  `enable_post` enum('no','yes') NOT NULL,
  `enable_page` enum('no','yes') NOT NULL,
  `enable_index` enum('no','yes') NOT NULL,
  `position` enum('left','right','no_position') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `plugins`
--

INSERT INTO `plugins` (`id`, `name`, `description`, `version`, `author`, `uploaded_date`, `uploaded_by`, `source`, `enable_options`, `enable_translate`, `enable_event`, `enable_product`, `enable_post`, `enable_page`, `enable_index`, `position`) VALUES
(1, 'Show Page details', 'this plug to Show Page details', '1.0', 'diva', '2015-07-29 20:21:57', 1, 'show_page_details', '', 'yes', 'no', '', 'no', 'yes', 'no', 'left'),
(2, 'list view all posts in FAQ page', 'this plug to list to posts in FAQ page', '1.0', 'diva', '2015-07-31 09:50:32', 1, 'posts_list_for_faq', '', 'yes', 'no', '', 'no', 'yes', 'no', 'left'),
(3, 'Show contact us page details', 'this plug to Show contact us Page details', '1.0', 'diva', '2015-07-31 13:16:26', 1, 'contact_us_page', '', 'yes', 'no', '', 'no', 'yes', 'no', 'left'),
(6, 'list post of pages', 'this plug to list to posts in news page', '1.0', 'diva', '2015-08-01 12:20:35', 1, 'posts_list_for_page', '', 'yes', 'no', '', 'no', 'yes', 'no', 'left'),
(7, 'Show post details', 'this plug to list to Show post details', '1.0', 'diva', '2015-08-12 15:24:19', 1, 'show_post_details', '', 'yes', 'no', '', 'yes', 'no', 'no', 'left'),
(12, 'list view post', 'this plug to list to view all posts', '1.0', 'diva', '2015-09-08 18:29:58', 1, 'Search', '', 'yes', 'no', '', 'no', 'yes', 'no', 'left'),
(23, 'Stagte Home page', 'this plug To show stage home page', '1.0', 'diva', '2016-01-18 16:24:09', 1, 'stage_home_page', '', 'yes', 'no', '', 'no', 'yes', 'no', 'left'),
(24, 'list events in Event page', 'this plug to list all events in event page', '1.0', 'diva', '2016-01-19 14:08:31', 1, 'page_list_events', '', 'yes', 'no', '', 'no', 'yes', 'no', 'left'),
(25, 'list view post annoucment', 'this plug to list to view all page annoucment posts', '1.0', 'diva', '2016-01-19 14:48:15', 1, 'posts_list_for_annoucment', '', 'yes', 'no', '', 'no', 'yes', 'no', 'left'),
(26, 'list view pages', 'this plug to list to view all pages', '1.0', 'diva', '2016-01-19 16:15:39', 1, 'posts_list_for_curriculum', '', 'yes', 'no', '', 'no', 'yes', 'no', 'left'),
(27, 'list view  gallery pages', 'this plug to list to view all pages gallery', '1.0', 'diva', '2016-01-19 16:46:21', 1, 'posts_list_for_gallery', '', 'yes', 'no', '', 'no', 'yes', 'no', 'left'),
(28, 'list view pages staff', 'this plug to list to view all pages staff', '1.0', 'diva', '2016-01-19 17:27:44', 1, 'posts_list_for_staff', '', 'yes', 'no', '', 'no', 'yes', 'no', 'left'),
(29, 'list view post careers', 'this plug to list to view all page careers posts', '1.0', 'diva', '2016-01-20 11:14:42', 1, 'posts_list_for_careers', '', 'yes', 'no', '', 'no', 'yes', 'no', 'left'),
(33, 'list view post annoucment  for main', 'this plug to list to view all page annoucment posts', '1.0', 'diva', '2016-01-20 20:58:33', 1, 'posts_list_for_annoucment _main', '', 'yes', 'no', '', 'no', 'yes', 'no', 'left'),
(34, 'Show eventdetails', 'this plug to list to Show event details', '1.0', 'diva', '2016-01-24 17:07:36', 1, 'show_event_details', '', 'yes', 'yes', '', 'no', 'no', 'no', 'left');

-- --------------------------------------------------------

--
-- Table structure for table `poll_questions`
--

CREATE TABLE `poll_questions` (
  `id` int(11) NOT NULL,
  `poll` varchar(250) NOT NULL,
  `status` enum('publish','draft') NOT NULL,
  `inserted_by` int(11) NOT NULL,
  `inserted_date` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `last_update` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `poll_questions_options`
--

CREATE TABLE `poll_questions_options` (
  `id` int(11) NOT NULL,
  `poll_id` int(11) NOT NULL,
  `poll_option` varchar(250) NOT NULL,
  `option_counter` int(11) NOT NULL,
  `inserted_by` int(11) NOT NULL,
  `inserted_date` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `last_update` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `profile`
--

CREATE TABLE `profile` (
  `id` int(11) NOT NULL,
  `title` varchar(25) NOT NULL,
  `description` varchar(150) NOT NULL,
  `global_edit` enum('all_records','awn_record') NOT NULL,
  `global_delete` enum('all_records','awn_record') NOT NULL,
  `developer_mode` enum('yes','no') NOT NULL,
  `profile_block` enum('yes','no') NOT NULL,
  `post_publishing` enum('yes','no') NOT NULL,
  `page_publishing` enum('yes','no') NOT NULL,
  `event_publishing` enum('yes','no') NOT NULL,
  `inserted_by` int(11) NOT NULL,
  `inserted_date` datetime NOT NULL,
  `last_update` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `profile`
--

INSERT INTO `profile` (`id`, `title`, `description`, `global_edit`, `global_delete`, `developer_mode`, `profile_block`, `post_publishing`, `page_publishing`, `event_publishing`, `inserted_by`, `inserted_date`, `last_update`) VALUES
(1, 'access all', '', 'all_records', 'all_records', 'yes', 'no', 'yes', 'yes', 'yes', 1, '2014-05-04 10:42:08', '2016-04-26 18:15:19'),
(9, 'new admin', '', 'all_records', 'awn_record', 'no', 'yes', 'yes', 'yes', 'yes', 1, '2016-02-03 18:40:32', '2016-02-09 10:49:35'),
(10, 'admin', '', 'all_records', 'all_records', 'no', 'no', 'yes', 'yes', 'yes', 1, '2016-02-24 08:34:23', '0000-00-00 00:00:00'),
(11, 'Super Admin', '', 'awn_record', 'awn_record', 'no', 'no', 'no', 'no', 'no', 1, '2016-02-29 11:40:27', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `profile_modules_access`
--

CREATE TABLE `profile_modules_access` (
  `id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `access` enum('yes','no') NOT NULL,
  `profile_id` int(11) NOT NULL,
  `inserted_by` int(11) NOT NULL,
  `inserted_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `profile_modules_access`
--

INSERT INTO `profile_modules_access` (`id`, `module_id`, `access`, `profile_id`, `inserted_by`, `inserted_date`) VALUES
(1, 5, 'yes', 1, 1, '2014-05-04 10:42:08'),
(2, 6, 'yes', 1, 1, '2014-05-04 10:42:08'),
(3, 7, 'yes', 1, 1, '2014-05-04 10:42:09'),
(4, 8, 'yes', 1, 1, '2014-05-04 10:42:09'),
(5, 9, 'yes', 1, 1, '2014-05-04 10:42:09'),
(6, 10, 'yes', 1, 1, '2014-05-04 10:42:09'),
(7, 11, 'yes', 1, 1, '2014-05-04 10:42:09'),
(8, 12, 'yes', 1, 1, '2014-05-04 10:42:09'),
(9, 33, 'yes', 1, 1, '2014-05-10 08:51:36'),
(33, 275, 'yes', 1, 1, '2015-07-01 05:16:22'),
(107, 6, 'yes', 9, 1, '2016-02-03 18:40:32'),
(108, 5, 'yes', 9, 1, '2016-02-03 18:40:32'),
(109, 7, 'yes', 9, 1, '2016-02-03 18:40:32'),
(110, 8, 'yes', 9, 1, '2016-02-03 18:40:32'),
(111, 9, 'yes', 9, 1, '2016-02-03 18:40:32'),
(112, 10, 'yes', 9, 1, '2016-02-03 18:40:32'),
(113, 33, 'no', 9, 1, '2016-02-03 18:40:32'),
(114, 11, 'no', 9, 1, '2016-02-03 18:40:32'),
(115, 12, 'no', 9, 1, '2016-02-03 18:40:32'),
(116, 275, 'no', 9, 1, '2016-02-03 18:40:32'),
(117, 6, 'yes', 10, 1, '2016-02-24 08:34:23'),
(118, 5, 'yes', 10, 1, '2016-02-24 08:34:23'),
(119, 7, 'yes', 10, 1, '2016-02-24 08:34:23'),
(120, 8, 'yes', 10, 1, '2016-02-24 08:34:23'),
(121, 9, 'yes', 10, 1, '2016-02-24 08:34:23'),
(122, 10, 'yes', 10, 1, '2016-02-24 08:34:23'),
(123, 33, 'no', 10, 1, '2016-02-24 08:34:23'),
(124, 11, 'yes', 10, 1, '2016-02-24 08:34:23'),
(125, 12, 'no', 10, 1, '2016-02-24 08:34:23'),
(126, 275, 'no', 10, 1, '2016-02-24 08:34:23'),
(127, 6, 'yes', 11, 1, '2016-02-29 11:40:27'),
(128, 5, 'yes', 11, 1, '2016-02-29 11:40:27'),
(129, 7, 'yes', 11, 1, '2016-02-29 11:40:27'),
(130, 8, 'yes', 11, 1, '2016-02-29 11:40:27'),
(131, 9, 'yes', 11, 1, '2016-02-29 11:40:27'),
(132, 10, 'yes', 11, 1, '2016-02-29 11:40:27'),
(133, 33, 'yes', 11, 1, '2016-02-29 11:40:27'),
(134, 11, 'yes', 11, 1, '2016-02-29 11:40:27'),
(135, 12, 'yes', 11, 1, '2016-02-29 11:40:27'),
(136, 275, 'yes', 11, 1, '2016-02-29 11:40:27');

-- --------------------------------------------------------

--
-- Table structure for table `profile_pages_access`
--

CREATE TABLE `profile_pages_access` (
  `id` int(11) NOT NULL,
  `profile_id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  `access` enum('yes','no') NOT NULL,
  `inserted_by` int(11) NOT NULL,
  `inserted_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `profile_pages_access`
--

INSERT INTO `profile_pages_access` (`id`, `profile_id`, `module_id`, `page_id`, `access`, `inserted_by`, `inserted_date`) VALUES
(1, 1, 5, 13, 'yes', 1, '2014-05-04 10:42:09'),
(3, 1, 6, 16, 'yes', 1, '2014-05-04 10:42:09'),
(4, 1, 6, 17, 'yes', 1, '2014-05-04 10:42:09'),
(5, 1, 7, 29, 'yes', 1, '2014-05-04 10:42:09'),
(6, 1, 7, 30, 'yes', 1, '2014-05-04 10:42:09'),
(7, 1, 8, 18, 'yes', 1, '2014-05-04 10:42:09'),
(8, 1, 8, 19, 'yes', 1, '2014-05-04 10:42:09'),
(9, 1, 9, 20, 'yes', 1, '2014-05-04 10:42:09'),
(10, 1, 9, 21, 'yes', 1, '2014-05-04 10:42:09'),
(11, 1, 10, 22, 'yes', 1, '2014-05-04 10:42:09'),
(12, 1, 10, 23, 'yes', 1, '2014-05-04 10:42:09'),
(13, 1, 11, 24, 'yes', 1, '2014-05-04 10:42:09'),
(14, 1, 11, 25, 'yes', 1, '2014-05-04 10:42:09'),
(15, 1, 12, 26, 'yes', 1, '2014-05-04 10:42:09'),
(16, 1, 12, 27, 'yes', 1, '2014-05-04 10:42:09'),
(17, 1, 12, 28, 'yes', 1, '2014-05-04 10:42:09'),
(18, 1, 11, 31, 'no', 1, '2014-05-04 10:42:09'),
(20, 1, 33, 34, 'yes', 1, '2014-05-10 08:56:40'),
(21, 1, 33, 35, 'yes', 1, '2014-05-10 08:57:56'),
(22, 1, 33, 36, 'yes', 1, '2014-05-10 08:58:46'),
(23, 1, 33, 37, 'yes', 1, '2014-05-10 02:54:51'),
(24, 1, 33, 38, 'no', 1, '2014-05-11 08:38:53'),
(49, 1, 12, 39, 'yes', 1, '2014-05-31 09:45:31'),
(51, 1, 275, 40, 'yes', 1, '2014-06-23 07:44:38'),
(53, 1, 275, 42, 'yes', 1, '2014-06-23 08:04:35'),
(237, 1, 12, 43, 'yes', 1, '2014-07-19 15:06:57'),
(238, 1, 12, 44, 'yes', 1, '2014-07-19 15:24:29'),
(239, 1, 12, 45, 'yes', 1, '2014-07-19 15:27:29'),
(241, 1, 12, 47, 'yes', 1, '2014-07-19 15:43:14'),
(242, 1, 12, 48, 'yes', 1, '2014-07-19 15:46:43'),
(244, 1, 12, 50, 'yes', 1, '2014-07-19 15:55:50'),
(246, 1, 12, 52, 'yes', 1, '2014-07-19 15:59:55'),
(247, 1, 12, 53, 'yes', 1, '2014-07-19 16:03:18'),
(248, 1, 12, 54, 'yes', 1, '2014-07-19 16:07:07'),
(249, 1, 11, 55, 'yes', 1, '2014-07-19 16:11:20'),
(250, 1, 11, 56, 'yes', 1, '2014-07-19 16:12:43'),
(252, 1, 11, 58, 'yes', 1, '2014-07-19 16:15:54'),
(253, 1, 11, 59, 'yes', 1, '2014-07-19 16:18:08'),
(255, 1, 11, 61, 'yes', 1, '2014-07-19 16:20:41'),
(256, 1, 11, 62, 'yes', 1, '2014-07-19 16:21:21'),
(258, 1, 10, 64, 'yes', 1, '2014-07-19 16:24:05'),
(259, 1, 10, 65, 'yes', 1, '2014-07-19 16:25:03'),
(260, 1, 9, 66, 'yes', 1, '2014-07-19 16:26:46'),
(261, 1, 9, 67, 'yes', 1, '2014-07-19 16:29:40'),
(262, 1, 8, 68, 'yes', 1, '2014-07-19 16:30:29'),
(263, 1, 8, 69, 'yes', 1, '2014-07-19 16:30:54'),
(264, 1, 6, 70, 'yes', 1, '2014-07-19 16:31:33'),
(265, 1, 6, 71, 'yes', 1, '2014-07-19 16:32:05'),
(266, 1, 7, 72, 'yes', 1, '2014-07-19 16:33:37'),
(267, 1, 7, 73, 'yes', 1, '2014-07-19 16:33:54'),
(268, 1, 33, 74, 'yes', 1, '2014-07-19 16:36:05'),
(271, 1, 33, 77, 'yes', 1, '2014-07-19 16:47:13'),
(272, 1, 33, 78, 'yes', 1, '2014-07-19 16:47:49'),
(273, 1, 33, 79, 'yes', 1, '2014-07-19 16:48:28'),
(274, 1, 33, 80, 'yes', 1, '2014-07-19 16:51:47'),
(275, 1, 33, 81, 'yes', 1, '2014-07-19 16:53:27'),
(277, 1, 33, 83, 'yes', 1, '2014-07-19 16:56:50'),
(278, 1, 33, 84, 'yes', 1, '2014-07-19 16:58:58'),
(280, 1, 33, 86, 'yes', 1, '2014-07-19 17:09:19'),
(281, 1, 33, 87, 'no', 1, '2014-07-19 17:12:40'),
(283, 1, 8, 89, 'yes', 1, '2014-07-19 17:21:02'),
(284, 1, 9, 90, 'yes', 1, '2014-07-19 17:21:33'),
(285, 1, 6, 91, 'yes', 1, '2014-07-19 17:22:38'),
(286, 1, 7, 92, 'yes', 1, '2014-07-19 17:23:39'),
(287, 1, 10, 93, 'yes', 1, '2014-07-19 17:24:46'),
(288, 1, 11, 94, 'yes', 1, '2014-07-19 17:26:35'),
(289, 1, 11, 95, 'yes', 1, '2014-07-19 17:28:12'),
(290, 1, 11, 96, 'yes', 1, '2014-07-19 17:29:24'),
(291, 1, 12, 97, 'yes', 1, '2014-07-20 13:23:07'),
(292, 1, 7, 98, 'yes', 1, '2014-07-20 14:07:32'),
(293, 1, 7, 99, 'yes', 1, '2014-07-20 14:08:28'),
(295, 1, 7, 101, 'yes', 1, '2014-07-20 14:12:07'),
(296, 1, 7, 102, 'yes', 1, '2014-07-20 14:13:13'),
(297, 1, 7, 103, 'yes', 1, '2014-07-20 14:18:52'),
(301, 1, 12, 107, 'yes', 1, '2014-07-23 14:43:37'),
(302, 1, 12, 108, 'yes', 1, '2014-07-24 02:03:06'),
(303, 1, 33, 109, 'no', 1, '2014-07-27 19:28:05'),
(304, 1, 33, 110, 'yes', 1, '2014-07-27 19:43:43'),
(305, 1, 33, 111, 'yes', 1, '2014-07-27 20:19:35'),
(306, 1, 33, 112, 'yes', 1, '2014-07-31 10:45:13'),
(307, 1, 11, 113, 'yes', 1, '2014-07-31 10:47:42'),
(308, 1, 11, 114, 'yes', 1, '2014-07-31 10:49:27'),
(309, 1, 11, 115, 'yes', 1, '2014-07-31 10:51:07'),
(310, 1, 12, 116, 'no', 1, '2014-08-02 14:27:33'),
(311, 1, 12, 117, 'yes', 1, '2014-08-02 14:28:00'),
(312, 1, 12, 118, 'yes', 1, '2014-08-02 16:53:09'),
(313, 1, 5, 119, 'yes', 1, '2014-08-07 07:25:56'),
(314, 1, 5, 120, 'yes', 1, '2014-08-07 07:52:13'),
(315, 1, 5, 121, 'yes', 1, '2014-08-07 10:25:41'),
(316, 1, 12, 122, 'yes', 1, '2014-08-10 07:55:26'),
(317, 1, 12, 123, 'yes', 1, '2014-08-11 17:42:12'),
(318, 1, 12, 124, 'yes', 1, '2014-08-11 21:02:29'),
(319, 1, 9, 125, 'yes', 1, '2014-08-17 06:47:15'),
(320, 1, 8, 126, 'yes', 1, '2014-08-17 08:45:21'),
(321, 1, 10, 127, 'yes', 1, '2014-08-17 10:31:22'),
(322, 1, 8, 128, 'yes', 1, '2014-08-18 05:49:14'),
(323, 1, 7, 129, 'yes', 1, '2014-08-18 11:46:56'),
(324, 1, 9, 130, 'yes', 1, '2014-08-23 14:49:31'),
(325, 1, 10, 131, 'yes', 1, '2014-08-23 16:33:24'),
(326, 1, 33, 132, 'yes', 1, '2014-09-17 11:04:43'),
(327, 1, 33, 133, 'yes', 1, '2014-09-17 11:06:57'),
(328, 1, 33, 134, 'yes', 1, '2014-09-17 11:07:19'),
(329, 1, 33, 135, 'yes', 1, '2014-09-17 11:07:50'),
(330, 1, 33, 136, 'yes', 1, '2014-09-17 11:08:18'),
(331, 1, 33, 137, 'yes', 1, '2014-09-17 11:10:05'),
(332, 1, 33, 138, 'yes', 1, '2014-09-17 11:10:25'),
(333, 1, 33, 139, 'yes', 1, '2014-09-17 11:10:55'),
(334, 1, 33, 140, 'yes', 1, '2014-09-17 11:11:19'),
(335, 1, 33, 141, 'yes', 1, '2014-09-17 11:11:46'),
(336, 1, 33, 142, 'yes', 1, '2014-09-17 11:46:09'),
(337, 1, 33, 143, 'yes', 1, '2014-09-17 11:46:49'),
(338, 1, 33, 144, 'yes', 1, '2014-09-17 11:47:16'),
(339, 1, 33, 145, 'yes', 1, '2014-09-17 11:47:41'),
(340, 1, 33, 146, 'yes', 1, '2014-09-17 11:47:59'),
(341, 1, 33, 149, 'yes', 1, '2014-09-20 02:09:15'),
(342, 1, 33, 150, 'yes', 1, '2014-09-20 02:10:19'),
(343, 1, 33, 152, 'yes', 1, '2014-09-20 11:12:36'),
(344, 1, 33, 153, 'yes', 1, '2014-09-20 17:12:38'),
(345, 1, 5, 154, 'yes', 1, '2014-09-21 00:01:05'),
(466, 1, 11, 155, 'yes', 1, '2014-12-15 15:38:04'),
(468, 1, 11, 156, 'yes', 1, '2014-12-16 03:21:35'),
(470, 1, 11, 157, 'yes', 1, '2014-12-16 03:22:48'),
(472, 1, 11, 158, 'yes', 1, '2014-12-16 03:23:44'),
(537, 1, 12, 226, 'yes', 1, '2015-03-30 07:42:00'),
(538, 1, 12, 227, 'yes', 1, '2015-03-30 07:42:50'),
(539, 1, 12, 228, 'yes', 1, '2015-03-30 07:47:53'),
(540, 1, 12, 229, 'yes', 1, '2015-03-30 08:00:31'),
(545, 1, 12, 234, 'yes', 1, '2015-03-31 14:00:49'),
(546, 1, 12, 235, 'yes', 1, '2015-03-31 14:02:26'),
(548, 1, 12, 237, 'yes', 1, '2015-03-31 14:04:06'),
(549, 1, 12, 238, 'yes', 1, '2015-03-31 14:06:46'),
(565, 1, 12, 254, 'no', 1, '2015-04-06 18:30:02'),
(566, 1, 12, 255, 'no', 1, '2015-04-06 18:30:32'),
(567, 1, 12, 256, 'no', 1, '2015-04-06 18:30:57'),
(568, 1, 12, 257, 'no', 1, '2015-04-06 18:31:36'),
(569, 1, 12, 258, 'no', 1, '2015-04-06 18:32:04'),
(784, 1, 33, 267, 'yes', 1, '2015-05-24 16:14:17'),
(786, 1, 33, 268, 'yes', 1, '2015-05-24 16:14:45'),
(2401, 9, 6, 16, 'yes', 1, '2016-02-03 18:40:32'),
(2402, 9, 6, 17, 'yes', 1, '2016-02-03 18:40:32'),
(2403, 9, 6, 70, 'yes', 1, '2016-02-03 18:40:32'),
(2404, 9, 6, 91, 'yes', 1, '2016-02-03 18:40:32'),
(2405, 9, 6, 71, 'yes', 1, '2016-02-03 18:40:32'),
(2406, 9, 5, 13, 'yes', 1, '2016-02-03 18:40:32'),
(2407, 9, 5, 119, 'yes', 1, '2016-02-03 18:40:32'),
(2408, 9, 5, 154, 'yes', 1, '2016-02-03 18:40:32'),
(2409, 9, 5, 120, 'yes', 1, '2016-02-03 18:40:32'),
(2410, 9, 5, 121, 'yes', 1, '2016-02-03 18:40:32'),
(2411, 9, 7, 29, 'yes', 1, '2016-02-03 18:40:32'),
(2412, 9, 7, 30, 'yes', 1, '2016-02-03 18:40:32'),
(2413, 9, 7, 72, 'yes', 1, '2016-02-03 18:40:32'),
(2414, 9, 7, 92, 'yes', 1, '2016-02-03 18:40:32'),
(2415, 9, 7, 73, 'yes', 1, '2016-02-03 18:40:32'),
(2416, 9, 7, 98, 'yes', 1, '2016-02-03 18:40:32'),
(2417, 9, 7, 99, 'yes', 1, '2016-02-03 18:40:32'),
(2418, 9, 7, 101, 'yes', 1, '2016-02-03 18:40:32'),
(2419, 9, 7, 103, 'yes', 1, '2016-02-03 18:40:32'),
(2420, 9, 7, 102, 'yes', 1, '2016-02-03 18:40:32'),
(2421, 9, 7, 129, 'yes', 1, '2016-02-03 18:40:32'),
(2422, 9, 8, 18, 'yes', 1, '2016-02-03 18:40:32'),
(2423, 9, 8, 19, 'yes', 1, '2016-02-03 18:40:32'),
(2424, 9, 8, 68, 'yes', 1, '2016-02-03 18:40:32'),
(2425, 9, 8, 126, 'yes', 1, '2016-02-03 18:40:32'),
(2426, 9, 8, 89, 'yes', 1, '2016-02-03 18:40:32'),
(2427, 9, 8, 69, 'yes', 1, '2016-02-03 18:40:32'),
(2428, 9, 8, 128, 'yes', 1, '2016-02-03 18:40:32'),
(2429, 9, 9, 20, 'yes', 1, '2016-02-03 18:40:32'),
(2430, 9, 9, 21, 'yes', 1, '2016-02-03 18:40:32'),
(2431, 9, 9, 66, 'yes', 1, '2016-02-03 18:40:32'),
(2432, 9, 9, 125, 'yes', 1, '2016-02-03 18:40:32'),
(2433, 9, 9, 90, 'yes', 1, '2016-02-03 18:40:32'),
(2434, 9, 9, 67, 'yes', 1, '2016-02-03 18:40:32'),
(2435, 9, 9, 130, 'yes', 1, '2016-02-03 18:40:32'),
(2436, 9, 10, 22, 'yes', 1, '2016-02-03 18:40:32'),
(2437, 9, 10, 23, 'yes', 1, '2016-02-03 18:40:32'),
(2438, 9, 10, 64, 'yes', 1, '2016-02-03 18:40:32'),
(2439, 9, 10, 127, 'yes', 1, '2016-02-03 18:40:32'),
(2440, 9, 10, 93, 'yes', 1, '2016-02-03 18:40:32'),
(2441, 9, 10, 65, 'yes', 1, '2016-02-03 18:40:32'),
(2442, 9, 10, 131, 'yes', 1, '2016-02-03 18:40:32'),
(2443, 9, 33, 34, 'no', 1, '2016-02-03 18:40:32'),
(2444, 9, 33, 74, 'no', 1, '2016-02-03 18:40:32'),
(2445, 9, 33, 132, 'no', 1, '2016-02-03 18:40:32'),
(2446, 9, 33, 110, 'no', 1, '2016-02-03 18:40:32'),
(2447, 9, 33, 112, 'no', 1, '2016-02-03 18:40:32'),
(2448, 9, 33, 152, 'no', 1, '2016-02-03 18:40:32'),
(2449, 9, 33, 35, 'no', 1, '2016-02-03 18:40:32'),
(2450, 9, 33, 153, 'no', 1, '2016-02-03 18:40:32'),
(2451, 9, 33, 77, 'no', 1, '2016-02-03 18:40:32'),
(2452, 9, 33, 78, 'no', 1, '2016-02-03 18:40:32'),
(2453, 9, 33, 79, 'no', 1, '2016-02-03 18:40:32'),
(2454, 9, 33, 80, 'no', 1, '2016-02-03 18:40:32'),
(2455, 9, 33, 36, 'no', 1, '2016-02-03 18:40:32'),
(2456, 9, 33, 81, 'no', 1, '2016-02-03 18:40:32'),
(2457, 9, 33, 83, 'no', 1, '2016-02-03 18:40:32'),
(2458, 9, 33, 84, 'no', 1, '2016-02-03 18:40:32'),
(2459, 9, 33, 86, 'no', 1, '2016-02-03 18:40:32'),
(2460, 9, 33, 37, 'no', 1, '2016-02-03 18:40:32'),
(2461, 9, 33, 111, 'no', 1, '2016-02-03 18:40:32'),
(2462, 9, 33, 38, 'no', 1, '2016-02-03 18:40:32'),
(2463, 9, 33, 109, 'no', 1, '2016-02-03 18:40:32'),
(2464, 9, 33, 87, 'no', 1, '2016-02-03 18:40:32'),
(2465, 9, 33, 133, 'no', 1, '2016-02-03 18:40:32'),
(2466, 9, 33, 134, 'no', 1, '2016-02-03 18:40:32'),
(2467, 9, 33, 135, 'no', 1, '2016-02-03 18:40:32'),
(2468, 9, 33, 136, 'no', 1, '2016-02-03 18:40:32'),
(2469, 9, 33, 137, 'no', 1, '2016-02-03 18:40:32'),
(2470, 9, 33, 138, 'no', 1, '2016-02-03 18:40:32'),
(2471, 9, 33, 139, 'no', 1, '2016-02-03 18:40:32'),
(2472, 9, 33, 140, 'no', 1, '2016-02-03 18:40:32'),
(2473, 9, 33, 141, 'no', 1, '2016-02-03 18:40:32'),
(2474, 9, 33, 142, 'no', 1, '2016-02-03 18:40:32'),
(2475, 9, 33, 143, 'no', 1, '2016-02-03 18:40:32'),
(2476, 9, 33, 144, 'no', 1, '2016-02-03 18:40:32'),
(2477, 9, 33, 145, 'no', 1, '2016-02-03 18:40:32'),
(2478, 9, 33, 146, 'no', 1, '2016-02-03 18:40:32'),
(2479, 9, 33, 149, 'no', 1, '2016-02-03 18:40:32'),
(2480, 9, 33, 150, 'no', 1, '2016-02-03 18:40:32'),
(2481, 9, 33, 151, 'no', 1, '2016-02-03 18:40:32'),
(2482, 9, 33, 267, 'no', 1, '2016-02-03 18:40:32'),
(2483, 9, 33, 268, 'no', 1, '2016-02-03 18:40:32'),
(2484, 9, 11, 24, 'no', 1, '2016-02-03 18:40:32'),
(2485, 9, 11, 55, 'no', 1, '2016-02-03 18:40:32'),
(2486, 9, 11, 56, 'no', 1, '2016-02-03 18:40:32'),
(2487, 9, 11, 94, 'no', 1, '2016-02-03 18:40:32'),
(2488, 9, 11, 113, 'no', 1, '2016-02-03 18:40:32'),
(2489, 9, 11, 25, 'no', 1, '2016-02-03 18:40:32'),
(2490, 9, 11, 58, 'no', 1, '2016-02-03 18:40:32'),
(2491, 9, 11, 59, 'no', 1, '2016-02-03 18:40:32'),
(2492, 9, 11, 95, 'no', 1, '2016-02-03 18:40:32'),
(2493, 9, 11, 114, 'no', 1, '2016-02-03 18:40:32'),
(2494, 9, 11, 31, 'no', 1, '2016-02-03 18:40:32'),
(2495, 9, 11, 61, 'no', 1, '2016-02-03 18:40:32'),
(2496, 9, 11, 62, 'no', 1, '2016-02-03 18:40:32'),
(2497, 9, 11, 96, 'no', 1, '2016-02-03 18:40:32'),
(2498, 9, 11, 115, 'no', 1, '2016-02-03 18:40:32'),
(2499, 9, 11, 155, 'no', 1, '2016-02-03 18:40:32'),
(2500, 9, 11, 156, 'no', 1, '2016-02-03 18:40:32'),
(2501, 9, 11, 157, 'no', 1, '2016-02-03 18:40:32'),
(2502, 9, 11, 158, 'no', 1, '2016-02-03 18:40:32'),
(2503, 9, 12, 43, 'no', 1, '2016-02-03 18:40:32'),
(2504, 9, 12, 39, 'no', 1, '2016-02-03 18:40:32'),
(2505, 9, 12, 26, 'no', 1, '2016-02-03 18:40:32'),
(2506, 9, 12, 44, 'no', 1, '2016-02-03 18:40:32'),
(2507, 9, 12, 45, 'no', 1, '2016-02-03 18:40:32'),
(2508, 9, 12, 97, 'no', 1, '2016-02-03 18:40:32'),
(2509, 9, 12, 108, 'no', 1, '2016-02-03 18:40:32'),
(2510, 9, 12, 226, 'no', 1, '2016-02-03 18:40:32'),
(2511, 9, 12, 227, 'no', 1, '2016-02-03 18:40:32'),
(2512, 9, 12, 228, 'no', 1, '2016-02-03 18:40:32'),
(2513, 9, 12, 229, 'no', 1, '2016-02-03 18:40:32'),
(2514, 9, 12, 47, 'no', 1, '2016-02-03 18:40:32'),
(2515, 9, 12, 48, 'no', 1, '2016-02-03 18:40:32'),
(2516, 9, 12, 234, 'no', 1, '2016-02-03 18:40:32'),
(2517, 9, 12, 235, 'no', 1, '2016-02-03 18:40:32'),
(2518, 9, 12, 237, 'no', 1, '2016-02-03 18:40:32'),
(2519, 9, 12, 238, 'no', 1, '2016-02-03 18:40:32'),
(2520, 9, 12, 118, 'no', 1, '2016-02-03 18:40:32'),
(2521, 9, 12, 28, 'no', 1, '2016-02-03 18:40:32'),
(2522, 9, 12, 116, 'no', 1, '2016-02-03 18:40:32'),
(2523, 9, 12, 117, 'no', 1, '2016-02-03 18:40:32'),
(2524, 9, 12, 27, 'no', 1, '2016-02-03 18:40:32'),
(2525, 9, 12, 50, 'no', 1, '2016-02-03 18:40:32'),
(2526, 9, 12, 52, 'no', 1, '2016-02-03 18:40:32'),
(2527, 9, 12, 53, 'no', 1, '2016-02-03 18:40:32'),
(2528, 9, 12, 54, 'no', 1, '2016-02-03 18:40:32'),
(2529, 9, 12, 122, 'no', 1, '2016-02-03 18:40:32'),
(2530, 9, 12, 107, 'no', 1, '2016-02-03 18:40:32'),
(2531, 9, 12, 123, 'no', 1, '2016-02-03 18:40:32'),
(2532, 9, 12, 124, 'no', 1, '2016-02-03 18:40:32'),
(2533, 9, 12, 254, 'no', 1, '2016-02-03 18:40:32'),
(2534, 9, 12, 256, 'no', 1, '2016-02-03 18:40:32'),
(2535, 9, 12, 257, 'no', 1, '2016-02-03 18:40:32'),
(2536, 9, 12, 258, 'no', 1, '2016-02-03 18:40:32'),
(2537, 9, 12, 255, 'no', 1, '2016-02-03 18:40:32'),
(2538, 9, 275, 40, 'no', 1, '2016-02-03 18:40:32'),
(2539, 9, 275, 42, 'no', 1, '2016-02-03 18:40:32'),
(2540, 10, 6, 16, 'yes', 1, '2016-02-24 08:34:23'),
(2541, 10, 6, 17, 'yes', 1, '2016-02-24 08:34:23'),
(2542, 10, 6, 70, 'yes', 1, '2016-02-24 08:34:23'),
(2543, 10, 6, 91, 'yes', 1, '2016-02-24 08:34:23'),
(2544, 10, 6, 71, 'yes', 1, '2016-02-24 08:34:23'),
(2545, 10, 5, 13, 'yes', 1, '2016-02-24 08:34:23'),
(2546, 10, 5, 119, 'yes', 1, '2016-02-24 08:34:23'),
(2547, 10, 5, 154, 'yes', 1, '2016-02-24 08:34:23'),
(2548, 10, 5, 120, 'yes', 1, '2016-02-24 08:34:23'),
(2549, 10, 5, 121, 'yes', 1, '2016-02-24 08:34:23'),
(2550, 10, 7, 29, 'yes', 1, '2016-02-24 08:34:23'),
(2551, 10, 7, 30, 'yes', 1, '2016-02-24 08:34:23'),
(2552, 10, 7, 72, 'yes', 1, '2016-02-24 08:34:23'),
(2553, 10, 7, 92, 'yes', 1, '2016-02-24 08:34:23'),
(2554, 10, 7, 73, 'yes', 1, '2016-02-24 08:34:23'),
(2555, 10, 7, 98, 'yes', 1, '2016-02-24 08:34:23'),
(2556, 10, 7, 99, 'yes', 1, '2016-02-24 08:34:23'),
(2557, 10, 7, 101, 'yes', 1, '2016-02-24 08:34:23'),
(2558, 10, 7, 103, 'yes', 1, '2016-02-24 08:34:23'),
(2559, 10, 7, 102, 'yes', 1, '2016-02-24 08:34:23'),
(2560, 10, 7, 129, 'yes', 1, '2016-02-24 08:34:23'),
(2561, 10, 8, 18, 'yes', 1, '2016-02-24 08:34:23'),
(2562, 10, 8, 19, 'yes', 1, '2016-02-24 08:34:23'),
(2563, 10, 8, 68, 'yes', 1, '2016-02-24 08:34:23'),
(2564, 10, 8, 126, 'yes', 1, '2016-02-24 08:34:23'),
(2565, 10, 8, 89, 'yes', 1, '2016-02-24 08:34:23'),
(2566, 10, 8, 69, 'yes', 1, '2016-02-24 08:34:23'),
(2567, 10, 8, 128, 'yes', 1, '2016-02-24 08:34:23'),
(2568, 10, 9, 20, 'yes', 1, '2016-02-24 08:34:23'),
(2569, 10, 9, 21, 'yes', 1, '2016-02-24 08:34:23'),
(2570, 10, 9, 66, 'yes', 1, '2016-02-24 08:34:23'),
(2571, 10, 9, 125, 'yes', 1, '2016-02-24 08:34:23'),
(2572, 10, 9, 90, 'yes', 1, '2016-02-24 08:34:23'),
(2573, 10, 9, 67, 'yes', 1, '2016-02-24 08:34:23'),
(2574, 10, 9, 130, 'yes', 1, '2016-02-24 08:34:23'),
(2575, 10, 10, 22, 'yes', 1, '2016-02-24 08:34:23'),
(2576, 10, 10, 23, 'yes', 1, '2016-02-24 08:34:23'),
(2577, 10, 10, 64, 'yes', 1, '2016-02-24 08:34:23'),
(2578, 10, 10, 127, 'yes', 1, '2016-02-24 08:34:23'),
(2579, 10, 10, 93, 'yes', 1, '2016-02-24 08:34:23'),
(2580, 10, 10, 65, 'yes', 1, '2016-02-24 08:34:23'),
(2581, 10, 10, 131, 'yes', 1, '2016-02-24 08:34:23'),
(2582, 10, 33, 34, 'no', 1, '2016-02-24 08:34:23'),
(2583, 10, 33, 74, 'no', 1, '2016-02-24 08:34:23'),
(2584, 10, 33, 132, 'no', 1, '2016-02-24 08:34:23'),
(2585, 10, 33, 110, 'no', 1, '2016-02-24 08:34:23'),
(2586, 10, 33, 112, 'no', 1, '2016-02-24 08:34:23'),
(2587, 10, 33, 152, 'no', 1, '2016-02-24 08:34:23'),
(2588, 10, 33, 35, 'no', 1, '2016-02-24 08:34:23'),
(2589, 10, 33, 153, 'no', 1, '2016-02-24 08:34:23'),
(2590, 10, 33, 77, 'no', 1, '2016-02-24 08:34:23'),
(2591, 10, 33, 78, 'no', 1, '2016-02-24 08:34:23'),
(2592, 10, 33, 79, 'no', 1, '2016-02-24 08:34:23'),
(2593, 10, 33, 80, 'no', 1, '2016-02-24 08:34:23'),
(2594, 10, 33, 36, 'no', 1, '2016-02-24 08:34:23'),
(2595, 10, 33, 81, 'no', 1, '2016-02-24 08:34:23'),
(2596, 10, 33, 83, 'no', 1, '2016-02-24 08:34:23'),
(2597, 10, 33, 84, 'no', 1, '2016-02-24 08:34:23'),
(2598, 10, 33, 86, 'no', 1, '2016-02-24 08:34:23'),
(2599, 10, 33, 37, 'no', 1, '2016-02-24 08:34:23'),
(2600, 10, 33, 111, 'no', 1, '2016-02-24 08:34:23'),
(2601, 10, 33, 38, 'no', 1, '2016-02-24 08:34:23'),
(2602, 10, 33, 109, 'no', 1, '2016-02-24 08:34:23'),
(2603, 10, 33, 87, 'no', 1, '2016-02-24 08:34:23'),
(2604, 10, 33, 133, 'no', 1, '2016-02-24 08:34:23'),
(2605, 10, 33, 134, 'no', 1, '2016-02-24 08:34:23'),
(2606, 10, 33, 135, 'no', 1, '2016-02-24 08:34:23'),
(2607, 10, 33, 136, 'no', 1, '2016-02-24 08:34:23'),
(2608, 10, 33, 137, 'no', 1, '2016-02-24 08:34:23'),
(2609, 10, 33, 138, 'no', 1, '2016-02-24 08:34:23'),
(2610, 10, 33, 139, 'no', 1, '2016-02-24 08:34:23'),
(2611, 10, 33, 140, 'no', 1, '2016-02-24 08:34:23'),
(2612, 10, 33, 141, 'no', 1, '2016-02-24 08:34:23'),
(2613, 10, 33, 142, 'no', 1, '2016-02-24 08:34:23'),
(2614, 10, 33, 143, 'no', 1, '2016-02-24 08:34:23'),
(2615, 10, 33, 144, 'no', 1, '2016-02-24 08:34:23'),
(2616, 10, 33, 145, 'no', 1, '2016-02-24 08:34:23'),
(2617, 10, 33, 146, 'no', 1, '2016-02-24 08:34:23'),
(2618, 10, 33, 149, 'no', 1, '2016-02-24 08:34:23'),
(2619, 10, 33, 150, 'no', 1, '2016-02-24 08:34:23'),
(2620, 10, 33, 151, 'no', 1, '2016-02-24 08:34:23'),
(2621, 10, 33, 267, 'no', 1, '2016-02-24 08:34:23'),
(2622, 10, 33, 268, 'no', 1, '2016-02-24 08:34:23'),
(2623, 10, 11, 24, 'yes', 1, '2016-02-24 08:34:23'),
(2624, 10, 11, 55, 'yes', 1, '2016-02-24 08:34:23'),
(2625, 10, 11, 56, 'yes', 1, '2016-02-24 08:34:23'),
(2626, 10, 11, 94, 'yes', 1, '2016-02-24 08:34:23'),
(2627, 10, 11, 113, 'yes', 1, '2016-02-24 08:34:23'),
(2628, 10, 11, 25, 'yes', 1, '2016-02-24 08:34:23'),
(2629, 10, 11, 58, 'yes', 1, '2016-02-24 08:34:23'),
(2630, 10, 11, 59, 'yes', 1, '2016-02-24 08:34:23'),
(2631, 10, 11, 95, 'yes', 1, '2016-02-24 08:34:23'),
(2632, 10, 11, 114, 'yes', 1, '2016-02-24 08:34:23'),
(2633, 10, 11, 31, 'yes', 1, '2016-02-24 08:34:23'),
(2634, 10, 11, 61, 'yes', 1, '2016-02-24 08:34:23'),
(2635, 10, 11, 62, 'yes', 1, '2016-02-24 08:34:23'),
(2636, 10, 11, 96, 'yes', 1, '2016-02-24 08:34:23'),
(2637, 10, 11, 115, 'yes', 1, '2016-02-24 08:34:23'),
(2638, 10, 11, 155, 'yes', 1, '2016-02-24 08:34:23'),
(2639, 10, 11, 156, 'yes', 1, '2016-02-24 08:34:23'),
(2640, 10, 11, 157, 'yes', 1, '2016-02-24 08:34:23'),
(2641, 10, 11, 158, 'yes', 1, '2016-02-24 08:34:23'),
(2642, 10, 12, 43, 'no', 1, '2016-02-24 08:34:23'),
(2643, 10, 12, 39, 'no', 1, '2016-02-24 08:34:23'),
(2644, 10, 12, 26, 'no', 1, '2016-02-24 08:34:23'),
(2645, 10, 12, 44, 'no', 1, '2016-02-24 08:34:23'),
(2646, 10, 12, 45, 'no', 1, '2016-02-24 08:34:23'),
(2647, 10, 12, 97, 'no', 1, '2016-02-24 08:34:23'),
(2648, 10, 12, 108, 'no', 1, '2016-02-24 08:34:23'),
(2649, 10, 12, 226, 'no', 1, '2016-02-24 08:34:23'),
(2650, 10, 12, 227, 'no', 1, '2016-02-24 08:34:23'),
(2651, 10, 12, 228, 'no', 1, '2016-02-24 08:34:23'),
(2652, 10, 12, 229, 'no', 1, '2016-02-24 08:34:23'),
(2653, 10, 12, 47, 'no', 1, '2016-02-24 08:34:23'),
(2654, 10, 12, 48, 'no', 1, '2016-02-24 08:34:23'),
(2655, 10, 12, 234, 'no', 1, '2016-02-24 08:34:23'),
(2656, 10, 12, 235, 'no', 1, '2016-02-24 08:34:23'),
(2657, 10, 12, 237, 'no', 1, '2016-02-24 08:34:23'),
(2658, 10, 12, 238, 'no', 1, '2016-02-24 08:34:23'),
(2659, 10, 12, 118, 'no', 1, '2016-02-24 08:34:23'),
(2660, 10, 12, 28, 'no', 1, '2016-02-24 08:34:23'),
(2661, 10, 12, 116, 'no', 1, '2016-02-24 08:34:23'),
(2662, 10, 12, 117, 'no', 1, '2016-02-24 08:34:23'),
(2663, 10, 12, 27, 'no', 1, '2016-02-24 08:34:23'),
(2664, 10, 12, 50, 'no', 1, '2016-02-24 08:34:23'),
(2665, 10, 12, 52, 'no', 1, '2016-02-24 08:34:23'),
(2666, 10, 12, 53, 'no', 1, '2016-02-24 08:34:23'),
(2667, 10, 12, 54, 'no', 1, '2016-02-24 08:34:23'),
(2668, 10, 12, 122, 'no', 1, '2016-02-24 08:34:23'),
(2669, 10, 12, 107, 'no', 1, '2016-02-24 08:34:23'),
(2670, 10, 12, 123, 'no', 1, '2016-02-24 08:34:23'),
(2671, 10, 12, 124, 'no', 1, '2016-02-24 08:34:23'),
(2672, 10, 12, 254, 'no', 1, '2016-02-24 08:34:23'),
(2673, 10, 12, 256, 'no', 1, '2016-02-24 08:34:23'),
(2674, 10, 12, 257, 'no', 1, '2016-02-24 08:34:23'),
(2675, 10, 12, 258, 'no', 1, '2016-02-24 08:34:23'),
(2676, 10, 12, 255, 'no', 1, '2016-02-24 08:34:23'),
(2677, 10, 275, 40, 'no', 1, '2016-02-24 08:34:23'),
(2678, 10, 275, 42, 'no', 1, '2016-02-24 08:34:23'),
(2679, 11, 6, 16, 'yes', 1, '2016-02-29 11:40:27'),
(2680, 11, 6, 17, 'yes', 1, '2016-02-29 11:40:27'),
(2681, 11, 6, 70, 'yes', 1, '2016-02-29 11:40:27'),
(2682, 11, 6, 91, 'yes', 1, '2016-02-29 11:40:27'),
(2683, 11, 6, 71, 'yes', 1, '2016-02-29 11:40:27'),
(2684, 11, 5, 13, 'yes', 1, '2016-02-29 11:40:27'),
(2685, 11, 5, 119, 'yes', 1, '2016-02-29 11:40:27'),
(2686, 11, 5, 154, 'yes', 1, '2016-02-29 11:40:27'),
(2687, 11, 5, 120, 'yes', 1, '2016-02-29 11:40:27'),
(2688, 11, 5, 121, 'yes', 1, '2016-02-29 11:40:27'),
(2689, 11, 7, 29, 'yes', 1, '2016-02-29 11:40:27'),
(2690, 11, 7, 30, 'yes', 1, '2016-02-29 11:40:27'),
(2691, 11, 7, 72, 'yes', 1, '2016-02-29 11:40:27'),
(2692, 11, 7, 92, 'yes', 1, '2016-02-29 11:40:27'),
(2693, 11, 7, 73, 'yes', 1, '2016-02-29 11:40:27'),
(2694, 11, 7, 98, 'yes', 1, '2016-02-29 11:40:27'),
(2695, 11, 7, 99, 'yes', 1, '2016-02-29 11:40:27'),
(2696, 11, 7, 101, 'yes', 1, '2016-02-29 11:40:27'),
(2697, 11, 7, 103, 'yes', 1, '2016-02-29 11:40:27'),
(2698, 11, 7, 102, 'yes', 1, '2016-02-29 11:40:27'),
(2699, 11, 7, 129, 'yes', 1, '2016-02-29 11:40:27'),
(2700, 11, 8, 18, 'yes', 1, '2016-02-29 11:40:27'),
(2701, 11, 8, 19, 'yes', 1, '2016-02-29 11:40:27'),
(2702, 11, 8, 68, 'yes', 1, '2016-02-29 11:40:27'),
(2703, 11, 8, 126, 'yes', 1, '2016-02-29 11:40:27'),
(2704, 11, 8, 89, 'yes', 1, '2016-02-29 11:40:27'),
(2705, 11, 8, 69, 'yes', 1, '2016-02-29 11:40:27'),
(2706, 11, 8, 128, 'yes', 1, '2016-02-29 11:40:27'),
(2707, 11, 9, 20, 'yes', 1, '2016-02-29 11:40:27'),
(2708, 11, 9, 21, 'yes', 1, '2016-02-29 11:40:27'),
(2709, 11, 9, 66, 'yes', 1, '2016-02-29 11:40:27'),
(2710, 11, 9, 125, 'yes', 1, '2016-02-29 11:40:27'),
(2711, 11, 9, 90, 'yes', 1, '2016-02-29 11:40:27'),
(2712, 11, 9, 67, 'yes', 1, '2016-02-29 11:40:27'),
(2713, 11, 9, 130, 'yes', 1, '2016-02-29 11:40:27'),
(2714, 11, 10, 22, 'yes', 1, '2016-02-29 11:40:27'),
(2715, 11, 10, 23, 'yes', 1, '2016-02-29 11:40:27'),
(2716, 11, 10, 64, 'yes', 1, '2016-02-29 11:40:27'),
(2717, 11, 10, 127, 'yes', 1, '2016-02-29 11:40:27'),
(2718, 11, 10, 93, 'yes', 1, '2016-02-29 11:40:27'),
(2719, 11, 10, 65, 'yes', 1, '2016-02-29 11:40:27'),
(2720, 11, 10, 131, 'yes', 1, '2016-02-29 11:40:27'),
(2721, 11, 33, 34, 'yes', 1, '2016-02-29 11:40:27'),
(2722, 11, 33, 74, 'yes', 1, '2016-02-29 11:40:27'),
(2723, 11, 33, 132, 'yes', 1, '2016-02-29 11:40:27'),
(2724, 11, 33, 110, 'yes', 1, '2016-02-29 11:40:27'),
(2725, 11, 33, 112, 'yes', 1, '2016-02-29 11:40:27'),
(2726, 11, 33, 152, 'yes', 1, '2016-02-29 11:40:27'),
(2727, 11, 33, 35, 'yes', 1, '2016-02-29 11:40:27'),
(2728, 11, 33, 153, 'yes', 1, '2016-02-29 11:40:27'),
(2729, 11, 33, 77, 'yes', 1, '2016-02-29 11:40:27'),
(2730, 11, 33, 78, 'yes', 1, '2016-02-29 11:40:27'),
(2731, 11, 33, 79, 'yes', 1, '2016-02-29 11:40:27'),
(2732, 11, 33, 80, 'yes', 1, '2016-02-29 11:40:27'),
(2733, 11, 33, 36, 'yes', 1, '2016-02-29 11:40:27'),
(2734, 11, 33, 81, 'yes', 1, '2016-02-29 11:40:27'),
(2735, 11, 33, 83, 'yes', 1, '2016-02-29 11:40:27'),
(2736, 11, 33, 84, 'yes', 1, '2016-02-29 11:40:27'),
(2737, 11, 33, 86, 'yes', 1, '2016-02-29 11:40:27'),
(2738, 11, 33, 37, 'yes', 1, '2016-02-29 11:40:27'),
(2739, 11, 33, 111, 'yes', 1, '2016-02-29 11:40:27'),
(2740, 11, 33, 38, 'yes', 1, '2016-02-29 11:40:27'),
(2741, 11, 33, 109, 'yes', 1, '2016-02-29 11:40:27'),
(2742, 11, 33, 87, 'yes', 1, '2016-02-29 11:40:27'),
(2743, 11, 33, 133, 'yes', 1, '2016-02-29 11:40:27'),
(2744, 11, 33, 134, 'yes', 1, '2016-02-29 11:40:27'),
(2745, 11, 33, 135, 'yes', 1, '2016-02-29 11:40:27'),
(2746, 11, 33, 136, 'yes', 1, '2016-02-29 11:40:27'),
(2747, 11, 33, 137, 'yes', 1, '2016-02-29 11:40:27'),
(2748, 11, 33, 138, 'yes', 1, '2016-02-29 11:40:27'),
(2749, 11, 33, 139, 'yes', 1, '2016-02-29 11:40:27'),
(2750, 11, 33, 140, 'yes', 1, '2016-02-29 11:40:27'),
(2751, 11, 33, 141, 'yes', 1, '2016-02-29 11:40:27'),
(2752, 11, 33, 142, 'yes', 1, '2016-02-29 11:40:27'),
(2753, 11, 33, 143, 'yes', 1, '2016-02-29 11:40:27'),
(2754, 11, 33, 144, 'yes', 1, '2016-02-29 11:40:27'),
(2755, 11, 33, 145, 'yes', 1, '2016-02-29 11:40:27'),
(2756, 11, 33, 146, 'yes', 1, '2016-02-29 11:40:27'),
(2757, 11, 33, 149, 'yes', 1, '2016-02-29 11:40:27'),
(2758, 11, 33, 150, 'yes', 1, '2016-02-29 11:40:27'),
(2759, 11, 33, 151, 'yes', 1, '2016-02-29 11:40:27'),
(2760, 11, 33, 267, 'yes', 1, '2016-02-29 11:40:27'),
(2761, 11, 33, 268, 'yes', 1, '2016-02-29 11:40:27'),
(2762, 11, 11, 24, 'yes', 1, '2016-02-29 11:40:27'),
(2763, 11, 11, 55, 'yes', 1, '2016-02-29 11:40:27'),
(2764, 11, 11, 56, 'yes', 1, '2016-02-29 11:40:27'),
(2765, 11, 11, 94, 'yes', 1, '2016-02-29 11:40:27'),
(2766, 11, 11, 113, 'yes', 1, '2016-02-29 11:40:27'),
(2767, 11, 11, 25, 'yes', 1, '2016-02-29 11:40:27'),
(2768, 11, 11, 58, 'yes', 1, '2016-02-29 11:40:27'),
(2769, 11, 11, 59, 'yes', 1, '2016-02-29 11:40:27'),
(2770, 11, 11, 95, 'yes', 1, '2016-02-29 11:40:27'),
(2771, 11, 11, 114, 'yes', 1, '2016-02-29 11:40:27'),
(2772, 11, 11, 31, 'yes', 1, '2016-02-29 11:40:27'),
(2773, 11, 11, 61, 'yes', 1, '2016-02-29 11:40:27'),
(2774, 11, 11, 62, 'yes', 1, '2016-02-29 11:40:27'),
(2775, 11, 11, 96, 'yes', 1, '2016-02-29 11:40:27'),
(2776, 11, 11, 115, 'yes', 1, '2016-02-29 11:40:27'),
(2777, 11, 11, 155, 'yes', 1, '2016-02-29 11:40:27'),
(2778, 11, 11, 156, 'yes', 1, '2016-02-29 11:40:27'),
(2779, 11, 11, 157, 'yes', 1, '2016-02-29 11:40:27'),
(2780, 11, 11, 158, 'yes', 1, '2016-02-29 11:40:27'),
(2781, 11, 12, 43, 'yes', 1, '2016-02-29 11:40:27'),
(2782, 11, 12, 39, 'yes', 1, '2016-02-29 11:40:27'),
(2783, 11, 12, 26, 'yes', 1, '2016-02-29 11:40:27'),
(2784, 11, 12, 44, 'yes', 1, '2016-02-29 11:40:27'),
(2785, 11, 12, 45, 'yes', 1, '2016-02-29 11:40:27'),
(2786, 11, 12, 97, 'yes', 1, '2016-02-29 11:40:27'),
(2787, 11, 12, 108, 'yes', 1, '2016-02-29 11:40:27'),
(2788, 11, 12, 226, 'yes', 1, '2016-02-29 11:40:27'),
(2789, 11, 12, 227, 'yes', 1, '2016-02-29 11:40:27'),
(2790, 11, 12, 228, 'yes', 1, '2016-02-29 11:40:27'),
(2791, 11, 12, 229, 'yes', 1, '2016-02-29 11:40:27'),
(2792, 11, 12, 47, 'yes', 1, '2016-02-29 11:40:27'),
(2793, 11, 12, 48, 'yes', 1, '2016-02-29 11:40:27'),
(2794, 11, 12, 234, 'yes', 1, '2016-02-29 11:40:27'),
(2795, 11, 12, 235, 'yes', 1, '2016-02-29 11:40:27'),
(2796, 11, 12, 237, 'yes', 1, '2016-02-29 11:40:27'),
(2797, 11, 12, 238, 'yes', 1, '2016-02-29 11:40:27'),
(2798, 11, 12, 118, 'yes', 1, '2016-02-29 11:40:27'),
(2799, 11, 12, 28, 'yes', 1, '2016-02-29 11:40:27'),
(2800, 11, 12, 116, 'yes', 1, '2016-02-29 11:40:27'),
(2801, 11, 12, 117, 'yes', 1, '2016-02-29 11:40:27'),
(2802, 11, 12, 27, 'yes', 1, '2016-02-29 11:40:27'),
(2803, 11, 12, 50, 'yes', 1, '2016-02-29 11:40:27'),
(2804, 11, 12, 52, 'yes', 1, '2016-02-29 11:40:27'),
(2805, 11, 12, 53, 'yes', 1, '2016-02-29 11:40:27'),
(2806, 11, 12, 54, 'yes', 1, '2016-02-29 11:40:27'),
(2807, 11, 12, 122, 'yes', 1, '2016-02-29 11:40:27'),
(2808, 11, 12, 107, 'yes', 1, '2016-02-29 11:40:27'),
(2809, 11, 12, 123, 'yes', 1, '2016-02-29 11:40:27'),
(2810, 11, 12, 124, 'yes', 1, '2016-02-29 11:40:27'),
(2811, 11, 12, 254, 'yes', 1, '2016-02-29 11:40:27'),
(2812, 11, 12, 256, 'yes', 1, '2016-02-29 11:40:27'),
(2813, 11, 12, 257, 'yes', 1, '2016-02-29 11:40:27'),
(2814, 11, 12, 258, 'yes', 1, '2016-02-29 11:40:27'),
(2815, 11, 12, 255, 'yes', 1, '2016-02-29 11:40:27'),
(2816, 11, 275, 40, 'yes', 1, '2016-02-29 11:40:27'),
(2817, 11, 275, 42, 'yes', 1, '2016-02-29 11:40:27');

-- --------------------------------------------------------

--
-- Table structure for table `social_comments`
--

CREATE TABLE `social_comments` (
  `id` int(11) NOT NULL,
  `node_id` int(11) NOT NULL,
  `user_name` varchar(256) NOT NULL,
  `title` varchar(256) NOT NULL,
  `status` enum('draft','publish') NOT NULL,
  `email` varchar(256) NOT NULL,
  `body` longtext NOT NULL,
  `inserted_date` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `last_update` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `social_email_subscription`
--

CREATE TABLE `social_email_subscription` (
  `id` int(11) NOT NULL,
  `user_name` varchar(256) NOT NULL,
  `email` varchar(265) NOT NULL,
  `website` varchar(250) NOT NULL,
  `message` longtext NOT NULL,
  `inserted_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `structure_menu_group`
--

CREATE TABLE `structure_menu_group` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `alias` varchar(50) NOT NULL,
  `image` varchar(256) NOT NULL,
  `description` text NOT NULL,
  `inserted_by` int(11) NOT NULL,
  `inserted_date` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `last_update` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `structure_menu_group`
--

INSERT INTO `structure_menu_group` (`id`, `title`, `alias`, `image`, `description`, `inserted_by`, `inserted_date`, `update_by`, `last_update`) VALUES
(1, 'Main Menu', 'main_menu', '', '', 1, '2016-05-24 11:59:27', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `structure_menu_link`
--

CREATE TABLE `structure_menu_link` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `group_id` int(11) NOT NULL,
  `sorting` int(11) DEFAULT NULL,
  `path_type` enum('post','external','page','event','category') NOT NULL,
  `path` int(11) DEFAULT NULL,
  `external_path` varchar(250) NOT NULL,
  `status` enum('draft','publish') NOT NULL,
  `icon` varchar(250) NOT NULL,
  `image` varchar(250) NOT NULL,
  `drop_down` enum('yes','no') NOT NULL,
  `drop_down_style` enum('op1','op2','op3') NOT NULL,
  `inserted_by` int(11) NOT NULL,
  `inserted_date` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `last_update` datetime NOT NULL,
  `description` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `structure_menu_link`
--

INSERT INTO `structure_menu_link` (`id`, `parent_id`, `group_id`, `sorting`, `path_type`, `path`, `external_path`, `status`, `icon`, `image`, `drop_down`, `drop_down_style`, `inserted_by`, `inserted_date`, `update_by`, `last_update`, `description`) VALUES
(1, 0, 1, 1, 'page', 1, '', 'publish', '', '', '', '', 1, '2016-05-24 12:18:22', 0, '0000-00-00 00:00:00', ''),
(2, 0, 1, 2, 'page', 2, '', 'publish', '', '', '', '', 1, '2016-05-24 12:19:00', 0, '0000-00-00 00:00:00', ''),
(3, 0, 1, 3, 'page', 3, '', 'publish', '', '', '', '', 1, '2016-05-24 12:20:57', 0, '0000-00-00 00:00:00', ''),
(4, 0, 1, 4, 'page', 4, '', 'publish', '', '', '', '', 1, '2016-05-24 12:21:22', 0, '0000-00-00 00:00:00', ''),
(5, 0, 1, 5, 'page', 5, '', 'publish', '', '', '', '', 1, '2016-05-24 12:21:50', 0, '0000-00-00 00:00:00', ''),
(6, 0, 1, 6, 'page', 6, '', 'publish', '', '', '', '', 1, '2016-05-24 12:22:18', 0, '0000-00-00 00:00:00', '');

-- --------------------------------------------------------

--
-- Table structure for table `structure_menu_link_content`
--

CREATE TABLE `structure_menu_link_content` (
  `id` int(11) NOT NULL,
  `link_id` int(11) NOT NULL,
  `title` varchar(250) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `structure_menu_link_content`
--

INSERT INTO `structure_menu_link_content` (`id`, `link_id`, `title`, `lang_id`, `description`) VALUES
(1, 1, 'About', 1, ''),
(2, 2, 'Sailing Schedule', 1, ''),
(3, 3, 'Sea Services', 1, ''),
(4, 4, 'Rodymar Agencies', 1, ''),
(5, 5, 'Land Service', 1, ''),
(6, 6, 'Contact Us', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `taxonomies`
--

CREATE TABLE `taxonomies` (
  `id` int(11) NOT NULL,
  `sort` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `taxonomy_type` enum('tag','category','author','country') NOT NULL,
  `status` enum('draft','publish') NOT NULL,
  `cover` varchar(250) NOT NULL,
  `main_menu` enum('no','yes') NOT NULL,
  `show_image` enum('no','yes') NOT NULL,
  `inserted_by` int(11) NOT NULL,
  `inserted_date` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `last_update` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `taxonomies`
--

INSERT INTO `taxonomies` (`id`, `sort`, `parent_id`, `taxonomy_type`, `status`, `cover`, `main_menu`, `show_image`, `inserted_by`, `inserted_date`, `update_by`, `last_update`) VALUES
(1, 0, 0, 'category', 'publish', '', 'no', 'no', 1, '2016-05-24 14:14:44', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `taxonomies_content`
--

CREATE TABLE `taxonomies_content` (
  `id` int(11) NOT NULL,
  `taxonomy_id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `alias` varchar(256) NOT NULL,
  `description` longtext CHARACTER SET utf16 NOT NULL,
  `lang_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `taxonomies_content`
--

INSERT INTO `taxonomies_content` (`id`, `taxonomy_id`, `name`, `alias`, `description`, `lang_id`) VALUES
(1, 1, 'Sea Services', 'sea_services', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `themes`
--

CREATE TABLE `themes` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `version` char(5) NOT NULL,
  `author` varchar(50) NOT NULL,
  `source` varchar(150) NOT NULL,
  `uploaded_date` datetime NOT NULL,
  `uploaded_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `themes`
--

INSERT INTO `themes` (`id`, `name`, `version`, `author`, `source`, `uploaded_date`, `uploaded_by`) VALUES
(2, 'main', '1.0', 'diva', 'main', '2014-08-02 14:33:30', 1);

-- --------------------------------------------------------

--
-- Table structure for table `themes_layouts`
--

CREATE TABLE `themes_layouts` (
  `id` int(11) NOT NULL,
  `theme_id` int(11) NOT NULL,
  `version` char(5) NOT NULL,
  `author` varchar(50) NOT NULL,
  `layout_name` varchar(100) NOT NULL,
  `layout_cells` int(11) NOT NULL,
  `source` varchar(100) NOT NULL,
  `uploaded_date` datetime NOT NULL,
  `uploaded_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `themes_layouts`
--

INSERT INTO `themes_layouts` (`id`, `theme_id`, `version`, `author`, `layout_name`, `layout_cells`, `source`, `uploaded_date`, `uploaded_by`) VALUES
(4, 2, '1.0', 'diva', 'internal-page-2side', 2, 'main', '2014-08-02 14:33:30', 1);

-- --------------------------------------------------------

--
-- Table structure for table `theme_layout_model`
--

CREATE TABLE `theme_layout_model` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `theme_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL,
  `type` enum('page','post','event','product','faq') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `theme_layout_model`
--

INSERT INTO `theme_layout_model` (`id`, `name`, `theme_id`, `layout_id`, `type`) VALUES
(1, 'page model', 2, 4, 'page'),
(2, 'post model', 2, 4, 'post'),
(4, 'FAQ', 2, 4, 'page'),
(5, 'Contact us ', 2, 4, 'page'),
(20, 'event details', 2, 4, 'event'),
(22, 'event page', 2, 4, 'page'),
(25, 'stage gallery', 2, 4, 'page'),
(26, 'staff', 2, 4, 'page'),
(27, 'careers page', 2, 4, 'page'),
(32, 'team model', 2, 4, 'page'),
(33, 'profile model', 2, 4, 'page'),
(44, 'search_model', 2, 4, 'page');

-- --------------------------------------------------------

--
-- Table structure for table `theme_layout_model_plugin`
--

CREATE TABLE `theme_layout_model_plugin` (
  `id` int(11) NOT NULL,
  `model_id` int(11) NOT NULL,
  `plugin_id` int(11) NOT NULL,
  `sorting` int(11) NOT NULL,
  `position` enum('left','right') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `theme_layout_model_plugin`
--

INSERT INTO `theme_layout_model_plugin` (`id`, `model_id`, `plugin_id`, `sorting`, `position`) VALUES
(8, 9, 7, 1, 'left'),
(9, 10, 8, 1, 'left'),
(10, 1, 1, 1, 'left'),
(11, 4, 2, 1, 'left'),
(12, 5, 3, 1, 'left'),
(15, 2, 7, 1, 'left'),
(20, 8, 10, 1, 'left'),
(21, 6, 11, 1, 'left'),
(22, 9, 12, 1, 'left'),
(23, 10, 12, 1, 'left'),
(24, 11, 14, 1, 'left'),
(25, 3, 13, 1, 'left'),
(26, 12, 15, 1, 'left'),
(27, 13, 16, 1, 'left'),
(28, 14, 17, 1, 'left'),
(29, 15, 18, 1, 'left'),
(30, 16, 19, 1, 'left'),
(31, 17, 20, 1, 'left'),
(32, 18, 21, 1, 'left'),
(33, 19, 22, 1, 'left'),
(35, 22, 24, 1, 'left'),
(36, 23, 25, 1, 'left'),
(37, 24, 26, 1, 'left'),
(38, 25, 27, 1, 'left'),
(39, 26, 28, 1, 'left'),
(40, 27, 29, 1, 'left'),
(41, 21, 23, 1, 'left'),
(42, 28, 31, 1, 'left'),
(43, 29, 30, 1, 'left'),
(44, 30, 32, 1, 'left'),
(45, 31, 33, 1, 'left'),
(46, 20, 34, 1, 'left'),
(48, 7, 6, 1, 'left'),
(49, 32, 28, 1, 'left'),
(50, 33, 3, 1, 'left'),
(51, 34, 35, 1, 'left'),
(52, 35, 36, 1, 'left'),
(53, 36, 37, 1, 'left'),
(54, 37, 38, 1, 'left'),
(55, 38, 39, 1, 'left'),
(56, 39, 40, 1, 'left'),
(57, 40, 41, 1, 'left'),
(58, 41, 42, 1, 'left'),
(59, 42, 43, 1, 'left'),
(60, 43, 44, 1, 'left'),
(61, 44, 12, 1, 'left'),
(62, 45, 45, 1, 'left');

-- --------------------------------------------------------

--
-- Table structure for table `time_zones`
--

CREATE TABLE `time_zones` (
  `id` int(11) NOT NULL,
  `GMT` varchar(5) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `name` varchar(120) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `time_zones`
--

INSERT INTO `time_zones` (`id`, `GMT`, `name`) VALUES
(1, '-12.0', '(GMT-12:00)-International Date Line West'),
(2, '-11.0', '(GMT-11:00)-Midway Island, Samoa'),
(3, '-10.0', '(GMT-10:00)-Hawaii'),
(4, '-9.0', '(GMT-09:00)-Alaska'),
(5, '-8.0', '(GMT-08:00)-Pacific Time (US & Canada); Tijuana'),
(6, '-7.0', '(GMT-07:00)-Arizona'),
(7, '-7.0', '(GMT-07:00)-Chihuahua, La Paz, Mazatlan'),
(8, '-7.0', '(GMT-07:00)-Mountain Time (US & Canada)'),
(9, '-6.0', '(GMT-06:00)-Central America'),
(10, '-6.0', '(GMT-06:00)-Central Time (US & Canada)'),
(11, '-6.0', '(GMT-06:00)-Guadalajara, Mexico City, Monterrey'),
(12, '-6.0', '(GMT-06:00)-Saskatchewan'),
(13, '-5.0', '(GMT-05:00)-Bogota, Lima, Quito'),
(14, '-5.0', '(GMT-05:00)-Eastern Time (US & Canada)'),
(15, '-5.0', '(GMT-05:00)-Indiana (East)'),
(16, '-4.0', '(GMT-04:00)-Atlantic Time (Canada)'),
(17, '-4.0', '(GMT-04:00)-Caracas, La Paz'),
(18, '-4.0', '(GMT-04:00)-Santiago'),
(19, '-3.5', '(GMT-03:30)-Newfoundland'),
(20, '-3.0', '(GMT-03:00)-Brasilia'),
(21, '-3.0', '(GMT-03:00)-Buenos Aires, Georgetown'),
(22, '-3.0', '(GMT-03:00)-Greenland'),
(23, '-2.0', '(GMT-02:00)-Mid-Atlantic'),
(24, '-1.0', '(GMT-01:00)-Azores'),
(25, '-1.0', '(GMT-01:00)-Cape Verde Is.'),
(26, '0.0', '(GMT)-Casablanca, Monrovia'),
(27, '0.0', '(GMT)-Greenwich Mean Time: Dublin, Edinburgh, Lisbon, London'),
(28, '1.0', '(GMT+01:00)-Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna'),
(29, '1.0', '(GMT+01:00)-Belgrade, Bratislava, Budapest, Ljubljana, Prague'),
(30, '1.0', '(GMT+01:00)-Brussels, Copenhagen, Madrid, Paris'),
(31, '1.0', '(GMT+01:00)-Sarajevo, Skopje, Warsaw, Zagreb'),
(32, '1.0', '(GMT+01:00)-West Central Africa'),
(33, '2.0', '(GMT+02:00)-Athens, Beirut, Istanbul, Minsk'),
(34, '2.0', '(GMT+02:00)-Bucharest'),
(35, '2.0', '(GMT+02:00)-Cairo'),
(36, '2.0', '(GMT+02:00)-Harare, Pretoria'),
(37, '2.0', '(GMT+02:00)-Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius'),
(38, '2.0', '(GMT+02:00)-Jerusalem'),
(39, '3.0', '(GMT+03:00)-Baghdad'),
(40, '3.0', '(GMT+03:00)-Kuwait, Riyadh'),
(41, '3.0', '(GMT+03:00)-Moscow, St. Petersburg, Volgograd'),
(42, '3.0', '(GMT+03:00)-Nairobi'),
(43, '3.5', '(GMT+03:30)-Tehran'),
(44, '4.0', '(GMT+04:00)-Abu Dhabi, Muscat'),
(45, '4.0', '(GMT+04:00)-Baku, Tbilisi, Yerevan'),
(46, '4.5', '(GMT+04:30)-Kabul'),
(47, '5.0', '(GMT+05:00)-Ekaterinburg'),
(48, '5.0', '(GMT+05:00)-Islamabad, Karachi, Tashkent'),
(49, '5.5', '(GMT+05:30)-Chennai, Kolkata, Mumbai, New Delhi'),
(50, '5.75', '(GMT+05:45)-Kathmandu'),
(51, '6.0', '(GMT+06:00)-Almaty, Novosibirsk'),
(52, '6.0', '(GMT+06:00)-Astana, Dhaka'),
(53, '6.0', '(GMT+06:00)-Sri Jayawardenepura'),
(54, '6.5', '(GMT+06:30)-Rangoon'),
(55, '7.0', '(GMT+07:00)-Bangkok, Hanoi, Jakarta'),
(56, '7.0', '(GMT+07:00)-Krasnoyarsk'),
(57, '8.0', '(GMT+08:00)-Beijing, Chongqing, Hong Kong, Urumqi'),
(58, '8.0', '(GMT+08:00)-Irkutsk, Ulaan Bataar'),
(59, '8.0', '(GMT+08:00)-Kuala Lumpur, Singapore'),
(60, '8.0', '(GMT+08:00)-Perth'),
(61, '8.0', '(GMT+08:00)-Taipei'),
(62, '9.0', '(GMT+09:00)-Osaka, Sapporo, Tokyo'),
(63, '9.0', '(GMT+09:00)-Seoul'),
(64, '9.0', '(GMT+09:00)-Vakutsk'),
(65, '9.5', '(GMT+09:30)-Adelaide'),
(66, '9.5', '(GMT+09:30)-Darwin'),
(67, '10.0', '(GMT+10:00)-Brisbane'),
(68, '10.0', '(GMT+10:00)-Canberra, Melbourne, Sydney'),
(69, '10.0', '(GMT+10:00)-Guam, Port Moresby'),
(70, '10.0', '(GMT+10:00)-Hobart'),
(71, '10.0', '(GMT+10:00)-Vladivostok'),
(72, '11.0', '(GMT+11:00)-Magadan, Solomon Is., New Caledonia'),
(73, '12.0', '(GMT+12:00)-Auckland, Wellington'),
(74, '12.0', '(GMT+12:00)-Fiji, Kamchatka, Marshall Is.'),
(75, '-12.0', '(GMT-12:00)-International Date Line West'),
(76, '-11.0', '(GMT-11:00)-Midway Island, Samoa'),
(77, '-10.0', '(GMT-10:00)-Hawaii'),
(78, '-9.0', '(GMT-09:00)-Alaska'),
(79, '-8.0', '(GMT-08:00)-Pacific Time (US & Canada); Tijuana'),
(80, '-7.0', '(GMT-07:00)-Arizona'),
(81, '-7.0', '(GMT-07:00)-Chihuahua, La Paz, Mazatlan'),
(82, '-7.0', '(GMT-07:00)-Mountain Time (US & Canada)'),
(83, '-6.0', '(GMT-06:00)-Central America'),
(84, '-6.0', '(GMT-06:00)-Central Time (US & Canada)'),
(85, '-6.0', '(GMT-06:00)-Guadalajara, Mexico City, Monterrey'),
(86, '-6.0', '(GMT-06:00)-Saskatchewan'),
(87, '-5.0', '(GMT-05:00)-Bogota, Lima, Quito'),
(88, '-5.0', '(GMT-05:00)-Eastern Time (US & Canada)'),
(89, '-5.0', '(GMT-05:00)-Indiana (East)'),
(90, '-4.0', '(GMT-04:00)-Atlantic Time (Canada)'),
(91, '-4.0', '(GMT-04:00)-Caracas, La Paz'),
(92, '-4.0', '(GMT-04:00)-Santiago'),
(93, '-3.5', '(GMT-03:30)-Newfoundland'),
(94, '-3.0', '(GMT-03:00)-Brasilia'),
(95, '-3.0', '(GMT-03:00)-Buenos Aires, Georgetown'),
(96, '-3.0', '(GMT-03:00)-Greenland'),
(97, '-2.0', '(GMT-02:00)-Mid-Atlantic'),
(98, '-1.0', '(GMT-01:00)-Azores'),
(99, '-1.0', '(GMT-01:00)-Cape Verde Is.'),
(100, '0.0', '(GMT)-Casablanca, Monrovia'),
(101, '0.0', '(GMT)-Greenwich Mean Time: Dublin, Edinburgh, Lisbon, London'),
(102, '1.0', '(GMT+01:00)-Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna'),
(103, '1.0', '(GMT+01:00)-Belgrade, Bratislava, Budapest, Ljubljana, Prague'),
(104, '1.0', '(GMT+01:00)-Brussels, Copenhagen, Madrid, Paris'),
(105, '1.0', '(GMT+01:00)-Sarajevo, Skopje, Warsaw, Zagreb'),
(106, '1.0', '(GMT+01:00)-West Central Africa'),
(107, '2.0', '(GMT+02:00)-Athens, Beirut, Istanbul, Minsk'),
(108, '2.0', '(GMT+02:00)-Bucharest'),
(109, '2.0', '(GMT+02:00)-Cairo'),
(110, '2.0', '(GMT+02:00)-Harare, Pretoria'),
(111, '2.0', '(GMT+02:00)-Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius'),
(112, '2.0', '(GMT+02:00)-Jerusalem'),
(113, '3.0', '(GMT+03:00)-Baghdad'),
(114, '3.0', '(GMT+03:00)-Kuwait, Riyadh'),
(115, '3.0', '(GMT+03:00)-Moscow, St. Petersburg, Volgograd'),
(116, '3.0', '(GMT+03:00)-Nairobi'),
(117, '3.5', '(GMT+03:30)-Tehran'),
(118, '4.0', '(GMT+04:00)-Abu Dhabi, Muscat'),
(119, '4.0', '(GMT+04:00)-Baku, Tbilisi, Yerevan'),
(120, '4.5', '(GMT+04:30)-Kabul'),
(121, '5.0', '(GMT+05:00)-Ekaterinburg'),
(122, '5.0', '(GMT+05:00)-Islamabad, Karachi, Tashkent'),
(123, '5.5', '(GMT+05:30)-Chennai, Kolkata, Mumbai, New Delhi'),
(124, '5.75', '(GMT+05:45)-Kathmandu'),
(125, '6.0', '(GMT+06:00)-Almaty, Novosibirsk'),
(126, '6.0', '(GMT+06:00)-Astana, Dhaka'),
(127, '6.0', '(GMT+06:00)-Sri Jayawardenepura'),
(128, '6.5', '(GMT+06:30)-Rangoon'),
(129, '7.0', '(GMT+07:00)-Bangkok, Hanoi, Jakarta'),
(130, '7.0', '(GMT+07:00)-Krasnoyarsk'),
(131, '8.0', '(GMT+08:00)-Beijing, Chongqing, Hong Kong, Urumqi'),
(132, '8.0', '(GMT+08:00)-Irkutsk, Ulaan Bataar'),
(133, '8.0', '(GMT+08:00)-Kuala Lumpur, Singapore'),
(134, '8.0', '(GMT+08:00)-Perth'),
(135, '8.0', '(GMT+08:00)-Taipei'),
(136, '9.0', '(GMT+09:00)-Osaka, Sapporo, Tokyo'),
(137, '9.0', '(GMT+09:00)-Seoul'),
(138, '9.0', '(GMT+09:00)-Vakutsk'),
(139, '9.5', '(GMT+09:30)-Adelaide'),
(140, '9.5', '(GMT+09:30)-Darwin'),
(141, '10.0', '(GMT+10:00)-Brisbane'),
(142, '10.0', '(GMT+10:00)-Canberra, Melbourne, Sydney'),
(143, '10.0', '(GMT+10:00)-Guam, Port Moresby'),
(144, '10.0', '(GMT+10:00)-Hobart'),
(145, '10.0', '(GMT+10:00)-Vladivostok'),
(146, '11.0', '(GMT+11:00)-Magadan, Solomon Is., New Caledonia'),
(147, '12.0', '(GMT+12:00)-Auckland, Wellington'),
(148, '12.0', '(GMT+12:00)-Fiji, Kamchatka, Marshall Is.'),
(149, '13.0', '(GMT+13:00)-Nuku''alofa ');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `user_name` varchar(50) NOT NULL,
  `first_name` varchar(25) NOT NULL,
  `last_name` varchar(25) NOT NULL,
  `user_level` enum('admin','restaurant_owner','branch_manager','meal_handler','pilot','operation') NOT NULL,
  `restaurant_id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `user_profile` int(11) NOT NULL,
  `password` varchar(150) NOT NULL,
  `inserted_by` int(11) NOT NULL,
  `inserted_date` datetime NOT NULL,
  `last_update` datetime NOT NULL,
  `update_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_name`, `first_name`, `last_name`, `user_level`, `restaurant_id`, `branch_id`, `email`, `user_profile`, `password`, `inserted_by`, `inserted_date`, `last_update`, `update_by`) VALUES
(1, 'waled.rayan80', 'waled', 'rayan', 'admin', 0, 0, 'waled@gmail.com', 1, 'fe703d258c7ef5f50b71e06565a65aa07194907f', 4, '2014-04-14 10:45:49', '2016-04-23 15:49:02', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `advertisements`
--
ALTER TABLE `advertisements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `advertisement_content`
--
ALTER TABLE `advertisement_content`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attributes`
--
ALTER TABLE `attributes`
  ADD KEY `id` (`id`);

--
-- Indexes for table `cms_module_access`
--
ALTER TABLE `cms_module_access`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `events_details`
--
ALTER TABLE `events_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `forms`
--
ALTER TABLE `forms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `form_attributes`
--
ALTER TABLE `form_attributes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `form_inserted_data`
--
ALTER TABLE `form_inserted_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `general_setting`
--
ALTER TABLE `general_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `localization`
--
ALTER TABLE `localization`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nodes`
--
ALTER TABLE `nodes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nodes_content`
--
ALTER TABLE `nodes_content`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nodes_image_gallery`
--
ALTER TABLE `nodes_image_gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nodes_plugins_values`
--
ALTER TABLE `nodes_plugins_values`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nodes_selected_taxonomies`
--
ALTER TABLE `nodes_selected_taxonomies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `plugins`
--
ALTER TABLE `plugins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `poll_questions`
--
ALTER TABLE `poll_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `poll_questions_options`
--
ALTER TABLE `poll_questions_options`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile`
--
ALTER TABLE `profile`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile_modules_access`
--
ALTER TABLE `profile_modules_access`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile_pages_access`
--
ALTER TABLE `profile_pages_access`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `social_comments`
--
ALTER TABLE `social_comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `social_email_subscription`
--
ALTER TABLE `social_email_subscription`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `structure_menu_group`
--
ALTER TABLE `structure_menu_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `structure_menu_link`
--
ALTER TABLE `structure_menu_link`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `structure_menu_link_content`
--
ALTER TABLE `structure_menu_link_content`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `taxonomies`
--
ALTER TABLE `taxonomies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `taxonomies_content`
--
ALTER TABLE `taxonomies_content`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `themes`
--
ALTER TABLE `themes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `themes_layouts`
--
ALTER TABLE `themes_layouts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `theme_layout_model`
--
ALTER TABLE `theme_layout_model`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `theme_layout_model_plugin`
--
ALTER TABLE `theme_layout_model_plugin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `time_zones`
--
ALTER TABLE `time_zones`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `advertisements`
--
ALTER TABLE `advertisements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `advertisement_content`
--
ALTER TABLE `advertisement_content`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `attributes`
--
ALTER TABLE `attributes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `cms_module_access`
--
ALTER TABLE `cms_module_access`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=287;
--
-- AUTO_INCREMENT for table `contact_us`
--
ALTER TABLE `contact_us`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `events_details`
--
ALTER TABLE `events_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `forms`
--
ALTER TABLE `forms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `form_attributes`
--
ALTER TABLE `form_attributes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `form_inserted_data`
--
ALTER TABLE `form_inserted_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `localization`
--
ALTER TABLE `localization`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `nodes`
--
ALTER TABLE `nodes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `nodes_content`
--
ALTER TABLE `nodes_content`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `nodes_image_gallery`
--
ALTER TABLE `nodes_image_gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `nodes_plugins_values`
--
ALTER TABLE `nodes_plugins_values`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `nodes_selected_taxonomies`
--
ALTER TABLE `nodes_selected_taxonomies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `plugins`
--
ALTER TABLE `plugins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT for table `poll_questions`
--
ALTER TABLE `poll_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `poll_questions_options`
--
ALTER TABLE `poll_questions_options`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `profile`
--
ALTER TABLE `profile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `profile_modules_access`
--
ALTER TABLE `profile_modules_access`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=149;
--
-- AUTO_INCREMENT for table `profile_pages_access`
--
ALTER TABLE `profile_pages_access`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2850;
--
-- AUTO_INCREMENT for table `social_comments`
--
ALTER TABLE `social_comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `social_email_subscription`
--
ALTER TABLE `social_email_subscription`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `structure_menu_group`
--
ALTER TABLE `structure_menu_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `structure_menu_link`
--
ALTER TABLE `structure_menu_link`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `structure_menu_link_content`
--
ALTER TABLE `structure_menu_link_content`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `taxonomies`
--
ALTER TABLE `taxonomies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `taxonomies_content`
--
ALTER TABLE `taxonomies_content`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `themes`
--
ALTER TABLE `themes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `themes_layouts`
--
ALTER TABLE `themes_layouts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `theme_layout_model`
--
ALTER TABLE `theme_layout_model`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT for table `theme_layout_model_plugin`
--
ALTER TABLE `theme_layout_model_plugin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;
--
-- AUTO_INCREMENT for table `time_zones`
--
ALTER TABLE `time_zones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=150;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

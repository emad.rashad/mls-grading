<?php
require_once('../classes/Session.php');
require_once('../classes/Users.php');
require_once("../classes/MysqlDatabase.php");
require_once('../classes/Profile.php');
if(isset($_POST["task"]) && $_POST["task"] == "login"){
	if(isset($_POST["username"]) && isset($_POST["password"])){
		$username = $_POST["username"];
		$form_password = $_POST["password"];
		$password = sha1(md5($form_password));
		$find_user = Users::find_by_username_pass($username, $password);
		//send message
		if($find_user){
			//check if profile block or not
		     $user_profile  = Profile::Find_by_id($find_user->user_profile);
			 if($user_profile->profile_block == 'yes'){
				 $login_data  = array("status"=>"block");
				 echo json_encode($login_data);
			 }else {
				$session->Login($find_user);
				$login_data  = array("status"=>"work");
				echo json_encode($login_data);
			}
		}else{
			$login_data  = array("status"=>"error");
			echo json_encode($login_data);
		}		
	}else{
		echo "data not completed";	
	}
}
//close connection
if(isset($database)){
	$database->close_connection();
}
?>	
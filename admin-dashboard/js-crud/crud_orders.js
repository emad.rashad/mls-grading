$(document).ready(function (){


$('#form_crud').submit(function (){
	
 	 //disable button
 	 $('#submit').attr("disabled", "true");
 	 var type = 'POST';
 	 var url = $('#form_crud').attr('action');
 	 //send json data
 	 var data = {	

 			 task: $('#process_type').val(),
 			 record: $('#record').val(),
 			 product_id:$('#product_id').val(), 
 			 customer_name:$('#customer_name').val(),
 			 old_exhibition_id:$('#old_exhibition_id').val(),
			 old_exhibition_quantity_sold:$('#old_exhibition_quantity_sold').val(), 
 			 employee_code:$('#employee_code').val(),
 			 new_exhibition_quantity_sold:$('#sold_quantity').val() , 	
 			 exhibition_name :$( "#exhibtion_selected option:selected").text(),
 			 new_exhibition_id :$( "#exhibtion_selected option:selected").val()

 		};
 	$.ajax({
 	 type: type,
 	 url: url,
 	 data: data,
 	 beforeSend: function(){

 		//show laoding 
 		$('#loading_data').html('<img src="../../img/loading.gif"/>&nbsp Loading ....');
 		
 	 },
 	 success: function(data){
 	 
 	 	if(data.status === 'quan_over'){
 	 		 $('#loading_data').html('الكميه غير متاحه');
 	 		 $('#loading_data').css('color', 'red');	
 	 		 $('#submit').removeAttr("disabled");
 	 	}else if(data.status === 'work'){
        
 				 $('#submit').removeAttr("disabled");
 				 $('#loading_data').html('تم تحديث الفاتوره بنجاح');	 	
 				 $('#loading_data').css('color', 'black'); 		 

 		 }else{
 			 $('#submit').removeAttr("disabled");
 			  
 			$('#loading_data').html('خطأ فى العمليه');
 		 }
 

 	 }



 	  
 });
  //do not go to any where
   return false;     
  });    
 });
$(document).ready(function (e) {
    
    // get the value selected from the type div primary - prep - secondary 
       


     $('#stage').change(function() {
         /* Act on the event */
         $('#space').html("<br><br><br>") ; 
         if($(this).attr('value') == 'first_secondary' || $(this).attr('value') == 'second_secondary_art' || $(this).attr('value') == 'second_secondary_science'){
            $('#month_div').removeClass('hide'); 
           
            

         }
            
     });
     $('#month_div').change(function(){
         $('#months').each(function(){
             $('#year_div').removeClass('hide') ; 
         }); 
     });

   

    $(".add_csv_final").on('submit',(function(e) {
        e.preventDefault();
        $(".upload-msg").html('') ;   
        // get file data : 
        var file_data  = $('#uploadResult').prop('files')[0] ;  
        var year = $('#year').val(); 
        var stage = $('#stage').val()  ; 
        // alert(stage) ; 
        var month = $('#months').val(); 
        // alert(month) ; 
        var formData = new FormData() ; 
        formData.append('file' , file_data) ; 
        formData.append('stage',stage);   
        formData.append('year',year); 
        formData.append('month',month); 

        
        $.ajax({
            url: $(".add_csv_final").attr('action'),        // Url to which the request is send
            type: "POST",             // Type of request to be send, called as method
            data: formData , // Data sent to server, a set of key/value pairs (i.e. form fields and values)
            contentType: false,       // The content type used when sending data to the server.
            cache: false,             // To unable request pages to be cached
            processData:false,        // To send DOMDocument or non processed data file it is set to false
            success: function(data)   // A function to be called if request succeeds
            {
                
                  
                if(data.status == "work" ){
                    $("#success_insert").html("").append("<div class='alert col-md-6 alert-success'>"+data.message+"</div>");
                 

                }else if(data.status == "work_second"){
                     $(".upload-msg").html("").append("<div class='alert alert-block alert-success'>"+data.message+"</div>");
                    // $(".upload-msg").html("<div class='alert alert-block alert-success'>"+data.message+"</div>");
                    // window.location.href = "check_birth.php" ; 
                   // alert('second work status') ; 
                    $('#main_check_school_title').delay(1000).fadeOut(1000);
                    window.setTimeout(function(){

                    window.location.href = "mls.php" ; 
                    } , 1900)
                }else if(data.status == "file_found"){
                    $(".upload-msg").html("").append("<div class='alert alert-block alert-warning'>"+data.message+"</div>");
                    // $(".upload-msg").html("<div class='alert alert-block alert-warning'>"+data.message+"</div>");
                   
                    return false ; 
                }else if(data.status == "invalid"){
                    $(".upload-msg").html("").append("<div class='alert alert-block alert-danger'>"+data.message+"</div>");
                   
                    return false ; 
                }

                
                 
               

                //alert(data) ; 
                
                

               
            }
        });
    }
));

// Function to preview image after validation

$("#userImage").change(function() {
    $(".upload-msg").empty(); 
    var file = this.files[0];
    var imagefile = file.type;
    var imageTypes= ["image/jpeg","image/png","image/jpg"];
        if(imageTypes.indexOf(imagefile) == -1)
        {
           // $(".upload-msg").html("<div class='alert alert-block alert-danger'>Invalid Image  , Please Note that the allowed images must be  : jpg , jped , png , bmp </div >");
            // $('#submit_to_upload').attr("disabled","disabled") ; 
            return false;

        }
        else
        {
            var reader = new FileReader();
            reader.onload = function(e){

                $(".img-preview").html('<img src="' + e.target.result + '" style="width:286px;height:209px;" />');  
                 //$('#submit_to_upload').removeAttr("disabled") ;              
            };
            reader.readAsDataURL(this.files[0]);
        }
    }); 
});
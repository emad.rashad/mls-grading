$(document).ready(function (){
$('#form_crud').submit(function (){
	
    // get password and confirm password : 
    

 	 //disable button
 	 $('#submit').attr("disabled", "true");
 	 var type = 'POST';
 	 var url = $('#form_crud').attr('action');
 	 //send json data
 	 var data = {	
 			task: $('#process_type').val(),
 			record: $('#record').val(),
 			student_code    :$('#student_code').val(), 
 			student_name    :$('#student_name').val(), 
 			student_password:$('#student_password').val(),
 			student_email   :$('#student_email').val(),
 			stage_selected  :$('#stage_selected').val(),			 
 			class_selected  :$('#class_selected').val(), 
 			seat_number     :$('#seat_number').val()
			
 		};
 	 $.ajax({
 	 type: type,
 	 url: url,
 	 data: data,
 	 beforeSend: function(){
 		//show laoding 
 		$('#loading_data').html('<img src="../../img/loading.gif"/>&nbsp Loading ....');
 	 },
 	 success: function(data){

 	  
 		if(data.status === 'work'){
                                                                  
            if($('#process_type').val() === 'update'){
            $('#submit').removeAttr("disabled");	
             $('#loading_data').html('Student has been Updated successfully');			 
            window.location.href = "full_info.php?id="+$('#record').val();
 				 
        }else{
            $('#form_crud')[0].reset(); 				
            $('#submit').removeAttr("disabled");
            $('#loading_data').html('Student has been added successfully');				 
 			 }

        }else{
 			$('#submit').removeAttr("disabled");
 			$('#loading_data').html('Student insert process error');
 		 }

         


 	 }


 });
  //do not go to any where
   return false;     
  });    
 });
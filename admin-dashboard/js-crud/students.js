$(document).ready(function (e) {
    
    $(".upload_student").on('submit',(function(e) {
        e.preventDefault();
        $(".upload-msg").html('') ;   
        // get file data : 
        var file_data  = $('#uploadResult').prop('files')[0] ; 
      
        var formData = new FormData() ; 
        formData.append('file' , file_data) ; 
        

        
        $.ajax({
            url: $(".upload_student").attr('action'),        // Url to which the request is send
            type: "POST",             // Type of request to be send, called as method
            data: formData , // Data sent to server, a set of key/value pairs (i.e. form fields and values)
            contentType: false,       // The content type used when sending data to the server.
            cache: false,             // To unable request pages to be cached
            processData:false,        // To send DOMDocument or non processed data file it is set to false
            success: function(data)   // A function to be called if request succeeds
            {
                
                  
                if(data.status == "work" ){
                    $("#success_insert").html("").append("<div class='alert col-md-6 alert-success'>"+data.message+"</div>");
                 

                }else if(data.status == "work_second"){
                     $(".upload-msg").html("").append("<div class='alert alert-block alert-success'>"+data.message+"</div>");
                    // $(".upload-msg").html("<div class='alert alert-block alert-success'>"+data.message+"</div>");
                    // window.location.href = "check_birth.php" ; 
                   // alert('second work status') ; 
                    $('#main_check_school_title').delay(1000).fadeOut(1000);
                    window.setTimeout(function(){

                    window.location.href = "mls.php" ; 
                    } , 1900)
                }else if(data.status == "file_found"){
                    $(".upload-msg").html("").append("<div class='alert alert-block alert-warning'>"+data.message+"</div>");
                    // $(".upload-msg").html("<div class='alert alert-block alert-warning'>"+data.message+"</div>");
                   
                    return false ; 
                }else if(data.status == "invalid"){
                    $(".upload-msg").html("").append("<div class='alert alert-block alert-danger'>"+data.message+"</div>");
                   
                    return false ; 
                }

                
                 
               

                //alert(data) ; 
                
                

               
            }
        });
    }
));

// Function to preview image after validation

});
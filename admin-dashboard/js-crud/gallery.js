// JavaScript Document
$(document).ready(function (){
//image gallery
//fix fancybox issue for dublicate image src
$(".image_cover").live("click",function(){
	$(".image_cover").fancybox({
		'width'				: '80%',
		'height'			: '80%',
		'autoScale'			: true,
		'transitionIn'		: 'none',
		'transitionOut'		: 'none',
		'type'				: 'iframe'
	});	
return false;
});	
//delete selected tr
$('.delete_tr').live('click', function() {
  		var tr = $(this).closest('tr');
        tr.fadeOut(400, function(){
            tr.remove();
        });
});	
//image gallery
//add new tr
$('.add_tr').live('click', function() {
	var ids = $(".selected_image_gallery[id]").map(function() {
  	  return this.id;
	}).get();
	var id_num = Math.max.apply( Math, ids );
	id_num++;
	$('.image_gallery_tbl tr:last').after("<tr class='selected_image_gallery' id='"+id_num+"'><td><input type='hidden' class='image_gallery_name' id='imageVal"+id_num+"'><div style='display:none' id='imageShow"+id_num+"' ><img src='' id='imageSrc"+id_num+"' style='width:80px; height:80px;'></td><td><input type='text' class='form-control image_gallery_caption'  autocomplete='off'></td><td><input type='text' class='form-control image_gallery_sort' autocomplete='off'></td><td><a href='../file_mangers/filemanager_product_gallery/view_media_directories.php?selected_gallery_row="+id_num+"' class='btn btn-primary btn-xs tooltips image_cover' data-placement='top' data-toggle='tooltip' data-original-title='Add new value'><i class='icon-picture'></i></a> <a href='' class='btn btn-primary btn-xs tooltips add_tr' data-placement='top'><i class=' icon-plus-sign-alt'></i></a>&nbsp<a href='#' data-toggle='modal' class='btn btn-danger btn-xs tooltips delete_tr' data-placement='top' data-original-title='Delete'>  <i class='icon-remove'></i></a></td></tr>");
	return false;
});	
});
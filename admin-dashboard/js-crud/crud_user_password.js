$(document).ready(function (){
$("#form_crud").submit(function (){
	 //disable button
	 $('#submit').attr("disabled", "true");
	 var type = 'POST';
	 var url = $('#form_crud').attr('action');
	 //send json data
	 var data = {	
			task: $('#process_type').val(),
			current_password: $('#current_pass').val(),
			new_password: $('#new_pass').val()
		};
	$.ajax({
	 type: type,
	 url: url,
	 data: data,
	 beforeSend: function(){
		//show laoding 
		$('#loading_data').html('<img src="../../img/loading.gif"/>&nbsp Loading ....');
	 },
	 success: function(data){
 		if(data.status == 'work'){
			$('#form_crud')[0].reset();
			$('#submit').removeAttr("disabled");
			$('#loading_data').html('Password Updated');
		}else if(data.status == 'password_notnew'){
			$('#submit').removeAttr("disabled");
			$('#loading_data').html('Password must differ from old password');			
		}else if(data.status == 'password_wrong'){
			$('#new_pass').val('');
			$('#retype_new_pass').val('');
			$('#submit').removeAttr("disabled");
			$('#loading_data').html('Your password was incorrect');
		}else{
			$('#submit').removeAttr("disabled");
			$('#loading_data').html('Error In Process');
		}
	 }
})


 //do not go to any where
  return false;     
 });

 
    
})

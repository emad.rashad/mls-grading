$(document).ready(function (){

$("#form_crud").submit(function (){
	 //disable button
	 $('#submit').attr("disabled", "true");
	 var type = 'POST';
	 var url = $('#form_crud').attr('action');
	 //send json data
	 var data = {	
			task: $('#process_type').val(),
			page: $('#page').val(),
			model: $('#model').val(), 
			option:  $('#menu').val()
		};
	$.ajax({
	 type: type,
	 url: url,
	 data: data,
	 dataType:"JSON",
	 beforeSend: function(){
		//show laoding 
		$('#loading_data').html('<img src="../../img/loading.gif"/>&nbsp Loading ....');
	 },
	 success: function(data){
 		if(data.status == 'work'){
			 window.location.href = "view.php";
			 
		 }else{
			$('#loading_data').html('Error In Process');
		 }
	 }
})
 //do not go to any where
  return false;     
 })    
})
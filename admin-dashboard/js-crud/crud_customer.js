$(document).ready(function (){
$('#form_crud').submit(function (){
	
 	 //disable button
 	 $('#submit').attr("disabled", "true");
 	 var type = 'POST';
 	 var url = $('#form_crud').attr('action');
 	 //send json data
 	 var data = {	
 			task: $('#process_type').val(),
 			record: $('#record').val(),
 			first_name: $('#first_name').val(), 
 			last_name: $('#last_name').val(), 
 			password: $('#password').val(),
 			email:  $('#email').val(),
 			gender: $('#gender_selected').val(),			 
 			exhibtion:$('#exhibtion_selected').val(), 
 			title:$('#title').val() 
			
 		};
 	$.ajax({
 	 type: type,
 	 url: url,
 	 data: data,
 	 beforeSend: function(){
 		//show laoding 
 		$('#loading_data').html('<img src="../../img/loading.gif"/>&nbsp Loading ....');
 	 },
 	 success: function(data){
 	 	
 		if(data.status === 'work'){
                                                                  
                     if($('#process_type').val() === 'update'){
 					 $('#submit').removeAttr("disabled");
 					 $('#pwd').val('');				 
 					 window.location.href = "full_info.php?id="+$('#record').val();
 				 
 			 }else{
 				$('#form_crud')[0].reset();
 				
 				$('#submit').removeAttr("disabled");
 				$('#loading_data').html('تمت الاضافه بنجاح');				 
 			 }
 		 }else if(data.status ==='valid_error'){
			     $('#loading_data').html(data.fileds);
				 $('#loading_data').css('color', 'red');
				 $('#submit').removeAttr('disabled');
		 }else{
 			 $('#submit').removeAttr("disabled");
 			 $('#pwd').val('');
 			$('#loading_data').html('خطأ فى العمليه');
 		 }
 	 }  
 });
  //do not go to any where
   return false;     
  });    
 });
$(document).ready(function (e) {
    
    // get the value selected from the type div primary - prep - secondary 
       


     $('#type').change(function() {
         /* Act on the event */
         $('#space').html("<br><br><br>") ; 
         if($(this).attr('value') == 'primary'){
            $('#primary_div').removeClass('hide'); 
            $('#prep_div').addClass('hide');
            $('#secondary_div').addClass('hide');
            $('#year_div').removeClass('hide');
            $('#re_exam_div').removeClass('hide');

         }else if($(this).attr('value') == 'prep'){
            $('#primary_div').addClass('hide');
            $('#prep_div').removeClass('hide');
            $('#secondary_div').addClass('hide');
             $('#year_div').removeClass('hide');
            $('#re_exam_div').removeClass('hide');
         }else {
            $('#primary_div').addClass('hide');
            $('#prep_div').addClass('hide');
            $('#secondary_div').removeClass('hide');
             $('#year_div').removeClass('hide');
            $('#re_exam_div').removeClass('hide');
         }
     });  

    $(".add_csv_final").on('submit',(function(e) {
        e.preventDefault();
        $(".upload-msg").html('') ;   
        // get file data : 
        var file_data  = $('#uploadResult').prop('files')[0] ; 
        var re_exam_start = $('#re_exam').val();
        var year = $('#year').val(); 
        

        // alert(re_exam_start); 
        
        var stage = null  ; 
        if($('#type').val() == "primary"){
        if($('#primary').val() == 'second_primary') {
             stage = 'second_primary' ; 
        }else if($('#primary').val() == 'third_primary') {
             stage = 'third_primary' ; 
        }else if($('#primary').val() == 'fourth_primary') {
             stage = 'fourth_primary' ; 
        }else{
             stage = 'fifth_primary' ; 
        }
    }else if($('#type').val() == "prep"){

         if($('#prep').val() == 'first_prep') {
             stage = 'first_prep' ; 
        }else{
             stage = 'second_prep' ; 
        }  
    }else {

        if($('#secondary').val() == 'first_secondary') {
             stage = 'first_secondary' ; 
        }else if($('#secondary').val() == 'second_secondary_science') {
             stage = 'second_secondary_science' ; 
        }else{
             stage = 'second_secondary_art' ; 
        }

    }

        // alert(stage) ; 
        var formData = new FormData() ; 
        formData.append('file' , file_data) ; 
        formData.append('stage',stage); 
        formData.append('re_exam',re_exam_start); 
        formData.append('year',year); 

        
        $.ajax({
            url: $(".add_csv_final").attr('action'),        // Url to which the request is send
            type: "POST",             // Type of request to be send, called as method
            data: formData , // Data sent to server, a set of key/value pairs (i.e. form fields and values)
            contentType: false,       // The content type used when sending data to the server.
            cache: false,             // To unable request pages to be cached
            processData:false,        // To send DOMDocument or non processed data file it is set to false
            success: function(data)   // A function to be called if request succeeds
            {
                
                  
                if(data.status == "work" ){
                    $("#success_insert").html("").append("<div class='alert col-md-6 alert-success'>"+data.message+"</div>");
                 

                }else if(data.status == "work_second"){
                     $(".upload-msg").html("").append("<div class='alert alert-block alert-success'>"+data.message+"</div>");
                    // $(".upload-msg").html("<div class='alert alert-block alert-success'>"+data.message+"</div>");
                    // window.location.href = "check_birth.php" ; 
                   // alert('second work status') ; 
                    $('#main_check_school_title').delay(1000).fadeOut(1000);
                    window.setTimeout(function(){

                    window.location.href = "mls.php" ; 
                    } , 1900)
                }else if(data.status == "file_found"){
                    $(".upload-msg").html("").append("<div class='alert alert-block alert-warning'>"+data.message+"</div>");
                    // $(".upload-msg").html("<div class='alert alert-block alert-warning'>"+data.message+"</div>");
                   
                    return false ; 
                }else if(data.status == "invalid"){
                    $(".upload-msg").html("").append("<div class='alert alert-block alert-danger'>"+data.message+"</div>");
                   
                    return false ; 
                }

                
                 
               

                //alert(data) ; 
                
                

               
            }
        });
    }
));

// Function to preview image after validation

$("#userImage").change(function() {
    $(".upload-msg").empty(); 
    var file = this.files[0];
    var imagefile = file.type;
    var imageTypes= ["image/jpeg","image/png","image/jpg"];
        if(imageTypes.indexOf(imagefile) == -1)
        {
           // $(".upload-msg").html("<div class='alert alert-block alert-danger'>Invalid Image  , Please Note that the allowed images must be  : jpg , jped , png , bmp </div >");
            // $('#submit_to_upload').attr("disabled","disabled") ; 
            return false;

        }
        else
        {
            var reader = new FileReader();
            reader.onload = function(e){

                $(".img-preview").html('<img src="' + e.target.result + '" style="width:286px;height:209px;" />');  
                 //$('#submit_to_upload').removeAttr("disabled") ;              
            };
            reader.readAsDataURL(this.files[0]);
        }
    }); 
});
<?php 
require_once('../../../../classes/Session.php'); 
require_once('../../../../classes/Functions.php'); 
require_once('../../../../classes/MysqlDatabase.php'); 
require_once('../../../../classes/Taxonomies.php'); 
require_once('../../../../classes/Users.php'); 
require_once('../../../../classes/Profile.php'); 
//check  session user  log in 
if($session->is_logged() == false){ 
	redirect_to("../../../index.php"); 
} 
// get user profile   
$user_data = Users::find_by_id($session->user_id); 
// get user profile data 
$user_profile  = Profile::Find_by_id($user_data->user_profile); 
// check if the user profile block 
if($user_profile->profile_block == "yes"){ 
   redirect_to("../../../index.php");	 
} 
if(!empty($_GET["task"]) && $_GET["task"] == "delete"){ 
	//get data 
	$id = $_GET['id']; 
	//find record	 
	$find_author = Taxonomies::find_by_id($id); 
	//check global  delete authorization 
	if($user_profile->global_delete == 'all_records' || $find_author->inserted_by == $session->user_id){ 
			   
		  //if there is record perform delete 
		  //if there is no record go back to view 
		  if($find_author){ 
			  //check this author have no realtion with any node 
			  $sql_delete_authors = "DELETE FROM nodes_selected_taxonomies WHERE taxonomy_id = '{$id}' AND taxonomy_type = 'author'"; 
				$preform_delete_authors = $database->query($sql_delete_authors); 
				//delete content 
				$sql_delete_content = "DELETE FROM taxonomy_content WHERE taxonomy_id = '{$id}'"; 
				$preform_delete_content = $database->query($sql_delete_content); 
			   
				  $delete = $find_author->delete(); 
				  if($delete){ 
						  redirect_to("../view.php"); 
				  }else{ 
						  redirect_to("../view.php"); 
				  }	 
			   
		  }else{ 
			  redirect_to("../view.php");	 
		  } 
	   }else { 
		redirect_to("../view.php"); 
	   }	 
		   
}else{ 
	//if task wasnot delete go back to view 
	redirect_to("../view.php");	 
} 
//close connection 
if(isset($database)){ 
	$database->close_connection(); 
} 
?>
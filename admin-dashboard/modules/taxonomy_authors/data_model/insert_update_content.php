<?php 
require_once('../../../../classes/Session.php'); 
require_once('../../../../classes/Functions.php'); 
require_once('../../../../classes/MysqlDatabase.php'); 
require_once('../../../../classes/TaxonomiesContent.php'); 
//send json data 
header('Content-Type: application/json'); 
if(!empty($_POST["task"]) && $_POST["task"] == "insert_info"){ 
	$taxonomy_id = $_POST["taxonomy_id"]; 
	$lang = $_POST["lang"]; 
	$insert_transaltion = new TaxonomiesContent(); 
	$insert_transaltion->taxonomy_id = $taxonomy_id;	 
	$insert_transaltion->name = $_POST["name"]; 
	$insert_transaltion->alias = $_POST["alias"]; 
	$insert_transaltion->lang_id = $lang; 
	$insert_transaltion->description = $_POST["description"]; 
	$insert_content = $insert_transaltion->insert(); 
	if($insert_content){ 
		$data  = array("status"=>"work"); 
		echo json_encode($data); 
	}else{ 
		$data  = array("status"=>"error"); 
		echo json_encode($data); 
	} 
} 
//close connection 
if(isset($database)){ 
	$database->close_connection(); 
} 
?>
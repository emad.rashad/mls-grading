<?php	 
require_once("../layout/initialize.php"); 
if(isset($_GET['id']) && is_numeric($_GET['id'])){ 
	$record_id = $_GET['id']; 
	$define_class = new Taxonomies(); 
	$define_class->enable_relation(); 
	$record_info = $define_class->taxonomies_data('author',$record_id); 
	//check id access 
	if(empty($record_info->id)){ 
		redirect_to("view.php");	 
	}else{ 
		//define user class to get inserted/created by 
		$define_user_class = new users(); 
		$created_by = $define_user_class->find_by_id($record_info->inserted_by); 
		$updated_by = $define_user_class->find_by_id($record_info->update_by);			 
	} 
}else{ 
	redirect_to("view.php");	 
} 
require_once("../layout/header.php");	 
?> 
<!--header end--> 
<!--sidebar start--> 
<?php require_once("../layout/navigation.php");?> 
<!--sidebar end-->  
<!--main content start--> 
<section id="main-content"> 
  <section class="wrapper site-min-height"> 
    <h4>Taxonomy Authors Module</h4> 
    <div class="row"> 
      <aside class="col-lg-8"> 
        <section> 
          <div class="panel"> 
            <div class="panel-heading"> Author Info</div> 
            <div class="panel-body"> 
              <form class="form-horizontal tasi-form" role="form" id="form_crud"> 
                <input type="hidden" id="process_type" value="insert"> 
                <section class="panel col-lg-9"> 
                  <header class="panel-heading tab-bg-dark-navy-blue "> 
                    <ul class="nav nav-tabs"> 
                      <?php 
                            //create tabs for all available languages  
                            $languages = Localization::find_all('id','desc'); 
                            $serial_tabs = 1; 
                            foreach($languages as $language){ 
                                $lang_tab_header = ucfirst($language->name); 
                                echo "<li class='";if($serial_tabs == 1){ echo " active ";}  echo"'> <a data-toggle='tab' href='#$language->name'> 
                                <strong>$lang_tab_header</strong></a></li>"; 
                                $serial_tabs++; 
                            } 
                          ?> 
                    </ul> 
                  </header> 
                  <div class="panel-body"> 
                    <div class="tab-content"> 
                      <?php 
                            $serial_tabs_content = 1; 
                            foreach($languages as $language){ 
								//get data by language 
								$main_content = $define_class->get_taxonomy_content($record_id, $language->id); 
								echo "<div id='$language->name' class='tab-pane"; if($serial_tabs_content == 1){ echo " active ";} echo"'>"; 
								if($main_content){ 
									echo " 
										<div class='form-group'> 
										<label  class='col-lg-2'>Name:</label> 
										<div class='col-lg-9'>$main_content->name</div> 
									  </div> 
									  <div class='form-group'> 
										<label class='col-lg-2'>Alias:</label> 
										<div class='col-lg-9'>$main_content->alias</div> 
									  </div> 
									  <div class='form-group'> 
										<label class='col-lg-2'>Description:</label> 
										<div class='col-md-9'>$main_content->description</div> 
									  </div>"; 
                            	} 
								echo "</div>"; 
								$serial_tabs_content++; 
							} 
                          ?> 
                    </div> 
                  </div> 
                </section> 
              </form> 
            </div> 
          </div> 
        </section> 
      </aside> 
      <div class="col-lg-4"> 
        <section class="panel panel-primary"> 
          <header class="panel-heading tab-bg-dark-navy-blue"> 
            <ul class="nav nav-tabs"> 
              <li class=" center-block active" ><a data-toggle="tab" href="#op1" class="text-center"> <i class=" icon-check"> </i> <strong> Entry Info</strong> </a> </li> 
              <li class=" center-block " ><a data-toggle="tab" href="#op2" class="text-center"> <i class=" icon-check"> </i> <strong> Publish Option</strong> </a> </li> 
            </ul> 
          </header> 
          <div class="panel-body"> 
            <div id="list_info"> 
              <ul> 
                <div class="tab-content"> 
                  <div id="op1" class="tab-pane active "> 
                    <li><span style="color:#428bca">> Created By:</span> <?php echo $created_by->user_name?></li> 
                    <li><span style="color:#428bca">> Created Date:</span> <?php echo $record_info->inserted_date?></li> 
                    <li><span style="color:#428bca">> Last Update By:</span> 
                      <?php  
						if($record_info->update_by){ 
							echo $record_info->update_by; 
						}else{ 
							echo "--"; 
						} 
					 	?> 
                    </li> 
                    <li><span style="color:#428bca">> Last Update Date:</span> 
                      <?php  
						if($record_info->last_update!=""){ 
							echo $record_info->last_update; 
						}else{ 
							echo "--"; 
						} 
					  ?> 
                    </li> 
                  </div> 
                  <div id="op2" class="tab-pane"> 
                    <li><span style="color:#428bca">> Status:</span> <?php echo $record_info->status?></li> 
                  </div> 
                </div> 
              </ul> 
            </div> 
          </div> 
        </section> 
        <section class="panel panel-primary"> 
          <header class="panel-heading">Included Images</header> 
          <div class="panel-body"> 
            <div class="form-group"> 
              <label  class="col-lg-4">Image Cover</label> 
              <div class="col-lg-8"> <img src="../../../media-library/<?php echo $record_info->cover?>"  style="width:100px; height:100px;"> </div> 
            </div> 
          </div> 
        </section> 
      </div> 
    </div> 
     
    <!-- page end-->  
  </section> 
</section> 
<!--main content end-->  
<!--footer start--> 
<?php require_once("../layout/footer.php");?>
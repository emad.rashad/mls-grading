<?php 
require_once("../layout/initialize.php"); 
if(isset($_GET['id']) && is_numeric($_GET['id'])){ 
	$record_id = $_GET['id']; 
	$define_class = new Taxonomies(); 
	$define_class->enable_relation(); 
	$record_info = $define_class->taxonomies_data('author',$record_id); 
	//check id access 
	if(empty($record_info->id)){ 
		redirect_to("view.php");	 
	} 
	if($user_profile->global_edit != 'all_records' && $record_info->inserted_by != $session->user_id ){ 
	  redirect_to('view.php');	 
	} 
}else{ 
	redirect_to("view.php");	 
} 
require_once("../layout/header.php"); 
include("../../assets/texteditor4/head.php");  
?> 
<script type="text/javascript" src="../../js-crud/taxonomy_authors.js"></script> 
<!--header end--> 
<!--sidebar start--> 
<?php require_once("../layout/navigation.php");?> 
<!--sidebar end-->  
<!--main content start--> 
<section id="main-content"> 
  <section class="wrapper site-min-height"> 
    <h4>Taxonomy Authors Module</h4> 
    <!-- page start--> 
    <div class="row"> 
      <aside class="col-lg-8"> 
        <section> 
          <div class="panel"> 
            <div class="panel-heading"> Edit Author</div> 
            <div class="panel-body"> 
              <form class="form-horizontal tasi-form" role="form"  id="form_crud" action="data_model/update.php"> 
                <input type="hidden" id="process_type" value="update"> 
                <input type="hidden" id="record" value="<?php echo $record_id; ?>"> 
                <section class="panel col-lg-12"> 
                  <header class="panel-heading tab-bg-dark-navy-blue "> 
                    <ul class="nav nav-tabs"> 
                      <?php 
                            //create tabs for all available languages  
                            $languages = Localization::find_all('id','desc'); 
                            $serial_tabs = 1; 
                            foreach($languages as $language){ 
                                $lang_tab_header = ucfirst($language->name); 
                                echo "<li class='";if($serial_tabs == 1){ echo " active ";}  echo"'> <a data-toggle='tab' href='#$language->name'> 
                                <strong>$lang_tab_header</strong></a></li>"; 
                                $serial_tabs++; 
                            } 
                          ?> 
                    </ul> 
                  </header> 
                  <div class="panel-body"> 
                    <div class="tab-content"> 
                      <?php 
                            $serial_tabs_content = 1; 
                            foreach($languages as $language): 
							//get data by lang 
							$main_content = $define_class->get_taxonomy_content($record_id, $language->id); 
								echo " 
								<div id='$language->name' class='tab-pane"; if($serial_tabs_content == 1){ echo " active ";} echo"'>"; 
								?> 
                          <input class='main_content' type='hidden' id='<?php echo "content_id_$language->label"; ?>' 
                                                 value='<?php if(!empty($main_content )){echo $main_content->id;}else{ echo "0";} ?>'> 
                          <div class='form-group'> 
                            <label  class='col-lg-2'>Name:</label> 
                            <div class='col-lg-9'> 
                              <input type='text' class='form-control main_content' id='<?php echo "name_$language->label";?>' autocomplete='off'  
                                value='<?php if(!empty($main_content ))echo $main_content->name; ?>'  
                                onchange="add_char('<?php echo "name_$language->label"?>','<?php echo "alias_$language->label"?>')"> 
                            </div> 
                          </div> 
                          <div class='form-group'> 
                            <label class='col-lg-2'>Alias:</label> 
                            <div class='col-lg-9'> 
                              <input type='text' class='form-control main_content' id='<?php echo "alias_$language->label";?>'  
                              value='<?php if(!empty($main_content ))echo $main_content->alias; ?>'> 
                            </div> 
                          </div> 
                          <div class='form-group'> 
                            <label  class='col-lg-2'>Description:</label> 
                            <div class='col-lg-9'> 
                              <textarea class=' form-control main_content' id='<?php echo "description_$language->label";?>'> 
                               <?php if(!empty($main_content ))echo $main_content->description; ?></textarea> 
                            </div> 
                          </div> 
                      <?php  
							echo "</div>"; 
							$serial_tabs_content++; 
							 endforeach; 
						?> 
                    </div> 
                  </div> 
                </section> 
                <div class="form-group"> 
                  <div class="col-lg-offset-2 col-lg-8"> 
                    <button type="submit" id="submit" class="btn btn-info">Save</button> 
                    <button type="button" class=" btn btn-info "   
                    onClick="window.location.href = 'full_info.php?id='+<?php echo $record_id?>" > <i class="icon-info-sign"></i> View Full Info </button> 
                    <div id="loading_data"></div> 
                  </div> 
                </div> 
              </form> 
            </div> 
          </div> 
        </section> 
      </aside> 
      <div class="col-lg-4"> 
        <section class="panel panel-primary"> 
          <header class="panel-heading">Included Images:</header> 
          <div class="panel-body"> 
            <form class="form-horizontal tasi-form" role="form" id="form_category"> 
              <div class="form-group"> 
                <label  class="col-lg-4">Image Cover:</label> 
                <div class="col-lg-8"> <a href="../file_mangers/media_filemanager/view_media_directories.php?type=authors" id="image_cover">Select Image</a> <br /> 
                  <br /> 
                  <input type="hidden" class="form-control" id="imageVal" value="<?php "authors/small/".$record_info->cover?>"> 
                  <?php if(!empty($record_info->cover)){?> 
                  <div  id="imageShow" > <img src="../../../media-library-thumb/<?php echo "authors/medium/".$record_info->cover;?>" id="imageSrc"  
                       style="width:200px; height:100;"></div> 
                </div> 
              </div> 
              <?php }?> 
            </form> 
          </div> 
        </section> 
      </div> 
    </div> 
     
    <!-- page end-->  
  </section> 
</section> 
<!--main content end-->  
<!--footer start--> 
<?php require_once("../layout/footer.php");?>
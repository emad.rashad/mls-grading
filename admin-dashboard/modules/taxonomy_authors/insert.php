<?php  
	require_once("../layout/initialize.php"); 
	require_once("../layout/header.php"); 
?> 
<script type="text/javascript" src="../../js-crud/taxonomy_authors.js"></script> 
<!--header end--> 
<!--sidebar start--> 
<?php require_once("../layout/navigation.php");?> 
<!--sidebar end-->  
<!--main content start--> 
<?php include("../../assets/texteditor4/head.php"); ?> 
<section id="main-content"> 
<section class="wrapper site-min-height"> 
<h4>Authors Module</h4> 
<div class="row"> 
<aside class="col-lg-8"> 
  <section> 
    <div class="panel"> 
      <div class="panel-heading"> Add Author</div> 
      <div class="panel-body"> 
        <form class="form-horizontal tasi-form" role="form" action="data_model/insert.php" id="form_crud"> 
          <input type="hidden" id="process_type" value="insert"> 
          <section class="panel col-lg-12"> 
            <header class="panel-heading tab-bg-dark-navy-blue "> 
              <ul class="nav nav-tabs"> 
                <?php 
				//create tabs for all available languages  
				$languages = Localization::find_all('id','desc'); 
				$serial_tabs = 1; 
				foreach($languages as $language){ 
					$lang_tab_header = ucfirst($language->name); 
					echo "<li class='";if($serial_tabs == 1){ echo " active ";}  echo"'> <a data-toggle='tab' href='#$language->name'> 
					<strong>$lang_tab_header</strong></a></li>"; 
					$serial_tabs++; 
				} 
			  ?> 
              </ul> 
            </header> 
            <div class="panel-body"> 
              <div class="tab-content"> 
                <?php 
				$serial_tabs_content = 1; 
				foreach($languages as $language){ 
						echo " 
						<div id='$language->name' class='tab-pane"; if($serial_tabs_content == 1){ echo " active ";} echo"'> 
								<div class='form-group'> 
								<label  class='col-lg-2'>Name:</label> 
								<div class='col-lg-9'> 
								  <input type='text' class='form-control main_content' id='name_$language->label'  autocomplete='off'  
								  onchange=\"add_char('name_$language->label','alias_$language->label')\"> 
								</div> 
							  </div> 
							  <div class='form-group'> 
								<label class='col-lg-2'>Alias:</label> 
								<div class='col-lg-9'> 
								  <input type='text' class='form-control main_content' id='alias_$language->label'> 
								</div> 
							  </div> 
							  <div class='form-group'> 
								<label class='col-lg-2'>Description:</label> 
								<div class='col-md-9'> 
								  <textarea class='form-control main_content' id='description_$language->label'></textarea> 
								</div> 
							  </div> 
						</div>"; 
					$serial_tabs_content++; 
				} 
                ?> 
              </div> 
            </div> 
          </section> 
          <div class="form-group"> 
            <div class="col-lg-offset-2 col-lg-6"> 
              <button type="submit" id="submit" class="btn btn-info">Save</button> 
              <button type="submit" class="btn btn-default">Cancel</button> 
              <div id="loading_data"></div> 
            </div> 
          </div> 
        </form> 
      </div> 
    </div> 
  </section> 
</aside> 
<div class="col-lg-4"> 
  <section class="panel panel-primary"> 
    <header class="panel-heading"> Publish Options </header> 
    <div class="panel-body"> 
      <form class="form-horizontal tasi-form" role="form"> 
        <div class="form-group"> 
          <label class="col-lg-5">Status</label> 
          <div class="col-lg-6"> 
            <label class="checkbox-inline"> 
              <input type="radio" name="shadow" class="radio" value="draft"> 
              Draft</label> 
            <label class="checkbox-inline"> 
              <input type="radio" name="shadow" class="radio" value="publish" checked> 
              Publish</label> 
          </div> 
        </div> 
      </form> 
    </div> 
  </section> 
</div> 
<div class="col-lg-4"> 
  <section class="panel panel-primary"> 
    <header class="panel-heading">Cover Image: </header> 
    <div class="panel-body"> 
      <form class="form-horizontal tasi-form" role="form" id="form_img"> 
        <div class="form-group"> 
          <label  class="col-lg-4">Image Cover:</label> 
          <div class="col-lg-8"> <a href="../file_mangers/media_filemanager/view_media_directories.php?type=authors" id="image_cover">Select Image</a> <br /> 
            <input type="hidden" class="form-control" id="imageVal" placeholder=" " autocomplete="off"> 
            <div style="display:none" id="imageShow"> <img src="" id="imageSrc" style="width:100px; height:100px;"></div> 
          </div> 
        </div> 
      </form> 
    </div> 
  </section> 
</div> 
<!-- page end--> 
</section> 
</section> 
<!--main content end-->  
<!--footer start--> 
<?php require_once("../layout/footer.php");?> 

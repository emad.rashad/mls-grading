<?php
require_once('../../../../classes/Session.php');
require_once('../../../../classes/Functions.php');
require_once('../../../../classes/MysqlDatabase.php');
require_once('../../../../classes/Advertisements.php');
require_once('../../../../classes/Users.php');
require_once('../../../../classes/Profile.php');
require_once('../../../../classes/AdvertisementContent.php');
//check log in 
if($session->is_logged() == false){
	redirect_to("../../index.php");
}
//send json data
header('Content-Type: application/json');
// user log in profile details to chech authority
// get user profile  
$user_data = Users::find_by_id($session->user_id);
// get user profile data
$user_profile  = Profile::Find_by_id($user_data->user_profile);
//send notifiction by json 
header('Content-Type: application/json');
if(!empty($_POST["task"]) && $_POST["task"] == "update"){
	//get data
	$id = $_POST['record'];
	$edit = Advertisements::find_by_id($id);
	 if($user_profile->globel_edit != 'all_records' && $edit->inserted_by != $session->user_id  ){	
		   redirect_to("../view.php");
	 }else{
			//validite required required
		  $required_fields = array('title_one'=>"- Insert Title");
		  $check_required_fields = check_required_fields($required_fields);
		  if(count($check_required_fields) == 0){
				//update
				 $edit->page_id = $_POST['page_id'];
				  $old_image = $edit->image_cover; 
				  $new_image = $_POST['imageVal'];
				  if($old_image != $new_image){
					 if(!empty($_POST['imageVal'])){
					  $current_file = $_POST['imageVal'];
					  $parts = explode('/',$current_file);
					  $image_cover = $parts[count($parts)-1];
					  $edit->image_cover = $image_cover;
				   }
		          }else{
					     $edit->image_cover = $_POST['imageVal'];
					   }
				
				$update = $edit->update();
				if($update){
					$update_content = AdvertisementContent::find_by_custom_filed('adv_id',$id);
				     $update_content->title_one = $_POST["title_one"];
				     $update_content->title_two = $_POST["title_two"];
					 $edit_content = $update_content->update();
					$data  = array("status"=>"work");
					echo json_encode($data);
				}else{
					$data  = array("status"=>"error");
					echo json_encode($data);
				}
		  }else{
				//validation erro
				$comma_separated = implode("<br>", $check_required_fields);
				$data  = array("status"=>"valid_error", "fileds"=>$comma_separated);
				echo json_encode($data);
			}	
		}
  }
//close connection
if(isset($database)){
	$database->close_connection();
}

?>
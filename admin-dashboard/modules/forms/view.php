<?php
	require_once("../layout/initialize.php");
	$define_class= new Forms();	
	$records = $define_class->form_data('inserted_date', 'DESC');
	//get lang 
	$languages = Localization::find_all();
	require_once("../layout/header.php");
?>
<!--header end-->
<!--sidebar start-->

<?php require_once("../layout/navigation.php");?>

<!--sidebar end--> 
<!--main content start-->
<section id="main-content">
  <section class="wrapper site-min-height">
    <h4>Forms</h4>
    <section class="panel">
      <header class="panel-heading"> View Forms </header>
      <div class="panel-body">
        <div class="adv-table editable-table ">
          <button type="button" class="btn btn-danger" style="margin-left:15px" onClick="window.location.href = 'insert.php'" 
                            <?php
							  $module_name = $opened_url_parts[count($opened_url_parts) - 2];
							  $opened_module_page_insert = $module_name.'/insert';
							  if(!in_array($opened_module_page_insert, $user_allowed_page_array)){
								echo "disabled";
							  }
							  ?>>
          <li class="icon-plus-sign"></li>
          Add New Form</button>
          <br/>
          <br/>
          <table class="table table-striped table-hover table-bordered" id="editable-sample">
            <thead>
              <tr>
                <th>#</th>
                <th>Name</th>
                <th>Label</th>
                <th>Created Date</th>
                <th>Created by</th>
                <th>Translate Labes</th>
                <th>Attributes Actions</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php 
				   $serialize=1;
				   foreach($records as $record){
					 echo "<tr>
					  <td>{$serialize}</td>
					  <td> <a href='full_info.php?id={$record->id}'>{$record->name}</a></td>
					  <td>{$record->label}</td>
					  <td>{$record->inserted_date}</td>
					  <td>{$record->inserted_by}</td>
					 <td>";
							  foreach($languages as $language){
							  echo "<a href='insert_content.php?id={$record->id}&lang={$language->label}'>" 
							  .mb_strtoupper($language->label)."</a> &nbsp;";
							  }
				  echo "</td>
						<td>";
						$opened_module_view_attributes = 'form_attributes/view';
						$opened_module_add_attributes = 'form_attributes/insert';
						$opened_module_view_inserted_data = 'form_attributes/inserted_data';
						//full info
						if(!in_array($opened_module_view_attributes, $user_allowed_page_array)){
						echo "<a href=''  class='btn btn-success btn-xs tooltips' 
						data-placement='top' data-toggle='tooltip' data-original-title='View attributes ' disabled> <i class='icon-list-ul'></i></a>";	
						}else{
							echo " <a href='../form_attributes/view.php?form_id={$record->id}'  class='btn btn-success btn-xs tooltips' 
							data-placement='top' data-toggle='tooltip' data-original-title='View attributes' > <i class='icon-list-ul'></i> </a> ";	
						}
						if(!in_array($opened_module_add_attributes, $user_allowed_page_array)){
							echo "<a href='' class='btn btn-success btn-xs tooltips' 
							data-placement='top' data-toggle='tooltip' data-original-title='Add attributes' disabled> <i class=' icon-plus'></i></a>
							&nbsp;";	
						}else{
							echo "<a href='../form_attributes/insert.php?form_id={$record->id}' class='btn btn-success btn-xs tooltips' 
							data-placement='top' data-toggle='tooltip' data-original-title='Add attributes' > <i class=' icon-plus'></i></a>&nbsp;";	
						}
						if(!in_array($opened_module_view_inserted_data, $user_allowed_page_array)){
						echo "<a href=''  class='btn btn-success btn-xs tooltips' 
						data-placement='top' data-toggle='tooltip' data-original-title='View attributes ' disabled> <i class='icon-th'></i></a>";	
						}else{
							echo " <a href='../form_attributes/inserted_data.php?form_id={$record->id}'  class='btn btn-success btn-xs tooltips' 
							data-placement='top' data-toggle='tooltip' data-original-title='View inserted data' > <i class='icon-th'></i> </a> ";	
						}
						echo "</td>						  
						<td>";
						$module_name = $opened_url_parts[count($opened_url_parts) - 2];
						include('../layout/btn_control.php');
				echo "</td>		
				  </tr>";   //delete dialoge 
				  echo   "<div class='modal fade' id='my{$record->id}' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'>
					  <div class='modal-dialog'>
						  <div class='modal-content'>
							  <div class='modal-header'>
								  <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
								  <h4 class='modal-title'>Delete</h4>
							  </div>
							  <div class='modal-body'>

							   <p> Are you sure you want delete $record->name??</p>

							  </div>
							  <div class='modal-footer'>
								  <button class='btn btn-warning' type='button' 
								  onClick=\"window.location.href = 'data_model/delete.php?task=delete&id={$record->id}'\"/> Confirm</button>  
								  <button data-dismiss='modal' class='btn btn-default' type='button'>cancle</button>
							  </div>
						  </div>
					  </div>
				  </div>";
			   $serialize++;
             }?>
            </tbody>
          </table>
        </div>
      </div>
    </section>
    <!-- page end--> 
  </section>
</section>
<!--main content end--> 
<!--footer start-->
<?php require_once("../layout/footer.php");?>

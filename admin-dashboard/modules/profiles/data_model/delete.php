<?php 
require_once('../../../../classes/Session.php'); 
require_once('../../../../classes/Functions.php'); 
require_once('../../../../classes/MysqlDatabase.php'); 
require_once('../../../../classes/Users.php'); 
require_once('../../../../classes/Profile.php'); 
//check  session user  log in 
if($session->is_logged() == false){ 
	redirect_to("../../../index.php"); 
} 
// get user profile   
$user_data = Users::find_by_id($session->user_id); 
// get user profile data 
$user_profile  = Profile::Find_by_id($user_data->user_profile); 
// check if the user profile block 
if($user_profile->profile_block == "yes"){ 
   redirect_to("../../../index.php");	 
} 
if(!empty($_GET["task"]) && $_GET["task"] == "delete"){ 
	//get data 
	$id = $_GET['id']; 
	//find record	 
	$find_profile = Profile::find_by_id($id); 
	//check global  delete authorization 
	if($user_profile->global_delete == 'all_records' || $find_event->inserted_by == $session->user_id){ 
		  //if there is record perform delete 
		  //if there is no record go back to view 
		  if($find_profile){ 
			  //check this author have no realtion with any post 
			  $check_relation = Users::find_all_by_custom_filed('user_profile', $id); 
			  if(count($check_relation) != 0){ 
					  redirect_to("../view.php?post=relation"); 
			  }else{ 
				  $delete = $find_profile->delete(); 
				  if($delete){ 
					  //delete all realted  modules and pages 
				  //delete modules 
					  $sql_delete_modules= "DELETE FROM profile_modules_access  WHERE profile_id = '{$id}'"; 
					  $preform_delete_modules = $database->query($sql_delete_modules); 
					  //delete pages 
					  $sql_delete_pages = "DELETE FROM profile_pages_access  WHERE profile_id = '{$id}'"; 
					  $preform_delete_pages = $database->query($sql_delete_pages); 
					   
						  redirect_to("../view.php"); 
				  }else{ 
						  redirect_to("../view.php"); 
				  }	 
			  } 
		  }else{ 
					redirect_to("../view.php");	 
		  } 	 
	}else{ 
		redirect_to("../view.php");	 
	} 
}else{ 
	//if task wasnot delete go back to view 
	redirect_to("../view.php");	 
} 
//close connection 
if(isset($database)){ 
	$database->close_connection(); 
} 
?>
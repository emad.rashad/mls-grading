<?php  
	require_once("../layout/initialize.php"); 
	$define_profile = new Profile(); 
	$define_profile->enable_relation(); 
	$records = $define_profile->profile_data('title','ASC'); 
	require_once("../layout/header.php"); 
?> 
<script type="text/javascript" src="../../js-crud/crud_profile.js"></script> 
  <!--header end-->  
  <!--sidebar start--> 
  <?php require_once("../layout/navigation.php");?> 
  <!--sidebar end-->  
  <!--main content start--> 
  <section id="main-content"> 
    <section class="wrapper site-min-height"> 
      <h4>Profile Module</h4>         
       <!-- page start--> 
      <div class="row"> 
        <div class="col-lg-12" > 
          <section class="panel"> 
            <header class="panel-heading">Show All Profile </header> 
             <div class="space15"> 
			 <?php 
				if(isset($_GET["post"]) && $_GET["post"]=="relation"){ 
					echo "<p style='text-align:right; font-size:16px; color:red'>Can't delete profile related to user<p>"; 
				} 
			  ?></div> 
                          <br> 
              <button type="button" class="btn btn-danger" style="margin-left:15px" onClick="window.location.href = 'insert.php'"  
			  <?php 
			  $module_name = $opened_url_parts[count($opened_url_parts) - 2]; 
			  $opened_module_page_insert = $module_name.'/insert'; 
			  if(!in_array($opened_module_page_insert, $user_allowed_page_array)){ 
				echo "disabled"; 
			  } 
			  ?>> 
               <li class="icon-plus-sign"></li> Add New Profile</button> 
             <br> 
             <br> 
            <table class="table table-striped table-advance table-hover"> 
              <thead> 
                <tr> 
                  <th>#</th> 
                  <th>Name</th> 
                  <th>Action</th> 
                </tr> 
              </thead> 
              <tbody> 
                <?php   
					  $serialize = 1; 
					  foreach($records as $record){ 
						  echo "<tr> 
						  <td>{$serialize}</td> 
						  <td>{$record->title}</td> 
						  <td>"; 
						  	 $opened_module_profile_page_access= 'profile_pages/update'; 
							 if(!in_array($opened_module_profile_page_access, $user_allowed_page_array)){ 
									echo "<a href='' data-toggle='modal' class='btn btn-success btn-xs tooltips' data-placement='top' data-original-title='Edit 
									 Pages Access' disabled style='margin-left:25px' ><i class='icon-file-text-alt'></i></a>";								  
							 }else{ 
								 if($user_profile->global_edit == 'all_records' || $record->inserted_by == $user_data->id){ 
									echo "<a href='../profile_pages/update.php?id={$record->id}' data-toggle='modal' class='btn btn-success btn-xs tooltips'  
									data-placement='top' data-original-title='Edit Pages Access' ><i class='icon-file-text-alt'></i></a>"; 
								 }else { 
				echo "<a href='' data-toggle='modal' class='btn btn-success btn-xs tooltips' data-placement='top' data-original-title='Edit 
									 Pages Access' disabled style='margin-left:25px' ><i class='icon-file-text-alt'></i></a>"; 
								 }								  
							 } 
							$module_name = $opened_url_parts[count($opened_url_parts) - 2]; 
							include('../layout/btn_control.php'); 
							//delete dialoge 
						  echo "</td> 
					   </tr> 
					  <div class='modal fade' id='my{$record->id}' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'> 
										  <div class='modal-dialog'> 
											  <div class='modal-content'> 
												  <div class='modal-header'> 
													  <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button> 
													  <h4 class='modal-title'>Delete</h4> 
												  </div> 
												  <div class='modal-body'> 
		 
												   <p> Are you sure you want delete  $record->title??</p> 
		 
												  </div> 
												  <div class='modal-footer'> 
													  <button class='btn btn-warning' type='button'  
													  onClick=\"window.location.href = 'data_model/delete.php?task=delete&id={$record->id}'\"/> Confirm</button>  <button data-dismiss='modal' class='btn btn-default' type='button'>cancle</button> 
												  </div> 
											  </div> 
										  </div> 
									  </div>"; 
					   $serialize++; 
					 }?> 
              </tbody> 
            </table> 
          </section> 
      
        </div> 
      </div> 
      <!-- page end-->  
    </section> 
  </section> 
  <!--main content end-->  
  <!--footer start--> 
  <?php require_once("../layout/footer.php");?>
<?php 
require_once("../layout/initialize.php"); 
if(isset($_GET['group_id']) && is_numeric($_GET['group_id'])){ 
	$define_class = new MenuLink(); 
	$define_class->enable_relation(); 
	$group_id = $_GET["group_id"]; 
	$lang_id = $general_setting_info->translate_lang_id; 
	$menu_data = MenuGroup::find_by_id($group_id);	 
	if(empty($menu_data->id)){ 
		redirect_to('view.php'); 
	} 
}else{ 
	redirect_to('view.php');	 
} 
require_once("../layout/header.php"); 
include("../../assets/texteditor4/head.php");  
?> 
<script type="text/javascript" src="../../js-crud/menu_links.js"></script> 
<!--header end--> 
<!--sidebar start--> 
<?php require_once("../layout/navigation.php");?> 
<!--sidebar end-->  
<!--main content start--> 
<section id="main-content"> 
  <section class="wrapper site-min-height"> 
    <h4>Menu link Module </h4> 
    <div class="row"> 
      <aside class="col-lg-8"> 
        <section> 
          <div class="panel"> 
            <div class="panel-heading"> Add Menu link to <?php echo '"'.$menu_data->title.'"' ?> </div> 
            <div class="panel-body"> 
              <form class="form-horizontal tasi-form" role="form" id="form_crud" action="data_model/insert.php"> 
                <input type="hidden" id="process_type" value="insert"> 
                <input type="hidden" id="group_id" value="<?php echo $group_id;?>" /> 
                 <input type="hidden" id="lang_id" value="<?php echo $lang_id;?>" /> 
                <section class="panel"> 
                  <header class="panel-heading tab-bg-dark-navy-blue"> 
                    <ul class="nav nav-tabs"> 
                      <li class=" center-block active" > <a data-toggle="tab" href="#main_info" class="text-center"><strong>Main Info</strong></a></li> 
                      <li class=" center-block"> <a data-toggle="tab" href="#link_option" class="text-center"><strong>Link Option</strong></a></li> 
                    </ul> 
                  </header> 
                  <div class="panel-body"> 
                    <div class="tab-content"> 
                      <div id="main_info" class="tab-pane active "> 
                        <section class="panel col-lg-9"> 
                          <header class="panel-heading tab-bg-dark-navy-blue "> 
                            <ul class="nav nav-tabs"> 
                              <?php 
                            //create tabs for all available languages  
                            $languages = Localization::find_all('id','desc'); 
                            $serial_tabs = 1; 
                            foreach($languages as $language){ 
                                $lang_tab_header = ucfirst($language->name); 
                                echo "<li class='";if($serial_tabs == 1){ echo " active ";}  echo"'> <a data-toggle='tab' href='#$language->name'> 
                                <strong>$lang_tab_header</strong></a></li>"; 
                                $serial_tabs++; 
                            } 
                          ?> 
                            </ul> 
                          </header> 
                          <div class="panel-body"> 
                            <div class="tab-content"> 
                              <?php 
                            $serial_tabs_content = 1; 
                            foreach($languages as $language){ 
                                    echo " 
                                    <div id='$language->name' class='tab-pane"; if($serial_tabs_content == 1){ echo " active ";} echo"'> 
                                            <div class='form-group'> 
                                            <label  class='col-lg-2'>Title:</label> 
                                            <div class='col-lg-9'> 
                                              <input type='text' class='form-control main_content' id='title_$language->label' autocomplete='off'> 
                                            </div> 
                                          </div> 
                                          <div class='form-group'> 
                                            <label class='col-lg-2'>Description:</label> 
                                            <div class='col-md-9'> 
                                              <textarea class='form-control main_content' id='description_$language->label'></textarea> 
                                            </div> 
                                          </div> 
                                    </div>"; 
                                $serial_tabs_content++; 
                            } 
                          ?> 
                            </div> 
                          </div> 
                        </section> 
                      </div> 
                      <div id="link_option" class="tab-pane "> 
                        <div class="form-group"> 
                          <label  class="col-lg-2">Sorting:</label> 
                          <div class="col-lg-8"> 
                            <input type="text" class="form-control" id="sorting" placeholder=" " autocomplete="off" style="width:50px;"/> 
                          </div> 
                        </div> 
                        <div class="form-group"> 
                          <label  class="col-lg-2 ">Parent Link:</label> 
                          <div class="col-lg-8"> 
                            <select  class="form-control" id="sid"> 
                              <option   value="0">Root</option> 
                              <?php $define_class->getMenu(0,0,$group_id,$lang_id);?> 
                            </select> 
                          </div> 
                        </div> 
                        <div class="form-group"> 
                          <label  class="col-lg-2 "> Path Type:</label> 
                          <div class="col-lg-8"> 
                            <select class="form-control" id="path_type"> 
                              <option value=""> Select Type </option> 
                              <option value="page">Page</option> 
                              <option value="post">Post</option> 
                              <option value="event">Event</option> 
                              <option value="external">External</option> 
                              <option value="category">Category</option> 
                            </select> 
                          </div> 
                        </div> 
                        <div class="form-group"  > 
                          <label  class="col-lg-2 "> Path:</label> 
                          <div class="col-lg-8" id="select_div"> 
                            <input type="text" class="form-control hide " id="external_path" placeholder=" "/> 
                            <select class="form-control" id="path"> 
                              <option value=""> Select Node </option> 
                            </select> 
                          </div> 
                          <div id="node_loading_data"></div> 
                        </div> 
                      </div> 
                    </div> 
                  </div> 
                </section> 
                <div class="form-group"> 
                  <div class="col-lg-offset-2 col-lg-10"> 
                    <button type="submit" class="btn btn-info" id="submit">Save</button> 
                    <button type="submit" class="btn btn-default">Cancel</button> 
                    <button type="button" class="btn btn-info"  
                    onClick="window.location.href = 'view.php?group_id='+<?php echo $group_id?>" >View Links </button> 
                    <div id="loading_data"></div> 
                  </div> 
                </div> 
              </form> 
            </div> 
          </div> 
        </section> 
      </aside> 
      <div class="col-lg-4"> 
        <section class="panel panel-primary"> 
          <header class="panel-heading"> Publish Options: </header> 
          <div class="panel-body"> 
            <form class="form-horizontal tasi-form" role="form" id="form_option"> 
              <div class="form-group"> 
                <label class="col-lg-5">Status:</label> 
                <div class="col-lg-6"> 
                  <label class="checkbox-inline"> 
                    <input type="radio" name="shadow" class="radio" value="draft"> 
                    Draft</label> 
                  <label class="checkbox-inline"> 
                    <input type="radio" name="shadow" class="radio" value="publish" checked> 
                    Publish</label> 
                </div> 
              </div> 
              <div class="form-group  hide" id="drop_down"> 
                <label class="col-lg-5">Drop Down:</label> 
                <div class="col-lg-6"> 
                  <label class="checkbox-inline"> 
                    <input type="radio" name="drop_dwon" class="radio" value="yes"> 
                    Yes</label> 
                  <label class="checkbox-inline"> 
                    <input type="radio" name="drop_dwon" class="radio" value="no" checked> 
                    No</label> 
                </div> 
              </div> 
              <div class="form-group hide" id="op_div"> 
                  <label class="col-lg-5">Drop Down Style:</label> 
                  <div class="col-lg-10" style="padding-left:160px;"> 
                  <div class="radio"> 
                      <label> 
                        <input type="radio" name="drop_dwon_style"  value="0" checked>No Design</label> 
                    </div> 
                    <div class="radio"> 
                      <label> 
                        <input type="radio" name="drop_dwon_style" value="op1" >Option 1</label> 
                    </div> 
                    <div class="radio"> 
                      <label> 
                        <input type="radio" name="drop_dwon_style" value="op2">Option 2</label> 
                    </div> 
                    <div class="radio"> 
                      <label> 
                        <input type="radio" name="drop_dwon_style"  value="op3">Option 3</label> 
                    </div>                     
                  </div> 
                </div> 
            </form> 
          </div> 
        </section> 
      </div> 
      <div class="col-lg-4"> 
        <section class="panel panel-primary"> 
          <header class="panel-heading"> Included Images & Icons :</header> 
          <div class="panel-body"> 
            <form class="form-horizontal tasi-form" role="form" id="form_image"> 
              <div class="form-group"> 
                <label  class="col-lg-2 ">Icon:</label> 
                <div class="col-lg-8"> 
                  <input type="text" class="form-control" id="icon" placeholder=" " autocomplete="off"> 
                </div> 
              </div> 
              <div class="form-group"> 
                <label  class="col-lg-3 ">Image Cover:</label> 
                <div class="col-lg-7"> <a href="../file_mangers/media_filemanager/view_media_directories.php" id="image_cover">Select Image</a> <br /> 
                  <br /> 
                  <input type="hidden" class="form-control" id="imageVal" placeholder=" " autocomplete="off"> 
                  <div style="display:none" id="imageShow"> <img src="" id="imageSrc" style="width:80px; height:80px;"></div> 
                </div> 
              </div> 
            </form> 
          </div> 
        </section> 
      </div> 
      <div class="col-lg-4"> 
        <section class="panel panel-primary"> 
          <header class="panel-heading">Menu Links</header> 
          <div class="panel-body" style="max-height: 276px;overflow-y: auto;"> 
            <?php  $define_class->view_inserted_menu_links(0,$group_id,$lang_id);?> 
          </div> 
        </section> 
      </div> 
    </div> 
     
    <!-- page end-->  
  </section> 
</section> 
<!--main content end-->  
<!--footer start--> 
<?php require_once("../layout/footer.php");?> 
<script src="../../js-crud/load_nodes.js"></script>
<?php  
	require_once("../layout/initialize.php"); 
	$record_id = $_GET['id']; 
	$define_class = new Users(); 
	$define_class->enable_relation(); 
	$record_info = $define_class->user_data(null,null, $record_id); 
	require_once("../layout/header.php"); 
	 
?> 
<script type="text/javascript" src="../../js-crud/crud_user.js"></script> 
  <!--header end-->  
  <!--sidebar start--> 
  <?php require_once("../layout/navigation.php");?> 
  <!--sidebar end-->  
  <!--main content start--> 
  <section id="main-content"> 
    <section class="wrapper site-min-height"> 
      <h4>User Module</h4>    		   
      <!-- page start--> 
      <div class="row"> 
        <aside class="col-lg-8"> 
          <section> 
            <div class="panel"> 
              <div class="panel-heading"> User Info</div> 
              <div class="panel-body"> 
                <form class="form-horizontal tasi-form" role="form" id="form_crud" action="data_model/insert.php"> 
                <input type="hidden" id="process_type" value="insert"> 
                  <div class="form-group"> 
                    <label  class="col-lg-2">User Name:</label> 
                    <div class="col-lg-6"> 
                    <?php echo $record_info->user_name?> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                    <label  class="col-lg-2">First Name:</label> 
                    <div class="col-lg-6"> 
                    <?php echo $record_info->first_name?> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                    <label  class="col-lg-2">Last Name:</label> 
                    <div class="col-lg-6"> 
                    <?php echo $record_info->last_name?> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                    <label  class="col-lg-2">Email:</label> 
                    <div class="col-lg-6"> 
                    <?php echo $record_info->email?> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                  <div class="col-lg-offset-2 col-lg-10"> 
                    <button type="button" class="btn btn-info"   
                        onClick="window.location.href = 'update.php?id='+<?php echo $record_id?>" > <li class="icon-edit-sign"></li> Update </button> 
                        <a  id="show_inserted_data" href="show_inserted_data.php?id=<?php echo $record_id;?>"  style="margin-right:15px;"   
       class="btn btn-default  btn-info"> <i class="icon-link"></i>&nbsp;&nbsp;Show Related Data </a> 
                        </div> 
                  </div> 
                </form> 
              </div> 
            </div> 
          </section> 
        </aside> 
        <div class="col-lg-4"> 
        <section class="panel panel-primary"> 
            <header class="panel-heading"> Rules Information: </header> 
            <div class="panel-body"> 
                <div id="list_info"> 
                <ul> 
                   <li><span style="color:#428bca">> Profile Name:</span> <a id="profile_info" href="profile_info.php?id=<?php echo $record_info->user_profile_id?>"><?php echo $record_info->user_profile?></a></li> 
                   
                </ul> 
              </div> 
              </div> 
          </section> 
          <section class="panel panel-primary"> 
            <header class="panel-heading"> Entry Information: </header> 
            <div class="panel-body"> 
              <div id="list_info"> 
                <ul> 
                  <li><span style="color:#428bca">> Created By:</span> <?php echo $record_info->inserted_by?></li> 
                  <li><span style="color:#428bca">> Created Date:</span> <?php echo $record_info->inserted_date?></li> 
                  <li><span style="color:#428bca">> Last Update By:</span> 
                    <?php if($record_info->update_by!=""){ 
					  echo $record_info->update_by; 
				      }else{ 
						  echo "--";} 
					  ?> 
                  </li> 
                  <li><span style="color:#428bca">> Last Update Date:</span> 
                    <?php if($record_info->last_update!="0000-00-00 00:00:00"){ 
					  echo $record_info->last_update; 
				      }else{ 
						  echo "--";} 
					  ?> 
                  </li> 
                </ul> 
              </div> 
            </div> 
          </section> 
           
        </div> 
      </div> 
       
      <!-- page end-->  
    </section> 
  </section> 
  <!--main content end-->  
  <!--footer start--> 
  <?php require_once("../layout/footer.php");?> 
   
	 
   
<?php 
require_once("../../../classes/GeneralSettings.php"); 
//get main lnaguage info 
$define_general_setting = new GeneralSettings(); 
$define_general_setting->enable_relation(); 
$general_setting_info = $define_general_setting->general_settings_data(); 
$main_lang_id = $general_setting_info->translate_lang_id;


?> 
<!DOCTYPE html> 
<html lang="en"><head> 
	<meta charset="UTF-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
    <link rel="shortcut icon" href="../../img/favicon.png"> 
    <title>Diva-CMS</title>     
     <link href="../../css/bootstrap.min.css" rel="stylesheet"> 
    <link href="../../css/bootstrap-reset.css" rel="stylesheet"> 
    <!--external css--> 
    <link href="../../assets/font-awesome/css/font-awesome.css" rel="stylesheet" /> 
	<!--table css--> 
    <link rel="stylesheet" href="../../assets/data-tables/DT_bootstrap.css" /> 
    <link rel="stylesheet" type="text/css" href="../../js/jquery.datetimepicker.css"/> 
    <!-- Custom styles for this template --> 
    <link href="../../css/style.css" rel="stylesheet"> 
     <link type="text/css" href="../../assets/selectmultiple_drag_drop/css/jquery-ui.css" rel="stylesheet" /> 
    <link href="../../css/style-responsive.css" rel="stylesheet" /> 
    <link href="../../assets/dropzone/css/dropzone.css" rel="stylesheet"/> 
    
   <script src="../../js/jquery-1.8.3.min.js"></script>  
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries --> 
    <!--[if lt IE 9]> 
      <script src="js/html5shiv.js"></script> 
      <script src="js/respond.min.js"></script> 
    <![endif]--> 
  </head> 
<body> 

<section id="container" class=""> 
<script> 
//for alias 
//replace spaces with _ underscore 
function add_char(field1,field2) { 
	var field1 = document.getElementById(field1).value; 
	var field_lowercase = field1.toLowerCase(); 
	document.getElementById(field2).value = field_lowercase.replace(/ |!|@|#|\$|%|\^|\&|\*|\"|\'|\’|\:|\?|\)|\)/g, "_"); 
} 

</script> 
<!--header start--> 

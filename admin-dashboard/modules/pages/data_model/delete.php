<?php 
require_once('../../../../classes/Session.php'); 
require_once('../../../../classes/Functions.php'); 
require_once('../../../../classes/MysqlDatabase.php'); 
require_once('../../../../classes/Nodes.php'); 
require_once('../../../../classes/Users.php'); 
require_once('../../../../classes/Profile.php'); 
//check  session user  log in 
if($session->is_logged() == false){ 
	redirect_to("../../../index.php"); 
} 
// get user profile   
$user_data = Users::find_by_id($session->user_id); 
// get user profile data 
$user_profile  = Profile::Find_by_id($user_data->user_profile); 
// check if the user profile block 
if($user_profile->profile_block == "yes"){ 
   redirect_to("../../../index.php");	 
} 
if(!empty($_GET["task"]) && $_GET["task"] == "delete"){ 
	//send json
	header('Content-Type: application/json');
    //get data 
	$id = $_GET['id']; 
	//find record	 
	$find_page= Nodes::find_by_id($id); 
	//check global  delete authorization 
   if($user_profile->global_delete == 'all_records' || $find_page->inserted_by == $session->user_id){ 
		//if there is record perform delete 
		//if there is no record go back to view 
		if($find_page){ 
			$delete = $find_page->delete(); 
			if($delete){ 
				//delete all realted  Pages 
				//delete  Pages categories 
				$sql_delete_categories = "DELETE FROM nodes_selected_taxonomies WHERE node_id = '{$id}' AND node_type ='page'"; 
				$preform_delete_categories = $database->query($sql_delete_categories); 
				//delete page content 
				$sql_delete_info = "DELETE FROM nodes_content WHERE node_id = '{$id}' "; 
				$preform_delete_info = $database->query($sql_delete_info); 
				//delete from node plugins value 
				$sql_delete_node_plugins_values = "DELETE FROM nodes_plugins_values WHERE node_id = '{$id}' AND type = 'page'"; 
				$preform_delete_node_plugins_values = $database->query($sql_delete_node_plugins_values);										 
				 $data = array("status"=>"work");
				echo json_encode($data);
			}else{ 
				redirect_to("../view.php"); 
			}	 
			//if there is no record go back to view 
		}else{ 
			redirect_to("../view.php");	 
		}  
	}else { 
		  redirect_to("../view.php"); 
   }	 
}else{ 
	//if task wasnot delete go back to view 
				$data = array("status"=>"failed");
				echo json_encode($data); 
} 
//close connection 
if(isset($database)){ 
	$database->close_connection(); 
} 
?>
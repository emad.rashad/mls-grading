<?php 

require_once('../../../../classes/Session.php'); 

require_once('../../../../classes/Functions.php'); 

require_once('../../../../classes/MysqlDatabase.php'); 

require_once('../../../../classes/Users.php'); 

require_once('../../../../classes/Profile.php'); 

require_once('../../../../classes/Nodes.php'); 

require_once('../../../../classes/NodesContent.php'); 

require_once('../../../../classes/NodesSelectedTaxonomies.php'); 

require_once('../../../../classes/Localization.php'); 

require_once('../../../../classes/NodesImageGallery.php');



// get user profile   

$user_data = Users::find_by_id($session->user_id);

// get user profile data 

$user_profile  = Profile::Find_by_id($user_data->user_profile); 

// check if the user profile block 

if($user_profile->profile_block == "yes"){ 

	redirect_to("../../../index.php");	 

} 

//send json data 

header('Content-Type: application/json');

if(!empty($_POST["task"]) && $_POST["task"] == "update"){ 

//get data 

$node_id = $_POST['record']; 

$edit = Nodes::find_by_id($node_id); 

//check globel edit authority	 

if($user_profile->global_edit != 'all_records' && $edit->inserted_by == $session->user_id){ 

	redirect_to("../view.php");	 

}else{ 

//validite required fields 

$required_fields = array('model'=>'- select model'); 

$check_required_fields = check_required_fields($required_fields); 

//update 

	if(count($check_required_fields) == 0){ 

		//retrieve all available languages 

		$languages = Localization::find_all('label','asc'); 

		//edit main table node 

		$edit->status = $_POST["shadow"]; 

		$edit->enable_comments = $_POST["enable_comments"]; 

		$edit->front_page = $_POST["front_page"]; 

	    $edit->slide_show = $_POST["slide_show"]; 

		$edit->start_publishing = $_POST["start_time"]; 

		$edit->end_publishing = $_POST["end_time"]; 

		if(!empty($_POST['imageVal'])){ 

		  $current_file = $_POST['imageVal']; 

		  $parts = explode('/',$current_file); 

		  $image_cover = $parts[count($parts)-1]; 

		  $folder = $parts[count($parts)-2]; 

		  $path = $folder."/".$image_cover; 

		  $edit->cover_image = $path; 

		} 
		if($_POST['imageVal_slider']){ 
				$current = $_POST['imageVal_slider']; 
				$parts_slider = explode('/',$current); 
				$image_covers = $parts_slider[count($parts_slider)-1]; 
				$edit->slider_cover = $image_covers; 
	        } 

		$edit->update_by = $session->user_id; 

		$edit->last_update = date_now();	 

		$edit->model = $_POST["model"]; 

		$update = $edit->update(); 

		if($update){ 

			//update page content  

			

				//update page content  

			foreach($languages as $language){ 

				$add_update_content = new NodesContent(); 

				$add_update_content->node_id = $node_id; 

				$add_update_content->title = $_POST['main_content']['title_'.$language->label]; 

				$add_update_content->alias = $_POST['main_content']['alias_'.$language->label];	 

				$add_update_content->body = $_POST['main_content']['full_content_'.$language->label]; 

				$add_update_content->summary = $_POST['main_content']['summary_'.$language->label]; 

				$add_update_content->lang_id = $language->id; 

				$add_update_content->meta_description = $_POST['main_content']['meta_description_'.$language->label]; 

				$add_update_content->meta_keys = $_POST['main_content']['meta_keys_'.$language->label]; 

				//check content id exist or not 

				//if exist update if not insert new record 

				$check_content_exist = $add_update_content->find_by_id($_POST['main_content']['content_id_'.$language->label]); 

				if($check_content_exist){ 

					$add_update_content->id = $_POST['main_content']['content_id_'.$language->label]; 

					$add_update_content->update();				 

				}else{ 

					$add_update_content->insert(); 

				} 

			} 

			//update gallery

			if(!empty($_POST["selected_image_gallery"])){ 

				//remove duplicates 

				$selected_image_gallery  = array_unique($_POST["selected_image_gallery"]);		 

				//update & insert values		 

				foreach($selected_image_gallery as $key => $value){ 

					if($key != 0){ 

						 $explode_value = explode(',', $value); 

						 if(!empty($explode_value[0])){ 

							 $add_update_value = new NodesImageGallery(); 

							 $add_update_value->image = file_folder_src($explode_value[0]); 

							 $add_update_value->sort = $explode_value[1]; 

							 $add_update_value->related_id = $node_id; 

							 $add_update_value->caption = $explode_value[2];

							 //check if id exit update  

							 //if new id insert  

							 $check_value_exist = $add_update_value->find_by_id($key); 

							 if($check_value_exist){ 

								 $add_update_value->id = $key; 

								 $add_update_value->update();  

								 $values_id[] = $key; 

							 }else{ 

								$add_update_value->insert();  

								$values_id[] = $add_update_value->id; 

							 }							 

						 } 

					} 

				}	 

			} 

			//update selected categories 

			if(!empty($_POST["categories"])){ 

				  //remove duplicates 

				  $selected_categories  = array_unique($_POST["categories"]); 

				  //delete all categories that exist in PageCategories tbl and doesn exist in new selections 

				  $implode_categories = implode(",", $selected_categories); 

				  $sql_delete_categories_not_in_array = "DELETE FROM nodes_selected_taxonomies WHERE node_id = '$node_id'  

				  AND taxonomy_id NOT IN ($implode_categories) 

				  AND  node_type = 'page' 

				  AND taxonomy_type = 'category'"; 

				  $preform_categories_tags = $database->query($sql_delete_categories_not_in_array); 

				  //check first this categories does not exist for this page 

				 foreach($selected_categories as $category){ 

				  $check_category_exist = NodesSelectedTaxonomies::check_taxonomy_exist($node_id,$category,'page','category'); 

				  if(empty($check_category_exist)){ 

					  $add_category = new NodesSelectedTaxonomies(); 

					  $add_category->node_id = $node_id; 

					  $add_category->taxonomy_type = 'category'; 

					  $add_category->taxonomy_id = $category; 

					  $add_category->node_type = 'page'; 

					  $add_category->insert(); 

				  } 

				} 

			}else{ 

			  $sql_delete_categories = "DELETE FROM nodes_selected_taxonomies WHERE node_id = '$node_id' 

			  AND node_type = 'page' "; 

			  $preform_delete_categories = $database->query($sql_delete_categories);   

			} 

			

			

			

		 $data  = array("status"=>"work"); 

		  echo json_encode($data); 

		}else{ 

		  $data  = array("status"=>"error"); 

		  echo json_encode($data); 

		} 

	}else{ 

		//validation error 

		$comma_separated = implode("<br>", $check_required_fields); 

		$data  = array("status"=>"valid_error", "fileds"=>$comma_separated); 

		echo json_encode($data); 

	}	 

} 

} 

//close connection 

if(isset($database)){ 

$database->close_connection(); 

} 

?>
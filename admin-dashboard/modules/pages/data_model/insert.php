<?php 
require_once('../../../../classes/Session.php'); 
require_once('../../../../classes/Functions.php'); 
require_once('../../../../classes/MysqlDatabase.php'); 
require_once('../../../../classes/Users.php'); 
require_once('../../../../classes/Profile.php'); 
require_once('../../../../classes/NodesSelectedTaxonomies.php'); 
require_once('../../../../classes/Nodes.php'); 
require_once('../../../../classes/NodesContent.php'); 
require_once('../../../../classes/Localization.php');
require_once('../../../../classes/NodesImageGallery.php'); 
//check  session user  log in 
if($session->is_logged() == false){ 
	redirect_to("../../../index.php"); 
} 
// get user profile   
$user_data = Users::find_by_id($session->user_id); 
// get user profile data 
$user_profile  = Profile::Find_by_id($user_data->user_profile); 
// check if the user profile block 
if($user_profile->profile_block == "yes"){ 
   redirect_to("../../../index.php");	 
} 
//send notifiction by json  
header('Content-Type: application/json'); 
if(!empty($_POST["task"]) && $_POST["task"] == "insert"){ 
//retrieve all available languages 
$languages = Localization::find_all('label','asc'); 
//validite required fields 
$required_fields = array('model'=>'- Select model'); 
$check_required_fields = check_required_fields($required_fields); 
if(count($check_required_fields) == 0){ 
	$add = new Nodes(); 
	$add->node_type = 'page'; 
	$add->status = $_POST["shadow"]; 
	$add->enable_comments = $_POST["enable_comments"]; 
	$add->front_page = $_POST["front_page"]; 
	$add->slide_show = $_POST["slide_show"]; 
	if(!empty($_POST['imageVal'])){ 
	  $current_file = $_POST['imageVal']; 
	  $parts = explode('/',$current_file); 
	  $image_cover = $parts[count($parts)-1]; 
	  $folder = $parts[count($parts)-2]; 
	  $path = $folder."/".$image_cover; 
	  $add->cover_image = $path; 
	}  
	if($_POST['imageVal_slider']){ 
	        $current = $_POST['imageVal_slider']; 
			$parts_slider = explode('/',$current); 
			$image_covers = $parts_slider[count($parts_slider)-1]; 
			$add->slider_cover = $image_covers; 
	 } 
	$add->inserted_by = $session->user_id; 
	if(!empty($_POST["start_time"])){ 
		$add->start_publishing = $_POST["start_time"]; 
	}else{ 
		$add->start_publishing =  date_now(); 
	} 
	$add->model = $_POST["model"]; 
	$add->shortcut_link = strtotime(date_now()); 
	$add->end_publishing = $_POST["end_time"]; 
	$add->inserted_date = date_now(); 
	$insert = $add->insert(); 
	$page_inserted_id = $add->id; 
	if($insert){  
		//insert page content  
		foreach($languages as $language){ 
			$add_page_content = new NodesContent(); 
			$add_page_content->node_id = $page_inserted_id; 
			$add_page_content->title = $_POST['main_content']['title_'.$language->label]; 
			$add_page_content->alias = $_POST['main_content']['alias_'.$language->label];	 
			$add_page_content->body = $_POST['main_content']['full_content_'.$language->label]; 
			$add_page_content->summary = $_POST['main_content']['summary_'.$language->label]; 
			$add_page_content->lang_id = $language->id; 
			$add_page_content->meta_description = $_POST['main_content']['meta_description_'.$language->label]; 
			$add_page_content->meta_keys = $_POST['main_content']['meta_keys_'.$language->label]; 
			$insert_page_content = $add_page_content->insert(); 
		}
		//insert selected_image_gallery 
		if(!empty($_POST["selected_image_gallery"])){ 
			//update orders table 
			$selected_image_gallery = $_POST["selected_image_gallery"]; 
			foreach($selected_image_gallery as $key => $value){ 
				if($key != 0){ 
					 $explode_value = explode(',', $value); 
					 if(!empty($explode_value[0])){ 
						 $add_image_gallery = new  NodesImageGallery(); 
						 $add_image_gallery->image = file_folder_src($explode_value[0]); 
						 $add_image_gallery->sort = $explode_value[1];
						 $add_image_gallery->caption = $explode_value[2]; 
						 $add_image_gallery->related_id = $page_inserted_id;
						 $add_image_gallery->insert();						  
					 } 
				} 
			}
		}
		//insert categories 
		if(!empty($_POST["categories"])){ 
			//remove duplicates 
			$selected_categories = array_unique($_POST["categories"]); 
			//convert to be array 
			foreach($selected_categories as $category){ 
				$add_category = new NodesSelectedTaxonomies(); 
				$add_category->node_id = $page_inserted_id; 
				$add_category->taxonomy_id = $category; 
				$add_category->taxonomy_type = 'category'; 
				$add_category->node_type = 'page'; 
				$insert_category = $add_category->insert(); 
			} 
		} 
		
 		
		
	$data  = array("status"=>"work","inserted_id"=>$page_inserted_id); 
		echo json_encode($data); 
	}else{ 
		$data  = array("status"=>"error"); 
		echo json_encode($data); 
	}	 
}else{ 
	//validation error 
	$comma_separated = implode("<br>", $check_required_fields); 
	$data  = array("status"=>"valid_error", "fileds"=>$comma_separated); 
	echo json_encode($data); 
} 
} 
//close connection 
if(isset($database)){ 
	$database->close_connection(); 
} 
?>
<!DOCTYPE html> 
<html lang="en"> 
  <head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
    <meta name="description" content=""> 
    <meta name="author" content="Mosaddek"> 
    <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina"> 
    <!-- Bootstrap core CSS --> 
    <link href="../../../css/bootstrap.min.css" rel="stylesheet"> 
    <link href="../../../css/bootstrap-reset.css" rel="stylesheet"> 
    <!--external css--> 
    <link href="../../../assets/font-awesome/css/font-awesome.css" rel="stylesheet" /> 
   <?php  
    require_once("../../../../classes/Nodes.php"); 
	require_once("../../../../classes/NodesSelectedTaxonomies.php"); 
	// get category id 
	$category_id = $_GET['category']; 
	$define_nodcategory_class = new NodesSelectedTaxonomies(); 
	$define_nodcategory_class->enable_relation(); 
    $category_pages = $define_nodcategory_class->return_taxonomy_nodes($category_id,'page','category','many'); 
	?> 
    <!-- Custom styles for this template --> 
    <link href="../../../css/style.css" rel="stylesheet"> 
    <link href="../../../css/style-responsive.css" rel="stylesheet" /> 
 </head> 
 <body style="background:#fff"> 
  <section class="panel"> 
   <header class="panel-heading">Category Pages</header> 
    <table class="table table-bordered"> 
           <thead> 
                <tr> 
                  <th>#</th> 
                  <th><i class=""></i> Title</th> 
                  <th><i class=""></i> Language</th> 
                  <th><i class=""></i> Created Date</th>                   
                  <th class=""><i class=""></i> Created By</th> 
                </tr> 
              </thead> 
              <tbody> 
                <?php  
				 $serialize = 1; 
				$node_class = new Nodes(); 
				$node_class->enable_relation(); 
                foreach($category_pages as $page){ 
	              $post_data = $node_class->node_data('page',$post->node_id); 
					 //get post categories 
					  				 
				 echo "<tr> 
					  <td>{$serialize}</td> 
					  <td><a href='full_info.php?id={$post_data->id}'>{$post_data->title}</a></td> 
					  <td>{$post_data->lang_name}</td> 
					 <td>{$post_data->inserted_date}</td> 
					  <td>{$post_data->inserted_by}</td>					   
					   
				  </tr>"; 
				  $serialize++; 
				  }?> 
                   
            </tbody> 
      </table> 
     <!-- END JAVASCRIPTS --> 
      <script src="../../../js/jquery.js"></script> 
    <script src="../../../js/bootstrap.min.js"></script> 
    <script class="include" type="text/javascript" src="../../../js/jquery.dcjqaccordion.2.7.js"></script> 
    <script src="../../../js/jquery.scrollTo.min.js"></script> 
    <script src="../../../js/jquery.nicescroll.js" type="text/javascript"></script> 
    <script src="../../../js/respond.min.js" ></script> 
    <!--common script for all pages--> 
    <script src="../../../js/common-scripts.js"></script> 
    </section> 
 </body> 
</html> 

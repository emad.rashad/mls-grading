<?php 
	require_once("../layout/initialize.php"); 
	$define_class= new Taxonomies(); 
	$define_class->enable_relation(); 
	$records = $define_class->taxonomies_data('tag',null,null,'inserted_date', 'DESC',null,'many',0); 
	//get texonomy content class 
    $define_taxonomy_content = new TaxonomiesContent(); 
	//localization langauges without retrive main langauges 		  
	require_once("../layout/header.php"); 
?>  <!--header end--> 
      <!--sidebar start--> 
      <script src="../../js-crud/crud_taxonomy_category.js"></script> 
       <?php require_once("../layout/navigation.php");?> 
        
      <!--sidebar end--> 
      <!--main content start--> 
      <section id="main-content"> 
         <section class="wrapper site-min-height"> 
          <h4>Taxonomy Tags Module</h4>  
              <section class="panel"> 
                 <header class="panel-heading"> 
                      View Taxonomy tags 
                  </header> 
                  <div class="panel-body"> 
                      <div class="adv-table editable-table "> 
                          <!--<div class="clearfix"> 
                              <div class="btn-group"> 
                                  <button id="editable-sample_new" class="btn green"> 
                                      Add New <i class="icon-plus"></i> 
                                  </button> 
                              </div> 
                              <div class="btn-group pull-right"> 
                                  <button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="icon-angle-down"></i> 
                                  </button> 
                                  <ul class="dropdown-menu pull-right"> 
                                      <li><a href="#">Print</a></li> 
                                      <li><a href="#">Save as PDF</a></li> 
                                      <li><a href="#">Export to Excel</a></li> 
                                  </ul> 
                              </div> 
                          </div>--> 
                          <div class="space15"></div> 
                            <button type="button" class="btn btn-danger" style="margin-left:15px" onClick="window.location.href = 'insert.php'"  
                            <?php 
							  $module_name = $opened_url_parts[count($opened_url_parts) - 2]; 
							  $opened_module_page_insert = $module_name.'/insert'; 
							  if(!in_array($opened_module_page_insert, $user_allowed_page_array)){ 
								echo "disabled"; 
							  } 
							  ?>> 
               <li class="icon-plus-sign"></li>   Add New Tag</button> 
                               <br/> 
                               <br/> 
                          <table class="table table-striped table-hover table-bordered" id="editable-sample"> 
                              <thead> 
                               <tr> 
                                 <th>#</th> 
                                 <th><i class=""></i> name</th> 
                                <th><i class=""></i> Created Date</th> 
                                 <th class=""><i class=""></i> Created by</th> 
                                  <th>Action</th> 
                              </tr> 
                              </thead> 
                              <tbody> 
                              <?php  
							   $serialize=1; 
							   foreach($records as $record){ 
								  echo "<tr> 
										<td>{$serialize}</td> 
										<td> <a href='full_info.php?id={$record->id}'>{$record->name}</a></td> 
										<td>{$record->inserted_date}</td> 
										<td>{$record->inserted_by}</td>"; 
										 
									echo "<td>"; 
										  $module_name = $opened_url_parts[count($opened_url_parts) - 2]; 
										  include('../layout/btn_control.php'); 
								  echo "</td>		 
									</tr>";   //delete dialoge  
									echo   "<div class='modal fade' id='my{$record->id}' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'> 
										<div class='modal-dialog'> 
											<div class='modal-content'> 
												<div class='modal-header'> 
													<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button> 
													<h4 class='modal-name'>Delete</h4> 
												</div> 
												<div class='modal-body'> 
	   
												 <p> Are you sure you want delete  $record->name??</p> 
	   
												</div> 
												<div class='modal-footer'> 
													 
													<button class='btn btn-warning' type='button'  
													onClick=\"window.location.href = 'data_model/delete.php?task=delete&id={$record->id}'\"/> Confirm</button>  <button data-dismiss='modal' class='btn btn-default' type='button'>cancle</button> 
												</div> 
											</div> 
										</div> 
									</div>"; 
					 $serialize++; 
             }?> 
							 
           </tbody> 
        </table> 
                          
                      </div> 
                  </div> 
              </section> 
              <!-- page end--> 
          </section> 
      </section> 
      <!--main content end--> 
      <!--footer start--> 
     <?php require_once("../layout/footer.php");?> 

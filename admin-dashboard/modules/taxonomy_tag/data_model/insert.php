<?php 
require_once('../../../../classes/Session.php'); 
require_once('../../../../classes/Functions.php'); 
require_once('../../../../classes/MysqlDatabase.php'); 
require_once('../../../../classes/Taxonomies.php'); 
require_once('../../../../classes/TaxonomiesContent.php'); 
require_once('../../../../classes/Users.php'); 
require_once('../../../../classes/Profile.php'); 
header('Content-Type: application/json'); 
//check  session user  log in 
if($session->is_logged() == false){ 
	redirect_to("../../../index.php"); 
} 
// get user profile   
$user_data = Users::find_by_id($session->user_id); 
// get user profile data 
$user_profile  = Profile::Find_by_id($user_data->user_profile); 
// check if the user profile block 
if($user_profile->profile_block == "yes"){ 
   redirect_to("../../../index.php");	 
} 
if(!empty($_POST["task"]) && $_POST["task"] == "insert"){ 
	//validite required required 
	$required_fields = array('name'=>"- Insert name", 'alias'=>'- Insert alias'); 
	$check_required_fields = check_required_fields($required_fields); 
	if(count($check_required_fields) == 0){ 
	// chech if category name exist 
			$check_name_availability = TaxonomiesContent::find_by_custom_filed('name',$_POST['name']); 
			if($check_name_availability == ""){ 
			//insert data 
			$add = new Taxonomies(); 
			$add->taxonomy_type = 'tag'; 
			$add->status = $_POST["shadow"]; 
			$add->inserted_date = date_now(); 
			$add->inserted_by = $session->user_id; 
			$insert = $add->insert(); 
			$inserted_id = $add->id; 
			if($insert){ 
				//insert category content 
				$add_taxonomy_content = new TaxonomiesContent(); 
				$add_taxonomy_content->taxonomy_id = $inserted_id; 
				$add_taxonomy_content->name = $_POST["name"]; 
				$add_taxonomy_content->alias = $_POST["alias"]; 
				$add_taxonomy_content->lang_id = 0; 
				$add_taxonomy_content->description = $_POST["description"]; 
				$insert_content = $add_taxonomy_content->insert(); 
				 
			   //return json  
				$data  = array("status"=>"work","id"=>$inserted_id); 
				echo json_encode($data); 
			}else{ 
				$data  = array("status"=>"error"); 
				echo json_encode($data); 
			} 
		  } 
		else  
		{ 
			$data  = array("status"=>"exist"); 
			echo json_encode($data); 
				 
		} 
  }else{ 
		  //validation error 
		  $comma_separated = implode("<br>", $check_required_fields); 
		  $data  = array("status"=>"valid_error", "fileds"=>$comma_separated); 
		  echo json_encode($data); 
	  } 
} 
//close connection 
if(isset($database)){ 
	$database->close_connection(); 
} 
?>
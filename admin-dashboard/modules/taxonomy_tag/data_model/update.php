<?php 
require_once('../../../../classes/Session.php'); 
require_once('../../../../classes/Functions.php'); 
require_once('../../../../classes/MysqlDatabase.php'); 
require_once('../../../../classes/Taxonomies.php'); 
require_once('../../../../classes/TaxonomiesContent.php'); 
require_once('../../../../classes/Users.php'); 
require_once('../../../../classes/Profile.php'); 
//check  session user  log in 
if($session->is_logged() == false){ 
	redirect_to("../../../index.php"); 
} 
// get user profile   
$user_data = Users::find_by_id($session->user_id); 
// get user profile data 
$user_profile  = Profile::Find_by_id($user_data->user_profile); 
// check if the user profile block 
if($user_profile->profile_block == "yes"){ 
   redirect_to("../../../index.php");	 
} 
if(!empty($_POST["task"]) && $_POST["task"] == "update"){ 
	//get data 
	 $id = $_POST["record"]; 
	 $edit = Taxonomies::find_by_id($id); 
     if($user_profile->global_edit != 'all_records' && $edit->inserted_by != $session->user_id  ){	 
		   redirect_to("../view.php"); 
	}else{ 
		  header('Content-Type: application/json'); 
		  //validite required required 
		  $required_fields = array('name'=>"- Insert name", 'alias'=>'- Insert alias'); 
		  $check_required_fields = check_required_fields($required_fields); 
		  if(count($check_required_fields) == 0){ 
				$check_name_availability = TaxonomiesContent::find_by_custom_filed('name',$_POST['name']); 
					 
				if( !empty($check_name_availability) && $check_name_availability->taxonomy_id != $id){ 
					$data  = array("status"=>"exist"); 
					echo json_encode($data); 
				}else{ 
				//update	 
				     $edit->update_by=$session->user_id; 
					  $edit->last_update = date_now(); 
					  $update = $edit->update(); 
					  if($update){ 
						  //incase user add new langauge and make it  main langauge  
			              //the system need to check first is this langauge entered before 
						  $add_update_content = new TaxonomiesContent(); 
						  $check_content_exist = $add_update_content->get_taxonomy_content($id,0); 
						  if(!empty($check_content_exist)){ 
							$add_update_content->id = $_POST["content_id"]; 
							$add_update_content->taxonomy_id = $id; 
							$add_update_content->name = $_POST["name"]; 
							$add_update_content->alias = $_POST["alias"]; 
							$add_update_content->description = $_POST["description"]; 
							$update_content = $add_update_content->update(); 
						}else{ 
							$add_update_content->taxonomy_id = $id; 
							$add_update_content->name = $_POST["name"]; 
							$add_update_content->alias = $_POST["alias"]; 
							$add_update_content->description = $_POST["description"]; 
							$add_update_content->lang_id = $lang; 
							$insert_content = $add_update_content->insert(); 
						} 
						   
						  $data  = array("status"=>"work"); 
						  echo json_encode($data); 
					  }else{ 
						  $data  = array("status"=>"error"); 
						  echo json_encode($data); 
					  } 
					 } 
		    
		}else{ 
			  //validation error 
			  $comma_separated = implode("<br>", $check_required_fields); 
			  $data  = array("status"=>"valid_error", "fileds"=>$comma_separated); 
			  echo json_encode($data); 
		} 
  }	 
} 
//close connection 
if(isset($database)){ 
	$database->close_connection(); 
} 
?>
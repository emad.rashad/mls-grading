<?php  
require_once("../layout/initialize.php"); 
require_once("../layout/header.php"); 
include("../../assets/texteditor4/head.php");  
$define_class = new Taxonomies(); 
$define_class->enable_relation(); 
//$records = $define_class->taxonomy_category_data('inserted_date', 'DESC',null,0); 
?> 
<script type="text/javascript" src="../../js-crud/taxonomy_tags.js"></script> 
  <!--header end-->  
  <!--sidebar start--> 
  <?php require_once("../layout/navigation.php");?> 
  <!--sidebar end-->  
  <!--main content start--> 
  <section id="main-content"> 
    <section class="wrapper site-min-height"> 
      <h4>Taxonomy Tags Module</h4> 
      <!-- page start--> 
      <div class="row"> 
        <aside class="col-lg-8"> 
          <section> 
            <div class="panel"> 
              <div class="panel-heading"> Add  Taxonomy Tag</div> 
              <div class="panel-body"> 
                 <form class="form-horizontal tasi-form" role="form" action="data_model/insert.php"  id="form_crud"> 
                  <input type="hidden" id="process_type" value="insert"> 
                  <input type="hidden" id="lang" value="<?php echo $general_setting_info->translate_lang_id?>"> 
                   <div class="form-group"> 
                     <label for="" class="col-lg-2 ">name:</label> 
                      <div class="col-lg-8"> 
                        <input type="text" class="form-control" id="name" onchange="add_char('name','alias');"> 
                       </div> 
                     </div> 
                     <div class="form-group"> 
                     <label for="" class="col-lg-2 ">alias:</label> 
                      <div class="col-lg-8"> 
                        <input type="text" class="form-control" id="alias"> 
                       </div> 
                     </div> 
                     
                    <div class="form-group"> 
                        <label for="" class="col-lg-2 ">Description:</label> 
                        <div class="col-lg-8"> 
                          <textarea class=" wysihtml5 form-control" id="description" placeholder="" rows="6"></textarea>                                 
                        </div> 
                        </div> 
                         
                    <div class="form-group"> 
                        <div class="col-lg-offset-2 col-lg-6"> 
                            <button type="submit" id="submit" class="btn btn-info">Save</button> 
                             <button type="submit" class="btn btn-default">Cancel</button> 
                             <div id="loading_data"></div 
                        ></div> 
                    </div> 
            </form> 
                              
              </div> 
            </div> 
          </section> 
        </aside> 
        <div class="col-lg-4"> 
          <section class="panel panel-primary"> 
            <header class="panel-heading"> Publish Options: </header> 
            <div class="panel-body"> 
                <form class="form-horizontal tasi-form" role="form"> 
                 <div class="form-group"> 
              <label class="col-lg-5">Status:</label> 
              <div class="col-lg-6"> 
                <label class="checkbox-inline"> 
                  <input type="radio" name="shadow" class="radio" value="draft"> 
                  Draft</label> 
                <label class="checkbox-inline"> 
                  <input type="radio" name="shadow" class="radio" value="publish" checked> 
                  Publish</label> 
              </div> 
            </div> 
               </form> 
              </div> 
          </section> 
        </div> 
         
      </div> 
      <!-- page end-->  
    </section> 
  </section> 
  <!--main content end-->  
  <!--footer start--> 
   
  <?php require_once("../layout/footer.php");?>
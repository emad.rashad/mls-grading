<?php	 
	require_once("../layout/initialize.php"); 
    if(isset($_GET['id']) && is_numeric($_GET['id'])){ 
		$record_id = $_GET['id']; 
		$define_class = new Taxonomies(); 
		$define_class->enable_relation(); 
		$record_info = $define_class->taxonomies_data('tag',$record_id,null,null,null,null,'one',0); 
	  //check id access 
		if(empty($record_info->id)){ 
			redirect_to("view.php");	 
		} 
	}else{ 
		redirect_to("view.php");	 
	} 
	require_once("../layout/header.php");	 
?> 
<!--header end--> 
<!--sidebar start--> 
<?php require_once("../layout/navigation.php");?> 
<!--sidebar end-->  
<!--main content start--> 
<section id="main-content"> 
  <section class="wrapper site-min-height"> 
    <h4>Taxonomy Categories Module</h4> 
    <div class="row"> 
      <aside class="col-lg-8"> 
        <section> 
          <div class="panel"> 
            <div class="panel-heading"> Taxonomy tag Info</div> 
            <div class="panel-body"> 
              <form class="form-horizontal tasi-form" role="form" id="form_crud"> 
                <input type="hidden" id="process_type" value="insert"> 
                <div class="form-group"> 
                  <label  class="col-lg-2 ">name:</label> 
                  <div class="col-lg-8" > <?php echo $record_info->name?> </div> 
                </div> 
                 <div class="form-group"> 
                  <label  class="col-lg-2 ">Alias:</label> 
                  <div class="col-lg-8" > <?php echo $record_info->alias?> </div> 
                </div> 
                <div class="form-group"> 
                  <label  class="col-lg-2 ">Description:</label> 
                  <div class="col-lg-8" > <?php echo html_entity_decode($record_info->description)?> </div> 
                </div> 
                <div class="form-group"> 
                  <div class="col-lg-offset-2 col-lg-10"> 
                    <button type="button" class="btn btn-info" onClick="window.location.href = 'update.php?id=<?php echo $record_id?>'" > 
                    <li class="icon-edit-sign"></li> 
                    Update </button> 
                   </div> 
                </div> 
              </form> 
            </div> 
          </div> 
        </section> 
      </aside> 
      <div class="col-lg-4"> 
          <section class="panel panel-primary"> 
            <header class="panel-heading tab-bg-dark-navy-blue"> 
              <ul class="nav nav-tabs"> 
                <li class=" center-block active" ><a data-toggle="tab" href="#op1" class="text-center"> <i class=" icon-check"> </i> <strong> Entry Info</strong> </a> </li> 
                <li class=" center-block " ><a data-toggle="tab" href="#op2" class="text-center"> <i class=" icon-check"> </i> <strong> Publish Option</strong> </a> </li> 
                 
              </ul> 
            </header> 
            <div class="panel-body"> 
              <div id="list_info"> 
                <ul> 
                  <div class="tab-content"> 
                    <div id="op1" class="tab-pane active "> 
                      <li><span style="color:#428bca">> Created By:</span> <?php echo $record_info->inserted_by?></li> 
                      <li><span style="color:#428bca">> Created Date:</span> <?php echo $record_info->inserted_date?></li> 
                      <li><span style="color:#428bca">> Last Update By:</span> 
                        <?php if($record_info->update_by!=""){ 
					  echo $record_info->update_by; 
				      }else{ 
						  echo "--";} 
					  ?> 
                      </li> 
                      <li><span style="color:#428bca">> Last Update Date:</span> 
                        <?php if($record_info->last_update!=""){ 
					  echo $record_info->last_update; 
				      }else{ 
						  echo "--"; 
					 } 
					  ?> 
                      </li> 
                    </div> 
                          <div id="op2" class="tab-pane  "> 
                      <li><span style="color:#428bca">> Status:</span> <?php echo $record_info->status?></li> 
                    </div> 
                     
                  </div> 
                </ul> 
              </div> 
            </div> 
          </section> 
           
        </div> 
    </div> 
     
    <!-- page end-->  
  </section> 
</section> 
<!--main content end-->  
<!--footer start--> 
<?php require_once("../layout/footer.php");?>
<?php 
	require_once("../layout/initialize.php"); 
	if(isset($_GET['id']) && is_numeric($_GET['id'])){ 
		$record_id = $_GET['id']; 
		$define_class = new Taxonomies(); 
        $define_class->enable_relation(); 
		$record_info = $define_class->taxonomies_data('tag',$record_id,null,null,null,null,'one',0); 
		//check id access 
		if(empty($record_info->id)){ 
			redirect_to("view.php");	 
		} 
		if($user_profile->global_edit != 'all_records' && $record_info->inserted_by != $session->user_id ){ 
		  redirect_to('view.php');	 
	    } 
	}else{ 
		redirect_to("view.php");	 
	} 
	require_once("../layout/header.php"); 
	include("../../assets/texteditor4/head.php");  
?> 
<script type="text/javascript" src="../../js-crud/taxonomy_tags.js"></script> 
  <!--header end-->  
  <!--sidebar start--> 
  <?php require_once("../layout/navigation.php");?> 
  <!--sidebar end-->  
  <!--main content start--> 
  <section id="main-content"> 
    <section class="wrapper site-min-height"> 
      <h4>Taxonomy Tags Module</h4>  
      <!-- page start--> 
  <div class="row"> 
    <aside class="profile-info col-lg-8"> 
      <section> 
        <div class="panel"> 
          <div class="panel-heading"> Edit  Taxonomy Tag</div> 
          <div class="panel-body"> 
            <form class="form-horizontal tasi-form" role="form"  id="form_crud" action="data_model/update.php"> 
               <input type="hidden" id="process_type" value="update"> 
               <input type="hidden" id="record" value="<?php echo $record_id; ?>"> 
               <input type="hidden" id="content_id" value="<?php echo $record_info->content_id; ?>"> 
                <div class="form-group"> 
                    <label for="" class="col-lg-2">name:</label> 
                    <div class="col-lg-8"> 
                        <input type="text" class="form-control" id="name" placeholder="" value="<?php echo $record_info->name;?>" 
                         onchange="add_char('name','alias');"> 
                  </div> 
                </div> 
                <div class="form-group"> 
                    <label for="" class="col-lg-2">alias:</label> 
                    <div class="col-lg-8"> 
                        <input type="text" class="form-control" id="alias" placeholder="" value="<?php echo $record_info->alias;?>"> 
                  </div> 
                </div> 
                <div class="form-group"> 
                  <label for="inputPassword1" class="col-lg-2">Description:</label> 
                  <div class="col-lg-8"> 
                    <textarea class=" wysihtml5 form-control" id="description" placeholder=""  
                    rows="6"><?php echo $record_info->description;?></textarea>  
                   </div> 
                 </div> 
                  
                <div class="form-group"> 
                   <div class="col-lg-offset-2 col-lg-8"> 
                      <button type="submit" id="submit" class="btn btn-info">Save</button> 
                      <button type="button" class=" btn btn-info "   
                    onClick="window.location.href = 'full_info.php?id='+<?php echo $record_id?>" > <i class="icon-info-sign"></i> View Full Info </button> 
                   <div id="loading_data"></div> 
                    </div> 
                </div> 
            </form> 
          </div> 
          </div> 
          </section> 
          </aside> 
          <div class="col-lg-4"> 
          <section class="panel panel-primary"> 
            <header class="panel-heading"> Publish Options: </header> 
            <div class="panel-body"> 
                <form class="form-horizontal tasi-form" role="form"> 
                    <div class="form-group "> 
                <label class="col-lg-5">Status:</label> 
                <div class="col-lg-6"> 
                  <label class="checkbox-inline"> 
                    <input type="radio" name="shadow" class="radio" value="draft" <?php if($record_info->status=="draft") echo 'checked'?>> 
                    Draft</label> 
                  <label class="checkbox-inline"> 
                    <input type="radio"  name="shadow" class="radio" value="publish" <?php if($record_info->status=="publish") echo 'checked'?>> 
                   Publish</label> 
                </div> 
              </div> 
                </form> 
              </div> 
          </section> 
        </div> 
       </div> 
       
      <!-- page end-->  
    </section> 
  </section> 
  <!--main content end-->  
  <!--footer start--> 
   
  <?php require_once("../layout/footer.php");?>
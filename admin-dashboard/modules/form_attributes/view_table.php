<?php
	require_once("../layout/initialize.php");
	if(isset($_GET['form_id'])){
	$form_id = $_GET['form_id'];
	$form_name = Forms::find_by_id($form_id);
   }else{
	 redirect_to('../forms/view.php');	
   }  
	$define_class= new FormAttributes();
	$define_class->enable_relation();
	$records = $define_class->form_attribute_data('sorting', 'ASC',null,$form_id);
	require_once("../layout/header.php");
?>  <!--header end-->
      <!--sidebar start-->
      
       <?php require_once("../layout/navigation.php");?>
       
      <!--sidebar end-->
      <!--main content start-->
      <section id="main-content">
         <section class="wrapper site-min-height">
          <h4>Forms</h4> 
              <section class="panel">
                 <header class="panel-heading">
                      View Forms Attributes
                  </header>
                  <div class="panel-body">
                      <div class="adv-table editable-table ">
                          <!--<div class="clearfix">
                              <div class="btn-group">
                                  <button id="editable-sample_new" class="btn green">
                                      Add New <i class="icon-plus"></i>
                                  </button>
                              </div>
                              <div class="btn-group pull-right">
                                  <button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="icon-angle-down"></i>
                                  </button>
                                  <ul class="dropdown-menu pull-right">
                                      <li><a href="#">Print</a></li>
                                      <li><a href="#">Save as PDF</a></li>
                                      <li><a href="#">Export to Excel</a></li>
                                  </ul>
                              </div>
                          </div>-->
                          <div class="space15"></div>
                          <br/>
                               <br/>
                          <table class="table table-striped table-hover table-bordered" id="editable-sample">
                              <thead>
                               <tr>
                                  <th>#</th>
                                 <th><i class=""></i> Label</th>
                                 <th><i class=""></i> Form </th>
                                 <th class=""><i class=""></i> Attribute type</th>
                                 
                                 <th>Action</th>
                              </tr>
                              </thead>
                              <tbody>
                              <?php 
							   $serialize=1;
							   foreach($records as $record){
								 echo "<tr>
								  <td>{$serialize}</td>
                                 <td>{$record->attribute_label}</td>
								  <td>{$record->form_name}</td>
								  <td>{$record->attribute_type}</td>
								 "; 
								 
								echo "  <td>";
									$module_name = $opened_url_parts[count($opened_url_parts) - 2];
									include('../layout/btn_control.php');
							echo "</td>		
							  </tr>";   //delete dialoge 
                              echo   "<div class='modal fade' id='my{$record->id}' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'>
                                  <div class='modal-dialog'>
                                      <div class='modal-content'>
                                          <div class='modal-header'>
                                              <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
                                              <h4 class='modal-title'>Delete</h4>
                                          </div>
                                          <div class='modal-body'>

                                           <p> Are you sure you want delete  $record->attribute_label??</p>

                                          </div>
                                          <div class='modal-footer'>
                                              
                                              <button class='btn btn-warning' type='button' 
                                              onClick=\"window.location.href = 'data_model/delete.php?task=delete&id={$record->id}'\"/> Confirm</button>  <button data-dismiss='modal' class='btn btn-default' type='button'>cancle</button>
                                          </div>
                                      </div>
                                  </div>
                              </div>";
			   $serialize++;
             }?>
							
           </tbody>
        </table>
                         
                      </div>
                  </div>
              </section>
              <!-- page end-->
          </section>
      </section>
      <!--main content end-->
      <!--footer start-->
     <?php require_once("../layout/footer.php");?>

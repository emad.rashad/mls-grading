<?php
require_once('../../../../classes/Session.php');
require_once('../../../../classes/Functions.php');
require_once('../../../../classes/MysqlDatabase.php');
require_once('../../../../classes/FormAttributes.php');
require_once('../../../../classes/Users.php');
require_once('../../../../classes/Profile.php');
//check  session user  log in
if($session->is_logged() == false){
	redirect_to("../../../index.php");
}
// get user profile  
$user_data = Users::find_by_id($session->user_id);
// get user profile data
$user_profile  = Profile::Find_by_id($user_data->user_profile);
// check if the user profile block
if($user_profile->profile_block == "yes"){
   redirect_to("../../../index.php");	
}
if(!empty($_POST["task"]) && $_POST["task"] == "insert"){
	//validite required required
	$required_fields = array('attribute_label'=>"- Insert label");
	$check_required_fields = check_required_fields($required_fields);
	if(count($check_required_fields) == 0){
	   //insert data
			$add = new FormAttributes();
		 	$add->attribute_label = $_POST["attribute_label"];
			$add->required = $_POST["required"];
		    $add->attribute_id = $_POST["attribute_id"];
		    $add->sorting = $_POST["sorting"];
			$add->form_id = $_POST["form_id"];
			$add->attribute_values = $_POST["attribute_values"];
			$add->inserted_date = date_now();
			$add->inserted_by = $session->user_id;
			$insert = $add->insert();
			header('Content-Type: application/json');
			if($insert){
				$data  = array("status"=>"work");
				echo json_encode($data);
			}else{
				$data  = array("status"=>"error");
				echo json_encode($data);
			}
		  
  }else{
		  //validation error
		  $comma_separated = implode("<br>", $check_required_fields);
		  $data  = array("status"=>"valid_error", "fileds"=>$comma_separated);
		  echo json_encode($data);
	  }
}
//close connection
if(isset($database)){
	$database->close_connection();
}

?>
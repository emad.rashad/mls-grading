<?php  
	require_once("../layout/initialize.php"); 
	if(isset($_GET['id']) && is_numeric($_GET['id'])){ 
		$record_id = $_GET['id']; 
		$define_class = new ContactUs(); 
		$define_class->enable_relation(); 
		$record_info = $define_class->find_by_id($record_id); 
		//check id access 
		if(empty($record_info->id)){ 
			redirect_to("view.php");
		}
	}else{ 
		redirect_to("view.php");	 
	} 
	require_once("../layout/header.php"); 
?> 
<!--header end--> 
<!--sidebar start--> 
<?php require_once("../layout/navigation.php");?> 
<!--sidebar end-->  
<!--main content start--> 
<section id="main-content"> 
  <section class="wrapper site-min-height"> 
    <h4>Social Activity Module</h4> 
    <!-- page start--> 
    <div class="row"> 
      <aside class="col-lg-8"> 
        <section> 
          <div class="panel"> 
            <div class="panel-heading"> Contact us Info</div> 
            <div class="panel-body"> 
              <div class="panel-body"> 
                <form class="form-horizontal tasi-form" role="form" > 
                    <div class="form-group"> 
                          <label  class="col-lg-4"> UserName:</label> 
                          <div class="col-lg-8"> <?php echo $record_info->user_name;?> </div> 
                        </div> 
                        <div class="form-group"> 
                          <label  class="col-lg-4">Email:</label> 
                          <div class="col-lg-8"> <?php echo $record_info->email;?> </div> 
                        </div> 
                        <div class="form-group"> 
                          <label  class="col-lg-4">Inserted Date:</label> 
                          <div class="col-lg-8"> <?php echo $record_info->inserted_date;?> </div> 
                        </div> 
                        <div class="form-group"> 
                        <label  class="col-lg-4">Message Body :</label> 
                        <div class="col-lg-8"> <?php echo $record_info->body;?></div> 
                      </div>  
                
                  
                  <div class="form-group"> 
                  <div class="col-lg-offset-2 col-lg-10"> 
                    <button type="button" class="btn btn-info"   
                        onClick="window.location.href = 'view.php'" > <li class="icon-info"></li> Back to view </button> 
                     
                        </div> 
                  </div> 
                </form> 
              </div> 
            </div> 
          </div> 
        </section> 
      </aside> 
    </div> 
    <!-- page end-->  
  </section> 
</section> 
<!--main content end-->  
<!--footer start--> 
<?php require_once("../layout/footer.php");?> 

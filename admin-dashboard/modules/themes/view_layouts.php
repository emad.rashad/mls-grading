<?php  
 	require_once("../layout/initialize.php"); 
	$theme_id = $_GET['id']; 
	$define_class = new ThemesLayouts(); 
	$records = $define_class->theme_layouts_data("uploaded_date","DESC", $theme_id); 
 	require_once("../layout/header.php"); 
?> 
<script type="text/javascript" src="../../js/fancybox/jquery.fancybox-1.3.4.pack.js"></script> 
  <script type="text/javascript" src="../../js/fancybox/jquery.mousewheel-3.0.4.pack.js"></script> 
  <!--header end-->  
  <!--sidebar start--> 
  <?php require_once("../layout/navigation.php");?> 
  <!--sidebar end-->  
  <!--main content start--> 
  <section id="main-content"> 
    <section class="wrapper site-min-height"> 
      <h4>Theme Module</h4> 
      <!-- page start--> 
      <section class="panel"> 
        <header class="panel-heading"> View Theme Layouts </header> 
        <div class="panel-body"> 
          <div class="adv-table editable-table ">  
            <!--<div class="clearfix"> 
                              <div class="btn-group"> 
                                  <button id="editable-sample_new" class="btn green"> 
                                      Add New <i class="icon-plus"></i> 
                                  </button> 
                              </div> 
                              <div class="btn-group pull-right"> 
                                  <button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="icon-angle-down"></i> 
                                  </button> 
                                  <ul class="dropdown-menu pull-right"> 
                                      <li><a href="#">Print</a></li> 
                                      <li><a href="#">Save as PDF</a></li> 
                                      <li><a href="#">Export to Excel</a></li> 
                                  </ul> 
                              </div> 
                          </div>--> 
            <div class="space15"></div> 
                          <table class="table table-striped table-advance table-hover" style="width:550px;"> 
              <thead> 
                <tr> 
                  <th>Name</th> 
                  <th>Information</th> 
                </tr> 
              </thead> 
              <tbody> 
                <?php   
				  foreach($records as $record){ 
					  echo "<tr> 
					  <td>$record->layout_name</td> 
					  <td><span style='font-weight:bold'>Version:</span> {$record->version} 
					  <br><span style='font-weight:bold'>Author:</span> {$record->author} 
					  <br><span style='font-weight:bold'>Uploaded By:</span> {$record->uploaded_by} 
					  <br><span style='font-weight:bold'>Uploaded In:</span> {$record->uploaded_date} 
					  <br><br> 
					   <a  id='show_model{$record->id}' 
					   href='view_model.php?theme={$theme_id}&layout={$record->id}' 
					   style='font-weight:bold'>Show Layout Models </a> 
					  </td> 
					</tr>";?> 
                    <script> 
                   $("#show_model<?php echo $record->id;?>").fancybox({ 
				'width'				: '100%', 
				'height'			: '100%', 
				'autoScale'			: false, 
				'transitionIn'		: 'none', 
				'transitionOut'		: 'none', 
				'type'				: 'iframe' 
			 }); 
                     
                    </script> 
					 
				 <?php } 
				  ?> 
              </tbody> 
            </table> 
          </div> 
        </div> 
      </section> 
      <!-- page end-->  
    </section> 
  </section> 
  <!--main content end-->  
  <!--footer start--> 
  <?php require_once("../layout/footer.php");?>
<?php  
require_once("../layout/light_box_header.php"); 
require_once("../../../classes/EcomCompanies.php"); 
//get all auhtor and company 
$companies = EcomCompanies::find_all(); 
?> 
<script> 
$(document).ready(function (){ 
	//get current selected Companies & checked it 
	parent.$(".selected_company li" ).each(function() { 
		var li_id = $(this).attr("id"); 
		$('.company_'+li_id).prop("checked",true); 
	}); 
	//when user submit get selected Companies 
	//print selected Companies in li view 
	$("#form_Companies").submit(function (){	 
		parent.$(".selected_company").html(''); 
		$('input:checkbox[name="companies[]"]:checked').each(function(){ 
			var company_id = $(this).attr("id"); 
			var company_name = $(this).val(); 
			parent.$(".selected_company"). 
			append('<li id="'+company_id+'">- '+company_name +'&nbsp<a href="#" class="Deletecompany glyphicon glyphicon-remove"></a></li>'); 
			$('#feature_loading').html('The items were added successfully'); 
		});	 
		return false; 
	});	 
}); 
</script> 
<br> 
<div class="col-lg-6"> 
  <section class="panel"> 
    <header class="panel-heading"> <strong>All Companies:</strong> </header> 
    <div class="panel-body"> 
      <form id="form_Companies" > 
        <div class="form-group " > 
              <div class="form-group "> 
            <label  class="col-lg-2">Search:</label> 
            <div class="col-lg-3"> 
              <input type="text" class="form-control" id="search_input" placeholder=" " autocomplete="off" style="width:250px" /> 
            </div> 
          </div> 
         <div class="form-group" > 
        <label  class="col-lg-2">Companies:</label> 
                 <div class="checkboxes"> 
                 <ul id="search_list">
                 <?php  
                  foreach($companies as $company){ 
                   echo "<label class='label_check'><input type='checkbox' value='{$company->name}' name='companies[]'  id ='{$company->id}' class='company_{$company->id}'"; 
                   echo ">{$company->name}</label>"; 
                 }?> 
               </ul>
                </div> 
           </div> 
         </div> 
        <div class="form-group" > 
          <div class="col-lg-8"> 
            <button type="submit" class="btn btn-info">Select</button> 
            <div id="feature_loading"></div> 
          </div> 
        </div> 
      </form> 
    </div> 
  </section> 
</div> 
<?php require_once("../layout/light_box_footer.php");?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="../../js/jquery.fastLiveFilter.js"></script> 
<script>
    $(function() {
        $('#search_input').fastLiveFilter('#search_list');
    });
</script>
<?php  
require_once("../layout/light_box_header.php"); 
require_once("../../../classes/EcomStores.php"); 
require_once("../../../classes/EcomStoresAddress.php");
//get all stores
$define_class_store = new EcomStores();
$define_class_store->enable_relation();
$define_class_store_address =  new EcomStoresAddress();
$define_class_store_address->enable_relation();
$stores = $define_class_store->find_all(); 
?> 
<script> 
$(document).ready(function (){ 
	//get current selected stores & checked it 
	parent.$(".selected_store li" ).each(function() { 
		var li_id = $(this).attr("id"); 
		$('.store_'+li_id).prop("checked",true); 
	}); 
	//when user submit get selected stores 
	//print selected stores in li view 
	$("#form_stores").submit(function (){	
	 var categories = [];  
		parent.$(".selected_store").html(''); 
		$('input:checkbox[name="stores[]"]:checked').each(function(){ 
		   categories.push($(this).attr("id"));
			var company_id = $(this).attr("id"); 
			var company_name = $(this).val(); 
			parent.$(".selected_store"). 
			append('<li id="'+company_id+'">- '+company_name +'&nbsp<a href="#" class="Deletecompany glyphicon glyphicon-remove"></a></li>');
		    parent.$("#show_inserted_data").attr('href',"../layout/store_categories.php?stores="+categories) 
			$('#feature_loading').html('The items were added successfully'); 
		});	 
		return false; 
	});	 
}); 
</script> 
<br> 
<div class="col-lg-6"> 
  <section class="panel"> 
    <header class="panel-heading"> <strong>All stores:</strong> </header> 
    <div class="panel-body"> 
      <form id="form_stores" > 
        <div class="form-group " > 
              <div class="form-group "> 
            <label  class="col-lg-2">Search:</label> 
            <div class="col-lg-3"> 
              <input type="text" class="form-control" id="search_input" placeholder=" " autocomplete="off" style="width:250px" /> 
            </div> 
          </div> 
         <div class="form-group" > 
        
                 <div class="checkboxes"> 
                 <ul id="search_list">
                 <?php  
                  foreach($stores as $store){ 
				  $store_data = $define_class_store->get_store_content($store->id,$main_lang_id);
				     echo " <p style='font-size:15px'><strong>$store_data->name</strong></p>";
					 
				  $store_branchs = $define_class_store_address->find_all_by_custom_filed('store_id',$store->id);
				  
				  foreach($store_branchs as $branch){
					  $branch_data = $define_class_store_address->get_stores_address_content($branch->id,$main_lang_id);
				  
                   echo " <label class='label_check'><input type='checkbox' value='{$branch_data->branch_name}' name='stores[]'  id ='{$branch->id}' 
				   class='store_{$branch->id}'"; echo ">{$branch_data->branch_name}</label>"; 
				  }
                 }?> 
                
               </ul>
                </div> 
           </div> 
           
         </div> 
        <div class="form-group" > 
          <div class="col-lg-8"> 
            <button type="submit" class="btn btn-info">Select</button> 
            <div id="feature_loading"></div> 
          </div> 
        </div> 
      </form> 
    </div> 
  </section> 
</div> 
<?php require_once("../layout/light_box_footer.php");?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="../../js/jquery.fastLiveFilter.js"></script> 
<script>
    $(function() {
        $('#search_input').fastLiveFilter('#search_list');
    });
</script>
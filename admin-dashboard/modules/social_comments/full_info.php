<?php  
	require_once("../layout/initialize.php"); 
	//check id access 
	if(isset($_GET['id']) && is_numeric($_GET['id'])){ 
		$record_id = $_GET['id']; 
	    $define_class = new SocialComments(); 
		$define_class->enable_relation(); 
	    $record_info = $define_class->comment_data(null,null,$record_id); 
		if(empty($record_info->id)){ 
			redirect_to("view.php");	 
		} 
	}else{ 
		redirect_to("view.php");	 
	} 
	require_once("../layout/header.php"); 
?> 
  <script type="text/javascript" src="../../js-crud/crud_comments.js"></script>  
  <!--header end-->  
  <!--sidebar start--> 
  <?php require_once("../layout/navigation.php");?> 
  <!--sidebar end-->  
  <!--main content start--> 
  <section id="main-content"> 
    <section class="wrapper site-min-height"> 
      <h4>Social Comments Module</h4> 
      <div class="row"> 
        <aside class="col-lg-8"> 
          <section> 
            <div class="panel"> 
              <div class="panel-heading">Comment Info</div> 
              <div class="panel-body"> 
                <form class="form-horizontal tasi-form" role="form" id="form_crud" > 
                  <div class="form-group"> 
                    <label  class="col-lg-2">Title:</label> 
                    <div class="col-lg-8" > <?php echo $record_info->title?> </div> 
                  </div> 
                  <div class="form-group"> 
                    <label  class="col-lg-2 ">Node Title:</label> 
                    <div class="col-lg-8" ><?php echo $record_info->node_title?> </div> 
                  </div> 
                    <div class="form-group"> 
                    <label  class="col-lg-2 ">Node Type:</label> 
                    <div class="col-lg-8" ><?php echo $record_info->node_type?> </div> 
                  </div> 
                  <div class="form-group"> 
                    <label  class="col-lg-2 ">User Name:</label> 
                    <div class="col-lg-8" ><?php echo $record_info->user_name?> </div> 
                  </div> 
                  <div class="form-group"> 
                    <label  class="col-lg-2 ">Email:</label> 
                    <div class="col-lg-8" > <?php echo $record_info->email?> </div> 
                  </div> 
                  <div class="form-group"> 
                    <label  class="col-lg-2 ">Body:</label> 
                    <div class="col-lg-8" > <?php echo $record_info->body;?> </div> 
                  </div> 
                </form> 
              </div> 
            </div> 
          </section> 
        </aside> 
        <div class="col-lg-3"> 
          <section class="panel panel-primary"> 
            <header class="panel-heading"> Entry Information: </header> 
            <div class="panel-body"> 
              <div id="list_info"> 
                <ul> 
                   <li><span style="color:#428bca">> status:</span> <?php echo $record_info->status?></li> 
                  <li><span style="color:#428bca">> Created Date:</span> <?php echo $record_info->inserted_date?></li> 
                  <li><span style="color:#428bca">> Last Update By:</span> 
                    <?php if($record_info->update_by!=""){ 
					  echo $record_info->update_by; 
				      }else{ 
						  echo "--";} 
					  ?> 
                  </li> 
                  <li><span style="color:#428bca">> Last Update Date:</span> 
                    <?php if($record_info->last_update!="0000-00-00 00:00:00"){ 
					  echo $record_info->last_update; 
				      }else{ 
						  echo "--";} 
					  ?> 
                  </li> 
                 
                </ul> 
              </div> 
            </div> 
          </section> 
           
           
        </div> 
      </div> 
      <!-- page end-->  
    </section> 
  </section> 
  <!--main content end-->  
  <!--footer start--> 
   
  <?php require_once("../layout/footer.php");?>
<?php  
	require_once("../layout/initialize.php"); 
	$record_id = $_GET['id']; 
	$define_class = new Customers(); 
	
	$record_info = $define_class->find_by_custom_filed('id',$record_id);
	require_once("../layout/header.php"); 
	 
?> 
<script type="text/javascript" src="../../js-crud/crud_customer.js"></script> 
  <!--header end-->  
  <!--sidebar start--> 
  <?php require_once("../layout/navigation.php");?> 
  <!--sidebar end-->  
  <!--main content start--> 
  <section id="main-content"> 
    <section class="wrapper site-min-height"> 
      <h4>Hamasa Employees Info Module</h4>    		   
      <!-- page start--> 
      <div class="row"> 
        <aside class="col-lg-8"> 
          <section> 
            <div class="panel"> 
              <div class="panel-heading"> Customer Info For :  <?php echo $record_info->full_name() ;  ?> </div> 
              <div class="panel-body"> 
                <form class="form-horizontal tasi-form" role="form" id="form_crud" action="data_model/insert.php"> 
                <input type="hidden" id="process_type" value="insert"> 
                  <div class="form-group"> 
                    <label  class="col-lg-2">First Name :</label> 
                    <div class="col-lg-6"> 
                    <?php echo $record_info->first_name ; ?> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                    <label  class="col-lg-2">Last Name :</label> 
                    <div class="col-lg-6"> 
                    <?php echo $record_info->last_name?> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                    <label  class="col-lg-2">Password</label> 
                    <div class="col-lg-6"> 
                    <?php echo $record_info->password  ; ?> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                    <label  class="col-lg-2">Email :</label> 
                    <div class="col-lg-6"> 
                    <?php echo $record_info->email ;  ?> 
                    </div> 
                  </div> 
                  
                  
                   <div class="form-group"> 
                    <label  class="col-lg-2">Gender  :</label> 
                    <div class="col-lg-6"> 
                    <?php echo $record_info->gender ?> 
                    </div> 
                  </div> 
                  
                  
                    <div class="form-group"> 
                    <label  class="col-lg-2">Exhibtion :</label> 
                    <div class="col-lg-6"> 
                    <?php echo $record_info->exhibtion ; ?> 
                    </div> 
                  </div> 

                  

                  

                     <div class="form-group"> 
                    <label  class="col-lg-2">Title  :</label> 
                    <div class="col-lg-6"> 
                    <?php echo $record_info->title?> 
                    </div> 
                  </div> 

                   


                  <div class="form-group"> 
                  <div class="col-lg-offset-2 col-lg-10"> 
                    <button type="button" class="btn btn-info"   
                        onClick="window.location.href = 'update.php?id='+<?php echo $record_id?>" > <li class="icon-edit-sign"></li> Update </button> 
                      
                        </div> 
                  </div> 
                </form> 
              </div> 
            </div> 
          </section> 
        </aside> 
        
      </div> 
       
      <!-- page end-->  
    </section> 
  </section> 
  <!--main content end-->  
  <!--footer start--> 
  <?php require_once("../layout/footer.php");?> 
   
	 
   
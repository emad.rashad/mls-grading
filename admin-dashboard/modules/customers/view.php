<?php 
ob_start(); 
	require_once("../layout/initialize.php"); 
	// define user class 
	$define_class = new Customers(); 
	
	$relation_tables = array(); 
	$count_relation_tables = ""; 
	//POST info 
	//filter data 

	if(isset($_GET["id"]) && $_GET["task"]=="delete"){ 
			$id = $_GET['id']; 
			 
			$count_relation_tables = count($relation_tables); 
			if($count_relation_tables  == 0){  
				redirect_to("data_model/delete.php?task=delete&id=$id");	 
			} 
		} 

	
	
	$records = $define_class->find_all();
	// delete record check 
    
             require_once("../layout/header.php"); 
?> 
<!--header end--> 
<!--sidebar start--> 
<?php require_once("../layout/navigation.php");?> 
<!--sidebar end-->  
<!--main content start--> 
<section id="main-content"> 
  <section class="wrapper site-min-height"> 
    <h4>Indoors Employees</h4> 
    <!-- page start--> 
    <section class="panel"> 
      <header class="panel-heading">View Indoors Employees </header> 
      <br> 
      <button type="button" class="btn btn-danger" style="margin-left:15px" onClick="window.location.href = 'insert.php'" <?php 
	  $module_name = $opened_url_parts[count($opened_url_parts) - 2]; 
	  $opened_module_page_insert = $module_name.'/insert'; 
      if(!in_array($opened_module_page_insert, $user_allowed_page_array)){ 
	  	echo "disabled"; 
	  } 
	  ?>> 
      <li class="icon-plus-sign"></li>Add New Employee </button> 
     <br> 
      <div class="panel-body"> 
        <?php if($count_relation_tables  != 0){  
		        echo" <div id='delete_error'><p> Can't delete because this user  related to   </p><ul>"; 
				foreach($relation_tables as $table){ 
					echo"<li>{$table}</li>"; 
			    } 
				echo "</ul></div>"; 
			} 
		?> 
        <div class="adv-table editable-table ">  
          
          <div class="space15"></div> 
          <table class="table table-striped table-hover table-bordered" id="editable-sample"> 
            <thead> 
              <tr> 
                <th>#</th> 
                <th>Full Name</th> 
                <th>Email</th> 
                <th>Exhibtion</th> 
                <th>Title</th> 
                <th>Action</th> 
              </tr> 
            </thead> 
            <tbody> 
              <?php   
				  $serialize = 1; 
				  foreach($records as $record){ 
					  echo "<tr> 
					  <td>{$serialize}</td> 
					  <td><a href='full_info.php?id={$record->id}'>{$record->full_name()}</a></td> 
					  <td>{$record->email}</td> 
					  <td>{$record->exhibtion}</td> 
					  <td>{$record->title}</a></td> 
					<td>"; 
					$module_name = $opened_url_parts[count($opened_url_parts) - 2]; 
					include('../layout/btn_control.php');  
					//delete dialoge 
					echo "</td>	 
			 </tr> 
			     <div class='modal fade' id='my{$record->id}' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'> 
								  <div class='modal-dialog'> 
									  <div class='modal-content'> 
										  <div class='modal-header'> 
											  <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button> 
											  <h4 class='modal-title'>Delete</h4> 
										  </div> 
										  <div class='modal-body'> 
										   <p> Are you sure you want delete  $record->first_name $record->last_name ?</p> 
										  </div> 
										  <div class='modal-footer'> 
											  <button class='btn btn-warning' type='button'  
											  onClick=\"window.location.href = 'view.php?task=delete&id={$record->id}'\"/>Confirm</button>  <button data-dismiss='modal' class='btn btn-default' type='button'>cancel</button> 
										  </div> 
									  </div> 
								  </div> 
							  </div>"; 
  				$serialize++; 
				  } 
				  ?> 
            </tbody> 
          </table> 
        </div> 
      </div> 
    </section> 
    <!-- page end-->  
  </section> 
</section> 

</div> 
<!--main content end-->  
<!--footer start--> 
<?php require_once("../layout/footer.php");?>
<?php  
	require_once("../layout/initialize.php"); 
	// define user class 
	$define_class = new Points(); 
	
	$count_relation_tables = ""; 
	//POST info 
	//filter data 

	
//get filter data 
	$id = $_GET['id'] ;  
	$records = $define_class->find_all_by_custom_filed('customer_id', $id);
	// delete record check 
    
             require_once("../layout/header.php"); 
?> 
<!--header end--> 
<!--sidebar start--> 
<?php require_once("../layout/navigation.php");?> 
<!--sidebar end-->  
<!--main content start--> 
<section id="main-content"> 
  <section class="wrapper site-min-height"> 
    <h4>Hamasa Points System</h4> 
    <!-- page start--> 
    <section class="panel"> 
      <header class="panel-heading">View Points Module </header> 
      <br> 
      
     <br> 
      <div class="panel-body"> 
        <?php if($count_relation_tables  != 0){  
		        echo" <div id='delete_error'><p> Can't delete because this user  related to   </p><ul>"; 
				foreach($relation_tables as $table){ 
					echo"<li>{$table}</li>"; 
			    } 
				echo "</ul></div>"; 
			} 
		?> 
        <div class="adv-table editable-table ">  
          
          <div class="space15"></div> 
          <table class="table table-striped table-hover table-bordered" id="editable-sample"> 
            <thead> 
              <tr> 
                <th>#</th> 
                <th>Point Type</th> 
                <th>Numbers Gained</th> 
                <th>Date</th> 
                <th>Action</th> 
              </tr> 
            </thead> 
            <tbody> 
              <?php   
				  $serialize = 1; 
				  foreach($records as $record){ 
					  echo "<tr> 
					  <td>{$serialize}</td> 
					  <td>{$record->type}</a></td> 
					  <td>{$record->number}</td> 
					  <td>{$record->date}</td> 
					  
					<td>"; 
					$module_name = $opened_url_parts[count($opened_url_parts) - 2]; 
					//include('../layout/btn_control.php'); 
					echo "&nbsp;<a href='update_points.php?id=$record->id' class='btn btn-success btn-xs tooltips' 
							data-placement='top' data-toggle='tooltip' data-original-title='Update Point' ><i class='    	                            icon-edit'></i></a>&nbsp;" ; 

					echo "&nbsp;<a href='#'  class='btn btn-success btn-xs tooltips' 
						  data-placement='top' data-toggle='tooltip' data-original-title='Delete Point ' > <i                          class='icon-remove'></i></a>&nbsp;" ;
					
					//delete dialoge 
					echo "</td>	 
			 </tr> 
			     <div class='modal fade' id='my{$record->id}' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'> 
								  <div class='modal-dialog'> 
									  <div class='modal-content'> 
										  <div class='modal-header'> 
											  <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button> 
											  <h4 class='modal-title'>Delete</h4> 
										  </div> 
										  <div class='modal-body'> 
										   <p> Are you sure you want delete  $record->type ?</p> 
										  </div> 
										  <div class='modal-footer'> 
											  <button class='btn btn-warning' type='button'  
											  onClick=\"window.location.href = 'view.php?task=delete&id={$record->id}'\"/>Confirm</button>  <button data-dismiss='modal' class='btn btn-default' type='button'>cancel</button> 
										  </div> 
									  </div> 
								  </div> 
							  </div>"; 
  				$serialize++; 
				  } 
				  ?> 
            </tbody> 
          </table> 
        </div> 
      </div> 
    </section> 
    <!-- page end-->  
  </section> 
</section> 

</div> 
<!--main content end-->  
<!--footer start--> 
<?php require_once("../layout/footer.php");?>
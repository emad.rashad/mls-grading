<?php  
	require_once("../layout/initialize.php"); 
	if(isset($_GET['id']) && is_numeric($_GET['id'])){ 
		$record_id = $_GET['id']; 
		$record_info = Customers::find_by_custom_filed('id', $record_id); 
		$profiles = Profile::find_all();	 
		//check id access 
		if(empty($record_info->id)){ 
			redirect_to('view.php');	 
		} 
		if($user_profile->global_edit != 'all_records' ){ 
		  redirect_to('view.php');	 
		} 
	}else{ 
		redirect_to('view.php');	 
	} 
	require_once("../layout/header.php");	 
 ?>
<script type="text/javascript" src="../../js-crud/crud_customer.js"></script>
<!--header end-->
<!--sidebar start-->
<?php require_once("../layout/navigation.php");?>
<!--sidebar end--> 
<!--main content start-->
<section id="main-content">
  <section class="wrapper site-min-height">
    <h4>Emplyee  Module</h4>
    <!-- page start-->
    <div class="row">
      <aside class="col-lg-8">
        <section>
          <div class="panel">
            <div class="panel-heading"> Edit Indoors Employee : <?php echo $record_info->full_name() ;  ?></div>
            <div class="panel-body">
              <form class="form-horizontal tasi-form" role="form" id="form_crud" action="data_model/update.php">
                <input type="hidden" id="process_type" value="update">
                <input type="hidden" id="record" value="<?php echo $record_id ; ?>">
                <div class="form-group">
                  <label  class="col-lg-2">First Name</label>
                  <div class="col-lg-8">
                    <input type="text" class="form-control" id="first_name"  value="<?php echo $record_info->first_name ; ?>" name="lastname" autocomplete="off">
                  </div>
                </div>
                
                <div class="form-group">
                  <label  class="col-lg-2">Last Name</label>
                  <div class="col-lg-8">
                    <input type="text" class="form-control" id="last_name"  value="<?php echo $record_info->last_name ; ?>"                           name="lastname" autocomplete="off">
                  </div>
                </div>
                
                
                
                
                
                <div class="form-group">
                  <label  class="col-lg-2">Password</label>
                  <div class="col-lg-8">
                    <input type="password" class="form-control" id="password" placeholder="كلمه المرور " autocomplete="off" name="password" value="<?php echo $record_info->password ; ?>">
                  </div>
                </div>
                
                <div class="form-group">
                  <label  class="col-lg-2">Email</label>
                  <div class="col-lg-8">
                    <input type="text" class="form-control" id="email" placeholder=" البريد الالكترونى " autocomplete="off" name="email" value="<?php echo  $record_info->email; ?>">
                  </div>
                </div>
             
                
                <div class="form-group">
                  <label  class="col-lg-2">Gender</label>
                  <div class="col-lg-8">
                    <select name="gender" id="gender_selected" class="form-control" >
                      <option value="" name="gn" disabled="" >اختر النوع </option>
                      <option value="زكر" <?php if($record_info->gender == 'زكر'){echo 'selected' ; } ?> name="gn">زكر
                      </option>
                      <option value="انثى" <?php if($record_info->gender == 'انثى'){echo 'selected' ; } ?> name="gn">انثى
                      </option>
                    </select>
                  </div>
                </div>
                
                
                <div class="form-group">
                  <label  class="col-lg-2">Exhibtion </label>
                  <div class="col-lg-8">
                    <select name="level" class="form-control"  id="exhibtion_selected">
                      <option value="" name="lv" disabled="">اختر المعرض </option>
                      <option value="معرض مدينتى" <?php if($record_info->exhibtion == 'معرض مدينتى'){echo 'selected' ; } ?> name="lv">معرض مدينتى</option>
                      <option value="معرض الرحاب"  <?php if($record_info->exhibtion == 'معرض الرحاب'){echo 'selected' ; } ?> name="lv">معرض الرحاب</option>
                      <option value="معرض المستقبل"  <?php if($record_info->exhibtion == 'معرض المستقبل'){echo 'selected' ; } ?> name="lv">معرض المستقبل</option>
                      <option value="معرض الرمايه" <?php if($record_info->exhibtion == 'معرض الرمايه'){echo 'selected' ; } ?> name="lv">معرض الرمايه</option>
                      <option value="معرض الوثاق" <?php if($record_info->exhibtion == 'معرض الوثاق'){echo 'selected' ; } ?> name="lv">معرض الوثاق</option>
                    </select>
                  </div>
                </div>
                
                
                
                
                
                <div class="form-group">
                  <label  class="col-lg-2">Title </label>
                  <div class="col-lg-8">
                    <input type="text" class="form-control" id="title" placeholder=" " autocomplete="off" value = "<?php echo $record_info->title ; ?>">
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-lg-offset-2 col-lg-10">
                    <button type="submit" class="btn btn-info" id="submit">Save</button>
                    <button type="button" class=" btn btn-info "   
					 onClick="window.location.href = 'full_info.php?id='+<?php echo $record_id ; ?>" > <i class="icon-info-sign"></i> View Full Info </button>
                    <div id="loading_data"></div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </section>
      </aside>
    </div>
    <!-- page end--> 
  </section>
</section>
<!--main content end--> 
<!--footer start-->

<?php require_once("../layout/footer.php");?>
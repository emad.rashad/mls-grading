<?php  
	require_once("../layout/initialize.php"); 
			
	require_once("../layout/header.php");	 
?> 
<script type="text/javascript" src="../../js-crud/crud_customer.js"></script> 
  <!--header end-->  
  <!--sidebar start--> 
  <?php require_once("../layout/navigation.php");?> 
  <!--sidebar end-->  
  <!--main content start--> 
  <section id="main-content"> 
    <section class="wrapper site-min-height"> 
      <h4>Indoors  Employees</h4>    		   
      <!-- page start--> 
      <div class="row">      
        <aside class="col-lg-8"> 
          <section> 
            <div class="panel"> 
              <div class="panel-heading"> Add New Employee</div> 
              <div class="panel-body"> 
                <form class="form-horizontal tasi-form" role="form" id="form_crud" action="data_model/insert.php"> 
                <input type="hidden" id="process_type" value="insert"> 
                  <div class="form-group"> 
                    <label  class="col-lg-3">First Name</label> 
                    <div class="col-lg-8"> 
                      <input type="text" class="form-control" id="first_name" placeholder="الاسم الاول" autocomplete="off"> 
                    </div> 
                  </div> 
                  
                  <div class="form-group"> 
                    <label  class="col-lg-3">Last Name</label> 
                    <div class="col-lg-8"> 
                      <input type="text" class="form-control" id="last_name" placeholder="اسم العائله" autocomplete="off"> 
                    </div> 
                  </div>
                  
                  <div class="form-group"> 
                    <label  class="col-lg-3">Password</label> 
                    <div class="col-lg-8"> 
                      <input type="password" class="form-control" id="password" placeholder="كلمه المرور " autocomplete="off"> 
                    </div> 
                  </div>
                  
                 
                 <div class="form-group"> 
                    <label  class="col-lg-3">Confirm Password</label> 
                    <div class="col-lg-8"> 
                      <input type="password" class="form-control" id="cpassword" placeholder=" تاكيد كلمه المرور" autocomplete="off"> 
                    </div> 
                  </div>
                  
                  
                  
                  <div class="form-group"> 
                    <label  class="col-lg-3">Email</label> 
                    <div class="col-lg-8"> 
                      <input type="text" class="form-control" id="email" placeholder=" البريد الالكترونى " autocomplete="off"> 
                    </div> 
                  </div>
                  
                  
                  <div class="form-group"> 
                    <label  class="col-lg-3">Gender</label> 
                    <div class="col-lg-8"> 
                      <select name="gender" id="gender_selected" class="form-control" >
                            <option value="" name="gn" disabled="" selected>اختر النوع </option>
                             <option value="زكر" name="gn">زكر</option>
                              <option value="انثى" name="gn">انثى</option>
                      </select>
                    </div> 
                  </div> 
                   

                  <div class="form-group"> 
                    <label  class="col-lg-3">Exhibtion</label> 
                    <div class="col-lg-8"> 
                         <select name="exhibtion" class="form-control"  id="exhibtion_selected">
                            <option value="" name="lv" disabled selected>اختر المعرض </option>
                            <option value="معرض مدينتى" name="lv">معرض مدينتى</option>
                             <option value="معرض الرحاب" name="lv">معرض الرحاب</option>
                             <option value="معرض المستقبل" name="lv">معرض المستقبل</option>
                             <option value="معرض الرمايه" name="lv">معرض الرمايه</option>
                             <option value="معرض الميثاق" name="lv">معرض الميثاق</option>
                      </select>
                    </div> 
                  </div> 

                  <div class="form-group"> 
                    <label  class="col-lg-3">Title</label> 
                    <div class="col-lg-8"> 
                            <input type="text" class="form-control" id="title" placeholder="المسمى الوظيفى" autocomplete="off"> 
                    </div> 
                  </div> 


                  <div class="form-group"> 
                    <div class="col-lg-offset-2 col-lg-10"> 
                      <button type="submit" class="btn btn-info" id="submit">Save</button> 
                      <button type="reset" class="btn btn-default">Cancel</button> 
                      <div id="loading_data"></div> 
                    </div> 
                  </div> 
                </form> 
              </div> 
            </div> 
          </section> 
        </aside> 
       
      </div> 
      <!-- page end-->  
    </section> 
  </section> 
  <!--main content end-->  
  <!--footer start--> 
  <?php require_once("../layout/footer.php");?>
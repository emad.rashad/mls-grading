<?php  
	require_once("../layout/initialize.php"); 
			
	require_once("../layout/header.php");	 

 $id = $_GET['id'] ; 
 $record_info = Customers::find_by_id($id) ; 
 
 
?> 
<script type="text/javascript" src="../../js-crud/js-crud-points.js"></script> 
  <!--header end-->  
  <!--sidebar start--> 
  <?php require_once("../layout/navigation.php");?> 
  <!--sidebar end-->  
  <!--main content start--> 

  <section id="main-content"> 
  
    <section class="wrapper site-min-height"> 
      <h4>Hamasa Customers</h4>    		   
      <!-- page start--> 
      <div class="row">      
        <aside class="col-lg-8"> 
          <section> 
            <div class="panel"> 
              <div class="panel-heading"> Add Points to : <?php

               echo $record_info->userName ; ?>  

               </div> 
              <div class="panel-body"> 
                <form class="form-horizontal tasi-form" role="form" id="form_add_points" action="data_model/add-point.php"> 
                <input type="hidden" id="process_type" value="insert"> 
                <input type="hidden" id="customer_id" value="<?php echo $id ; ?>"> 
                   
                   <div class="form-group"> 
                    <label  class="col-lg-3">Point Type </label> 
                    <div class="col-lg-8"> 
                      <select name="gender" id="points" class="form-control" >
                              <option value="" name="gn" disabled="" selected>اختر النوع </option>
                             <option value="Share" name="gn" >شير للصفحة 25 حصة</option>
                              <option value="Like" name="gn" >لايك للصفحة 25 حصة</option>
                              <option value="Invite" name="gn" >دعوه اصدقاء 30 حصه</option>
                              <option value="Share-post" name="gn">شير للبوست 10 حصص</option>
                              <option value="like-post" name="gn" >لايك للبوست 10 حصص</option>
                              <option value="Comment" name="gn">تعليق علي بوست 5 حصص</option>
                      </select>
                    </div> 
                  </div> 

                 <div class="form-group"> 
                    <label  class="col-lg-3">Number</label> 
                    <div class="col-lg-8"> 
                      <input type="text" class="form-control" id="number" readonly="" value="" placeholder="عدد النقط" autocomplete="off"> 
                    </div> 
                  </div> 
                 
                

                  <div class="form-group"> 
                    <div class="col-lg-offset-2 col-lg-10"> 
                      <button type="submit" class="btn btn-info" id="submit">Save</button> 
                      <button type="reset" class="btn btn-default">Cancel</button> 
                      <div id="loading_data"></div> 
                    </div> 
                  </div> 
                </form> 
              </div> 
            </div> 
          </section> 
        </aside> 
       
      </div> 
      <!-- page end-->  
    </section> 
  </section> 
  <!--main content end-->  
  <!--footer start--> 
  <?php require_once("../layout/footer.php");?>
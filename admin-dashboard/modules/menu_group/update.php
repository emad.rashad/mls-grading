<?php 
	require_once("../layout/initialize.php"); 
	if(isset($_GET['id']) && is_numeric($_GET['id'])){ 
		$record_id = $_GET["id"]; 
		$record_info = MenuGroup::find_by_id($record_id); 
		//check id access 
		if(empty($record_info->id)){ 
			redirect_to("view.php"); 
		} 
		//check globel edit authority 
		if($user_profile->global_edit != 'all_records' && $record_info->inserted_by != $user_data->id ){ 
		  redirect_to('view.php');	 
	    }  
	}else{ 
		redirect_to("view.php"); 
	} 
	require_once("../layout/header.php"); 
?> 
<script type="text/javascript" src="../../js-crud/menu_group.js"></script> 
<!--header end--> 
<!--sidebar start--> 
<?php require_once("../layout/navigation.php");?> 
<!--sidebar end-->  
<!--main content start--> 
<section id="main-content"> 
  <section class="wrapper site-min-height"> 
    <h4> Structure Menu Group  Module</h4> 
    <div class="row"> 
      <aside class="col-lg-8"> 
        <section> 
          <div class="panel"> 
            <div class="panel-heading"> Edit Menu Group</div> 
            <div class="panel-body"> <strong> 
              <form class="form-horizontal tasi-form" role="form" id="form_crud" action="data_model/update.php"> 
                <input type="hidden" id="process_type" value="update"> 
                <input type="hidden" id="record" value="<?php  echo $record_id; ?>"> 
                <div class="form-group"> 
                  <label  class="col-lg-2 ">Title:</label> 
                  <div class="col-lg-8"> 
                    <input type="text" class="form-control" id="title" placeholder="" value="<?php echo $record_info->title;?>" autocomplete="off" 
                       onchange="add_char('title','alias');"> 
                  </div> 
                </div> 
                <div class="form-group"> 
                  <label  class="col-lg-2">Alias:</label> 
                  <div class="col-lg-8"> 
                    <input type="text" class="form-control" id="alias" value="<?php echo $record_info->alias;?>" autocomplete="off"> 
                  </div> 
                </div> 
                <div class="form-group"> 
                  <label  class="col-lg-2 ">Description:</label> 
                  <div class="col-lg-8"> 
                    <textarea class="wysihtml5 form-control" id="description" placeholder=" "  
                      rows="10"><?php echo $record_info->description;?></textarea> 
                  </div> 
                </div> 
                <div class="form-group"> 
                  <div class="col-lg-offset-2 col-lg-10"> 
                    <button type="submit" class="btn btn-info" id="submit">Save</button> 
                    <button type="button" class=" btn btn-info "   
                    onClick="window.location.href = 'full_info.php?id='+<?php echo $record_id?>" > <i class="icon-info-sign"></i> View Full Info </button> 
                    <div id="loading_data"></div> 
                  </div> 
                </div> 
              </form> 
            </div> 
          </div> 
        </section> 
      </aside> 
      <div class="col-lg-4"> 
        <section class="panel panel-primary"> 
          <header class="panel-heading">  Included Images :</header> 
          <div class="panel-body"> 
            <form class="form-horizontal tasi-form" role="form" id="form_image"> 
            
                <div class="form-group"> 
                  <label  class="col-lg-4 ">Image Cover:</label> 
                  <div class="col-lg-6"> <a href="../media_filemanager_menu/view_media_directories.php?type=group" id="image_cover">Select Image</a> <br /> 
                    <br /> 
                    <input type="hidden" class="form-control" id="imageVal" placeholder=" " autocomplete="off" value="<?php echo $record_info->image?>"> 
                    <div  id="imageShow"> 
                     <img src="../../../media-library/group/<?php echo $record_info->image?>" id="imageSrc" style="width:80px; height:80px;"></div> 
                  </div> 
                </div> 
              
            </form> 
          </div> 
        </section> 
      </div> 
    </div> 
     
    <!-- page end-->  
  </section> 
</section> 
<!--main content end-->  
<!--footer start--> 
<?php require_once("../layout/footer.php");?>
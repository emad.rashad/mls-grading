<?php 
	 require_once("../layout/initialize.php"); 
	//get directory data 
	$directory_id = $_GET['dir_id']; 
	$get_directory_data = MediaLibraryDirectories::find_by_id($directory_id); 
	//get file data 
	$file_id = $_GET["id"]; 
	$define_class = new MediaLibraryFiles();	 
	$record_info = $define_class->media_library_files_data(null,null,$file_id); 
	require_once("../layout/header.php"); 
?> 
  <script type="text/javascript" src="../../js-crud/crud_media_file.js"></script>  
  <!--header end-->  
  <!--sidebar start--> 
  <?php require_once("../layout/navigation.php");?> 
  <!--sidebar end-->  
  <!--main content start--> 
  <section id="main-content"> 
    <section class="wrapper site-min-height"> 
      <h4>Media Library Module</h4> 
      <!-- page start--> 
      <div class="row"> 
        <aside class="profile-info col-lg-8"> 
          <section> 
            <div class="panel"> 
              <div class="panel-heading"> Edit File</div> 
              <div class="panel-body"> 
                <form class="form-horizontal tasi-form" role="form" id="form_crud" action="data_model/update.php"> 
                  <input type="hidden" id="process_type" value="update"> 
                  <input type="hidden" id="record" value="<?php echo $record_info->id?>"> 
                  <input type="hidden" id="dir_id" value="<?php echo $directory_id?>"> 
                  <div class="form-group"> 
                    <label  class="col-lg-3">File Name:</label> 
                    <div class="col-lg-7"> 
                      <input type="text" class="form-control" id="file_name" value="<?php echo $record_info->name?>" autocomplete="off"> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                    <label  class="col-lg-3">Source File:</label> 
                    <div class="col-lg-6"> <?php echo "<img src='../../../media-library/{$get_directory_data->title}/{$record_info->name}.{$record_info->type}' style='width:90%; height:80%;'>";?></div> 
                  </div> 
                  <div class="form-group"> 
                    <label  class="col-lg-3">Alternative Text:</label> 
                    <div class="col-lg-7"> 
                      <input type="text" class="form-control" id="alt_text" value="<?php echo $record_info->alternative_text?>" autocomplete="off"> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                    <label  class="col-lg-3">Description:</label> 
                    <div class="col-lg-7"> 
                      <textarea class="wysihtml5 form-control" rows= "5" id="description"><?php echo $record_info->description?></textarea> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                    <div class="col-lg-offset-2 col-lg-10"> 
                      <button type="submit" class="btn btn-info" id="submit">Save</button> 
                      <button type="button" class="btn btn-info"   
                        onClick="window.location.href = 'full_info.php?id='+<?php echo $file_id?>+'&dir_id='+<?php echo $directory_id?>" >View Full Info </button> 
                      <div id="loading_data"></div> 
                    </div> 
                  </div> 
                </form> 
              </div> 
            </div> 
          </section> 
        </aside> 
        <div class="col-lg-3"> 
          <section class="panel panel-primary"> 
            <header class="panel-heading"> File Information: </header> 
            <div class="panel-body"> 
              <div id="image_info"> 
                <ul> 
                  <li><span style="color:#428bca">> File Name:</span> <?php echo $record_info->name.".".$record_info->type?></li> 
                  <li><span style="color:#428bca">> File Type:</span> <?php echo $record_info->type?></li> 
                  <li><span style="color:#428bca">> File Size:</span> <?php echo formatBytes($record_info->size)?></li> 
                  <li><span style="color:#428bca">> Created By:</span> <?php echo $record_info->inserted_by?></li> 
                  <li><span style="color:#428bca">> Created Date:</span> <?php echo $record_info->inserted_date?></li> 
                  <li><span style="color:#428bca">> Last Update By:</span> <?php echo $record_info->updated_by?></li> 
                  <li><span style="color:#428bca">> Last Update Date:</span> <?php echo $record_info->last_update?></li> 
                </ul> 
              </div> 
            </div> 
          </section> 
        </div> 
      </div> 
      <!-- page end-->  
    </section> 
  </section> 
  <!--main content end-->  
  <!--footer start--> 
  <?php require_once("../layout/footer.php");?>
<?php 
require_once('../../../../classes/Session.php'); 
require_once('../../../../classes/Functions.php'); 
require_once('../../../../classes/MysqlDatabase.php'); 
require_once('../../../../classes/PollQuestions.php'); 
//check log in  
if($session->is_logged() == false){ 
	redirect_to("../../index.php"); 
} 
if(!empty($_GET["task"]) && $_GET["task"] == "delete"){ 
	//get data 
	$id = $_GET['id']; 
	//find record	 
	$find_poll = PollQuestions::find_by_id($id); 
	//if there is record perform delete 
	//if there is no record go back to view 
	if($find_poll){ 
		$delete = $find_poll->delete(); 
		if($delete){ 
			//delete all related answers  
			$sql_delete_tags = "DELETE FROM poll_questions_options WHERE poll_id = '{$id}'"; 
			$preform_delete_tags = $database->query($sql_delete_tags); 
				redirect_to("../view.php"); 
		}else{ 
				redirect_to("../view.php"); 
		}	 
		//if there is no record go back to view 
	}else{ 
		redirect_to("../view.php");	 
	}  
}else{ 
	//if task wasnot delete go back to view 
	redirect_to("../view.php");	 
} 
//close connection 
if(isset($database)){ 
	$database->close_connection(); 
} 
?>
<?php 
require_once('../../../../classes/Session.php'); 
require_once('../../../../classes/Functions.php'); 
require_once('../../../../classes/MysqlDatabase.php'); 
require_once('../../../../classes/PollQuestionsOptions.php'); 
//check log in  

if(!empty($_POST["task"]) && $_POST["task"] == "insert"){ 
	$add = new PollQuestionsOptions(); 
	$add->poll_option = $_POST["option"]; 
	$add->inserted_by  =$session->user_id; 
	$add->inserted_date = date_now(); 
	$add->poll_id = $_POST["poll_id"]; 
	$insert = $add->insert(); 
 	header('Content-Type: application/json'); 
	if($insert){ 
		$data  = array("status"=>"work"); 
		echo json_encode($data); 
	}else{ 
		$data  = array("status"=>"error"); 
		echo json_encode($data); 
	}	 
} 
//close connection 
if(isset($database)){ 
	$database->close_connection(); 
} 
?>
<?php 
	require_once("../layout/initialize.php"); 
    require_once("../layout/header.php"); 
    require_once("../layout/header.php"); 
    require_once("../../../classes/MidPrimaryOne.php"); 
?> 
<!--header end--> 
<!--sidebar start--> 
<?php require_once("../layout/navigation.php");?> 
<!--sidebar end-->  
<!--main content start--> 
<section id="main-content"> 
 <!-- <script src="../../js-crud/upload_mid_term.js"></script> -->
  <section class="wrapper site-min-height"> 
    <h4>MLS - Grading System</h4> 
    <!-- page start--> 
    <section class="panel"> 
      <header class="panel-heading">Mid Term Results Module</header> 
      <br> 
     

     
		</div>

     <br> 
      <div class="panel-body" style="height:300px; "> 
       
        <div class="adv-table editable-table ">  
          
          <div class="space15">



          <?php 

        		if(isset($_POST['submit'])){


        			$file_info = $_FILES['uploadResult']['tmp_name'] ; 
        			echo $file_info ; 
						// check open status : 
						$handle_file = fopen($file_info,'r') ; 
						// loop through the file  : 
						
						while (($file_opened = fgetcsv($handle_file,1000,',')) != false ) {
							# code...
							$general_mid = new MidPrimaryOne() ; 
							$general_mid->student_code = $file_opened[0] ; 
							$general_mid->subjects     = $file_opened[1] ; 
							$general_mid->max          = $file_opened[2] ; 
							$general_mid->marks        = $file_opened[3] ; 
							$general_mid->class        = $file_opened[4] ; 
							$general_mid->term         = "first_mid_term" ;  
							$general_mid->stage        = "second primary" ;  
							

							//echo $file_opened[0] ; 
							
							$insert_result = $general_mid->insert() ; 
						}

						if($insert_result){
							echo "<div id='message'>Data inserted</div>" ; 
						}else{
							echo "error" ; 
						}






        		}


        	?>

          		<form action="add_mid_term.php" method="post" class="" id="form_mid_term" enctype="multipart/form-data">
          			
          		<input type="hidden" id="process_type" value="mid_term">
          		

          		
				 <div class="form-group"> 
                          <label  class="col-lg-2 ">Mid-Term Type</label> 
                          <div class="col-lg-8"> 
                            <select class="form-control" id="mid_term"> 
                              <option value="" disabled selected>Select Mid-Term Type</option> 
                              <option name="mt" value="first_mid_term">First Mid-Term</option> 
                              <option name="mt" value="second_mid_term">Second Mid-Term</option> 
                               
                            </select> 
                          </div> 
                 </div>

                 <br>
                 <br>
                 <br>

                  <div class="form-group"> 
                          <label  class="col-lg-2 ">Stage Selection</label> 
                          <div class="col-lg-8"> 
                            <select class="form-control" id="stage"> 
                              <option value="" disabled selected>Select Stage</option> 
                              <option name="st" value="second_primary">Second Primary</option> 
                              <option name="st" value="third_primary">Third Primary</option> 
                              <option name="st" value="fourth_primary">Fourth Primary</option> 
                              <option name="st" value="fifth_primary">Fifth Primary</option> 
                              <option name="st" value="first_prep">First Preparatory</option> 
                              <option name="st" value="second_prep">Second Preparatory</option> 
                              <option name="st" value="first_secondary">First Secondary</option> 
                              <option name="st" value="second_secondary_science">Second Secondary (Science Section)</option> 
                              <option name="st" value="second_secondary_art">Second Secondary (Art Section)</option> 
                               
                            </select> 
                          </div> 
                 </div>
			     <br>
                 <br>
                 <br>
                 	 <div class="form-group">
                 	 	 <label  class="col-lg-2 ">Select Result Sheet</label> 
						<input type="file" style="margin-left:198px;" name="uploadResult" id="uploadResult">
					</div>
				 <br>
                 

          		 <div style="margin-top:20px; padding-left:5px;">
			      <button type="submit" name="submit" class="btn btn-danger" id="submit" style="margin-left:192px;"> 
			      <li class="icon-circle-arrow-right" style="padding-right: 5px;"></li>Click To Upload Results</button> 
          		</div> 

          		
        	</form>

        	
        </div> 
      </div> 
    </section> 
    <!-- page end-->  
  </section> 
</section> 

</div> 
<!--main content end-->  
<!--footer start--> 
<?php require_once("../layout/footer.php");?>
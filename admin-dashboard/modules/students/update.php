<?php  
	require_once("../layout/initialize.php"); 
	if(isset($_GET['id']) && is_numeric($_GET['id'])){ 
		$record_id = $_GET['id']; 
		$record_info = Students::find_by_custom_filed('id', $record_id); 
		$profiles = Profile::find_all();	 
		//check id access 
		if(empty($record_info->id)){ 
			redirect_to('view.php');	 
		} 
		if($user_profile->global_edit != 'all_records' ){ 
		  redirect_to('view.php');	 
		} 
	}else{ 
		redirect_to('view.php');	 
	} 
	require_once("../layout/header.php");	 
 ?>
<script type="text/javascript" src="../../js-crud/add_new_student.js"></script>
<!--header end-->
<!--sidebar start-->
<?php require_once("../layout/navigation.php");?>
<!--sidebar end--> 
<!--main content start-->
<section id="main-content">
  <section class="wrapper site-min-height">
    <h4>MLS - Students  Module</h4>
    <!-- page start-->
    <div class="row">
      <aside class="col-lg-8">
         <section> 
            <div class="panel"> 
              <div class="panel-heading"> Add New Student</div> 
              <div class="panel-body"> 
                <form class="form-horizontal tasi-form" role="form" id="form_crud" action="data_model/update.php"> 
                <input type="hidden" id="process_type" value="update"> 
                <input type="hidden" id="record" value="<?php echo $record_info->id?>"> 
                  <div class="form-group"> 
                    <label  class="col-lg-3">Student Code</label> 
                    <div class="col-lg-8"> 
                      <input type="text" class="form-control" id="student_code" placeholder="student code" autocomplete="off" required oninvalid="this.setCustomValidity('Please Enter Student Code')" onchange="this.setCustomValidity('')" value="<?php echo  $record_info->student_code ; ?>" readonly> 
                    </div> 
                  </div> 
                  
                  <div class="form-group"> 
                    <label  class="col-lg-3">Student Name</label> 
                    <div class="col-lg-8"> 
                      <input type="text" class="form-control" id="student_name" placeholder="student name" autocomplete="off"required oninvalid="this.setCustomValidity('Please Enter Student Name')" onchange="this.setCustomValidity('')" value="<?php echo  $record_info->student_name ; ?>"> 
                    </div> 
                  </div>
                  
                  <div class="form-group"> 
                    <label  class="col-lg-3">Student Password</label> 
                    <div class="col-lg-8"> 
                      <input type="password" class="form-control" id="student_password" placeholder="student password" autocomplete="off" required oninvalid="this.setCustomValidity('Please Enter Password')" onchange="this.setCustomValidity('')" value="<?php echo $record_info->student_password ;?>"> 
                    </div> 
                  </div>
                  
                 
                 
                  
                  
                  
                  <div class="form-group"> 
                    <label  class="col-lg-3">Student Email</label> 
                    <div class="col-lg-8"> 
                      <input type="text" class="form-control" id="student_email" placeholder="student email" autocomplete="off" required oninvalid="this.setCustomValidity('Please Enter Student Email')" onchange="this.setCustomValidity('')" value="<?php echo  $record_info->student_email ; ?>"> 
                    </div> 
                  </div>
                  
                  
                  <div class="form-group"> 
                    <label  class="col-lg-3">Stage</label> 
                    <div class="col-lg-8"> 
                      <select name="stage" id="stage_selected" class="form-control" required  oninvalid="this.setCustomValidity('Please Select Student Stage')" onchange="this.setCustomValidity('')">

                             <option value="" name="st" disabled="" selected>Select Stage</option>
                             <option value="second_primary" name="st" <?php if($record_info->student_stage == 'second_primary'){echo 'selected' ; } ?> >Second Primary</option>
                             <option value="third_primary" name="st" <?php if($record_info->student_stage  == 'third_primary'){echo 'selected' ; } ?> >Third Primary</option>
                             <option value="fourth_primary" name="st" <?php if($record_info->student_stage  == 'fourth_primary'){echo 'selected' ; } ?> >Fourth Primary</option>
                             <option value="fifth_primary" name="st" <?php if($record_info->student_stage  == 'fifth_primary'){echo 'selected' ; } ?> >Fifth Primary</option>
                             <option value="first_prep" name="st"  <?php if($record_info->student_stage  == 'first_prep'){echo 'selected' ; } ?> >First Preparatory</option>
                             <option value="second_prep" name="st" <?php if($record_info->student_stage  == 'second_prep'){echo 'selected' ; } ?> >Second Preparatory</option>
                             <option value="first_secondary" name="st"  <?php if($record_info->student_stage  == 'first_secondary'){echo 'selected' ; } ?> >First Secondary</option>
                             <option value="second_secondary_art" name="st" <?php if($record_info->student_stage  == 'second_secondary_art'){echo 'selected' ; } ?> >Second Secondary - Art Section</option>
                             <option value="second_secondary_science" name="st" <?php if($record_info->student_stage  == 'second_secondary_science'){echo 'selected' ; } ?> >Second Secondary - Science Section</option>
                            
                      </select>
                    </div> 
                  </div> 
                   

                  <div class="form-group"> 
                    <label  class="col-lg-3">Class</label> 
                    <div class="col-lg-8"> 
                         <select name="class" class="form-control"  id="class_selected" required  oninvalid="this.setCustomValidity('Please Select Student Class')" onchange="this.setCustomValidity('')">
                             <option value="" name="cl" disabled selected>Select Class</option>
                             <option value="a" name="cl" <?php if($record_info->student_class == 'a'){echo 'selected' ; } ?> >A</option>
                             <option value="b" name="cl" <?php if($record_info->student_class == 'b'){echo 'selected' ; } ?> >B</option>
                             <option value="c" name="cl" <?php if($record_info->student_class == 'c'){echo 'selected' ; } ?>>C</option>
                             <option value="d" name="cl" <?php if($record_info->student_class == 'c'){echo 'selected' ; } ?> >D</option>
                              
                      </select>
                    </div> 
                  </div> 

                  <div class="form-group"> 
                    <label  class="col-lg-3">Seat Number</label> 
                    <div class="col-lg-8"> 
                            <input type="text" class="form-control" id="seat_number" placeholder="seat number" autocomplete="off" required  oninvalid="this.setCustomValidity('Please Enter student seat number')" onchange="this.setCustomValidity('')" value="<?php echo $record_info->seat_number ; ?>"> 
                    </div> 
                  </div> 


                  <div class="form-group"> 
                    <div class="col-lg-offset-2 col-lg-10"> 
                      <button type="submit" class="btn btn-info" id="submit">Save</button> 
                      <button type="button" class=" btn btn-info "   
					 onClick="window.location.href = 'full_info.php?id='+<?php echo $record_id ; ?>" > <i class="icon-info-sign"></i> View Full Info </button>
                      <br>
                      <div id="loading_data"></div> 
                    </div> 
                  </div> 
                </form> 
              </div> 
            </div> 
          </section>
      </aside>
    </div>
    <!-- page end--> 
  </section>
</section>
<!--main content end--> 
<!--footer start-->

<?php require_once("../layout/footer.php");?>
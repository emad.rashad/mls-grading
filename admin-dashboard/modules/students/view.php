<?php  
	require_once("../layout/initialize.php"); 
	// define student class

	$student = new Students();  
  $student->enable_relation(); 
	 

    $relation_tables = array(); 
    $count_relation_tables = ""; 

if(isset($_GET["id"]) && $_GET["task"]=="delete"){ 
			$id = $_GET['id']; 
			 
			$count_relation_tables = count($relation_tables); 
			if($count_relation_tables  == 0){  
				redirect_to("data_model/delete.php?task=delete&id=$id");	 
			}
		} 

    $all_students = $student::find_all() ;

	require_once("../layout/header.php"); 
?> 
<!--header end--> 
<!--sidebar start--> 
<?php require_once("../layout/navigation.php");?> 
<!--sidebar end-->  
<!--main content start--> 
<section id="main-content"> 
  <section class="wrapper site-min-height"> 
    <h4>Students Module</h4> 
    <!-- page start--> 
    <section class="panel"> 
      <header class="panel-heading"> View Students </header> 
      <br> 
      <button type="button" class="btn btn-danger" style="margin-left:15px" onClick="window.location.href = 'insert.php'" <?php 
	  $module_name = $opened_url_parts[count($opened_url_parts) - 2]; 
	  $opened_module_page_insert = $module_name.'/insert'; 
      if(!in_array($opened_module_page_insert, $user_allowed_page_array)){ 
	  	echo "disabled"; 
	  } 
	  ?>> 
      <li class="icon-plus-sign"></li>Add New Student </button> 
      <br> 
      <div class="panel-body"> 
        
        <div class="adv-table editable-table ">  
          <!--<div class="clearfix"> 
                              <div class="btn-group"> 
                                  <button id="editable-sample_new" class="btn green"> 
                                      Add New <i class="icon-plus"></i> 
                                  </button> 
                              </div> 
                              <div class="btn-group pull-right"> 
                                  <button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="icon-angle-down"></i> 
                                  </button> 
                                  <ul class="dropdown-menu pull-right"> 
                                      <li><a href="#">Print</a></li> 
                                      <li><a href="#">Save as PDF</a></li> 
                                      <li><a href="#">Export to Excel</a></li> 
                                  </ul> 
                              </div> 
                          </div>--> 
          <div class="space15"></div> 
          <table class="table table-striped table-hover table-bordered" id="editable-sample"> 
            <thead> 
              <tr> 
                <th>#</th> 
                <th>Student Code</th> 
                <th>Student Name</th> 
                <th>Email</th> 
								<th>Stage</th> 
                <th>Date Inserted</th> 
								<th>Last Update</th> 
                <th>Action</th> 
              </tr> 
            </thead> 
            <tbody> 
              <?php   
				  $serialize = 1; 
				  foreach($all_students as $record){ 
					  echo "<tr> 
					  <td>{$serialize}</td> 
					  <td><a href='full_info.php?id={$record->id}'>{$record->student_code}</a></td> 
						 <td>{$record->student_name}</td>
					  <td>{$record->student_email}</td> 
					  <td>{$record->student_stage}</td> 
						<td>{$record->insert_date}</td> 
						<td>{$record->last_update}</td>
					  
					<td>"; 
					$module_name = $opened_url_parts[count($opened_url_parts) - 2]; 
					include('../layout/btn_control.php'); 
					//delete dialoge 
					echo "</td>	 
			 </tr> 
			     <div class='modal fade' id='my{$record->id}' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'> 
								  <div class='modal-dialog'> 
									  <div class='modal-content'> 
										  <div class='modal-header'> 
											  <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button> 
											  <h4 class='modal-title'>Delete</h4> 
										  </div> 
										  <div class='modal-body'> 
										   <p> Are you sure you want delete  $record->student_name ?</p> 
										  </div> 
										  <div class='modal-footer'> 
											  <button class='btn btn-warning' type='button'  
											  onClick=\"window.location.href = 'view.php?task=delete&id={$record->id}'\"/>Confirm</button>  <button data-dismiss='modal' class='btn btn-default' type='button'>cancle</button> 
										  </div> 
									  </div> 
								  </div> 
							  </div>"; 
  				$serialize++; 
				  } 
				  ?> 
            </tbody> 
          </table> 
        </div> 
      </div> 
    </section> 
    <!-- page end-->  
  </section> 
</section> 

<!--main content end-->  
<!--footer start--> 
<?php require_once("../layout/footer.php");?>
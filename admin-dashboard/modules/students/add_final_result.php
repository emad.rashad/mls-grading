<?php 
	require_once("../layout/initialize.php"); 
  require_once("../layout/header.php"); 
  require_once("../layout/header.php"); 

?> 
<!--header end--> 
<!--sidebar start--> 
<?php require_once("../layout/navigation.php");?> 
<!--sidebar end-->  
<!--main content start--> 
<section id="main-content"> 
 <script src="../../js-crud/final_primary_prep.js"></script>
  <section class="wrapper site-min-height"> 
    <h4>MLS - Grading System</h4> 
    <!-- page start--> 
    <section class="panel"> 
      <header class="panel-heading">Final Results Module</header> 
      <br> 
     

     
		</div>

     <br> 
      <div class="panel-body" style="height:300px; "> 
       
        <div class="adv-table editable-table ">  
          
          <div class="space15">



         
          		<form action="data_model/add_final_result.php" method="post" class="add_csv_final" id="form_mid_term" enctype="multipart/form-data">
          			
          		<input type="hidden" id="process_type" value="final">
          		
                              <div class="form-group"> 
                                      <label  class="col-lg-2 ">Stage Selection</label> 
                                      <div class="col-lg-8"> 
                                        <select class="form-control" id="stage"> 
                                          <option value="" disabled selected>Select Stage</option> 
                                          <option name="st" value="second_primary">Second Primary</option> 
                                          <option name="st" value="third_primary">Third Primary</option> 
                                          <option name="st" value="fourth_primary">Fourth Primary</option> 
                                          <option name="st" value="fifth_primary">Fifth Primary</option> 
                                          <option name="st" value="first_prep">First Preparatory</option> 
                                          <option name="st" value="second_prep">Second Preparatory</option> 
                                          <option name="st" value="first_secondary">First Secondary</option> 
                                          <option name="st" value="second_secondary_science">Second Secondary (Science Section)</option> 
                                          <option name="st" value="second_secondary_art">Second Secondary (Art Section)</option> 
                                           
                                        </select> 
                                      </div> 
                             </div>
			               <br>
                     <br>
                     <br>
                 	 <div class="form-group">
                 	 	 <label  class="col-lg-2 ">Select Result Sheet</label> 
        						<input type="file" style="margin-left:290px;" name="uploadResult" id="uploadResult">
        				   </div>
        				     <br>
                 

                		 <div style="margin-top:20px; padding-left:5px;">
        		      <button type="submit" name="submit" class="btn btn-danger" id="submit" style="margin-left:282px;"> 
        		      <li class="icon-circle-arrow-right" style="padding-right: 5px;"></li>Click To Upload Results</button> 
                		</div> 
                  
                  <br>
                  <div class="form-group">
                  <div id="success_insert" style="margin-left:282px;"></div>
                  </div>
               
          		
        	     </form>




        	
        </div> 
      </div> 
    </section> 
    <!-- page end-->  
  </section> 
</section> 

</div> 
<!--main content end-->  
<!--footer start--> 
<?php require_once("../layout/footer.php");?>
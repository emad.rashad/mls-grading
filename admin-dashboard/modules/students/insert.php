<?php  
	require_once("../layout/initialize.php"); 
			
	require_once("../layout/header.php");	 
?> 
<script type="text/javascript" src="../../js-crud/add_new_student.js"></script> 
  <!--header end-->  
  <!--sidebar start--> 
  <?php require_once("../layout/navigation.php");?> 
  <!--sidebar end-->  
  <!--main content start--> 
  <section id="main-content"> 
    <section class="wrapper site-min-height"> 
      <h4>MLS Students </h4>    		   
      <!-- page start--> 
      <div class="row">      
        <aside class="col-lg-8"> 
          <section> 
            <div class="panel"> 
              <div class="panel-heading"> Add New Student</div> 
              <div class="panel-body"> 
                <form class="form-horizontal tasi-form" role="form" id="form_crud" action="data_model/insert.php"> 
                <input type="hidden" id="process_type" value="insert"> 
                  <div class="form-group"> 
                    <label  class="col-lg-3">Student Code</label> 
                    <div class="col-lg-8"> 
                      <input type="text" class="form-control" id="student_code" placeholder="student code" autocomplete="off" required oninvalid="this.setCustomValidity('Please Enter Student Code')" onchange="this.setCustomValidity('')"> 
                    </div> 
                  </div> 
                  
                  <div class="form-group"> 
                    <label  class="col-lg-3">Student Name</label> 
                    <div class="col-lg-8"> 
                      <input type="text" class="form-control" id="student_name" placeholder="student name" autocomplete="off"required oninvalid="this.setCustomValidity('Please Enter Student Name')" onchange="this.setCustomValidity('')"> 
                    </div> 
                  </div>
                  
                  <div class="form-group"> 
                    <label  class="col-lg-3">Student Password</label> 
                    <div class="col-lg-8"> 
                      <input type="password" class="form-control" id="student_password" placeholder="student password" autocomplete="off" required oninvalid="this.setCustomValidity('Please Enter Password')" onchange="this.setCustomValidity('')"> 
                    </div> 
                  </div>
                  
                 
                 <div class="form-group"> 
                    <label  class="col-lg-3">Confirm Password</label> 
                    <div class="col-lg-8"> 
                      <input type="password" class="form-control" id="student_cpassword" placeholder="password confirmation" autocomplete="off" required> 
                    </div> 
                  </div>
                  
                  
                  
                  <div class="form-group"> 
                    <label  class="col-lg-3">Student Email</label> 
                    <div class="col-lg-8"> 
                      <input type="text" class="form-control" id="student_email" placeholder="student email" autocomplete="off" required oninvalid="this.setCustomValidity('Please Enter Student Email')" onchange="this.setCustomValidity('')"> 
                    </div> 
                  </div>
                  
                  
                  <div class="form-group"> 
                    <label  class="col-lg-3">Stage</label> 
                    <div class="col-lg-8"> 
                      <select name="stage" id="stage_selected" class="form-control" required  oninvalid="this.setCustomValidity('Please Select Student Stage')" onchange="this.setCustomValidity('')">

                             <option value="" name="st" disabled="" selected>Select Stage</option>
                             <option value="second_primary" name="st">Second Primary</option>
                             <option value="third_primary" name="st">Third Primary</option>
                             <option value="fourth_primary" name="st">Fourth Primary</option>
                             <option value="fifth_primary" name="st">Fifth Primary</option>
                             <option value="first_prep" name="st">First Preparatory</option>
                             <option value="second_prep" name="st">Second Preparatory</option>
                             <option value="first_secondary" name="st">First Secondary</option>
                             <option value="second_secondary_art" name="st">Second Secondary - Art Section</option>
                             <option value="second_secondary_science" name="st">Second Secondary - Science Section</option>
                            
                      </select>
                    </div> 
                  </div> 
                   

                  <div class="form-group"> 
                    <label  class="col-lg-3">Class</label> 
                    <div class="col-lg-8"> 
                         <select name="class" class="form-control"  id="class_selected" required  oninvalid="this.setCustomValidity('Please Select Student Class')" onchange="this.setCustomValidity('')">
                             <option value="" name="cl" disabled selected>Select Class</option>
                             <option value="a" name="cl">A</option>
                             <option value="b" name="cl">B</option>
                             <option value="c" name="cl">C</option>
                             <option value="d" name="cl">D</option>
                              
                      </select>
                    </div> 
                  </div> 

                  <div class="form-group"> 
                    <label  class="col-lg-3">Seat Number</label> 
                    <div class="col-lg-8"> 
                            <input type="text" class="form-control" id="seat_number" placeholder="seat number" autocomplete="off" required  oninvalid="this.setCustomValidity('Please Enter student seat number')" onchange="this.setCustomValidity('')"> 
                    </div> 
                  </div> 


                  <div class="form-group"> 
                    <div class="col-lg-offset-2 col-lg-10"> 
                      <button type="submit" class="btn btn-info" id="submit">Save</button> 
                      <button type="reset" class="btn btn-default">Cancel</button> 
                      <br>
                      <div id="loading_data"></div> 
                    </div> 
                  </div> 
                </form> 
              </div> 
            </div> 
          </section> 
        </aside> 
       
      </div> 
      <!-- page end-->  
    </section> 
  </section> 
  <!--main content end-->  
  <!--footer start--> 

  <script>
 var  password         = document.getElementById("student_password") ; 
 var  confirm_password = document.getElementById("student_cpassword");

    function validatePassword(){
      if(password.value != confirm_password.value) {
        confirm_password.setCustomValidity("Passwords Don't Match");
      } else {
        confirm_password.setCustomValidity('');
      }
    }

    // password.onchange = validatePassword;
    confirm_password.onfocus = validatePassword;
    confirm_password.onkeyup = validatePassword;


  
  </script>
  <?php require_once("../layout/footer.php");?>
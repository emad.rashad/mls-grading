<?php  
	require_once("../layout/initialize.php"); 
	$record_id = $_GET['id']; 
	$define_class = new Students(); 
	
	$record_info = $define_class->find_by_custom_filed('id',$record_id);
	require_once("../layout/header.php"); 
	 
?> 
<script type="text/javascript" src="../../js-crud/add_new_student.js"></script> 
  <!--header end-->  
  <!--sidebar start--> 
  <?php require_once("../layout/navigation.php");?> 
  <!--sidebar end-->  
  <!--main content start--> 
  <section id="main-content"> 
    <section class="wrapper site-min-height"> 
      <h4>MLS - Student Full Info Module</h4>    		   
      <!-- page start--> 
      <div class="row"> 
        <aside class="col-lg-8"> 
          <section> 
            <div class="panel"> 
              <div class="panel-heading"> Student Info For :  <?php echo $record_info->student_name ;  ?> </div> 
              <div class="panel-body"> 
                <form class="form-horizontal tasi-form" role="form" id="form_crud" action="data_model/insert.php"> 
                <input type="hidden" id="process_type" value="insert"> 
                  <div class="form-group"> 
                    <label  class="col-lg-2">Student Code :</label> 
                    <div class="col-lg-6"> 
                    <?php echo $record_info->student_code ; ?> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                    <label  class="col-lg-2">Student Name :</label> 
                    <div class="col-lg-6"> 
                    <?php echo $record_info->student_name ;?> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                    <label  class="col-lg-2">Student Email</label> 
                    <div class="col-lg-6"> 
                    <?php echo $record_info->student_email  ; ?> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                    <label  class="col-lg-2">Student Stage :</label> 
                    <div class="col-lg-6"> 
                    <?php
                    if($record_info->student_stage == "second_primary"){
                      echo "Second Primary"; 
                    }else if($record_info->student_stage == "third_primary"){
                      echo "Third Primary"; 
                    }else if($record_info->student_stage == "fourth_primary"){
                      echo "Fourth Primary"; 
                    }else if($record_info->student_stage == "fifth_primary"){
                      echo "Fifth Primary"; 
                    }else if($record_info->student_stage == "first_prep"){
                      echo "First Prepratory"; 
                    }else if($record_info->student_stage == "second_prep"){
                      echo "Second Prepratory"; 
                    }else if($record_info->student_stage == "first_secondary"){
                      echo "First Secondary"; 
                    }else if($record_info->student_stage == "second_secondary_art"){
                      echo "Second Secondary - Art Section"; 
                    }else{
                      echo "Second Secondary - Science Section"; 
                    }
                      
                    
                    ?> 
                    </div> 
                  </div> 
                  
                  
                   <div class="form-group"> 
                    <label  class="col-lg-2">Class :</label> 
                    <div class="col-lg-6"> 
                    
                    <?php 
                     
                     if($record_info->student_class == "a" ){
                        echo "Class => A"; 
                      }else if($record_info->student_class == "b"){
                        echo "Class => B"; 
                      }else if($record_info->student_class == "c"){
                        echo "Class => C"; 
                      }else if($record_info->student_class == "d"){
                        echo "Class => D"; 
                      }
                    ?> 
                    </div> 
                  </div> 
                  
                  
                    <div class="form-group"> 
                    <label  class="col-lg-2">Seat Number :</label> 
                    <div class="col-lg-6"> 
                    <?php echo $record_info->seat_number ; ?> 
                    </div> 
                  </div> 

                  

                  

                  <div class="form-group"> 
                    <label  class="col-lg-2">Date Of Insert  :</label> 
                    <div class="col-lg-6"> 
                    <?php echo $record_info->insert_date?> 
                    </div> 
                  </div> 

                   <div class="form-group"> 
                    <label  class="col-lg-2">Date Of Modify  :</label> 
                    <div class="col-lg-6"> 
                    <?php echo $record_info->last_update ?> 
                    </div> 
                  </div> 

                   


                  <div class="form-group"> 
                  <div class="col-lg-offset-2 col-lg-10"> 
                    <button type="button" class="btn btn-info"   
                        onClick="window.location.href = 'update.php?id='+<?php echo $record_id?>" > <li class="icon-edit-sign"></li> Update </button> 
                      
                        </div> 
                  </div> 
                </form> 
              </div> 
            </div> 
          </section> 
        </aside> 
        
      </div> 
       
      <!-- page end-->  
    </section> 
  </section> 
  <!--main content end-->  
  <!--footer start--> 
  <?php require_once("../layout/footer.php");?> 
   
	 
   
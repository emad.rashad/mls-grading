<?php 
ob_start();
require_once('../../../../classes/Session.php'); 
require_once('../../../../classes/Functions.php'); 
require_once('../../../../classes/MysqlDatabase.php'); 
require_once('../../../../classes/Users.php'); 
require_once('../../../../classes/Students.php'); 

//get the response .


//check log in  
if($session->is_logged() == false){ 
	redirect_to("../../index.php"); 
} 
if(!empty($_POST["task"]) && $_POST["task"] == "insert"){ 


	 
	
	header('Content-Type: application/json');

	$student_code = $_POST["student_code"]; 
	$student_name = $_POST["student_name"]; 
	$student_password = $_POST["student_password"]; 
	$student_email = $_POST["student_email"]; 
	$stage_selected = $_POST["stage_selected"]; 
	$class_selected = $_POST["class_selected"]; 
	$seat_number = $_POST["seat_number"]; 

		 
				//check email 
				$add = new  Students(); 
				$add->student_code     = $student_code	;
				$add->student_name     = $student_name;		 
				$add->student_password = $student_password; 
				$add->student_email    = $student_email; 
				$add->student_stage    = $stage_selected; 
				$add->student_class    = $class_selected;  
				$add->seat_number      = $seat_number; 

				$date_insertion = date_now('Y m d');

				$add->insert_date      = date('F j, Y',strtotime($date_insertion));
				$add->last_update      = date('F j, Y, g:i a' , strtotime($date_insertion)); 
				
				$insert = $add->insert();
					 
				if($insert){ 
					$data  = array("status"=>"work"); 
					echo json_encode($data); 
				}else{ 
					$data  = array("status"=>"error"); 
					echo json_encode($data); 
				}				 
						
}
		
	  	 
 	 
//close connection 
if(isset($database)){ 
	$database->close_connection(); 
} 
?>
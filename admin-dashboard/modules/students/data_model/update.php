<?php 
ob_start(); 
 require_once('../../../../classes/Session.php'); 
 require_once('../../../../classes/Functions.php'); 
 require_once('../../../../classes/MysqlDatabase.php'); 
 require_once('../../../../classes/Users.php'); 
 require_once('../../../../classes/Students.php'); 
 //check log in  
 if($session->is_logged() == false){ 
 redirect_to("../../index.php"); 
 } 

 date_default_timezone_set('UTC');

 header('Content-Type: application/json'); 
 // user log in profile details to chech authority 
 // get user profile   
 // get user profile data 
 if(!empty($_POST["task"]) && $_POST["task"] == "update"){ 
 //get data 
 $id = $_POST['record']; 
 $edit = Students::find_by_id($id); 
 //check global edit and update authorization 
 	
 		
 		
		
		//update : 	 
		$edit->student_code     = $_POST["student_code"];	
		$edit->student_name     = $_POST["student_name"];		 
		$edit->student_password = $_POST["student_password"]; 
		$edit->student_email    = $_POST["student_email"]; 
		$edit->student_stage    =  $_POST["stage_selected"]; 
		$edit->student_class    = $_POST["class_selected"];  
		$edit->seat_number      = $_POST["seat_number"]; 

		$date_update            = date_now('Y m d'); 
		$edit->last_update      = date('F j, Y, g:i a', strtotime($date_update));  

 			 
 			 
		$update = $edit->update(); 
			if($update){ 
				$data  = array("status"=>"work"); 
				echo json_encode($data); 
			}else{ 
				$data  = array("status"=>"error"); 
				echo json_encode($data);  
			}
	
 }
 //close connection 
 if(isset($database)){ 
 $database->close_connection(); 
 } 
 ?> 
 
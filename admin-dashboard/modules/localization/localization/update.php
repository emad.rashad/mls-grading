<?php 
	require_once("../layout/initialize.php"); 
	if(isset($_GET['id']) && is_numeric($_GET['id'])){ 
		$record_id = $_GET['id']; 
		$record_info = Localization::find_by_id($record_id); 
		//check id access 
		if(empty($record_info->id)){ 
			redirect_to("view.php");	 
		} 
	}else{ 
		redirect_to("view.php");	 
	} 
	require_once("../layout/header.php"); 
?> 
<script type="text/javascript" src="../../js-crud/crud_localization.js"></script> 
  <!--header end-->  
  <!--sidebar start--> 
  <?php require_once("../layout/navigation.php");?> 
  <!--sidebar end-->  
  <!--main content start--> 
   <section id="main-content"> 
     <section class="wrapper site-min-height">  
       <h4>Localization Module </h4> 
        <!-- page start--> 
       <div class="row"> 
          <aside class="profile-info col-lg-8"> 
            <section> 
              <div class="panel"> 
                <div class="panel-heading"> Edit Languages</div> 
                <div class="panel-body"> 
               <form role="form" action="data_model/update.php" id="form_crud"> 
                 <input type="hidden" id="process_type" value="update"> 
                 <input type="hidden" id="record" value="<?php  echo $record_id;?>"> 
                  <div class="form-group"> 
                      <label for="col-lg-2">Name:</label> 
                      <input type="text" class="form-control" id="name" placeholder="" value="<?php echo $record_info->name;?>"> 
                  </div> 
                  <div class="form-group"> 
                      <label for="col-lg-2">Label</label> 
                      <input type="text" class="form-control" id="label" placeholder=""  value="<?php echo $record_info->label;?>"> 
                  </div> 
                  <button type="submit" id="submit" class="btn btn-info">Submit</button> 
                  <button type="button" class=" btn btn-info "   
                    onClick="window.location.href = 'full_info.php?id='+<?php echo $record_id?>" > <i class="icon-info-sign"></i> View Full Info </button> 
                  <div id="loading_data"></div> 
               </form> 
              </div> 
             </div> 
           </section> 
        </aside> 
       </div> 
       <!-- page end-->  
    </section> 
  </section> 
  <!--main content end-->  
  <!--footer start--> 
  <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br> 
  <?php require_once("../layout/footer.php");?>
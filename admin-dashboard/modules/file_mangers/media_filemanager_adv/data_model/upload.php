<?php 
require_once('../../../../../classes/Session.php'); 
require_once('../../../../../classes/Functions.php'); 
require_once('../../../../../classes/MediaLibraryDirectories.php'); 
require_once('../../../../../classes/MediaLibraryFiles.php'); 
require_once('../../../../../classes/UploadFile.php'); 
require_once("../../../../../classes/resize_class.php"); 
//get file id and return file name 
$directory_title = $_GET['title']; 
$path = "../../../../../media-library/"; 
//if file exist get name and begin upload 
if(file_exists($path.$directory_title)){ 
	 
	//file 
	$file =  $_FILES['file']; 
	//root 
	$root = "../../../../../media-library/{$directory_title}/"; 
	$root_black_whit = "../../../../../media-library/{$directory_title}/black_whit/"; 
	 
	//initialize class, insert rrot , allow exte. size 
	$upload_file = new UploadFile($root , array('jpg','JPG','png','PNG','jpeg','JPEG','gif','GIF'), 5000000); 
	//uploading file 
	$upload_file->attach_file($file); 
	//if uploading succeeded 
	if($upload_file->succeeded == "yes"){ 
		 
		//resize images 
		//file name 
		$file_name = $upload_file->new_file_name;	 
		$file_type = $upload_file->file_type; 
		//resize	 
	 
		 
		//small 
		// *** 1) Initialise / load image 
		$resizeObj_medium = new resize($root.$file_name); 
		// *** 2) Resize image (options: exact, portrait, landscape, auto, crop) 
		$resizeObj_medium->resizeImage(295,169, 'exact'); 
		// *** 3) Save image 
		$resizeObj_medium->saveImage($root.$file_name,100); 
		 
	} 
	 
} 
?>      

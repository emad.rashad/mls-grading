<?php
require_once('../../../../classes/Session.php'); 
require_once('../../../../classes/Functions.php'); 
require_once('../../../../classes/MysqlDatabase.php'); 
require_once('../../../../classes/Users.php'); 
require_once('../../../../classes/FinalPrimarySecondThird.php'); 
require_once('../../../../classes/FinalPrimaryFourthFifth.php'); 
require_once('../../../../classes/FinalPrepFirstSecond.php'); 
require_once('../../../../classes/FinalSecondaryFirst.php'); 
require_once('../../../../classes/FinalSecondarySecondArt.php'); 
require_once('../../../../classes/FinalSecondarySecondScience.php'); 

header('Content-Type: application/json');


if($_SERVER['REQUEST_METHOD'] == "POST" && isset($_FILES["file"]["type"])){
	

	$msg = ''; 
	$uploaded = FALSE;
	$allowed_extensions = array("csv"); // file allowed_extensions to be checked
	$tmp = explode(".", $_FILES["file"]["name"]) ; 
	$extension = end($tmp) ; 
	$fileTypes = array("text/comma-separated-values","text/csv"); // file types to be checked
	$file = $_FILES["file"]['tmp_name'];



				// get stage posted : 
				$stage = $_POST['stage'] ; 
				$re_exam_start = $_POST['re_exam'] ; 
				$year = $_POST['year']; 
				// get file  info : 
				$handle = fopen($file , 'r') ; 
				/*
				======================================================== Demonstration ==========================================================
				i check stage here cause there are different subjects in each stage  but they will be controlled from one place  
				this is a challenge and needs concentration ,,,, more than concentration ,,,,, god will be with me -_- 
				*/
				// check stage 
				if($stage == "second_primary" || $stage == "third_primary"){


				while (($file_open = fgetcsv($handle , 1000 , ",")) !== false ) {
					
					// get fields in the excel file : 
					$student_code 								= $file_open[0] ; 
					$student_name 								= $file_open[1] ; 
					$max 		  								= $file_open[2] ; 
					$min          								= $file_open[3] ; 
					$class        								= $file_open[4] ; 
					$seat_number  								= $file_open[5] ; 
					$arabic       								= $file_open[6] ; 
					$arabic_st    								= $file_open[7] ; 
					$maths        								= $file_open[8] ; 
					$maths_st     								= $file_open[9] ; 
					$english_ol   								= $file_open[10] ; 
					$english_ol_st             					= $file_open[11] ; 
					$extra_curricular_one             			= $file_open[12] ; 
					$extra_curricular_one_st	        		= $file_open[13] ; 
					$extra_curricular_two                    	= $file_open[14] ; 
					$extra_curricular_two_st				    = $file_open[15] ; 
					$p_e 				    					= $file_open[16] ; 
					$p_e_st			    						= $file_open[17] ; 
					$art 	   									= $file_open[18] ; 
					$art_st    									= $file_open[19] ; 
					$total       								= $file_open[20] ; 
					$religion    								= $file_open[21] ; 
					$religion_st 								= $file_open[22] ; 
					$english_al									= $file_open[23] ; 
					$english_al_st 								= $file_open[24] ; 
					$french										= $file_open[25] ; 
					$french_st                     				= $file_open[26] ; 
					

				    // make object from final result class : 
				    $final_result = new FinalPrimarySecondThird() ; 

				    $final_result->student_code 		   = $student_code ; 
				    $final_result->student_name 		   = $student_name ; 
				    $final_result->max          		   = $max ; 
				    $final_result->min          		   = $min ; 
				    $final_result->class        		   = $class ; 
				    $final_result->stage        		   = $stage ; 
				    $final_result->seat_number  		   = $seat_number ; 
					$final_result->year  		           = $year ; 
				    $final_result->arabic       		   = $arabic ; 
				    $final_result->arabic_st    		   = $arabic_st ; 
				    $final_result->maths       			   = $maths ; 
				    $final_result->maths_st    			   = $maths_st ; 
				    $final_result->english_ol   		   = $english_ol ; 
				    $final_result->english_ol_st 		   = $english_ol_st ;  
				    $final_result->extra_curricular_one    = $extra_curricular_one ; 
				    $final_result->extra_curricular_one_st = $extra_curricular_one_st ; 
				    $final_result->extra_curricular_two    = $extra_curricular_two ; 
				    $final_result->extra_curricular_two_st = $extra_curricular_two_st ; 
				    $final_result->p_e 					   = $p_e ; 
				    $final_result->p_e_st 			       = $p_e_st ; 
				    $final_result->art 			           = $art ; 
				    $final_result->art_st 				   = $art_st ; 
				    $final_result->total 				   = $total ; 
				    $final_result->religion 			   = $religion ; 
				    $final_result->religion_st 			   = $religion_st ; 
				    $final_result->english_al 			   = $english_al ; 
				    $final_result->english_al_st 		   = $english_al_st ; 
				    $final_result->french 				   = $french ; 
				    $final_result->french_st               = $french_st ; 
				    $final_result->re_exam_start           = $re_exam_start ; 


				    // process insert and show me the fucken message   
				    $make_insert 						   = $final_result->insert() ; 

				    
					
				}

				if($make_insert){
	    			$data = array('status'=>'work' , 'message'=>"Results For Primary School Has Been Added To database successfully" ) ; 
							echo json_encode($data) ; 
				 
				    }


			}else if($stage == "fourth_primary" || $stage == "fifth_primary"){

				  while (($file_open = fgetcsv($handle , 1000 , ",")) !== false ) {
					
					// get fields in the excel file : 
					$student_code 								= $file_open[0] ; 
					$student_name 								= $file_open[1] ; 
					$max 		  								= $file_open[2] ; 
					$min          								= $file_open[3] ; 
					$class        								= $file_open[4] ; 
					$seat_number  								= $file_open[5] ; 
					$arabic       								= $file_open[6] ; 
					$arabic_st    								= $file_open[7] ; 
					$maths        								= $file_open[8] ; 
					$maths_st     								= $file_open[9] ; 
					$english_ol   								= $file_open[10] ; 
					$english_ol_st             					= $file_open[11] ; 
					$social_studies             				= $file_open[12] ; 
					$social_studies_st	        				= $file_open[13] ; 
					$science                    				= $file_open[14] ; 
					$science_st				                    = $file_open[15] ; 
					$extra_curricular_one 				        = $file_open[16] ; 
					$extra_curricular_one_st			        = $file_open[17] ; 
					$extra_curricular_two 	   					= $file_open[18] ; 
					$extra_curricular_two_st    				= $file_open[19] ; 
					$p_e       									= $file_open[20] ; 
					$p_e_st    									= $file_open[21] ; 
					$art 										= $file_open[22] ; 
					$art_st										= $file_open[23] ; 
					$total 										= $file_open[24] ; 
					$religion									= $file_open[25] ; 
					$religion_st                     			= $file_open[26] ; 
					$english_al                     			= $file_open[27] ; 
					$english_al_st                     			= $file_open[28] ; 
					$french                     				= $file_open[29] ; 
					$french_st                     				= $file_open[30] ; 
				 

				    // make object from final result class : 
				    $final_result = new FinalPrimaryFourthFifth() ; 

				    $final_result->student_code 		   = $student_code ; 
				    $final_result->student_name 		   = $student_name ; 
				    $final_result->max          		   = $max ; 
				    $final_result->min          		   = $min ; 
				    $final_result->class        		   = $class ; 
				    $final_result->stage        		   = $stage ; 
				    $final_result->seat_number  		   = $seat_number ; 
					$final_result->year  		           = $year ; 
				    $final_result->arabic       		   = $arabic ; 
				    $final_result->arabic_st    		   = $arabic_st ; 
				    $final_result->maths       			   = $maths ; 
				    $final_result->maths_st    			   = $maths_st ; 
				    $final_result->english_ol   		   = $english_ol ; 
				    $final_result->english_ol_st 		   = $english_ol_st ; 
				    $final_result->social_studies 		   = $social_studies ; 
				    $final_result->social_studies_st 	   = $social_studies_st ; 
				    $final_result->science 				   = $science ; 
				    $final_result->science_st			   = $science_st ; 
				    $final_result->extra_curricular_one    = $extra_curricular_one ; 
				    $final_result->extra_curricular_one_st = $extra_curricular_one_st ; 
				    $final_result->extra_curricular_two    = $extra_curricular_two ; 
				    $final_result->extra_curricular_two_st = $extra_curricular_two_st ;  
				    $final_result->p_e 			           = $p_e ; 
				    $final_result->P_e_st 			       = $p_e_st ; 
				    $final_result->art 			           = $art ; 
				    $final_result->art_st 				   = $art_st ; 
				    $final_result->total 				   = $total ; 
				    $final_result->religion 			   = $religion ; 
				    $final_result->religion_st 			   = $religion_st ; 
				    $final_result->english_al 			   = $english_al ; 
				    $final_result->english_al_st 		   = $english_al_st ; 
				    $final_result->french 				   = $french ; 
				    $final_result->french_st               = $french_st ;  
				    $final_result->re_exam_start           = $re_exam_start ;  


				    // process insert and show me the fucken message   
				    $make_insert 						   = $final_result->insert() ; 

				    
					
				}

				if($make_insert){
	    			$data = array('status'=>'work' , 'message'=>"Results For Primary School Has Been Added To database successfully" ) ; 
							echo json_encode($data) ; 
				 
				    }



			}else if ($stage == "first_prep" || $stage == "second_prep"){

				 while (($file_open = fgetcsv($handle , 1000 , ",")) !== false ) {
					
					// get fields in the excel file : 
					$student_code 								= $file_open[0] ; 
					$student_name 								= $file_open[1] ; 
					$max 		  								= $file_open[2] ; 
					$min          								= $file_open[3] ; 
					$class        								= $file_open[4] ; 
					$seat_number  								= $file_open[5] ; 
					$arabic       								= $file_open[6] ; 
					$arabic_st    								= $file_open[7] ; 
					$maths        								= $file_open[8] ; 
					$maths_st     								= $file_open[9] ; 
					$english_ol   								= $file_open[10] ; 
					$english_ol_st             					= $file_open[11] ; 
					$social_studies             				= $file_open[12] ; 
					$social_studies_st	        				= $file_open[13] ; 
					$science                    				= $file_open[14] ; 
					$science_st				                    = $file_open[15] ;
					$computer                                   = $file_open[16] ;
					$computer_st                                = $file_open[17] ;
					$art 										= $file_open[18] ; 
					$art_st										= $file_open[19] ;
					$extra_curricular_one 				        = $file_open[20] ; 
					$extra_curricular_one_st			        = $file_open[21] ; 
					$extra_curricular_two 	   					= $file_open[22] ; 
					$extra_curricular_two_st    				= $file_open[23] ; 
					$total 										= $file_open[24] ; 
					$religion									= $file_open[25] ; 
					$religion_st                     			= $file_open[26] ; 
					$english_al                     			= $file_open[27] ; 
					$english_al_st                     			= $file_open[28] ; 
					$french                     				= $file_open[29] ; 
					$french_st                     				= $file_open[30] ; 
				 

				    // make object from final result class : 
				    $final_result = new FinalPrepFirstSecond() ; 

				    $final_result->student_code 		   = $student_code ; 
				    $final_result->student_name 		   = $student_name ; 
				    $final_result->max          		   = $max ; 
				    $final_result->min          		   = $min ; 
				    $final_result->class        		   = $class ; 
				    $final_result->stage        		   = $stage ; 
				    $final_result->seat_number  		   = $seat_number ; 
					$final_result->year  		           = $year ; 
				    $final_result->arabic       		   = $arabic ; 
				    $final_result->arabic_st    		   = $arabic_st ; 
				    $final_result->maths       			   = $maths ; 
				    $final_result->maths_st    			   = $maths_st ; 
				    $final_result->english_ol   		   = $english_ol ; 
				    $final_result->english_ol_st 		   = $english_ol_st ; 
				    $final_result->social_studies 		   = $social_studies ; 
				    $final_result->social_studies_st 	   = $social_studies_st ; 
				    $final_result->science 				   = $science ; 
				    $final_result->science_st			   = $science_st ; 
				    $final_result->computer 			   = $computer ; 
				    $final_result->computer_st			   = $computer_st ; 
				    $final_result->art 			           = $art ; 
				    $final_result->art_st 				   = $art_st ; 
				    $final_result->extra_curricular_one    = $extra_curricular_one ; 
				    $final_result->extra_curricular_one_st = $extra_curricular_one_st ; 
				    $final_result->extra_curricular_two    = $extra_curricular_two ; 
				    $final_result->extra_curricular_two_st = $extra_curricular_two_st ;   
				    $final_result->total 				   = $total ; 
				    $final_result->religion 			   = $religion ; 
				    $final_result->religion_st 			   = $religion_st ; 
				    $final_result->english_al 			   = $english_al ; 
				    $final_result->english_al_st 		   = $english_al_st ; 
				    $final_result->french 				   = $french ; 
				    $final_result->french_st               = $french_st ;  
				    $final_result->re_exam_start           = $re_exam_start ;  


				    // process insert and show me the fucken message   
				    $make_insert 						   = $final_result->insert() ; 

				    
					
				}

				if($make_insert){
	    			$data = array('status'=>'work' , 'message'=>"Results For Prepratory School Has Been Added To database successfully" ) ; 
							echo json_encode($data) ; 
				 
				    }


			}else if($stage == "first_secondary"){

				 while (($file_open = fgetcsv($handle , 1000 , ",")) !== false ) {
					
					// get fields in the excel file : 
					$student_code 								= $file_open[0] ; 
					$student_name 								= $file_open[1] ; 
					$class 		  								= $file_open[2] ; 
					$seat_number          						= $file_open[3] ; 
					$arabic_max        							= $file_open[4] ; 
					$arabic_min  								= $file_open[5] ; 
					$arabic       								= $file_open[6] ; 
					$arabic_st    								= $file_open[7] ; 
					$english_ol_max   							= $file_open[8] ; 
					$english_ol_min             				= $file_open[9] ; 
					$english_ol             					= $file_open[10] ; 
					$english_ol_st	        						= $file_open[11] ; 
					$french_ol_max                    		    = $file_open[12] ; 
					$french_ol_min				                = $file_open[13] ;
					$french_ol                                  = $file_open[14] ;
					$french_ol_st                               = $file_open[15] ;
					$algebra_max 								= $file_open[16] ; 
					$algebra_min								= $file_open[17] ;
					$algebra 				        			= $file_open[18] ; 
					$algebra_st			        				= $file_open[19] ; 
					$geometry_max 	   							= $file_open[20] ; 
					$geometry_min    							= $file_open[21] ; 
					$geometry 								    = $file_open[22] ; 
					$geometry_st							    = $file_open[23] ; 
					$physics_max                     			= $file_open[24] ; 
					$physics_min                     			= $file_open[25] ; 
					$physics                     				= $file_open[26] ; 
					$physics_st                     			= $file_open[27] ; 
					$chemistry_max                     			= $file_open[28] ; 
					$chemistry_min                     			= $file_open[29] ; 
					$chemistry                     				= $file_open[30] ; 
					$chemistry_st                     			= $file_open[31] ; 
					$biology_max                     			= $file_open[32] ; 
					$biology_min                     			= $file_open[33] ; 
					$biology                     				= $file_open[34] ; 
					$biology_st                     			= $file_open[35] ; 
					$history_max                     			= $file_open[36] ; 
					$history_min                     			= $file_open[37] ; 
					$history                     				= $file_open[37] ; 
					$history_st                     			= $file_open[38] ; 
					$geography_max                     			= $file_open[39] ; 
					$geography_min                     			= $file_open[40] ; 
					$geography                     				= $file_open[41] ; 
					$geography_st                     			= $file_open[42] ; 
					$philosophy_max                     		= $file_open[43] ; 
					$philosophy_min                     		= $file_open[44] ; 
					$philosophy                     			= $file_open[45] ; 
					$philosophy_st                     			= $file_open[46] ; 
					$total                     					= $file_open[47] ; 
					$religion_max                     			= $file_open[48] ; 
					$religion_min                     			= $file_open[49] ; 
					$religion                     				= $file_open[50] ; 
					$religion_st                     			= $file_open[51] ; 
					$civics_max                     			= $file_open[52] ; 
					$civics_min                     			= $file_open[53] ; 
					$civics                     				= $file_open[54] ; 
					$civics_st                     				= $file_open[55] ; 
					$english_al_max                     		= $file_open[56] ; 
					$english_al_min                     		= $file_open[57] ; 
					$english_al                     			= $file_open[58] ; 
					$english_al_st                     			= $file_open[59] ; 
					$french_al_max                     			= $file_open[60] ; 
					$french_al_min                     			= $file_open[61] ; 
					$french_al                     				= $file_open[62] ; 
					$french_al_st                     			= $file_open[63] ; 
					$computer_max                     			= $file_open[64] ; 
					$computer_min                     			= $file_open[65] ; 
					$computer                     				= $file_open[66] ; 
					$computer_st                     			= $file_open[67] ; 
					

					 
				 

				    // make object from final result class : 
				    $final_result = new FinalSecondaryFirst() ; 

					$final_result->student_code 		   	   = $student_code ; 
					$final_result->student_name 		   	   = $student_name ; 
					$final_result->class          		  	   = $class ; 
					$final_result->stage          		  	   = $stage ; 
					$final_result->seat_number            	   = $seat_number ; 
					$final_result->year  		               = $year ; 
					$final_result->arabic_max        	  	   = $arabic_max ; 
				    $final_result->arabic_min        		   = $arabic_min ; 
				    $final_result->arabic  		  			   = $arabic ; 
				    $final_result->arabic_st       		   	   = $arabic_st ; 
				    $final_result->english_ol_max    		   = $english_ol_max ; 
				    $final_result->english_ol_min       	   = $english_ol_min ; 
				    $final_result->english_ol    			   = $english_ol ; 
				    $final_result->english_ol_st   		   	   = $english_ol_st ; 
				    $final_result->french_ol_max 		   	   = $french_ol_max ; 
				    $final_result->french_ol_min 		   	   = $french_ol_min ; 
				    $final_result->french_ol 	   			   = $french_ol ; 
				    $final_result->french_ol_st 			   = $french_ol_st ; 
				    $final_result->algebra_max			   	   = $algebra_max ; 
				    $final_result->algebra_min 			       = $algebra_min ; 
				    $final_result->algebra			           = $algebra ; 
				    $final_result->algebra_st 			       = $algebra_st ; 
				    $final_result->geometry_max 			   = $geometry_max ; 
				    $final_result->geometry_min   			   = $geometry_min ; 
				    $final_result->geometry 				   = $geometry ; 
				    $final_result->geometry_st    			   = $geometry_st ; 
				    $final_result->physics_max 				   = $physics_max  ;   
				    $final_result->physics_min 				   = $physics_min ; 
				    $final_result->physics 			   		   = $physics ; 
				    $final_result->physics_st 			   	   = $physics_st ; 
				    $final_result->chemistry_max 			   = $chemistry_max ; 
				    $final_result->chemistry_min 		       = $chemistry_min ; 
				    $final_result->chemistry 				   = $chemistry ; 
				    $final_result->chemistry_st                = $chemistry_st ;  
				    $final_result->biology_max                 = $biology_max ;  
					$final_result->biology_min                 = $biology_min ; 
					$final_result->biology                     = $biology ; 
					$final_result->biology_st                  = $biology_st ; 
					$final_result->history_max                 = $history_max ; 
					$final_result->history_min                 = $history_min ; 
					$final_result->history                     = $history ; 
					$final_result->history_st                  = $history_st ; 
					$final_result->geography_max               = $geography_max ; 				
					$final_result->geography_min               = $geography_min ; 
					$final_result->geography                   = $geography ; 				
					$final_result->geography_st                = $geography_st ; 
					$final_result->philosophy_max              = $philosophy_max ; 
					$final_result->philosophy_min              = $philosophy_min ; 
					$final_result->philosophy                  = $philosophy ; 				
					$final_result->philosophy_st               = $philosophy_st ; 
					$final_result->total           			   = $total ; 				
					$final_result->religion_max                = $religion_max ; 					
					$final_result->religion_min                = $religion_min ; 				
					$final_result->religion                    = $religion ; 				
					$final_result->religion_st                 = $religion_st ; 				
					$final_result->civics_max                  = $civics_max ; 
					$final_result->civics_min                  = $civics_min ; 					
					$final_result->civics                      = $civics ; 					
					$final_result->civics_st                   = $civics_st ; 					
					$final_result->english_al_max              = $english_al_max ; 					
					$final_result->english_al_min              = $english_al_min ; 					
					$final_result->english_al	               = $english_al	 ; 					
					$final_result->english_al_st               = $english_al_st ; 					
					$final_result->french_al_max               = $french_al_max ; 					
					$final_result->french_al_min               = $french_al_min ; 					
					$final_result->french_al                   = $french_al ; 					
					$final_result->french_al_st                = $french_al_st ; 					
					$final_result->computer_max                = $computer_max ; 				
					$final_result->computer_min                = $computer_min ; 					
					$final_result->computer                    = $computer ; 				
					$final_result->computer_st                 = $computer_st ; 	
					$final_result->re_exam_start               = $re_exam_start ; 
					
						   


				    // process insert and show me the fucken message   
				    $make_insert 						   = $final_result->insert() ; 

				    
					
				}

				if($make_insert){
	    			$data = array('status'=>'work' , 'message'=>"Results For Secondary School First Stage Has Been Added To database successfully" ) ; 
							echo json_encode($data) ; 
				 
				    }


			}else if($stage == "second_secondary_art"){

				 while (($file_open = fgetcsv($handle , 1000 , ",")) !== false ) {
					
					// get fields in the excel file : 
					$student_code 				= $file_open[0] ; 
					$student_name 				= $file_open[1] ; 
					$class 		  				= $file_open[2] ; 
					$seat_number          		= $file_open[3] ; 
					$arabic_max        		    = $file_open[4] ; 
					$arabic_min  				= $file_open[5] ; 
					$arabic       				= $file_open[6] ; 
					$arabic_st    				= $file_open[7] ; 
					$english_ol_max        		= $file_open[8] ; 
					$english_ol_min     		= $file_open[9] ; 
					$english_ol   				= $file_open[10] ; 
					$english_ol_st              = $file_open[11] ; 
					$french_ol_max              = $file_open[12] ; 
					$french_ol_min	            = $file_open[13] ; 
					$french_ol                  = $file_open[14] ; 
					$french_ol_st				= $file_open[15] ; 
					$algebra_max 				= $file_open[16] ; 
					$algebra_min			    = $file_open[17] ; 
					$algebra 	                = $file_open[18] ; 
					$algebra_st                 = $file_open[19] ; 
					$trigno_calculs_max         = $file_open[20] ; 
					$trigno_calculs_min         = $file_open[21] ; 	 
					$trigno_calculs 			= $file_open[22] ; 
					$trigno_calculs_st		    = $file_open[23] ; 
					$history_max                = $file_open[24] ; 
					$history_min                = $file_open[25] ; 
					$history                    = $file_open[26] ; 
					$history_st                 = $file_open[27] ; 
					$philosophy_max             = $file_open[28] ; 
					$philosophy_min             = $file_open[29] ; 
					$philosophy                 = $file_open[30] ; 
					$philosophy_st              = $file_open[31] ; 
					$psychology_max             = $file_open[32] ; 
					$psychology_min             = $file_open[33] ; 
					$psychology                 = $file_open[34] ; 
					$psychology_st              = $file_open[35] ; 
					$geography_max              = $file_open[36] ; 
					$geography_min              = $file_open[37] ; 
					$geography                  = $file_open[38] ; 
					$geography_st               = $file_open[39] ; 
					$total              		= $file_open[40] ; 
					$religion_max               = $file_open[41] ; 
					$religion_min               = $file_open[42] ; 
					$religion               	= $file_open[43] ; 
					$religion_st             	= $file_open[44] ; 
					$human_rights_max           = $file_open[45] ; 
					$human_rights_min           = $file_open[46] ; 
					$human_rights               = $file_open[47] ; 
					$human_rights_st            = $file_open[48] ; 
					$english_al_max             = $file_open[49] ; 
					$english_al_min             = $file_open[50] ; 
					$english_al                 = $file_open[51] ; 
					$english_al_st              = $file_open[52] ; 
					$french_al_max              = $file_open[53] ; 
					$french_al_min              = $file_open[54] ; 
					$french_al                  = $file_open[55] ; 
					$french_al_st               = $file_open[56] ; 
					$computer_max               = $file_open[57] ; 
					$computer_min               = $file_open[58] ; 
					$computer                   = $file_open[59] ; 
					$computer_st                = $file_open[60] ; 
					$agriculture_max            = $file_open[61] ; 
					$agriculture_min            = $file_open[62] ; 
					$agriculture                = $file_open[63] ; 
					$agriculture_st             = $file_open[64] ; 
					
					 

				    // make object from final result class : 
				    $final_result = new FinalSecondarySecondArt() ; 

				    $final_result->student_code 		               = $student_code ; 
				    $final_result->student_name 		               = $student_name ; 
				    $final_result->class          		               = $class ;  
				    $final_result->stage        		               = $stage ; 
				    $final_result->seat_number  		               = $seat_number ; 
					$final_result->year  		                       = $year ; 
				    $final_result->arabic_max       		           = $arabic_max ; 
				    $final_result->arabic_min    		               = $arabic_min ; 
				    $final_result->arabic       			           = $arabic ; 
				    $final_result->arabic_st    			           = $arabic_st ; 
				    $final_result->english_ol_max   		           = $english_ol_max ; 
				    $final_result->english_ol_min 		               = $english_ol_min ; 
				    $final_result->english_ol 		                   = $english_ol ; 
				    $final_result->english_ol_st 	                   = $english_ol_st ; 
				    $final_result->french_ol_max 				       = $french_ol_max ; 
				    $final_result->french_ol_min			           = $french_ol_min ; 
				    $final_result->french_ol 			               = $french_ol ; 
				    $final_result->french_ol_st                        = $french_ol_st ; 
				    $final_result->algebra_max                         = $algebra_max ; 
				    $final_result->algebra_min                         = $algebra_min ; 
				    $final_result->algebra                             = $algebra ; 
				    $final_result->algebra_st                          = $algebra_st ;  
				    $final_result->trigno_calculs_max                  = $trigno_calculs_max ; 
				    $final_result->trigno_calculs_min                  = $trigno_calculs_min ; 
				    $final_result->trigno_calculs                      = $trigno_calculs  ; 
				    $final_result->trigno_calculs_st                   = $trigno_calculs_st ; 
				  
				    $final_result->history_max                         = $history_max ; 
				    $final_result->history_min                         = $history_min ; 
				    $final_result->history                             = $history ; 
				    $final_result->history_st                          = $history_st ; 

					$final_result->philosophy_max                      = $philosophy_max ; 
				    $final_result->philosophy_min                      = $philosophy_min ; 
				    $final_result->philosophy                          = $philosophy ; 
				    $final_result->philosophy_st                       = $philosophy_st ; 

					$final_result->psychology_max                      = $psychology_max ; 
				    $final_result->psychology_min                      = $psychology_min ; 
				    $final_result->psychology                          = $psychology ; 
				    $final_result->psychology_st                       = $psychology_st ;

				    $final_result->geography_max                       = $geography_max ; 
				    $final_result->geography_min                       = $geography_min ; 
				    $final_result->geography                           = $geography ; 
				    $final_result->geography_st                        = $geography_st ; 

				  
				  
				  
				    
				    $final_result->total                               = $total ; 

				    $final_result->religion_max                        = $religion_max ; 
				    $final_result->religion_min                        = $religion_min ; 
				    $final_result->religion                            = $religion ; 
				    $final_result->religion_st                         = $religion_st ; 
				    $final_result->human_rights_max                    = $human_rights_max ; 
				    $final_result->human_rights_min                    = $human_rights_min ; 
				    $final_result->human_rights                        = $human_rights ; 
				    $final_result->human_rights_st                     = $human_rights_st ;

				    $final_result->english_al_max                      = $english_al_max ; 
				    $final_result->english_al_min                      = $english_al_min ; 
				    $final_result->english_al                          = $english_al ; 
				    $final_result->english_al_st                       = $english_al_st ; 
				    $final_result->french_al_max                       = $french_al_max ; 
				    $final_result->french_al_min                       = $french_al_min ; 
				    $final_result->french_al                           = $french_al ; 
				    $final_result->french_al_st                        = $french_al_st ; 
				    $final_result->computer_max                        = $computer_max ; 
				    $final_result->computer_min                        = $computer_min ; 
				    $final_result->computer                            = $computer ; 
				    $final_result->computer_st                         = $computer_st ; 
				   
				    $final_result->agriculture_max                     = $agriculture_max ; 
				    $final_result->agriculture_min                     = $agriculture_min ; 
				    $final_result->agriculture                         = $agriculture ; 
				    $final_result->agriculture_st                      = $agriculture_st ; 

					 $final_result->re_exam_start                      = $re_exam_start ; 
				   


				    // process insert and show me the fucken message   
				    $make_insert 						   = $final_result->insert() ; 

				    
					
				}

				if($make_insert){
				    			$data = array('status'=>'work' , 'message'=>"Results For Second Secondary Art - Section Has Been Added To database successfully" ) ; 
										echo json_encode($data) ; 
				 
				    }


			}else if ($stage == "second_secondary_science"){
				   

				    while (($file_open = fgetcsv($handle , 1000 , ",")) !== false ) {
					
					// get fields in the excel file : 
					$student_code 				= $file_open[0] ; 
					$student_name 				= $file_open[1] ; 
					$class 		  				= $file_open[2] ; 
					$seat_number          		= $file_open[3] ; 
					$arabic_max        		    = $file_open[4] ; 
					$arabic_min  				= $file_open[5] ; 
					$arabic       				= $file_open[6] ; 
					$arabic_st    				= $file_open[7] ; 
					$english_ol_max        		= $file_open[8] ; 
					$english_ol_min     		= $file_open[9] ; 
					$english_ol   				= $file_open[10] ; 
					$english_ol_st              = $file_open[11] ; 
					$french_ol_max              = $file_open[12] ; 
					$french_ol_min	            = $file_open[13] ; 
					$french_ol                  = $file_open[14] ; 
					$french_ol_st				= $file_open[15] ; 
					$algebra_max 				= $file_open[16] ; 
					$algebra_min			    = $file_open[17] ; 
					$algebra 	                = $file_open[18] ; 
					$algebra_st                 = $file_open[19] ; 
					$trigno_calculs_max         = $file_open[20] ; 
					$trigno_calculs_min         = $file_open[21] ; 	 
					$trigno_calculs 			= $file_open[22] ; 
					$trigno_calculs_st		    = $file_open[23] ; 
					$physics_max                = $file_open[24] ; 
					$physics_min                = $file_open[25] ; 
					$physics                    = $file_open[26] ; 
					$physics_st                 = $file_open[27] ; 
					$chemistry_max             = $file_open[28] ; 
					$chemistry_min             = $file_open[29] ; 
					$chemistry                 = $file_open[30] ; 
					$chemistry_st              = $file_open[31] ; 
					$biology_max             = $file_open[32] ; 
					$biology_min             = $file_open[33] ; 
					$biology                 = $file_open[34] ; 
					$biology_st              = $file_open[35] ; 
					$mechanics_max              = $file_open[36] ; 
					$mechanics_min              = $file_open[37] ; 
					$mechanics                  = $file_open[38] ; 
					$mechanics_st               = $file_open[39] ; 
					$total              		= $file_open[40] ; 
					$religion_max               = $file_open[41] ; 
					$religion_min               = $file_open[42] ; 
					$religion               	= $file_open[43] ; 
					$religion_st             	= $file_open[44] ; 
					$human_rights_max           = $file_open[45] ; 
					$human_rights_min           = $file_open[46] ; 
					$human_rights               = $file_open[47] ; 
					$human_rights_st            = $file_open[48] ; 
					$english_al_max             = $file_open[49] ; 
					$english_al_min             = $file_open[50] ; 
					$english_al                 = $file_open[51] ; 
					$english_al_st              = $file_open[52] ; 
					$french_al_max              = $file_open[53] ; 
					$french_al_min              = $file_open[54] ; 
					$french_al                  = $file_open[55] ; 
					$french_al_st               = $file_open[56] ; 
					$computer_max               = $file_open[57] ; 
					$computer_min               = $file_open[58] ; 
					$computer                   = $file_open[59] ; 
					$computer_st                = $file_open[60] ; 
					$agriculture_max            = $file_open[61] ; 
					$agriculture_min            = $file_open[62] ; 
					$agriculture                = $file_open[63] ; 
					$agriculture_st             = $file_open[64] ; 
					
					 

				    // make object from final result class : 
				    $final_result = new FinalSecondarySecondScience() ; 

				    $final_result->student_code 		               = $student_code ; 
				    $final_result->student_name 		               = $student_name ; 
				    $final_result->class          		               = $class ;  
				    $final_result->stage        		               = $stage ; 
				    $final_result->seat_number  		               = $seat_number ; 
					$final_result->year  		                       = $year ; 
				    $final_result->arabic_max       		           = $arabic_max ; 
				    $final_result->arabic_min    		               = $arabic_min ; 
				    $final_result->arabic       			           = $arabic ; 
				    $final_result->arabic_st    			           = $arabic_st ; 
				    $final_result->english_ol_max   		           = $english_ol_max ; 
				    $final_result->english_ol_min 		               = $english_ol_min ; 
				    $final_result->english_ol 		                   = $english_ol ; 
				    $final_result->english_ol_st 	                   = $english_ol_st ; 
				    $final_result->french_ol_max 				       = $french_ol_max ; 
				    $final_result->french_ol_min			           = $french_ol_min ; 
				    $final_result->french_ol 			               = $french_ol ; 
				    $final_result->french_ol_st                        = $french_ol_st ; 
				    $final_result->algebra_max                         = $algebra_max ; 
				    $final_result->algebra_min                         = $algebra_min ; 
				    $final_result->algebra                             = $algebra ; 
				    $final_result->algebra_st                          = $algebra_st ;  
				    $final_result->trigno_calculs_max                  = $trigno_calculs_max ; 
				    $final_result->trigno_calculs_min                  = $trigno_calculs_min ; 
				    $final_result->trigno_calculs                      = $trigno_calculs  ; 
				    $final_result->trigno_calculs_st                   = $trigno_calculs_st ; 
				  
				    $final_result->physics_max 				           = $physics_max ; 
				    $final_result->physics_min 			               = $physics_min ; 
				    $final_result->physics 			                   = $physics ; 
				    $final_result->physics_st 			               = $physics_st ; 

				    $final_result->chemistry_max 		               = $chemistry_max ; 
				    $final_result->chemistry_min 				       = $chemistry_min ; 
				    $final_result->chemistry                           = $chemistry ; 
				    $final_result->chemistry_st                        = $chemistry_st ; 
				    $final_result->biology_max                         = $biology_max ; 
				    $final_result->biology_min                         = $biology_min ; 
				    $final_result->biology                             = $biology ;  
				    $final_result->biology_st                          = $biology_st ; 

				    $final_result->mechanics_max                       = $mechanics_max ; 
				    $final_result->mechanics_min                       = $mechanics_min ; 
				    $final_result->mechanics                           = $mechanics ; 
				    $final_result->mechanics_st                        = $mechanics_st ;	    
				    $final_result->total                               = $total ; 

				    $final_result->religion_max                        = $religion_max ; 
				    $final_result->religion_min                        = $religion_min ; 
				    $final_result->religion                            = $religion ; 
				    $final_result->religion_st                         = $religion_st ; 

				    $final_result->human_rights_max                    = $human_rights_max ; 
				    $final_result->human_rights_min                    = $human_rights_min ; 
				    $final_result->human_rights                        = $human_rights ; 
				    $final_result->human_rights_st                     = $human_rights_st ;

				    $final_result->english_al_max                      = $english_al_max ; 
				    $final_result->english_al_min                      = $english_al_min ; 
				    $final_result->english_al                          = $english_al ; 
				    $final_result->english_al_st                       = $english_al_st ; 
				    $final_result->french_al_max                       = $french_al_max ; 
				    $final_result->french_al_min                       = $french_al_min ; 
				    $final_result->french_al                           = $french_al ; 
				    $final_result->french_al_st                        = $french_al_st ; 
				    $final_result->computer_max                        = $computer_max ; 
				    $final_result->computer_min                        = $computer_min ; 
				    $final_result->computer                            = $computer ; 
				    $final_result->computer_st                         = $computer_st ; 
				   
				    $final_result->agriculture_max                     = $agriculture_max ; 
				    $final_result->agriculture_min                     = $agriculture_min ; 
				    $final_result->agriculture                         = $agriculture ; 
				    $final_result->agriculture_st                      = $agriculture_st ; 

					 $final_result->re_exam_start                      = $re_exam_start ; 
				   


				    // process insert and show me the fucken message   
				    $make_insert 						   = $final_result->insert() ; 

				    
					
				}

				if($make_insert){
				    			$data = array('status'=>'work' , 'message'=>"Results For Second Secondary Science - Section Has Been Added To database successfully" ) ; 
										echo json_encode($data) ; 
				 
				    }





			}

			
					

				

			

}

?>
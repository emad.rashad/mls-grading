<?php 
require_once('../../../../classes/Session.php'); 
require_once('../../../../classes/Functions.php'); 
require_once('../../../../classes/MysqlDatabase.php'); 
require_once('../../../../classes/Users.php'); 
require_once('../../../../classes/MonthSecondaryFirst.php'); 
require_once('../../../../classes/MonthSecondarySecondArt.php'); 
require_once('../../../../classes/MonthSecondarySecondScience.php'); 


header('Content-Type: application/json');


if($_SERVER['REQUEST_METHOD'] == "POST" && isset($_FILES["file"]["type"])){
	

	$msg = ''; 
	$uploaded = FALSE;
	$allowed_extensions = array("csv"); // file allowed_extensions to be checked
	$tmp = explode(".", $_FILES["file"]["name"]) ; 
	$extension = end($tmp) ; 
	$fileTypes = array("text/comma-separated-values","text/csv"); // file types to be checked
	$file = $_FILES["file"]['tmp_name'];



				// get stage posted : 
				$stage = $_POST['stage'] ; 
				$year  = $_POST['year']; 
				$month = $_POST['month']; 

				// get file  info : 
				$handle = fopen($file , 'r') ; 
				
				#======================================================== Demonstration ==========================================================
				
				


			 if($stage == "first_secondary"){

					while (($file_open = fgetcsv($handle , 1000 , ",")) !== false ) {
					
					// get fields in the excel file : 
					$student_code 								= $file_open[0] ; 
					$student_name 								= $file_open[1] ; 		 
					$class        								= $file_open[2] ; 
					$seat_number  								= $file_open[3] ; 
					$arabic_max       							= $file_open[4] ; 
					$arabic_min       							= $file_open[5] ; 
					$arabic    								    = $file_open[6] ; 
					$english_ol_max        						= $file_open[7] ; 
					$english_ol_min        						= $file_open[8] ; 
					$english_ol     						    = $file_open[9] ; 
					$french_ol_max   							= $file_open[10] ; 
					$french_ol_min   							= $file_open[11] ; 
                    $french_ol                                  = $file_open[12] ; 
					$algebra_max             				    = $file_open[13] ; 
					$algebra_min             				    = $file_open[14] ; 
					$algebra                                    = $file_open[15] ; 
					$geometry_max             			        = $file_open[16] ; 
					$geometry_min             			        = $file_open[17] ; 
					$geometry                                   = $file_open[18] ; 
					$physics_max             				    = $file_open[19] ; 
					$physics_min            				    = $file_open[20] ; 
					$physics             				        = $file_open[21] ; 
                    $chemistry_max             				    = $file_open[22] ; 
                    $chemistry_min             				    = $file_open[23] ; 
                    $chemistry             				     	= $file_open[24] ; 
                    $biology_max             				    = $file_open[25] ;            
                    $biology_min             				    = $file_open[26] ;            
                    $biology             				     	= $file_open[27] ;
                    $history_max             				    = $file_open[28] ; 
                    $history_min             				    = $file_open[29] ; 
                    $history             			            = $file_open[30] ; 
                    $geography_max             		            = $file_open[31] ; 
                    $geography_min             		            = $file_open[32] ; 
                    $geography             			            = $file_open[33] ; 
                    $philosophy_max             		        = $file_open[34] ; 
                    $philosophy_min             		        = $file_open[35] ; 
                    $philosophy             				    = $file_open[36] ; 
                    $total             				            = $file_open[37] ; 
                    $religion_max             				    = $file_open[38] ; 
                    $religion_min             				    = $file_open[39] ; 
                    $religion             				     	= $file_open[40] ; 
                    $civics_max             				    = $file_open[41] ;
                    $civics_min             				    = $file_open[42] ;
					$civics             				     	= $file_open[43] ; 
                    $computer_max             				    = $file_open[44] ; 
					$computer_min             				    = $file_open[45] ; 
					$computer             				     	= $file_open[46] ; 
					$english_al_max             				= $file_open[47] ; 
					$english_al_min             				= $file_open[48] ; 
					$english_al             				    = $file_open[49] ; 
					$french_al_max             				    = $file_open[50] ; 
					$french_al_min             				    = $file_open[51] ; 
					$french_al             				     	= $file_open[52] ; 					
	                $attendance_max             				= $file_open[53] ; 
					$attendance_min             				= $file_open[54] ; 
					$attendance             				    = $file_open[55] ; 
	

				    // make object from final result class : 
				    $final_result = new MonthSecondaryFirst() ; 

				    $final_result->student_code 		   = $student_code ; 
				    $final_result->student_name 		   = $student_name ; 
				    $final_result->class        		   = $class ; 
					$final_result->stage        		   = $stage ; 
				    $final_result->seat_number             = $seat_number ;                        
					$final_result->year  		           = $year ; 				
					$final_result->month  		           = $month ; 				
				    $final_result->arabic_max       	   = $arabic_max ; 
				    $final_result->arabic_min       	   = $arabic_min ; 
				    $final_result->arabic    		   = $arabic ; 				    
				    $final_result->english_ol_max    		   = $english_ol_max ; 
				    $final_result->english_ol_min    		   = $english_ol_max ; 
				    $final_result->english_ol   		   = $english_ol ; 
				    $final_result->french_ol_max 		       = $french_ol_max ;  
				    $final_result->french_ol_min 		       = $french_ol_max ;  
                    $final_result->french_ol 		       = $french_ol ;
					$final_result->algebra_max 		       = $algebra_max ;  
					$final_result->algebra_min 		       = $algebra_max ;  
                    $final_result->algebra 		       = $algebra ;
					$final_result->geometry_max 		       = $geometry_max ;  
					$final_result->geometry_min 		       = $geometry_max ;  
                    $final_result->geometry 		       = $geometry ;
                    $final_result->physics_max 		       = $physics_max ;
                    $final_result->physics_min 		       = $physics_max ;
                    $final_result->physics 		       = $physics ;
                    $final_result->chemistry_max 		       = $chemistry_max ;
                    $final_result->chemistry_min 		       = $chemistry_max ;
                    $final_result->chemistry 		       = $chemistry ;                 
                    $final_result->biology_max 		       = $biology_max ;
                    $final_result->biology_min 		       = $biology_max ;
                    $final_result->biology 		       = $biology ;
                    $final_result->history_max 		       = $history_max ;
                    $final_result->history_min 		       = $history_max ;
                    $final_result->history 		       = $history ;
                    $final_result->geography_max 		       = $geography_max ;
                    $final_result->geography_min 		       = $geography_max ;
                    $final_result->geography 		       = $geography ;
                    $final_result->philosophy_max 		       = $philosophy_max ;
                    $final_result->philosophy_min 		       = $philosophy_max ;
                    $final_result->philosophy	 		       = $philosophy	 ;
                    $final_result->total	 		       = $total	 ;
                    $final_result->religion_max 		       = $religion_max ;
                    $final_result->religion_min 		       = $religion_max ;
                    $final_result->religion 		       = $religion ;
					$final_result->civics_max 		       = $civics_max ;
					$final_result->civics_min 		       = $civics_max ;
					$final_result->civics 		           = $civics ;
					$final_result->computer_max 		       = $computer_max ;
					$final_result->computer_min 		       = $computer_max ;
					$final_result->computer 		       = $computer ;
                    $final_result->english_al_max 		       = $english_al_max ;
                    $final_result->english_al_min 		       = $english_al_max ;
					$final_result->english_al 		       = $english_al ;
					$final_result->french_al_max 		       = $french_al_max ;
					$final_result->french_al_min 		       = $french_al_max ;
					$final_result->french_al 		       = $french_al ;
                    $final_result->attendance_max 		       = $attendance_max ;
					$final_result->attendance_min 		       = $attendance_min ;
					$final_result->attendance 		       = $attendance ;
					
				  
				    // process insert and show me the fucken message   
				    $make_insert 						   = $final_result->insert() ; 

				    
					
				}

				if($make_insert){
	    			$data = array('status'=>'work' , 'message'=>"Months  report  For Secondary School - First Stage Has Been Added To database successfully" ) ; 
							echo json_encode($data) ; 
				 
				    }


			}else if($stage == "second_secondary_art"){


					while (($file_open = fgetcsv($handle , 1000 , ",")) !== false ) {
					
					// get fields in the excel file : 
					$student_code 								= $file_open[0] ; 
					$student_name 								= $file_open[1] ; 		 
					$class        								= $file_open[2] ; 
					$seat_number  								= $file_open[3] ; 
					$arabic_max       							= $file_open[4] ; 
					$arabic_min       							= $file_open[5] ; 
					$arabic    								    = $file_open[6] ; 
					$english_ol_max        						= $file_open[7] ; 
					$english_ol_min        						= $file_open[8] ; 
					$english_ol     						    = $file_open[9] ; 
					$french_ol_max   							= $file_open[10] ; 
					$french_ol_min   							= $file_open[11] ; 
                    $french_ol                                  = $file_open[12] ; 
					$algebra_max             				    = $file_open[13] ; 
					$algebra_min             				    = $file_open[14] ; 
					$algebra                                    = $file_open[15] ; 
					$calculus_trig_max             			        = $file_open[16] ; 
					$calculus_trig_min             			        = $file_open[17] ; 
					$calculus_trig                                   = $file_open[18] ; 
                    $history_max             				    = $file_open[19] ; 
                    $history_min             				    = $file_open[20] ; 
                    $history             			            = $file_open[21] ;        
                    $philosophy_max             		        = $file_open[22] ; 
                    $philosophy_min             		        = $file_open[23] ; 
                    $philosophy             				    = $file_open[24] ; 

                    $psychology_max             		        = $file_open[25] ; 
                    $psychology_min             		        = $file_open[26] ; 
                    $psychology             				    = $file_open[27] ; 

                    $geography_max             		            = $file_open[28] ; 
                    $geography_min             		            = $file_open[29] ; 
                    $geography             			            = $file_open[30] ; 
                    $total             			            = $file_open[31] ; 

                    $religion_max             				    = $file_open[32] ; 
                    $religion_min             				    = $file_open[33] ; 
                    $religion             				     	= $file_open[34] ; 
                    $human_rights_max             				    = $file_open[35] ;
                    $human_rights_min             				    = $file_open[36] ;
					$human_rights             				     	= $file_open[37] ; 
                    $computer_max             				    = $file_open[38] ; 
					$computer_min             				    = $file_open[39] ; 
					$computer             				     	= $file_open[40] ; 
					$english_al_max             				= $file_open[41] ; 
					$english_al_min             				= $file_open[42] ; 
					$english_al             				    = $file_open[43] ; 
					$french_al_max             				    = $file_open[44] ; 
					$french_al_min             				    = $file_open[45] ; 
					$french_al             				     	= $file_open[46] ; 					
	                $attendance_max             				= $file_open[47] ; 
					$attendance_min             				= $file_open[48] ; 
					$attendance             				    = $file_open[49] ; 
	

				    // make object from final result class : 
				    $final_result = new MonthSecondarySecondArt() ; 

				    $final_result->student_code 		   = $student_code ; 
				    $final_result->student_name 		   = $student_name ; 
				    $final_result->class        		   = $class ; 
					$final_result->stage        		   = $stage ; 
				    $final_result->seat_number             = $seat_number ;                        
					$final_result->year  		           = $year ; 				
					$final_result->month  		           = $month ; 				
				    $final_result->arabic_max       	   = $arabic_max ; 
				    $final_result->arabic_min       	   = $arabic_min ; 
				    $final_result->arabic    		   = $arabic ; 				    
				    $final_result->english_ol_max    		   = $english_ol_max ; 
				    $final_result->english_ol_min    		   = $english_ol_max ; 
				    $final_result->english_ol   		   = $english_ol ; 
				    $final_result->french_ol_max 		       = $french_ol_max ;  
				    $final_result->french_ol_min 		       = $french_ol_max ;  
                    $final_result->french_ol 		       = $french_ol ;
					$final_result->algebra_max 		       = $algebra_max ;  
					$final_result->algebra_min 		       = $algebra_max ;  
                    $final_result->algebra 		       = $algebra ;
					$final_result->calculus_trig_max 		       = $calculus_trig_max ;  
					$final_result->calculus_trig_min 		       = $calculus_trig_min ;  
                    $final_result->calculus_trig 		       = $calculus_trig ;
                    $final_result->history_max 		       = $history_max ;
                    $final_result->history_min 		       = $history_max ;
                    $final_result->history 		       = $history ;
                    $final_result->philosophy_max 		       = $philosophy_max ;
                    $final_result->philosophy_min 		       = $philosophy_max ;
                    $final_result->philosophy	 		       = $philosophy	 ;
                    $final_result->psychology_max 		       = $psychology_max ;
                    $final_result->psychology_min 		       = $psychology_min ;
                    $final_result->psychology	 		       = $psychology	 ;
                    $final_result->geography_max 		       = $geography_max ;
                    $final_result->geography_min 		       = $geography_max ;
                    $final_result->geography 		       = $geography ;
                    $final_result->total 		       = $total ;
                   
                    $final_result->religion_max 		       = $religion_max ;
                    $final_result->religion_min 		       = $religion_max ;
                    $final_result->religion 		       = $religion ;
					$final_result->human_rights_max 		       = $human_rights_max ;
					$final_result->human_rights_min 		       = $human_rights_min ;
					$final_result->human_rights 		           = $human_rights ;
					$final_result->computer_max 		       = $computer_max ;
					$final_result->computer_min 		       = $computer_max ;
					$final_result->computer 		       = $computer ;
                    $final_result->english_al_max 		       = $english_al_max ;
                    $final_result->english_al_min 		       = $english_al_max ;
					$final_result->english_al 		       = $english_al ;
					$final_result->french_al_max 		       = $french_al_max ;
					$final_result->french_al_min 		       = $french_al_max ;
					$final_result->french_al 		       = $french_al ;
                    $final_result->attendance_max 		       = $attendance_max ;
					$final_result->attendance_min 		       = $attendance_min ;
					$final_result->attendance 		       = $attendance ;
					
				  
				    // process insert and show me the fucken message   
				    $make_insert 						   = $final_result->insert() ; 

				    
					
				}

				if($make_insert){
	    			$data = array('status'=>'work' , 'message'=>"Months  report  For Secondary School - Second Stage - Arts Section Has Been Added To database successfully" ) ; 
							echo json_encode($data) ; 
				 
				    }




			}else if ($stage == "second_secondary_science"){
				   

				  while (($file_open = fgetcsv($handle , 1000 , ",")) !== false ) {
					
					// get fields in the excel file : 
					$student_code 								= $file_open[0] ; 
					$student_name 								= $file_open[1] ; 		 
					$class        								= $file_open[2] ; 
					$seat_number  								= $file_open[3] ; 
					$arabic_max       							= $file_open[4] ; 
					$arabic_min       							= $file_open[5] ; 
					$arabic    								    = $file_open[6] ; 
					$english_ol_max        						= $file_open[7] ; 
					$english_ol_min        						= $file_open[8] ; 
					$english_ol     						    = $file_open[9] ; 
					$french_ol_max   							= $file_open[10] ; 
					$french_ol_min   							= $file_open[11] ; 
                    $french_ol                                  = $file_open[12] ; 
					$algebra_max             				    = $file_open[13] ; 
					$algebra_min             				    = $file_open[14] ; 
					$algebra                                    = $file_open[15] ; 
					$calculus_trig_max             			        = $file_open[16] ; 
					$calculus_trig_min             			        = $file_open[17] ; 
					$calculus_trig                                   = $file_open[18] ; 
                    $physics_max             				    = $file_open[19] ; 
                    $physics_min             				    = $file_open[20] ; 
                    $physics             			            = $file_open[21] ;        
                    $chemistry_max             		        = $file_open[22] ; 
                    $chemistry_min             		        = $file_open[23] ; 
                    $chemistry             				    = $file_open[24] ; 

                    $biology_max             		        = $file_open[25] ; 
                    $biology_min             		        = $file_open[26] ; 
                    $biology             				    = $file_open[27] ; 

                    $mechanics_max             		            = $file_open[28] ; 
                    $mechanics_min             		            = $file_open[29] ; 
                    $mechanics             			            = $file_open[30] ; 
                    $total             			            = $file_open[31] ; 

                    $religion_max             				    = $file_open[32] ; 
                    $religion_min             				    = $file_open[33] ; 
                    $religion             				     	= $file_open[34] ; 
                    $human_rights_max             				    = $file_open[35] ;
                    $human_rights_min             				    = $file_open[36] ;
					$human_rights             				     	= $file_open[37] ; 
                    $computer_max             				    = $file_open[38] ; 
					$computer_min             				    = $file_open[39] ; 
					$computer             				     	= $file_open[40] ; 
					$english_al_max             				= $file_open[41] ; 
					$english_al_min             				= $file_open[42] ; 
					$english_al             				    = $file_open[43] ; 
					$french_al_max             				    = $file_open[44] ; 
					$french_al_min             				    = $file_open[45] ; 
					$french_al             				     	= $file_open[46] ; 					
	                $attendance_max             				= $file_open[47] ; 
					$attendance_min             				= $file_open[48] ; 
					$attendance             				    = $file_open[49] ; 
	

				    // make object from final result class : 
				    $final_result = new MonthSecondarySecondScience() ; 

				    $final_result->student_code 		   = $student_code ; 
				    $final_result->student_name 		   = $student_name ; 
				    $final_result->class        		   = $class ; 
					$final_result->stage        		   = $stage ; 
				    $final_result->seat_number             = $seat_number ;                        
					$final_result->year  		           = $year ; 				
					$final_result->month  		           = $month ; 				
				    $final_result->arabic_max       	   = $arabic_max ; 
				    $final_result->arabic_min       	   = $arabic_min ; 
				    $final_result->arabic    		   = $arabic ; 				    
				    $final_result->english_ol_max    		   = $english_ol_max ; 
				    $final_result->english_ol_min    		   = $english_ol_max ; 
				    $final_result->english_ol   		   = $english_ol ; 
				    $final_result->french_ol_max 		       = $french_ol_max ;  
				    $final_result->french_ol_min 		       = $french_ol_max ;  
                    $final_result->french_ol 		       = $french_ol ;
					$final_result->algebra_max 		       = $algebra_max ;  
					$final_result->algebra_min 		       = $algebra_max ;  
                    $final_result->algebra 		       = $algebra ;
					$final_result->calculus_trig_max 		       = $calculus_trig_max ;  
					$final_result->calculus_trig_min 		       = $calculus_trig_min ;  
                    $final_result->calculus_trig 		       = $calculus_trig ;
                    $final_result->physics_max		       = $physics_max ;
                    $final_result->physics_min 		       = $physics_min ;
                    $final_result->physics 		       = $physics ;
                    $final_result->chemistry_max 		       = $chemistry_max ;
                    $final_result->chemistry_min 		       = $chemistry_min ;
                    $final_result->chemistry	 		       = $chemistry	 ;
                    $final_result->biology_max 		       = $biology_max ;
                    $final_result->biology_min 		       = $biology_min ;
                    $final_result->biology	 		       = $biology	 ;
                    $final_result->mechanics_max 		       = $mechanics_max ;
                    $final_result->mechanics_min 		       = $mechanics_min ;
                    $final_result->mechanics 		       = $mechanics ;
                    $final_result->total 		       = $total ;
                   
                    $final_result->religion_max 		       = $religion_max ;
                    $final_result->religion_min 		       = $religion_max ;
                    $final_result->religion 		       = $religion ;
					$final_result->human_rights_max 		       = $human_rights_max ;
					$final_result->human_rights_min 		       = $human_rights_min ;
					$final_result->human_rights 		           = $human_rights ;
					$final_result->computer_max 		       = $computer_max ;
					$final_result->computer_min 		       = $computer_max ;
					$final_result->computer 		       = $computer ;
                    $final_result->english_al_max 		       = $english_al_max ;
                    $final_result->english_al_min 		       = $english_al_max ;
					$final_result->english_al 		       = $english_al ;
					$final_result->french_al_max 		       = $french_al_max ;
					$final_result->french_al_min 		       = $french_al_max ;
					$final_result->french_al 		       = $french_al ;
                    $final_result->attendance_max 		       = $attendance_max ;
					$final_result->attendance_min 		       = $attendance_min ;
					$final_result->attendance 		       = $attendance ;
					
				  
				    // process insert and show me the fucken message   
				    $make_insert 						   = $final_result->insert() ; 

				    
					
				}

				if($make_insert){
	    			$data = array('status'=>'work' , 'message'=>"Months  report  For Secondary School - Second Stage - Science Section Has Been Added To database successfully" ) ; 
							echo json_encode($data) ; 
				 
				    }



			}

			
					

				

			

}
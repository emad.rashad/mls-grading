<?php 
require_once('../../../../classes/Session.php'); 
require_once('../../../../classes/Functions.php'); 
require_once('../../../../classes/MysqlDatabase.php'); 
require_once('../../../../classes/Users.php'); 
require_once('../../../../classes/MidYearPrimarySecondThird.php'); 
require_once('../../../../classes/MidYearPrimaryFourthFifth.php'); 
require_once('../../../../classes/MidYearPrepFirstSecond.php'); 
require_once('../../../../classes/MidYearSecondaryFirst.php');
require_once('../../../../classes/MidYearSecondarySecondArt.php');
require_once('../../../../classes/MidYearSecondarySecondScience.php');

header('Content-Type: application/json');


if($_SERVER['REQUEST_METHOD'] == "POST" && isset($_FILES["file"]["type"])){
	

	$msg = ''; 
	$uploaded = FALSE;
	$allowed_extensions = array("csv"); // file allowed_extensions to be checked
	$tmp = explode(".", $_FILES["file"]["name"]) ; 
	$extension = end($tmp) ; 
	$fileTypes = array("text/comma-separated-values","text/csv"); // file types to be checked
	$file = $_FILES["file"]['tmp_name'];



				// get stage posted : 
				$stage = $_POST['stage'] ; 
				
				$year = $_POST['year']; 
				// get file  info : 
				$handle = fopen($file , 'r') ; 
				/*
				======================================================== Demonstration ==========================================================
				i check stage here cause there are different subjects in each stage  but they will be controlled from one place  
				this is a challenge and needs concentration ,,,, more than concentration ,,,,, god will be with me -_- 
				*/
				// check stage 
				if($stage == "second_primary" || $stage == "third_primary"){


				while (($file_open = fgetcsv($handle , 1000 , ",")) !== false ) {
					
					// get fields in the excel file : 
					$student_code 								= $file_open[0] ; 
					$student_name 								= $file_open[1] ; 		 
					$class        								= $file_open[2] ; 
					$seat_number  								= $file_open[3] ; 
					$max       								    = $file_open[4] ; 
					$arabic    								    = $file_open[5] ; 
					$arabic_eval        								= $file_open[6] ; 
					$maths     						    = $file_open[7] ; 
					$maths_eval   								= $file_open[8] ; 
                    $english_ol                                      = $file_open[9] ; 
					$english_ol_eval             				     	= $file_open[10] ; 
                    $religion             				     	= $file_open[11] ; 
                    $religion_eval             				     	= $file_open[12] ; 
                    $english_al             				     	= $file_open[13] ; 
                    $english_al_eval             				     	= $file_open[14] ; 
                    $french_max             				     	= $file_open[15] ;            
                    $french             				     	= $file_open[16] ;
                    $french_eval             				     	= $file_open[17] ; 
                    $extra_curricular_one             				     	= $file_open[18] ; 
                    $extra_curricular_one_eval             				     	= $file_open[19] ; 
                    $extra_curricular_two             				     	= $file_open[20] ; 
                    $extra_curricular_two_eval             				     	= $file_open[21] ; 
                    $art             				     	= $file_open[22] ; 
                    $art_eval             				     	= $file_open[23] ; 
                    $p_e             				     	= $file_open[24] ; 
                    $p_e_eval             				     	= $file_open[25] ; 
                   
                                        
					
					

				    // make object from final result class : 
				    $final_result = new MidYearPrimarySecondThird() ; 

				    $final_result->student_code 		   = $student_code ; 
				    $final_result->student_name 		   = $student_name ; 
				    $final_result->class        		   = $class ; 
					$final_result->stage        		   = $stage ; 
				    $final_result->seat_number             = $seat_number ;     
                    $final_result->max        		       = $max ;
					$final_result->year  		           = $year ; 
					
				    $final_result->arabic       		   = $arabic ; 
				    $final_result->arabic_eval    		       = $arabic_eval ; 
				    
				    $final_result->maths    		   = $maths ; 
				    $final_result->maths_eval   		   = $maths_eval ; 
				    $final_result->english_ol 		       = $english_ol ;  
                    $final_result->english_ol_eval 		       = $english_ol_eval ;
                    $final_result->religion 		       = $religion ;
                    $final_result->religion_eval 		       = $religion_eval ;
                    $final_result->english_al 		       = $english_al ;
                    $final_result->english_al_eval 		       = $english_al_eval ;
                    $final_result->french_max 		       = $french_max ;
                    $final_result->french 		       = $french ;
                    $final_result->french_eval 		       = $french_eval ;
                    $final_result->extra_curricular_one 		       = $extra_curricular_one ;
                    $final_result->extra_curricular_one_eval 		       = $extra_curricular_one_eval ;
                    $final_result->extra_curricular_two 		       = $extra_curricular_two ;
                    $final_result->extra_curricular_two_eval 		       = $extra_curricular_two_eval ;
                    $final_result->art 		       = $art ;
                    $final_result->art_eval	 		       = $art_eval	 ;
                    $final_result->p_e 		       = $p_e ;
                    $final_result->p_e_eval 		       = $p_e_eval ;
                  

				   
				 


				    // process insert and show me the fucken message   
				    $make_insert 						   = $final_result->insert() ; 

				    
					
				}

				if($make_insert){
	    			$data = array('status'=>'work' , 'message'=>"Mid Year report  For Primary School Has Been Added To database successfully" ) ; 
							echo json_encode($data) ; 
				 
				    }


			}else if($stage == "fourth_primary" || $stage == "fifth_primary"){

				while (($file_open = fgetcsv($handle , 1000 , ",")) !== false ) {
					
					// get fields in the excel file : 
					$student_code 								= $file_open[0] ; 
					$student_name 								= $file_open[1] ; 		 
					$class        								= $file_open[2] ; 
					$seat_number  								= $file_open[3] ; 
					$max       								    = $file_open[4] ; 
					$arabic    								    = $file_open[5] ; 
					$arabic_eval        						= $file_open[6] ; 
					$maths     						    		= $file_open[7] ; 
					$maths_eval   								= $file_open[8] ; 
                    $english_ol                                 = $file_open[9] ; 
					$english_ol_eval             				= $file_open[10] ; 
					$social_studies                             = $file_open[11] ; 
					$social_studies_st             			    = $file_open[12] ; 
					$science                                    = $file_open[13] ; 
					$science_eval             				    = $file_open[14] ; 
					$english_al             				    = $file_open[15] ; 
                    $english_al_eval             				= $file_open[16] ; 
                    $religion             				     	= $file_open[17] ; 
                    $religion_eval             				    = $file_open[18] ;            
                    $french             				     	= $file_open[19] ;
                    $french_eval             				    = $file_open[20] ; 
                    $extra_curricular_one             			= $file_open[21] ; 
                    $extra_curricular_one_eval             		= $file_open[22] ; 
                    $extra_curricular_two             			= $file_open[23] ; 
                    $extra_curricular_two_eval             		= $file_open[24] ; 
                    $art             				     		= $file_open[25] ; 
                    $art_eval             				     	= $file_open[26] ; 
                    $p_e             				     		= $file_open[27] ; 
                    $p_e_eval             				     	= $file_open[28] ; 
                   
                                        
					
					

				    // make object from final result class : 
				    $final_result = new MidYearPrimaryFourthFifth() ; 

				    $final_result->student_code 		   = $student_code ; 
				    $final_result->student_name 		   = $student_name ; 
				    $final_result->class        		   = $class ; 
					$final_result->stage        		   = $stage ; 
				    $final_result->seat_number             = $seat_number ;     
                    $final_result->max        		       = $max ;
					$final_result->year  		           = $year ; 
					
				    $final_result->arabic       		   = $arabic ; 
				    $final_result->arabic_eval    		   = $arabic_eval ; 
				    
				    $final_result->maths    		   = $maths ; 
				    $final_result->maths_eval   		   = $maths_eval ; 

				    $final_result->english_ol 		       = $english_ol ;  
                    $final_result->english_ol_eval 		       = $english_ol_eval ;

					$final_result->social_studies 		       = $social_studies ;  
                    $final_result->social_studies_st 		       = $social_studies_st ;

					$final_result->science 		       = $science ;  
                    $final_result->science_eval 		       = $science_eval ;


                    $final_result->english_al 		       = $english_al ;
                    $final_result->english_al_eval 		       = $english_al_eval ;

                    $final_result->religion 		       = $religion ;
                    $final_result->religion_eval 		       = $religion_eval ;
                  
                   
                    $final_result->french 		       = $french ;
                    $final_result->french_eval 		       = $french_eval ;

                    $final_result->extra_curricular_one 		       = $extra_curricular_one ;
                    $final_result->extra_curricular_one_eval 		       = $extra_curricular_one_eval ;
                    $final_result->extra_curricular_two 		       = $extra_curricular_two ;
                    $final_result->extra_curricular_two_eval 		       = $extra_curricular_two_eval ;

                    $final_result->art 		       = $art ;
                    $final_result->art_eval	 		       = $art_eval	 ;
                    $final_result->p_e 		       = $p_e ;
                    $final_result->p_e_eval 		       = $p_e_eval ;
                  

				   
				 


				    // process insert and show me the fucken message   
				    $make_insert 						   = $final_result->insert() ; 

				    
					
				}

				if($make_insert){
	    			$data = array('status'=>'work' , 'message'=>"Mid Year report  For Primary School Has Been Added To database successfully" ) ; 
							echo json_encode($data) ; 
				 
				    }

			}else if ($stage == "first_prep" || $stage == "second_prep"){
					
					


					while (($file_open = fgetcsv($handle , 1000 , ",")) !== false ) {
					
					// get fields in the excel file : 
					$student_code 								= $file_open[0] ; 
					$student_name 								= $file_open[1] ; 		 
					$class        								= $file_open[2] ; 
					$seat_number  								= $file_open[3] ; 
					$max       								    = $file_open[4] ; 
					$mid_year    								    = $file_open[5] ; 
					$years_work        						= $file_open[6] ; 
					$arabic_mid_year     						    		= $file_open[7] ; 
					$arabic_years_work   								= $file_open[8] ; 
                    $arabic_total                                 = $file_open[9] ; 
					$arabic_eval             				= $file_open[10] ; 
					$maths_mid_year                             = $file_open[11] ; 
					$maths_years_work             			    = $file_open[12] ; 
					$maths_total                                    = $file_open[13] ; 
					$maths_eval             				    = $file_open[14] ; 
					$english_ol_mid_year             				    = $file_open[15] ; 
                    $english_ol_years_work             				= $file_open[16] ; 
                    $english_ol_total             				     	= $file_open[17] ; 
                    $english_ol_eval             				    = $file_open[18] ;            
                    $social_studies_mid_year             				     	= $file_open[19] ;
                    $social_studies_years_work             				    = $file_open[20] ; 
                    $social_studies_total             			= $file_open[21] ; 
                    $social_studies_eval             		= $file_open[22] ; 
                    $science_mid_year             			= $file_open[23] ; 
                    $science_years_work             		= $file_open[24] ; 
                    $science_total             				     		= $file_open[25] ; 
                    $science_eval             				     	= $file_open[26] ; 
                    $computer_mid_year             				     		= $file_open[27] ; 
                    $computer_years_work             				     	= $file_open[28] ;
					$computer_total             				     	= $file_open[29] ; 
					$computer_eval             				     	= $file_open[30] ; 
					$art_mid_year             				     	= $file_open[31] ; 
					$art_years_work             				     	= $file_open[32] ; 
					$art_total             				     	= $file_open[33] ; 
					$art_eval             				     	= $file_open[34] ; 

					$religion_mid_year             				     	= $file_open[35] ; 


					$religion_years_work             				     	= $file_open[36] ; 
					$religion_total             				     	= $file_open[37] ; 
					$religion_eval             				     	= $file_open[38] ; 
					$english_al_mid_year             				     	= $file_open[39] ; 
					$english_al_years_work             				     	= $file_open[40] ; 
					$english_al_total             				     	= $file_open[41] ; 
					$english_al_eval             				     	= $file_open[42] ; 
					$french_mid_year             				     	= $file_open[43] ; 
					$french_years_work             				     	= $file_open[44] ; 
					$french_total             				     	= $file_open[45] ; 
					$french_eval             				     	= $file_open[46] ; 
					$extra_curricular_one_max             				     	= $file_open[47] ; 
					$extra_curricular_one_total             				     	= $file_open[48] ; 
					$extra_curricular_one_eval             				     	= $file_open[49] ; 
					$extra_curricular_two_max             				     	= $file_open[50] ; 
					$extra_curricular_two_total             				     	= $file_open[51] ; 
					$extra_curricular_two_eval             				     	= $file_open[52] ; 
					
	

				    // make object from final result class : 
				    $final_result = new MidYearPrepFirstSecond() ; 

				    $final_result->student_code 		   = $student_code ; 
				    $final_result->student_name 		   = $student_name ; 
				    $final_result->class        		   = $class ; 
					$final_result->stage        		   = $stage ; 
				    $final_result->seat_number             = $seat_number ; 
					$final_result->max            		   = $max ;                        
					$final_result->year  		           = $year ;
					$final_result->mid_year_mark  		           = $mid_year ;
					$final_result->year_work_mark  		           = $years_work ; 	



				    $final_result->arabic_mid_year       		   = $arabic_mid_year ; 
				    $final_result->arabic_year_work    		   = $arabic_years_work ; 				    
				    $final_result->arabic_total    		   = $arabic_total ; 
				    $final_result->arabic_eval   		   = $arabic_eval ; 
				    $final_result->maths_mid_year 		       = $maths_mid_year ;  
                    $final_result->maths_years_work 		       = $maths_years_work ;
					$final_result->maths_total 		       = $maths_total ;  
                    $final_result->maths_eval 		       = $maths_eval ;
					$final_result->english_ol_mid_year 		       = $english_ol_mid_year ;  
                    $final_result->english_ol_years_work 		       = $english_ol_years_work ;
                    $final_result->english_ol_total 		       = $english_ol_total ;
                    $final_result->english_ol_eval 		       = $english_ol_eval ;

                    $final_result->social_studies_mid_year 		       = $social_studies_mid_year ;
                    $final_result->social_studies_years_work 		       = $social_studies_years_work ;                 
                    $final_result->social_studies_total 		       = $social_studies_total ;
                    $final_result->social_studies_eval 		       = $social_studies_eval ;

                    $final_result->science_mid_year 		       = $science_mid_year ;
                    $final_result->science_years_work 		       = $science_years_work ;
                    $final_result->science_total 		       = $science_total ;
                    $final_result->science_eval 		       = $science_eval ;

                    $final_result->computer_mid_year 		       = $computer_mid_year ;
                    $final_result->computer_years_work	 		       = $computer_years_work	 ;
                    $final_result->computer_total 		       = $computer_total ;
                    $final_result->computer_eval 		       = $computer_eval ;
					$final_result->art_mid_year 		       = $art_mid_year ;
					$final_result->art_years_work 		           = $art_years_work ;
					$final_result->art_total 		       = $art_total ;
					$final_result->art_eval 		       = $art_eval ;
					$final_result->religion_mid_year 		       = $religion_mid_year ;
					$final_result->religion_years_work 		       = $religion_years_work ;
					$final_result->religion_total 		       = $religion_total ;
					$final_result->religion_eval 		       = $religion_eval ;


					$final_result->english_al_mid_year 		       = $english_al_mid_year ;
					$final_result->english_al_years_work 		       = $english_al_years_work ;
					$final_result->english_al_total 		       = $english_al_total ;
					$final_result->english_al_eval 		       = $english_al_eval ;
					
					$final_result->french_mid_year 		       = $french_mid_year ;

					$final_result->french_years_work 		       = $french_years_work ;
					
					$final_result->french_total 		       = $french_total ;
					$final_result->french_eval 		       = $french_eval ;
					$final_result->extra_curricular_one_max 		       = $extra_curricular_one_max ;
					$final_result->extra_curricular_one_total 		       = $extra_curricular_one_total ;
					$final_result->extra_curricular_one_eval 		       = $extra_curricular_one_eval ;
					$final_result->extra_curricular_two_max 		       = $extra_curricular_two_max ;
					$final_result->extra_curricular_two_total 		       = $extra_curricular_two_total ;
					$final_result->extra_curricular_two_eval 		       = $extra_curricular_two_eval ;
					 

				  
				    // process insert and show me the fucken message   
				    $make_insert 						   = $final_result->insert() ; 

				    
					
				}

				if($make_insert){
	    			$data = array('status'=>'work' , 'message'=>"Mid Year report  For Prepratory School  Has Been Added To database successfully" ) ; 
							echo json_encode($data) ; 
				 
				    }

















			}else if($stage == "first_secondary"){

					while (($file_open = fgetcsv($handle , 1000 , ",")) !== false ) {
					
					// get fields in the excel file : 
					$student_code 								= $file_open[0] ; 
					$student_name 								= $file_open[1] ; 		 
					$class        								= $file_open[2] ; 
					$seat_number  								= $file_open[3] ; 
					$arabic_max       								    = $file_open[4] ; 
					$arabic    								    = $file_open[5] ; 
					$english_ol_max        						= $file_open[6] ; 
					$english_ol     						    		= $file_open[7] ; 
					$french_ol_max   								= $file_open[8] ; 
                    $french_ol                                 = $file_open[9] ; 
					$algebra_max             				= $file_open[10] ; 
					$algebra                             = $file_open[11] ; 
					$geometry_max             			    = $file_open[12] ; 
					$geometry                                    = $file_open[13] ; 
					$physics_max             				    = $file_open[14] ; 
					$physics             				    = $file_open[15] ; 
                    $chemistry_max             				= $file_open[16] ; 
                    $chemistry             				     	= $file_open[17] ; 
                    $biology_max             				    = $file_open[18] ;            
                    $biology             				     	= $file_open[19] ;
                    $history_max             				    = $file_open[20] ; 
                    $history             			= $file_open[21] ; 
                    $geography_max             		= $file_open[22] ; 
                    $geography             			= $file_open[23] ; 
                    $philosophy_max             		= $file_open[24] ; 
                    $philosophy             				     		= $file_open[25] ; 
                    $religion_max             				     	= $file_open[26] ; 
                    $religion             				     		= $file_open[27] ; 
                    $civics_max             				     	= $file_open[28] ;
					$civics             				     	= $file_open[29] ; 
					$english_al_max             				     	= $file_open[30] ; 
					$english_al             				     	= $file_open[31] ; 
					$french_al_max             				     	= $file_open[32] ; 
					$french_al             				     	= $file_open[33] ; 
					$computer_max             				     	= $file_open[34] ; 
					$computer             				     	= $file_open[35] ; 
	

				    // make object from final result class : 
				    $final_result = new MidYearSecondaryFirst() ; 

				    $final_result->student_code 		   = $student_code ; 
				    $final_result->student_name 		   = $student_name ; 
				    $final_result->class        		   = $class ; 
					$final_result->stage        		   = $stage ; 
				    $final_result->seat_number             = $seat_number ;                        
					$final_result->year  		           = $year ; 				
				    $final_result->arabic_max       		   = $arabic_max ; 
				    $final_result->arabic    		   = $arabic ; 				    
				    $final_result->english_ol_max    		   = $english_ol_max ; 
				    $final_result->english_ol   		   = $english_ol ; 
				    $final_result->french_ol_max 		       = $french_ol_max ;  
                    $final_result->french_ol 		       = $french_ol ;
					$final_result->algebra_max 		       = $algebra_max ;  
                    $final_result->algebra 		       = $algebra ;
					$final_result->geometry_max 		       = $geometry_max ;  
                    $final_result->geometry 		       = $geometry ;
                    $final_result->physics_max 		       = $physics_max ;
                    $final_result->physics 		       = $physics ;
                    $final_result->chemistry_max 		       = $chemistry_max ;
                    $final_result->chemistry 		       = $chemistry ;                 
                    $final_result->biology_max 		       = $biology_max ;
                    $final_result->biology 		       = $biology ;
                    $final_result->history_max 		       = $history_max ;
                    $final_result->history 		       = $history ;
                    $final_result->geography_max 		       = $geography_max ;
                    $final_result->geography 		       = $geography ;
                    $final_result->philosophy_max 		       = $philosophy_max ;
                    $final_result->philosophy	 		       = $philosophy	 ;
                    $final_result->religion_max 		       = $religion_max ;
                    $final_result->religion 		       = $religion ;
					$final_result->civics_max 		       = $civics_max ;
					$final_result->civics 		           = $civics ;
					$final_result->english_al_max 		       = $english_al_max ;
					$final_result->english_al 		       = $english_al ;
					$final_result->french_al_max 		       = $french_al_max ;
					$final_result->french_al 		       = $french_al ;
					$final_result->computer_max 		       = $computer_max ;
					$final_result->computer 		       = $computer ;
				  
				    // process insert and show me the fucken message   
				    $make_insert 						   = $final_result->insert() ; 

				    
					
				}

				if($make_insert){
	    			$data = array('status'=>'work' , 'message'=>"Mid Year report  For Secondary School - First Stage Has Been Added To database successfully" ) ; 
							echo json_encode($data) ; 
				 
				    }


			}else if($stage == "second_secondary_art"){


					 while (($file_open = fgetcsv($handle , 1000 , ",")) !== false ) {
					
					// get fields in the excel file : 
					$student_code 								= $file_open[0] ; 
					$student_name 								= $file_open[1] ; 		 
					$class        								= $file_open[2] ; 
					$seat_number  								= $file_open[3] ; 
					$arabic_max       								    = $file_open[4] ; 
					$arabic    								    = $file_open[5] ; 
					$english_ol_max        						= $file_open[6] ; 
					$english_ol     						    		= $file_open[7] ; 
					$french_ol_max   								= $file_open[8] ; 
                    $french_ol                                 = $file_open[9] ; 
					$algebra_max             				= $file_open[10] ; 
					$algebra                             = $file_open[11] ; 
					$calculus_trig_max             			    = $file_open[12] ; 
					$calculus_trig                                    = $file_open[13] ; 
					$history_max             				    = $file_open[14] ; 
					$history             				    = $file_open[15] ; 
                    $philosophy_max             				= $file_open[16] ; 
                    $philosophy             				     	= $file_open[17] ; 
                    $psychology_max             				    = $file_open[18] ;            
                    $psychology             				     	= $file_open[19] ;
                    $geography_max             				    = $file_open[20] ; 
                    $geography             			= $file_open[21] ; 
                    $religion_max             		= $file_open[22] ; 
                    $religion             			= $file_open[23] ; 
                    $civics_max             		= $file_open[24] ; 
                    $civics             				     		= $file_open[25] ; 
                    $english_al_max             				     	= $file_open[26] ; 
                    $english_al             				     		= $file_open[27] ; 
                    $french_al_max             				     	= $file_open[28] ;
					$french_al             				     	= $file_open[29] ; 
					$computer_max             				     	= $file_open[30] ; 
					$computer             				     	= $file_open[31] ; 
					$agriculture_max             				     	= $file_open[32] ; 
					$agriculture             				     	= $file_open[33] ; 
					
	

				    // make object from final result class : 
				    $final_result = new MidYearSecondarySecondArt() ; 

				    $final_result->student_code 		   = $student_code ; 
				    $final_result->student_name 		   = $student_name ; 
				    $final_result->class        		   = $class ; 
					$final_result->stage        		   = $stage ; 
				    $final_result->seat_number             = $seat_number ;                        
					$final_result->year  		           = $year ; 				
				    $final_result->arabic_max       		   = $arabic_max ; 
				    $final_result->arabic    		   = $arabic ; 				    
				    $final_result->english_ol_max    		   = $english_ol_max ; 
				    $final_result->english_ol   		   = $english_ol ; 
				    $final_result->french_ol_max 		       = $french_ol_max ;  
                    $final_result->french_ol 		       = $french_ol ;
					$final_result->algebra_max 		       = $algebra_max ;  
                    $final_result->algebra 		       = $algebra ;
					$final_result->calculus_trig_max 		       = $calculus_trig_max ;  
                    $final_result->calculus_trig 		       = $calculus_trig ;
    
                    $final_result->history_max 		       = $history_max ;
                    $final_result->history 		       = $history ;
					$final_result->philosophy_max 		       = $philosophy_max ;
                    $final_result->philosophy	 		       = $philosophy	 ;

					$final_result->psychology_max 		       = $psychology_max ;
                    $final_result->psychology	 		       = $psychology	 ;


                    $final_result->geography_max 		       = $geography_max ;
                    $final_result->geography 		           = $geography ;
                  
                    $final_result->religion_max 		       = $religion_max ;
                    $final_result->religion 		       = $religion ;
					$final_result->civics_max 		       = $civics_max ;
					$final_result->civics 		           = $civics ;
					$final_result->english_al_max 		       = $english_al_max ;
					$final_result->english_al 		       = $english_al ;
					$final_result->french_al_max 		       = $french_al_max ;
					$final_result->french_al 		       = $french_al ;
					$final_result->computer_max 		       = $computer_max ;
					$final_result->computer 		       = $computer ;

					$final_result->agriculture_max 		       = $agriculture_max ;
					$final_result->agriculture 		           = $agriculture ;
				  
				    // process insert and show me the fucken message   
				    $make_insert 						   = $final_result->insert() ; 

				    
					
				}

				if($make_insert){
	    			$data = array('status'=>'work' , 'message'=>"Mid Year report  For Secondary School - Second Stage - Arts Section Has Been Added To database successfully" ) ; 
							echo json_encode($data) ; 
				 
				    }





			}else if ($stage == "second_secondary_science"){
				   

				   while (($file_open = fgetcsv($handle , 1000 , ",")) !== false ) {
					
					// get fields in the excel file : 
					$student_code 								= $file_open[0] ; 
					$student_name 								= $file_open[1] ; 		 
					$class        								= $file_open[2] ; 
					$seat_number  								= $file_open[3] ; 
					$arabic_max       								    = $file_open[4] ; 
					$arabic    								    = $file_open[5] ; 
					$english_ol_max        						= $file_open[6] ; 
					$english_ol     						    		= $file_open[7] ; 
					$french_ol_max   								= $file_open[8] ; 
                    $french_ol                                 = $file_open[9] ; 
					$algebra_max             				= $file_open[10] ; 
					$algebra                             = $file_open[11] ; 
					$calculus_trig_max             			    = $file_open[12] ; 
					$calculus_trig                                    = $file_open[13] ; 
					$physics_max             				    = $file_open[14] ; 
					$physics             				    = $file_open[15] ; 
                    $chemistry_max             				= $file_open[16] ; 
                    $chemistry             				     	= $file_open[17] ; 
                    $biology_max             				    = $file_open[18] ;            
                    $biology             				     	= $file_open[19] ;
                    $mechanics_max             				    = $file_open[20] ; 
                    $mechanics             			= $file_open[21] ; 
                    $religion_max             		= $file_open[22] ; 
                    $religion             			= $file_open[23] ; 
                    $civics_max             		= $file_open[24] ; 
                    $civics             				     		= $file_open[25] ; 
                    $english_al_max             				     	= $file_open[26] ; 
                    $english_al             				     		= $file_open[27] ; 
                    $french_al_max             				     	= $file_open[28] ;
					$french_al             				     	= $file_open[29] ; 
					$computer_max             				     	= $file_open[30] ; 
					$computer             				     	= $file_open[31] ; 
					$agriculture_max             				     	= $file_open[32] ; 
					$agriculture             				     	= $file_open[33] ; 
					
	

				    // make object from final result class : 
				    $final_result = new MidYearSecondarySecondScience() ; 

				    $final_result->student_code 		   = $student_code ; 
				    $final_result->student_name 		   = $student_name ; 
				    $final_result->class        		   = $class ; 
					$final_result->stage        		   = $stage ; 
				    $final_result->seat_number             = $seat_number ;                        
					$final_result->year  		           = $year ; 				
				    $final_result->arabic_max       		   = $arabic_max ; 
				    $final_result->arabic    		   = $arabic ; 				    
				    $final_result->english_ol_max    		   = $english_ol_max ; 
				    $final_result->english_ol   		   = $english_ol ; 
				    $final_result->french_ol_max 		       = $french_ol_max ;  
                    $final_result->french_ol 		       = $french_ol ;
					$final_result->algebra_max 		       = $algebra_max ;  
                    $final_result->algebra 		       = $algebra ;
					$final_result->calculus_trig_max 		       = $calculus_trig_max ;  
                    $final_result->calculus_trig 		       = $calculus_trig ;
    
                    $final_result->physics_max 		       = $physics_max ;
                    $final_result->physics 		       = $physics ;
					$final_result->chemistry_max 		       = $chemistry_max ;
                    $final_result->chemistry	 		       = $chemistry	 ;

					$final_result->biology_max 		       = $biology_max ;
                    $final_result->biology	 		       = $biology	 ;


                    $final_result->mechanics_max 		       = $mechanics_max ;
                    $final_result->mechanics 		           = $mechanics ;
                  
                    $final_result->religion_max 		       = $religion_max ;
                    $final_result->religion 		       = $religion ;
					$final_result->civics_max 		       = $civics_max ;
					$final_result->civics 		           = $civics ;
					$final_result->english_al_max 		       = $english_al_max ;
					$final_result->english_al 		       = $english_al ;
					$final_result->french_al_max 		       = $french_al_max ;
					$final_result->french_al 		       = $french_al ;
					$final_result->computer_max 		       = $computer_max ;
					$final_result->computer 		       = $computer ;

					$final_result->agriculture_max 		       = $agriculture_max ;
					$final_result->agriculture 		           = $agriculture ;
				  
				    // process insert and show me the fucken message   
				    $make_insert 						   = $final_result->insert() ; 

				    
					
				}

				if($make_insert){
	    			$data = array('status'=>'work' , 'message'=>"Mid Year report  For Secondary School - Second Stage - Science Section Has Been Added To database successfully" ) ; 
							echo json_encode($data) ; 
				 
				    }





			}

			
					

				

			

}
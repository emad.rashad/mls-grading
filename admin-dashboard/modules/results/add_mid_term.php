<?php 
	require_once("../layout/initialize.php"); 
  require_once("../layout/header.php"); 
  

?> 
<!--header end--> 
<!--sidebar start--> 
<?php require_once("../layout/navigation.php");?> 
<!--sidebar end-->  
<!--main content start--> 
<section id="main-content"> 
 <script src="../../js-crud/midterm_js_file.js"></script>
  <section class="wrapper site-min-height"> 
    <h4>MLS - Grading System</h4> 
    <!-- page start--> 
    <section class="panel"> 
      <header class="panel-heading">Mid Term Module</header> 
      <br> 
     

     
		

     <br> 
      <div class="panel-body" style="height:600px; "> 
       
        <div class="adv-table editable-table ">  
          
          <div class="space15">



         
          		<form action="data_model/add_mid_term.php" method="post" class="add_csv_final" id="form_mid_term" enctype="multipart/form-data">
          			
          		<input type="hidden" id="process_type" value="mid-term">




   <div class="form-group" > 
        <label  class="col-lg-3 ">Type</label> 
      <div class="col-lg-8"> 
        <select class="form-control" id="type"> 
          <option value="" disabled selected>Select Type</option> 
          <option name="tp" value="primary">Primary</option> 
          <option name="tp" value="prep">Prepratory</option> 
            
        </select> 
      </div> 
   </div>
          		
                             
 <div id="space">
   
 </div>
  <div class="form-group hide " id="primary_div"> 
        <label  class="col-lg-3 ">Stage</label> 
      <div class="col-lg-8"> 
        <select class="form-control" id="primary"> 
          <option value="" disabled selected>Select Type</option> 
          <option name="pr" value="second_primary">Second Primary</option> 
          <option name="pr" value="third_primary">Third Primary</option> 
          <option name="pr" value="fourth_primary">Fourth Primary</option>    
          <option name="pr" value="fifth_primary">Fifth Primary</option>    
        </select> 
      </div>

   </div>


  <div class="form-group hide " id="prep_div"> 
        <label  class="col-lg-3 ">Stage</label> 
      <div class="col-lg-8"> 
        <select class="form-control" id="prep"> 
          <option value="" disabled selected>Select Type</option> 
          <option name="pe" value="first_prep">First Prepratory</option> 
          <option name="pe" value="second_prep">Second Prepratory</option> 
             
        </select> 
      </div>

   </div>

    

   
 <br>
 <br>

<div class="form-group hide" id="year_div"> 
        <label  class="col-lg-3 ">Academic Year : </label> 
      <div class="col-lg-8"> 
        <input type="text" class="form-control" id="year" placeholder="ex : 2015/2016" >
      </div>

   </div>

<br>
 <br>

<div class="form-group hide" id="mid_term_div"> 
        <label  class="col-lg-3 ">Mid Term Type : </label> 
      <div class="col-lg-8"> 
        <select class="form-control" id="mid_type"> 
          <option value="" disabled selected>Select Mid Term</option> 
          <option name="mi" value="first_mid_term">First Mid-Term</option> 
          <option name="mi" value="second_mid_term">Second Mid-Term</option> 
             
        </select>
      </div>

</div>
    


<br>
<br>
 	 <div class="form-group">
 	 	 <label  class="col-lg-3 ">Select Result Sheet</label> 
		<input type="file"  name="uploadResult" id="uploadResult">
   </div>
     <br>
 

		 
  <button type="submit" name="submit" class="btn btn-danger" id="submit"> 
  <i class="icon-circle-arrow-right" style="padding-right: 5px;"></i>Click To Upload Results</button> 
		
  
  <br>
  


</form>
<br>
<div class="form-group">
  <div id="success_insert" ></div>
</div>




        	
        </div> 
      </div> 
    </section> 
    <!-- page end-->  
  </section> 
</section> 

</div> 
<!--main content end-->  
<!--footer start--> 
<?php require_once("../layout/footer.php");?>
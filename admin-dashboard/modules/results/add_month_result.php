<?php 
	require_once("../layout/initialize.php"); 
  require_once("../layout/header.php"); 
  

?> 
<!--header end--> 
<!--sidebar start--> 
<?php require_once("../layout/navigation.php");?> 
<!--sidebar end-->  
<!--main content start--> 
<section id="main-content"> 
 <script src="../../js-crud/months_js_file.js"></script>
  <section class="wrapper site-min-height"> 
    <h4>MLS - Grading System</h4> 
    <!-- page start--> 
    <section class="panel"> 
      <header class="panel-heading">Months Results Module</header> 
      <br> 
     

     
		

     <br> 
      <div class="panel-body" style="height:600px; "> 
       
        <div class="adv-table editable-table ">  
          
          <div class="space15">



         
          		<form action="data_model/add_month_result.php" method="post" class="add_csv_final" id="form_mid_term" enctype="multipart/form-data">
          			
          		<input type="hidden" id="process_type" value="month">




   <div class="form-group" > 
        <label  class="col-lg-3 ">Stage</label> 
      <div class="col-lg-8"> 
        <select class="form-control" id="stage"> 
          <option value="" disabled selected>Select Type</option> 
          <option name="tp" value="first_secondary">First Secondary</option> 
          <option name="tp" value="second_secondary_art">Second Secondary Art</option> 
          <option name="tp" value="second_secondary_science">Second Secondary Science</option>    
        </select> 
      </div> 
   </div>
          		
                             
 <div id="space">
   
 </div>
  <div class="form-group hide " id="month_div"> 
        <label  class="col-lg-3 ">Months</label> 
      <div class="col-lg-8"> 
        <select class="form-control" id="months"> 
          <option value="" disabled selected>Select Month</option> 
          <option name="pr" value="oct">October</option> 
          <option name="pr" value="nov">November</option> 
          <option name="pr" value="dec">December</option>    
          <option name="pr" value="feb">February</option>    
          <option name="pr" value="march">March</option>    
          <option name="pr" value="april">April</option>    
        </select> 
      </div>

   </div>


 <br>
 <br>

<div class="form-group hide" id="year_div"> 
        <label  class="col-lg-3 ">Academic Year : </label> 
      <div class="col-lg-8"> 
        <input type="text" class="form-control" id="year" placeholder="ex : 2015/2016" >
      </div>

</div>
<br>
<br>

 	 <div class="form-group">
 	 	 <label  class="col-lg-3 ">Select Result Sheet</label> 
		<input type="file"  name="uploadResult" id="uploadResult">
   </div>
     <br>
 

		 
  <button type="submit" name="submit" class="btn btn-danger" id="submit"> 
  <i class="icon-circle-arrow-right" style="padding-right: 5px;"></i>Click To Upload Results</button> 
		
  
  <br>
  


</form>
<br>
<div class="form-group">
  <div id="success_insert" ></div>
</div>




        	
        </div> 
      </div> 
    </section> 
    <!-- page end-->  
  </section> 
</section> 

</div> 
<!--main content end-->  
<!--footer start--> 
<?php require_once("../layout/footer.php");?>
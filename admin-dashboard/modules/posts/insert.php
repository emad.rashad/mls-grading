<?php  
require_once("../layout/initialize.php"); 
$languages = Localization::find_all(); 
//get all layouts 
$models =  ThemeLayoutModel::find_all('id','DESC');	 
//get all auhtor and category	 
$define_class = new Taxonomies(); 
$define_class->enable_relation(); 
//get authors 
$authors = $define_class->taxonomies_data('author',null,null,'inserted_date', 'DESC',null,'many',$general_setting_info->translate_lang_id); 
//get taxonomy tags 
$taxonomy_tags = $define_class->taxonomies_data('tag',null,null,'inserted_date', 'DESC',null,'many',$general_setting_info->translate_lang_id); 
$all_tags = array(); 
foreach($taxonomy_tags as $tag){ 
  $all_tags[] = $tag->name; 
} 
$tags =  join("','", array_values($all_tags)); 
//folder gallery 
$path = "../../../media-library/"; 
$galleries = array_diff(scandir($path), array('..', '.'));  
require_once("../layout/header.php");	 
include("../../assets/texteditor4/head.php");  
?> 
<!--use below jquery and css for tags--> 

<script type="text/javascript" src="../../js-crud/post.js"></script> 
<script type="text/javascript" src="../../js-crud/gallery.js"></script> 
<script type="text/javascript" src="../../js-crud/auto_complete.js"></script> 

<!--header end--> 
<!--sidebar start--> 
<?php require_once("../layout/navigation.php");?> 
<!--sidebar end-->  
<!--main content start--> 
<section id="main-content"> 
  <section class="wrapper site-min-height"> 
    <h4>Posts Module</h4> 
    <!-- page start--> 
    <div class="row"> 
      <aside class="col-lg-8"> 
        <section> 
          <div class="panel"> 
            <div class="panel-heading"> Add Post </div> 
            <div class="panel-body "> 
              <form class="form-horizontal tasi-form" role="form" id="form_crud" action="data_model/insert.php"> 
                <input type="hidden" id="process_type" value="insert"> 
                <section class="panel "> 
                  <header class="panel-heading tab-bg-dark-navy-blue"> 
                    <ul class="nav nav-tabs"> 
                      <li class=" center-block active" > <a data-toggle="tab" href="#main_option" class="text-center"><strong> Main Info</strong></a></li> 
                       <li class=" center-block"> <a data-toggle="tab" href="#model_images" class="text-center"> <strong> Gallery & Images </strong></a></li> 
                      <li> <a data-toggle="tab" href="#taxonomies" class="text-center"><strong> Taxonomies</strong> </a> </li> 

                       <li> <a data-toggle="tab" href="#item_info" class="text-center"><strong>Item Info</strong> </a> </li
                    ></ul> 
                  </header> 
                  <div class="panel-body"> 
                    <div class="tab-content"> 
                      <div id="main_option" class="tab-pane active"> 
                        <section class="panel col-lg-9"> 
                          <header class="panel-heading tab-bg-dark-navy-blue "> 
                            <ul class="nav nav-tabs"> 
                              <?php 
                //create tabs for all available languages  
                $languages = Localization::find_all('id','desc'); 
                $serial_tabs = 1; 
                foreach($languages as $language){ 
                    $lang_tab_header = ucfirst($language->name); 
                    echo "<li class='";if($serial_tabs == 1){ echo " active ";}  echo"'> <a data-toggle='tab' href='#$language->name'><strong>$lang_tab_header</strong></a></li>"; 
                    $serial_tabs++; 
                } 
              ?> 
                            </ul> 
                          </header> 
                          <div class="panel-body"> 
                            <div class="tab-content"> 
                              <?php 
                $serial_tabs_content = 1; 
                foreach($languages as $language){ 
                        echo " 
                        <div id='$language->name' class='tab-pane"; if($serial_tabs_content == 1){ echo " active ";} echo"'> 
                                <div class='form-group'> 
                                <label  class='col-lg-3'>Title:</label> 
                                <div class='col-lg-9'> 
                                  <input type='text' class='form-control main_content' id='title_$language->label'  autocomplete='off'  
                                  onchange=\"add_char('title_$language->label','alias_$language->label')\"> 
                                </div> 
                              </div> 
                              <div class='form-group'> 
                                <label class='col-lg-3'>Alias:</label> 
                                <div class='col-lg-9'> 
                                  <input type='text' class='form-control main_content' id='alias_$language->label'> 
                                </div> 
                              </div> 
                              <div class='form-group'> 
                                <label  class='col-lg-3'>Summary:</label> 
                                <div class='col-lg-9'> 
                                  <textarea class=' form-control main_content' id='summary_$language->label'></textarea> 
                                </div> 
                              </div> 
                              <div class='form-group'> 
                                <label class='col-lg-3'>Body:</label> 
                                <div class='col-md-9'> 
                                  <textarea class='form-control main_content' id='full_content_$language->label'></textarea> 
                                </div> 
                              </div> 
                              <div class='form-group'> 
                                <label  class='col-lg-3'>Meta Keys:</label> 
                                <div class='col-lg-9'> 
                                  <input type='text' class='form-control main_content' id='meta_keys_$language->label' autocomplete='off'> 
                                </div> 
                              </div> 
                              <div class='form-group'> 
                                <label  class='col-lg-3'>Meta Description:</label> 
                                <div class='col-lg-9'> 
                                  <input type='text' class='form-control main_content' id='meta_description_$language->label'> 
                                </div> 
                              </div> 
                        </div>"; 
                    $serial_tabs_content++; 
                } 
              ?> 
                            </div> 
                          </div> 
                        </section> 
                      </div> 
                      <div id="model_images" class="tab-pane">
                        <div class="form-group"> 
                          <label  class="col-lg-2">Cover Image:</label> 
                          <div class="col-lg-9"> <a href="../file_mangers/media_filemanager/view_media_directories.php" id="image_cover">Select Image</a> 
                            <input type="hidden" class="form-control" id="imageVal" placeholder=" " autocomplete="off" value=""> 
                            <div style="display:none" id="imageShow"> <img src="" id="imageSrc" style="width:100px; height:200px;"></div> 
                          </div> 
                        </div> 
                        <div class="form-group"> 
                          <label  class="col-lg-2">Slider Image:</label> 
                          <div class="col-lg-9"> <a href="../file_mangers/media_filemanager_slider/view_media_files.php?title=slider" id="slider_cover">Select Image</a> <span style="font-size:12px">(Width:940XHeightt:390)</span> 
                            <input type="hidden" class="form-control" id="imageVal_slider" value=""> 
                            <div  id="imageSlider" style="display:none"> <img src="" id="imageSrcSlider" style="width:200px; height:100px"></div> 
                          </div> 
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2">Gallery Images:</label>
                            <div class="adv-table editable-table col-lg-10 ">
                                 <table class="table table-striped table-hover table-bordered image_gallery_tbl">
                              <thead>
                                <tr>
                                  <th class="name">Image</th>
                                  <th class="sort">Caption</th>
                                  <th class="sort" colspan="3">Sort</th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr class="selected_image_gallery" id="1">
                                    <td><input type="hidden" class="image_gallery_name" id="imageVal1">
                                    <div style="display:none" id="imageShow1"> <img src="" id="imageSrc1" style="width:80px; height:80px;"></div></td>
                                    <td><input type='text' class='form-control image_gallery_caption'  autocomplete='off'></td>
                                    <td><input type='text' class='form-control image_gallery_sort'  autocomplete='off'></td>
                                    <td width="30%">
                                     <a href='../file_mangers/filemanager_product_gallery/view_media_directories.php?selected_gallery_row=1'
                                     class='btn btn-primary btn-xs tooltips image_cover' data-placement='top' data-toggle='tooltip' data-original-title='Add new value'>
                                     <i class='icon-picture'></i></a>
                                    <a href='' class='btn btn-primary btn-xs tooltips add_tr' data-placement='top' data-toggle='tooltip'
                                    data-original-title='Add new value'><i class=' icon-plus-sign-alt'></i></a>
                                    </td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                      <div id="taxonomies" class="tab-pane "> 
                        <div class="form-group"> 
                          <label  class="col-lg-2 ">Author:</label> 
                          <div class="col-lg-8"> 
                            <select class="form-control" id="author"> 
                              <option value="0"> Select Author </option> 
                              <?php 
									foreach($authors as $author){ 
										 echo "<option value='$author->id'>$author->name</option>"; 
									 } 
								?> 
                            </select> 
                          </div> 
                        </div> 
                        <div class="form-group" > 
                          <label class="col-lg-2"> Categories:</label> 
                          <div class="col-lg-8"> 
                            <ul class="selected_category"> 
                            </ul> 
                            <br> 
                            <a id="show_inserted_data" href="../utilities/categories.php" class="btn btn-default btn-info">Select Category</a> </div> 
                        </div> 
                        <div class="form-group">
                        <label class="col-lg-2"> Tags:</label>
                        <div class="col-lg-8">
                         <input type="text" name="autosuggest" value="" id="autosuggest" autocomplete="off" class="form-control" />
                             <div id="autosuggest_container"></div>
                             <br />
                            
                             <div id="tagHtml" style="list-style:none"> </div>
                             <!-- Hidden fields don't delete lastNumber -->
                            <input type="hidden" name="tagsList" value="" id="tagsList"  />
                            <input type="hidden" name="lastNumber" value="0" id="lastNumber" />
                        </div>
                     </div> 
                      </div> 
                      
                      <div id="item_info" class="tab-pane "> 
                        <div class="form-group"> 
                          <label  class="col-lg-4">Product Price </label> 
                          <div class="col-lg-8"> 
                            <input type="text" placeholder="price" id="item_price" class="form-control">
                          </div> 
                        </div> 
                        
                        <div class="form-group" > 
                          <label class="col-lg-4">Product Before Discount </label> 
                          <div class="col-lg-8"> 
                             <input type="text" placeholder="price before discount" id="item_discount" class="form-control">
                        </div>
                        </div>
                        
                        <div class="form-group" > 
                          <label class="col-lg-4">Product Code </label> 
                          <div class="col-lg-8"> 
                             <input type="text" placeholder="product Code" id="item_code" class="form-control">
                        </div>
                        </div> 
                        <div class="form-group">
                        <label class="col-lg-4"> Product Quantity </label>
                        <div class="col-lg-8">
                         <input type="text"  value="" id="item_qty" placeholder=" quantity" class="form-control" />
                           
                            
                             
                             <!-- Hidden fields don't delete lastNumber -->
                            <input type="hidden" name="tagsList" value="" id="tagsList"  />
                            <input type="hidden" name="lastNumber" value="0" id="lastNumber" />
                        </div>
                     </div> 
                     
                     <div class="form-group" > 
                          <label class="col-lg-4">Product Company </label> 
                          <div class="col-lg-8"> 
                             <input type="text" placeholder="product company" id="item_company" class="form-control">
                        </div>
                        </div>
                        
                        <div class="form-group" > 
                          <label class="col-lg-4">Product Title </label> 
                          <div class="col-lg-8"> 
                             <input type="text" placeholder="product name" id="item_title" class="form-control">
                        </div>
                        </div>
                        
                        
                        <div class="form-group" > 
                          <label class="col-lg-4">Product Size </label> 
                          <div class="col-lg-8"> 
                             <input type="text" placeholder=" size" id="item_size" class="form-control">
                        </div>
                        </div>
                        
                        
                       
                        
                        
                     
                     
                      </div> 
                      
                      
                    </div> 
                  </div> 
                </section> `
                <div class="form-group"> 
                  <div class="col-lg-offset-2 col-lg-4"> 
                    <button type="submit" class="btn btn-info" id="submit">Save</button> 
                    <button type="reset" class="btn btn-default">Reset</button>
                    <div id="loading_data"></div> 
                  </div> 
                </div> 
              </form> 
            </div> 
          </div> 
        </section> 
      </aside> 
      <div class="col-lg-4"> 
        <section class="panel "> 
          <header class="panel-heading tab-bg-dark-navy-blue"> 
            <ul class="nav nav-tabs"> 
              <li class=" center-block active" style="width:170px"> <a data-toggle="tab" href="#op" class="text-center"> <i class=" icon-check"></i> <strong> Publish Option</strong></a></li> 
              <li style="width:170px"> <a data-toggle="tab" href="#op1" class="text-center"> <i class=" icon-calendar "></i> <strong> Publish Date </strong> </a> </li> 
            </ul> 
          </header> 
          <div class="panel-body"> 
            <form class="form-horizontal tasi-form" role="form" id="form_option"> 
              <div class="tab-content"> 
                <div id="op" class="tab-pane active "> <br /> 
                  <?php if($user_profile->post_publishing == 'yes'){?> 
                  <div class="form-group"> 
                    <label class="col-lg-6">Status:</label> 
                    <div class="col-lg-6"> 
                      <label class="checkbox-inline"> 
                        <input type="radio" name="shadow" class="radio" value="draft"> 
                        Draft</label> 
                      <label class="checkbox-inline"> 
                        <input type="radio" name="shadow" class="radio" value="publish" checked> 
                        Publish</label> 
                    </div> 
                  </div> 
                  <?php }else { ?> 
                  <input type="radio" name="shadow" class="radio" value="draft" checked style="display:none"> 
                  <?php }?> 
                  <div class="form-group"> 
                    <label class="col-lg-6 ">Enable Summary:</label> 
                    <div class="col-lg-5"> 
                      <label class="checkbox-inline"> 
                        <input type="radio" name="enable_summary" class="radio" value="yes"> 
                        Yas</label> 
                      <label class="checkbox-inline"> 
                        <input type="radio" name="enable_summary" class="radio" value="no" checked> 
                        No</label> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                    <label  class="col-lg-6 ">Enable Comments:</label> 
                    <div class="col-lg-5"> 
                      <label class="checkbox-inline"> 
                        <input type="radio" name="comments" class="radio" value="yes"> 
                        Yas</label> 
                      <label class="checkbox-inline"> 
                        <input type="radio"  name="comments" class="radio" value="no" checked> 
                        No</label> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                    <label class="col-lg-6 ">Front Page:</label> 
                    <div class="col-lg-5"> 
                      <label class="checkbox-inline"> 
                        <input type="radio" name="show_in_front" class="radio" value="yes" /> 
                        Yes</label> 
                      <label class="checkbox-inline"> 
                        <input type="radio" name="show_in_front" class="radio" value="no" checked/> 
                        No</label> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                    <label class="col-lg-6 ">Slide Show:</label> 
                    <div class="col-lg-5"> 
                      <label class="checkbox-inline"> 
                        <input type="radio" name="show_in_slide" class="radio" value="yes" /> 
                        Yes</label> 
                      <label class="checkbox-inline"> 
                        <input type="radio" name="show_in_slide" class="radio" value="no" checked/> 
                        No</label> 
                    </div> 
                  </div> 
                </div> 
                <div id="op1" class="tab-pane "> <br /> 
                  <div class="form-group"> 
                    <label class="col-lg-4">Start Publish:</label> 
                    <div class="col-lg-8"> 
                      <input type="text"  class="form-control"  value="" id="start_time"/> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                    <label class="col-lg-4">End Publish:</label> 
                    <div class="col-lg-8"> 
                      <input type="text"  class="form-control"  value="" id="end_time"/> 
                    </div> 
                  </div> 
                </div> 
              </div> 
            </form> 
          </div> 
        </section> 
      <section class="panel panel-primary"> 
          <header class="panel-heading">Layout Model: </header> 
          <div class="panel-body"> 
            <form class="form-horizontal tasi-form" role="form" id="form_category"> 
              <div class="form-group"> 
                <label class="col-lg-4"> Select Model:</label> 
                <div class="col-lg-8"> 
                  <select class="form-control" id="model"> 
                    <option value=""> Select Model </option> 
                    <?php 
					 	 foreach($models as $model){ 
							  if($model->type == 'post'){ 
							 echo "<option value='$model->id'>$model->name</option>"; 
						 }}?> 
                  </select> 
                </div> 
              </div> 
              
               
           </form> 
          </div> 
        </section> 
      </div> 
       
    </div> 
    <!-- page end-->  
  </section> 
</section> 
<!--main content end-->  
<!--footer start--> 
<?php require_once("../layout/footer.php");?> 
<!--use below jquery and css for tags--> 

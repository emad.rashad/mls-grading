<?php 
require_once("../layout/initialize.php"); 
if(isset($_GET['id']) && is_numeric($_GET['id'])){ 
	$record_id = $_GET['id']; 
	//get post info 
	$define_class = new Nodes(); 
	$define_class->enable_relation();
	$record_info = $define_class->node_data('post',$record_id) ;
	
 
	
	//check id access 
	if(empty($record_info->id)){ 
		redirect_to("view.php");	 
	}else{ 
		 //check globel edit authority 
		if($user_profile->global_edit != 'all_records' && $record_info->inserted_by != $user_data->id ){ 
			redirect_to('view.php');	 
		}else{  
			//define texonomy class to get all authors, tags 
			$define_taxonomy_class = new Taxonomies(); 
			$define_taxonomy_class->enable_relation(); 
			//get taxonomy tags 
			$taxonomy_tags = $define_taxonomy_class->taxonomies_data('tag',null,null,'inserted_date', 'DESC',null,'many',0); 
			$all_tags = array(); 
			foreach($taxonomy_tags as $tag){ 
				$all_tags[] = $tag->name; 
			} 
			$all_tags_separated =  join("','", array_values($all_tags)); 
			//all selected taxonomy  class 
			$define_node_selected_taxonomy = new NodesSelectedTaxonomies(); 
			$define_node_selected_taxonomy->enable_relation(); 
			$post_categories = $define_node_selected_taxonomy->return_node_taxonomy($record_id,'post','category','many',$general_setting_info->translate_lang_id);				 
			//tags inserted to post 
			$post_tags = $define_node_selected_taxonomy->return_node_taxonomy($record_id,'post','tag','many',0);	   
			//get all auhtor and category 
			$authors = $define_taxonomy_class->taxonomies_data('author',null,null,'inserted_date', 'DESC',null,'many',$general_setting_info->translate_lang_id); 
			//return selected author to post 
			$post_author = $define_node_selected_taxonomy->return_node_taxonomy($record_id,'post','author','one',$general_setting_info->translate_lang_id); 
			//get all layouts 
			$models =  ThemeLayoutModel::find_all_by_custom_filed('layout_id',4,'id','DESC'); 
			//get post model 
			$post_model = $define_class->get_model($record_id);	
			//image gallery 
			$image_gallery = NodesImageGallery::find_all_by_custom_filed("related_id",$record_id,'sort','asc');	
			//get all exhibitions 
			$exhibiions = $define_taxonomy_class->taxonomies_data('category',null,16,'inserted_date', 'ASC',null,'many',$main_lang_id); 	 
	} 
} 
}else{ 
  redirect_to("view.php");	 
} 
require_once("../layout/header.php"); 
include("../../assets/texteditor4/head.php");  
?>
<script type="text/javascript" src="../../js-crud/post.js"></script>
<script type="text/javascript" src="../../js-crud/gallery.js"></script>
<script type="text/javascript" src="../../js-crud/auto_complete.js"></script>  
<!--header end--> 
<!--sidebar start--> 
<?php require_once("../layout/navigation.php");?> 
<!--sidebar end-->  
<!--main content start--> 
<section id="main-content"> 
  <section class="wrapper site-min-height"> 
    <h4>Posts Module</h4> 
    <!-- page start--> 
    <div class="row"> 
    <aside class="col-lg-8"> 
      <section> 
        <div class="panel"> 
          <div class="panel-heading"> Edit Posts</div> 
          <div class="panel-body"> 
          <form class="form-horizontal tasi-form" role="form" id="form_crud" action="data_model/update.php" > 
            <input type="hidden" id="process_type" value="update"> 
            <input type="hidden" id="record" value="<?php echo $record_id; ?>"> 
            <section class="panel "> 
              <header class="panel-heading tab-bg-dark-navy-blue"> 
                <ul class="nav nav-tabs"> 
                  <li class=" center-block active" > <a data-toggle="tab" href="#main_option" class="text-center"><strong> Main Info</strong></a></li>
                  <li> <a data-toggle="tab" href="#model_images" class="text-center"><strong>Gallery &  Images</strong> </a> </li> 
                  <li> <a data-toggle="tab" href="#taxonomies" class="text-center"><strong> Taxonomies</strong> </a> </li> 
                   
                </ul> 
              </header> 
              <div class="panel-body"> 
                <div class="tab-content"> 
                  <div id="main_option" class="tab-pane active "> 
                  <section class="panel col-lg-9"> 
                    <header class="panel-heading tab-bg-dark-navy-blue "> 
                      <ul class="nav nav-tabs"> 
                        <?php 
                        //create tabs for all available languages  
                        $languages = Localization::find_all('id','desc'); 
                        $serial_tabs = 1; 
                        foreach($languages as $language){ 
                            $lang_tab_header = ucfirst($language->name); 
                            echo "<li class='";if($serial_tabs == 1){ echo " active ";}  echo"'> <a data-toggle='tab' href='#$language->name'> 
							<strong>$lang_tab_header</strong></a></li>"; 
                            $serial_tabs++; 
                        } 
                      ?> 
                      </ul> 
                    </header> 
                    <div class="panel-body"> 
                      <div class="tab-content"> 
                        <?php 
                        $serial_tabs_content = 1; 
                        foreach($languages as $language): 
                            //get data by language 
                            $main_content = $define_class->get_node_content($record_id, $language->id); 
							echo "<div id='$language->name' class='tab-pane"; if($serial_tabs_content == 1){ echo " active ";} echo"'>"; 
							?> 
                        <input class='main_content' type='hidden' id='<?php echo "content_id_$language->label"; ?>' 
                                 value='<?php if(!empty($main_content )){echo $main_content->id;}else{ echo "0";} ?>'> 
                        <div class='form-group'> 
                          <label  class='col-lg-3'>Title:</label> 
                          <div class='col-lg-9'> 
                            <input type='text' class='form-control main_content' id='<?php echo "title_$language->label";?>' autocomplete='off'  
                                     value='<?php if(!empty($main_content ))echo $main_content->title; ?>'  
                                     onchange="add_char('<?php echo "title_$language->label"?>','<?php echo "alias_$language->label"?>')"> 
                          </div> 
                        </div> 
                        <div class='form-group'> 
                          <label class='col-lg-3'>Alias:</label> 
                          <div class='col-lg-9'> 
                            <input type='text' class='form-control main_content' id='<?php echo "alias_$language->label";?>'  
                                     value='<?php if(!empty($main_content ))echo $main_content->alias; ?>'> 
                          </div> 
                        </div> 
                        <div class='form-group'> 
                          <label  class='col-lg-3'>Summary:</label> 
                          <div class='col-lg-9'> 
                            <textarea class=' form-control main_content' id='<?php echo "summary_$language->label";?>'> 
									<?php if(!empty($main_content ))echo $main_content->summary; ?></textarea> 
                          </div> 
                        </div> 
                        <div class='form-group'> 
                          <label class='col-lg-3'>Body:</label> 
                          <div class='col-lg-9'> 
                            <textarea class='form-control main_content' id='<?php echo "full_content_$language->label";?>'> 
                                    <?php if(!empty($main_content ))echo $main_content->body; ?></textarea> 
                          </div> 
                        </div> 
                        <div class='form-group'> 
                          <label  class='col-lg-3'>Meta Keys:</label> 
                          <div class='col-lg-9'> 
                            <input type='text' class='form-control main_content' id='<?php echo "meta_keys_$language->label";?>' autocomplete='off'  
									value='<?php if(!empty($main_content ))echo $main_content->meta_keys; ?>'> 
                          </div> 
                        </div> 
                        <div class='form-group'> 
                          <label  class='col-lg-3'>Meta Description:</label> 
                          <div class='col-lg-9'> 
                            <input type='text' class='form-control main_content' id='<?php echo "meta_description_$language->label";?>'  
									value='<?php if(!empty($main_content ))echo $main_content->meta_description; ?>'> 
                          </div> 
                        </div> 
                      </div> 
                      <?php   
					   $serial_tabs_content++; 
                       	endforeach; 
					?> 
                    </div> 
                 </div> 
                </div> 
                <div id="model_images" class="tab-pane  "> 
                <div class="form-group"> 
                    <label  class="col-lg-2">Cover Image:</label> 
                    <div class="col-lg-9"> <a href="../file_mangers/media_filemanager/view_media_directories.php" id="image_cover">Select Image</a> 
                      <input type="hidden" class="form-control" id="imageVal" placeholder=" " autocomplete="off" value="<?php if(!empty($record_info->cover_image)){?> ../../../media-library/<?php echo $record_info->cover_image?> <?php  }?>">  
                      <div <?php if(empty($record_info->cover_image)){?> style="display: none;"<?php }?> id="imageShow"> <img src="../../../media-library/<?php echo $record_info->cover_image?>" id="imageSrc" style="width:100px; height:200px;"></div> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                    <label  class="col-lg-2">Slider Cover:</label> 
                    <div class="col-lg-9"> <a href="../file_mangers/media_filemanager_slider/view_media_files.php?title=slider" id="slider_cover">Select Image</a> <span style="font-size:12px">(Width:940XHeightt:390)</span> 
                      <input type="hidden" class="form-control" id="imageVal_slider" value="<?php echo $record_info->slider_cover?>"> 
                      <div  id="imageSlider" <?php if(empty($record_info->slider_cover)){?> style="display: none;"<?php }?> > <img src="../../../media-library/slider/<?php echo $record_info->slider_cover;?>"  
   id="imageSrcSlider" style="width:200px; height:100;"></div> 
                    </div> 
                  </div>
                   <div class="form-group">
                        <label class="col-lg-2">Gallery Images : </label>
                        <div class="adv-table editable-table col-lg-10 ">
                            <table class="table table-striped table-hover table-bordered image_gallery_tbl">
                              <thead>
                                <tr>
                                  <th class="name">Image</th>
                                  <th class="caption">Caption</th>
                                  <th class="sort" colspan="3">Sort</th>
                                </tr>
                              </thead>
                              <tbody>
                               <?php
                              if(count($image_gallery) == 0){
                                  echo "
                                    <tr class='selected_image_gallery' id='1'>
                                        <td><input type='hidden' class='image_gallery_name' id='imageVal1'>
                                        <div style='display:none' id='imageShow1'> <img src='' id='imageSrc1' style='width:80px; height:80px;'></td>
                                        <td><input type='text' class='form-control image_gallery_caption'  autocomplete='off'></td>
                                        <td><input type='text' class='form-control image_gallery_sort'  autocomplete='off'></td>
                                        <td width='30%'>
                                        <a href='../file_mangers/filemanager_product_gallery/view_media_directories.php?selected_gallery_row=1'
                                         class='btn btn-primary btn-xs tooltips image_cover' data-placement='top' data-toggle='tooltip' data-original-title='Add new value'>
                                         <i class='icon-picture'></i></a>
                                        <a href='' class='btn btn-primary btn-xs tooltips add_tr' data-placement='top' data-toggle='tooltip'
                                        data-original-title='Add new value'><i class=' icon-plus-sign-alt'></i></a>
                                        </td>
                                    </tr>";
                              }else{
                                $serial = 1;
                                foreach($image_gallery as $record){
                                    echo "
                                        <tr class='selected_image_gallery' id='$record->id'>
                                        <td>
                                        <input type='hidden' class='image_gallery_name' id='imageVal$record->id' value='../../../media-library/$record->image'>
                                        <img src='../../../media-library/$record->image' id='imageSrc$record->id' style='width:80px; height:80px;'>
                                        </td>
                                        <td><input type='text' class='form-control image_gallery_caption'  autocomplete='off'  value='$record->caption'></td>
                                        <td><input type='text' class='form-control image_gallery_sort'  autocomplete='off'  value='$record->sort'></td>
                                         <td  width='30%'>
                                        <a href='../file_mangers/filemanager_product_gallery/view_media_directories.php?selected_gallery_row=$record->id'
                                         class='btn btn-primary btn-xs tooltips image_cover' data-placement='top' data-toggle='tooltip' data-original-title='Add new value'>
                                         <i class='icon-picture'></i></a>
                                        <a href='' class='btn btn-primary btn-xs tooltips add_tr' data-placement='top' data-toggle='tooltip'
                                        data-original-title='Add new value'><i class=' icon-plus-sign-alt'></i></a>&nbsp";
                           if($serial > 1){
                                 echo "<a href='#' data-toggle='modal' class='btn btn-danger btn-xs tooltips delete_tr' data-placement='top' data-original-title='Delete'>
                                   <i class='icon-remove'></i></a></td>";
                            }
                                         echo "
                                      </tr>";
                                      $serial++;
                                }
                              }
                            ?>
                              </tbody>
                            </table>

                         </div>
                        </div>
                    </div>
                <div id="taxonomies" class="tab-pane"> 
                  <div class="form-group"> 
                    <label  class="col-lg-2 ">Author:</label> 
                    <div class="col-lg-8"> 
                      <select class="form-control" id="author"> 
                        <option value="0"> Select Author </option> 
                        <?php 
								foreach($authors as $author){ 
									 echo "<option value='$author->id'"; 
									 if($post_author){ 
									  if($post_author->id == $author->id){ 
										  echo "selected"; 
									  } 
									 } 
									 echo">$author->name</option>"; 
								 } 
        					 ?> 
                      </select> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                      <label class="col-lg-2"> Categories:</label> 
                      <div class="col-lg-8"> 
                        <ul class="selected_category"> 
                          <?php  
                              foreach($post_categories as $category){ 
								  echo "<li id='$category->id'>- $category->taxonomy_name<a href='#' class='DeleteCategory glyphicon glyphicon-remove'></a></li>"; 
                              }?> 
                        </ul> 
                        <br> 
                        <a id="show_inserted_data" href="../utilities/categories.php" class="btn btn-default btn-info">Select Category</a> </div> 
                    </div> 
                  <div class="form-group">
                <label class="col-lg-2"> Tags:</label>
                <div class="col-lg-8">
                 <input type="text" name="autosuggest" value="" id="autosuggest" autocomplete="off" class="form-control" />
                     <div id="autosuggest_container"></div>
                     <br />
                    
                     <div id="tagHtml" style="list-style:none">
                     <?php 
					  $li_id = 1;
					  foreach($post_tags as $tag){
						     echo "<li class='tages'  id='li_{$li_id}' name='$tag->taxonomy_name'>$tag->taxonomy_name   <a onclick=removeTag('{$li_id}') href=javascript:void(0) id=tag_$li_id>X</a></li>";
							 $li_id++;
						}
						
					 ?>
                    </div>
                     <!-- Hidden fields don't delete lastNumber -->
                    <input type="hidden" name="tagsList" value="" id="tagsList"  />
                    <input type="hidden" name="lastNumber" value="<?php echo $li_id?>" id="lastNumber"  />
                </div>
                 
               </div> 
                </div> 
              
            </section> 
            
           
            
            
            <div class="form-group"> 
              <div class="col-lg-offset-2 col-lg-4">

                <button type="submit" class="btn btn-info" id="submit">Save</button> 
                <button type="reset" class="btn btn-default">Reset</button>
                <div id="loading_data"></div> 
              </div> 
            </div>
        </form>
      </section>
    </aside>
    <div class="col-lg-4"> 
        <section class="panel "> 
          <header class="panel-heading tab-bg-dark-navy-blue"> 
            <ul class="nav nav-tabs"> 
              <li class=" center-block active" style="width:170px"> <a data-toggle="tab" href="#op" class="text-center"> <i class=" icon-check"></i> <strong> Publish Option</strong></a></li> 
              <li style="width:170px"> <a data-toggle="tab" href="#op1" class="text-center"> <i class=" icon-calendar "></i> <strong> Publish Date </strong> </a> </li> 
            </ul> 
          </header> 
          <div class="panel-body"> 
            <form class="form-horizontal tasi-form" role="form" id="form_option"> 
              <div class="tab-content"> 
                <div id="op" class="tab-pane active "> <br /> 
                 <?php if($user_profile->post_publishing == 'yes'){?> 
                  <div class="form-group "> 
                    <label class="col-lg-6">Status:</label> 
                    <div class="col-lg-6"> 
                      <label class="checkbox-inline"> 
                        <input type="radio" name="shadow" class="radio" value="draft" <?php if($record_info->status=="draft") echo 'checked'?>> 
                        Draft</label> 
                      <label class="checkbox-inline"> 
                        <input type="radio"  name="shadow" class="radio" value="publish" <?php if($record_info->status=="publish") echo 'checked'?>> 
                        Publish</label> 
                    </div> 
                  </div> 
                  <?php }else { ?> 
                  <input type="radio" name="shadow" class="radio" value="<?php echo $record_info->status?>" checked style="display:none"> 
                  <?php }?> 
                  <div class="form-group"> 
                    <label class="col-lg-6">Enable Summary:</label> 
                    <div class="col-lg-6"> 
                      <label class="checkbox-inline"> 
                        <input type="radio" name="enable_summary" class="radio" value="yes" <?php if($record_info->enable_summary=="yes") echo 'checked'?>> 
                        Yes</label> 
                      <label class="checkbox-inline"> 
                        <input type="radio"  name="enable_summary" class="radio" value="no" <?php if($record_info->enable_summary=="no") echo 'checked'?>> 
                        No</label> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                    <label  class="col-lg-6">Enable Comments:</label> 
                    <div class="col-lg-6"> 
                      <label class="checkbox-inline"> 
                        <input type="radio" name="comments" class="radio" value="yes" <?php if($record_info->enable_comments == "yes") echo 'checked'?>> 
                        Yes</label> 
                      <label class="checkbox-inline"> 
                        <input type="radio"  name="comments" class="radio" value="no"  <?php if($record_info->enable_comments == "no") echo 'checked'?>> 
                        No</label> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                    <label class="col-lg-6 ">Front Page:</label> 
                    <div class="col-lg-6"> 
                      <label class="checkbox-inline"> 
                        <input type="radio" name="show_in_front" class="radio" value="yes" <?php if($record_info->front_page == "yes") echo 'checked'?>> 
                        Yes</label> 
                      <label class="checkbox-inline"> 
                        <input type="radio" name="show_in_front" class="radio" value="no" <?php if($record_info->front_page == "no") echo 'checked'?>> 
                        No</label> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                    <label class="col-lg-6">Slide Show:</label> 
                    <div class="col-lg-6"> 
                      <label class="checkbox-inline"> 
                        <input type="radio" name="show_in_slide" class="radio" value="yes" <?php if($record_info->slide_show == "yes") echo 'checked'?>> 
                        Yes</label> 
                      <label class="checkbox-inline"> 
                        <input type="radio" name="show_in_slide" class="radio" value="no" <?php if($record_info->slide_show == "no") echo 'checked'?>> 
                        No</label> 
                    </div> 
                  </div> 
                </div> 
                <div id="op1" class="tab-pane "> <br /> 
                  <div class="form-group"> 
                    <label class="col-lg-2">Start Publish:</label> 
                    <div class="col-lg-8"> 
                      <input type="text"  class="form-control"  value="<?php echo $record_info->start_publishing?>" id="start_time"/> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                    <label class="col-lg-2">End Publish:</label> 
                    <div class="col-lg-8"> 
                      <input type="text"  class="form-control"  value="<?php echo $record_info->end_publishing?>" id="end_time"/> 
                    </div> 
                  </div> 
              </div> 
            </form> 
          </div> 
        </section> 
        <section class="panel panel-primary"> 
          <header class="panel-heading">Layout Model: </header> 
          <div class="panel-body"> 
            <form class="form-horizontal tasi-form" role="form" id="form_category"> 
              <div class="form-group"> 
                    <label class="col-lg-4"> Select Model:</label> 
                    <div class="col-lg-8"> 
                      <select class="form-control" id="model"> 
                        <option value=""> Select Model </option> 
                        <?php 
							 foreach($models as $model){ 
								 if($model->type == 'post'){ 
								 	echo "<option value='$model->id'"; 
								  		if(!empty($post_model)){ 
									  		if($model->id == $post_model->model){ 
												 echo " selected ";  
									   		} 
									 	} 
									echo ">$model->name</option>"; 
							 	} 
							 } 
		 				?> 
                      </select> 
                    </div> 
                  </div> 
           </form> 
          </div> 
        </section> 
      </div> 
     
   
    </div> 
    <!-- page end-->  
  </section>
</section>




<!--main content end-->  
<!--footer start--> 
<?php require_once("../layout/footer.php");?> 
<!--use below jquery and css for tags--> 
  <script>

  $('.qun_store').keyup(function(){ // run anytime the value changes
    var total = 0
  $(".qun_store").each(function() {
	    var value = $(this).val()
		if(value != ""){
		  total+= parseInt(value);
		}
		//alert(value)

});
  $("#total_quantity").val(total);

  
  
  
  });

                
</script>


<?php 
require_once('../../../../classes/Session.php'); 
require_once('../../../../classes/Functions.php'); 
require_once('../../../../classes/MysqlDatabase.php'); 
require_once('../../../../classes/Users.php'); 
require_once('../../../../classes/Profile.php'); 
require_once('../../../../classes/NodesSelectedTaxonomies.php'); 
require_once('../../../../classes/Taxonomies.php'); 
require_once('../../../../classes/TaxonomiesContent.php'); 
require_once('../../../../classes/Nodes.php'); 
require_once('../../../../classes/NodesContent.php'); 
require_once('../../../../classes/Localization.php'); 
require_once('../../../../classes/NodesImageGallery.php');
require_once('../../../../classes/Quantity_Distribution.php');
//check  session user  log in 
if($session->is_logged() == false){ 
redirect_to("../../../index.php"); 
} 


// get user profile   
$user_data = Users::find_by_id($session->user_id); 
// get user profile data 
$user_profile  = Profile::Find_by_id($user_data->user_profile); 
// check if the user profile block 
if($user_profile->profile_block == "yes"){ 
	redirect_to("../../../index.php");	 
} 
if(!empty($_POST["task"]) && $_POST["task"] == "insert"){ 



//send json data 
header('Content-Type: application/json'); 
//validite required required 
$required_fields = array('model'=>'- Select Model'); 
$check_required_fields = check_required_fields($required_fields); 
if(count($check_required_fields) == 0){ 
	$add = new Nodes(); 
	$add->status = $_POST["shadow"]; 
	$add->node_type = 'post'; 
	$add->enable_comments = $_POST["enable_comments"]; 
	$add->enable_summary = $_POST["enable_summary"]; 
	$add->front_page = $_POST["front_page"]; 
	$add->slide_show = $_POST["slide_show"]; 
	$add->inserted_by = $session->user_id; 
    $add->model = $_POST["model"]; 
    $add->shortcut_link = strtotime(date_now()); 
	if(!empty($_POST['imageVal'])){ 
		$current_file = $_POST['imageVal']; 
		$parts = explode('/',$current_file); 
		$image_cover = $parts[count($parts)-1]; 
		$folder = $parts[count($parts)-2]; 
		$path = $folder."/".$image_cover; 
		$add->cover_image = $path; 
	}  
	 if($_POST['imageVal_slider']){ 
	        $current = $_POST['imageVal_slider']; 
			$parts_slider = explode('/',$current); 
			$image_covers = $parts_slider[count($parts_slider)-1]; 
			$add->slider_cover = $image_covers; 
	 } 
	$add->inserted_date = date_now();	 
	if(!empty($_POST["start_time"])){ 
		$add->start_publishing = $_POST["start_time"]; 
	}else{ 
		$add->start_publishing =  date_now(); 
	} 
	$add->end_publishing = $_POST["end_time"]; 
	$add->prod_price = $_POST["prod_price"];
	$add->prod_code = $_POST["prod_code"];
	$add->prod_qty = $_POST["prod_qty"];
	$add->prod_before_discount = $_POST["prod_before_discount"];
	$add->prod_company = $_POST["prod_company"];
	$add->prod_title = $_POST["prod_title"];
	$add->prod_size = $_POST["prod_size"];
	$insert = $add->insert(); 
	$inserted_post_id = $add->id; 
	if($insert){ 
	
	
	$qt_dis = new Quantity_Distribution(); 
 
	
		//retrieve all available languages 
		$languages = Localization::find_all('label','asc');		 
	   //insert post content  
		foreach($languages as $language){ 
			$add_post_content = new NodesContent(); 
			$add_post_content->node_id = $inserted_post_id; 
			$add_post_content->title = $_POST['main_content']['title_'.$language->label]; 
			$add_post_content->alias = $_POST['main_content']['alias_'.$language->label];	 
			$add_post_content->body = $_POST['main_content']['full_content_'.$language->label]; 
			$add_post_content->summary = $_POST['main_content']['summary_'.$language->label]; 
			$add_post_content->lang_id = $language->id; 
			$add_post_content->meta_description = $_POST['main_content']['meta_description_'.$language->label]; 
			$add_post_content->meta_keys = $_POST['main_content']['meta_keys_'.$language->label]; 
			$add_post_content->insert(); 
		} 
		//insert author 
		if($_POST["author_id"] != 0){ 
			$author_id = $_POST["author_id"]; 
			$add_author = new NodesSelectedTaxonomies(); 
			$add_author->node_id = $inserted_post_id; 
			$add_author->taxonomy_id = $author_id; 
			$add_author->node_type = 'post'; 
			$add_author->taxonomy_type = 'author'; 
			$insert_author = $add_author->insert();			 
		} 
		//insert selected_image_gallery 
		if(!empty($_POST["selected_image_gallery"])){ 
			//update orders table 
			$selected_image_gallery = $_POST["selected_image_gallery"]; 
			foreach($selected_image_gallery as $key => $value){ 
				if($key != 0){ 
					 $explode_value = explode(',', $value); 
					 if(!empty($explode_value[0])){ 
						 $add_image_gallery = new  NodesImageGallery(); 
						 $add_image_gallery->image = file_folder_src($explode_value[0]); 
						 $add_image_gallery->sort = $explode_value[1];
						 $add_image_gallery->caption = $explode_value[2]; 
						 $add_image_gallery->related_id = $inserted_post_id; 
						 $add_image_gallery->insert();						  
					 } 
				} 
			}	 
		}		 
		//insert categories 
		if(!empty($_POST["categories"])){ 
			//remove duplicates 
			$selected_categories = array_unique($_POST["categories"]); 
			//convert to be array 
			foreach($selected_categories as $category){ 
				$add_category = new NodesSelectedTaxonomies(); 
				$add_category->node_id = $inserted_post_id; 
				$add_category->taxonomy_id = $category; 
				$add_category->taxonomy_type = 'category'; 
				$add_category->node_type = 'post'; 
				$insert_category = $add_category->insert(); 
			} 
		} 
		//insert post tags 
	   if(!empty($_POST["tags"])){ 
			$tags = $_POST["tags"];
			$new_tags = array(); 
			foreach($tags as $tag){ 
			//get tag id from db 
				$tag_id = Taxonomies::get_tag_id($tag); 
				if($tag_id){ 
					//insert tag 
					$add_tag = new NodesSelectedTaxonomies(); 
					$add_tag->node_id = $inserted_post_id; 
					$add_tag->taxonomy_id = $tag_id->id; 
					$add_tag->node_type = 'post'; 
					$add_tag->taxonomy_type = 'tag'; 
					$insert_tag = $add_tag->insert(); 
				}else{ 
						$add_new_tag = new Taxonomies(); 
						$add_new_tag->parent_id = 0; 
						$add_new_tag->status = "publish"; 
						$add_new_tag->taxonomy_type = 'tag'; 
						$add_new_tag->inserted_date = date_now(); 
						$add_new_tag->inserted_by = $session->user_id; 
						$insert_new_tag = $add_new_tag->insert(); 
						$tag_new_id = $add_new_tag->id; 
						//insert new tag  content 
						$insert_new_tag_content = new TaxonomiesContent(); 
						$insert_new_tag_content->taxonomy_id = $tag_new_id; 
						$insert_new_tag_content->name = $tag; 
						$insert_new_tag_content->alias = preg_replace('/\s+/', '_',$tag); 
						$insert_new_tag_content->lang_id = 0; 
						$add_new_tag_content = $insert_new_tag_content->insert(); 
						$new_tags[] = $tag_new_id; 
				} 
			} 
			//insert new tags 
			foreach($new_tags as $tag){ 
				$add_new_tag = new NodesSelectedTaxonomies(); 
				$add_new_tag->node_id = $inserted_post_id; 
				$add_new_tag->taxonomy_id = $tag; 
				$add_new_tag->taxonomy_type  = 'tag'; 
				$add_new_tag->node_type = 'post'; 
				$insert_new_tag = $add_new_tag->insert();				 
			} 
	   } 
		$data  = array("status"=>"work","inserted_id"=>$inserted_post_id); 
		echo json_encode($data); 
	}else{ 
		$data  = array("status"=>"error"); 
		echo json_encode($data); 
	}	 
}else{ 
	//validation error 
	$comma_separated = implode("<br>", $check_required_fields); 
	$data  = array("status"=>"valid_error", "fileds"=>$comma_separated); 
	echo json_encode($data); 
} 
} 
//close connection 
if(isset($database)){ 
$database->close_connection(); 
} 
?>
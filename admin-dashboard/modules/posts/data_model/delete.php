<?php 
require_once('../../../../classes/Session.php'); 
require_once('../../../../classes/Functions.php'); 
require_once('../../../../classes/MysqlDatabase.php'); 
require_once('../../../../classes/Nodes.php'); 
require_once('../../../../classes/Users.php'); 
require_once('../../../../classes/Profile.php'); 
//check  session user  log in 
if($session->is_logged() == false){ 
	redirect_to("../../../index.php"); 
} 
// get user profile   
$user_data = Users::find_by_id($session->user_id); 
// get user profile data 
$user_profile  = Profile::Find_by_id($user_data->user_profile); 
// check if the user profile block 
if($user_profile->profile_block == "yes"){ 
   redirect_to("../../../index.php");	 
} 
if(!empty($_GET["task"]) && $_GET["task"] == "delete"){ 
	//send json
	header('Content-Type: application/json');
	//get data 
	$id = $_GET['id']; 
	//find record	 
	$find_post = Nodes::find_by_id($id); 
	//check global edit and delete authorization 
	if($user_profile->global_delete == 'all_records' || $find_post->inserted_by == $session->user_id){ 
		  //if there is record perform delete 
		  //if there is no record go back to view 
		  if($find_post){ 
			  $delete = $find_post->delete(); 
			  if($delete){ 
				//delete all realted tags and caegories 
				 
				$sql_delete_tags = "DELETE FROM nodes_selected_taxonomies WHERE node_id = '{$id}' 
				 AND  node_type = 'post'"; 
				$preform_delete_tags = $database->query($sql_delete_tags); 
				//delete post info 
				$sql_delete_info = "DELETE FROM nodes_content WHERE node_id = '{$id}'"; 
				$preform_delete_info = $database->query($sql_delete_info);				   
				//delete comments 
				$sql_delete_comments = "DELETE FROM social_comments WHERE node_id = '{$id}' "; 
				$preform_delete_comments = $database->query($sql_delete_comments);	 
				//delete from node plugins value 
				$sql_delete_node_plugins_values = "DELETE FROM nodes_plugins_values WHERE node_id = '{$id}' AND type = 'post'"; 
				$preform_delete_node_plugins_values = $database->query($sql_delete_node_plugins_values);		 
				$data = array("status"=>"work");
				echo json_encode($data);
			  }else{ 
			      	$data = array("status"=>"failed");
				   echo json_encode($data);
				
			  }	 
			  //if there is no record go back to view 
		  }else{ 
			  redirect_to("../view.php");	 
		  } 
	 }else { 
			   redirect_to("../view.php"); 
	 }   
}else{ 
	
	//if task wasnot delete go back to view 
				
				  redirect_to("../view.php"); 
} 
//close connection 
if(isset($database)){ 
	$database->close_connection(); 
} 
?>
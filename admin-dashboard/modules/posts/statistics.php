<?php 
	 require_once("../layout/initialize.php"); 
	//post info 
	//filter data 
	$status = null; 
	$date_from = null; 
	$date_to = null; 
	$cat_id = null; 
	$lang = null; 
	$inserted_by = null; 
	//get filter data 
	if(!empty($_GET["status"])){ 
	 $status = $_GET["status"]; 
	} 
	if(!empty($_GET["inserted_by"])){ 
	 $inserted_by = $_GET["inserted_by"]; 
	} 
	if(!empty($_GET["lang"])){ 
	 $lang = $_GET["lang"]; 
	} 
	if(!empty($_GET["category"])){ 
	 $cat_id = $_GET["category"]; 
	} 
	if(!empty($_GET["from"])){ 
	 $date_from = $newDate = date("Y-m-d", strtotime($_GET["from"])); 
	} 
	if(!empty($_GET["to"])){ 
	 $date_to = $newDate = date("Y-m-d", strtotime($_GET["to"])); 
	} 
	//post class 
	$define_class = new Nodes;	 
	$define_class->enable_relation(); 

	$current_page = isset($_GET['page']) ? $_GET['page'] : 1 ;
	$per_page = 30;
	$full_records = $define_class->node_data('post',null,$status,$cat_id,$date_from,$date_to,$general_setting_info->translate_lang_id,$inserted_by,"inserted_date","DESC"); 
	$total_records = count($full_records);
	$pagination = new Pagination($current_page,$per_page,$total_records);
    $offset = $pagination->offset();
	$records =   $define_class->node_data('post',null,$status,$cat_id,$date_from,$date_to,$general_setting_info->translate_lang_id,$inserted_by,"inserted_date","DESC",$per_page,$offset); 
	//get all categories 
	$define_cat_class= new Taxonomies(); 
	$define_cat_class->enable_relation(); 
    $categories = $define_cat_class->taxonomies_data('category',null,null,'inserted_date', 'DESC',null,'many',$general_setting_info->translate_lang_id); 
	 

   // GET QUANTITY DISTRIBUTION INFO . 
  $define_quantity_distirbution = new Quantity_Distribution(); 
  $define_quantity_distirbution->enable_relation();
  $record_for_quantity = $define_quantity_distirbution->find_all(); 
	require_once("../layout/header.php"); 
?> 
  <!--header end-->  
  <script src="../../js-crud/post.js"></script>
  <!--sidebar start--> 
  <?php require_once("../layout/navigation.php");?> 
   
  <!--sidebar end--> 
  <section id="main-content">  
    <!--main content start--> 
    <section class="wrapper site-min-height"> 
      <h4>Products Watcher</h4> 
      <!-- page start--> 
      <section class="panel" style="width:2500px;"> 
        <header class="panel-heading"> View Products </header> 
        
        <br/> 
        
        
        <form id="form_categories" >
      <div class="form-group "> 
            <label  class="col-lg-2">Search:</label> 
            <div class="col-lg-5"> 
              <input type="text" class="form-control" id="search_input" placeholder=" " autocomplete="off" style="width:250px" /> 
            </div> 
          </div> 
          </form>
          <br />
          <br />
          <br /> 
        <div class="panel-body"> 
          <div class="adv-table editable-table ">  
             <div class="space15"></div> 
            <table class="table table-striped table-advance table-hover">
              <thead> 
                <tr> 
                  <th>#</th> 
                  <th style="width:327px;"> Title</th> 
                  <th style=""> معرض مدينتى</th> 
                  <th style=""> معرض الرحاب</th> 
                  <th style="">معرض المستقبل</th> 
                  <th style="">معرض الرمايه</th> 
                  <th style="">معرض الميثاق</th> 
                  <th style="">المخزن</th> 
                  <th style="">اجمالى الكميه</th> 
  
                </tr> 
              </thead> 
              <tbody id="myTable" > 
                <?php  
                $serialize=$offset+1; 
			foreach($records as $record){ 
					//get content
					$node_contents = $define_class->get_node_content($record->id);
				         $posts_categories = new NodesSelectedTaxonomies(); 
                 $posts_categories->enable_relation(); 
                 $get_posts_categories = $posts_categories->return_node_taxonomy($record->id,'post','category','many',$general_setting_info->translate_lang_id); 
                 $posts_categories_array = array(); 
                 if(count($get_posts_categories)>0){

                  foreach($get_posts_categories as $category){
                    $posts_categories_array[$category->id] = $category->taxonomy_name ; 
                  }
                  $posts_categories_ids = array(); 
                  foreach($posts_categories_array as $key=>$value){
                    $posts_categories_ids [] = $key ; 
                  }

                  $categories = implode(',', $posts_categories_ids) ; 

                  foreach($get_posts_categories as $sub){
                    $name = $sub->taxonomy_name ;
                  
                  }
                  if($name == 'اثاث' || $name == 'مطابخ' || $name =='مراتب ومفروشات' || $name == 'اخرى' || $name == 'front products' || $name =='احدث العروض'){	

			 echo "<tr id=\"post_{$record->id}\"> 
				  <td>{$serialize}</td> 
				  <td><ul>";

				  foreach($node_contents as $node_content){
					  echo "<li><a href='full_info.php?id={$node_content->content_id}'>- {$node_content->title}</a><li>";
				  }
          echo "</ul></td>"; 


				 // product id
          $record_id = $record->id ;
           $record_info = $define_class->node_data('post',$record_id) ; 
          $exhibitions = $define_cat_class->taxonomies_data('category',null,16,'inserted_date', 'ASC',null,'many',$main_lang_id);

         foreach($exhibitions as $exhibition){
          echo "<td>";
          $find_quantity = Quantity_Distribution::find_exhibtions($record_id , $exhibition->id);
          echo $find_quantity->total_quantity ; 
          
          echo "</td>";  
         }
         echo "<td>";
         $exhibiion_quntaity_store  = Quantity_Distribution::find_exhibtions($record_id,44);
         echo $exhibiion_quntaity_store->total_quantity ; 
          
         echo "</td>"; 
         // get total sum for all quantities in one product : 
         $find_total_sum = Quantity_Distribution::find_by_custom_filed('product_id', $record_id); 
         echo "<td>"; 
          echo "<mark>"; 
         echo $record_info->prod_qty ; 
          echo "</mark>"; 
         echo "</td>"; 
		 echo "</td>		 
				  </tr> ";
          }
				  }
			   $serialize++; 
             }?> 
              </tbody> 
            </table> 
             <?php 
				if($total_records < 1){
					echo "<p> no data to retrieve .</p>";
					
			   }else{
                  echo "<p>Showing   ";
				    echo $offset+1 ;
				  echo  " to ";  
				  if($pagination->total_pages() == $current_page){echo $total_records;}else{echo $offset+$per_page;}
					echo" from ";
					echo $total_records;
					echo " results </p>";
				}
          ?>
        <ul class="pagination pagination-sm pull-right">
         <?php
            if($pagination->total_pages()>1){
              if($pagination->has_previous_page()){
                  echo "<li> <a href='view.php?page=";
                  echo $pagination->previous_page();
                  echo "'>&laquo; prev</a></li>";
              }
              for($i=1;$i<=$pagination->total_pages();$i++){
                  echo"<li ";
                  if($i == $current_page){
                      echo " class = 'active'";
                      }
                  echo "><a href='view.php?page={$i}'";
                  
                  echo " >{$i}</a></li>";
                  }
              if($pagination->has_next_page()){
                  echo "<li> <a href='view.php?page=";
                  echo $pagination->next_page();
                  echo "'>&raquo; next</a></li>";
              }  
                
            
            }
          ?>       
            </ul>
          </div> 
        </div> 
      </section> 
      <!-- page end-->  
    </section> 
  </section> 
  <div style="display:none"> 
    <div class="" id="filter_form"  style=" width:auto; height:500px; "> 
     
          <div class="panel" style=" width:auto;"> 
            <div class="panel-body" > 
              <form class="form-horizontal tasi-form" role="form"  method="get" action="view.php"> 
              <div class="form-group"> 
                  <label class="col-lg-3">Status:</label> 
                  <div class="col-lg-8"> 
                    <label class="checkbox-inline">  
                      <input type="radio" name="status" class="radio" value="draft" <?php if(isset($_GET["status"])){ 
						  if($status == "draft") echo "checked";} ?>> 
                      Draft</label> 
                    <label class="checkbox-inline"> 
                      <input type="radio" name="status" class="radio" value="publish" <?php if(isset($_GET["status"])){ 
						  if($status == "publish") echo "checked";} ?>> 
                      Publish</label> 
                  </div> 
                </div> 
                <div class="form-group"> 
                  <label class="col-lg-3"> Categories:</label> 
                  <div class="col-lg-8" style="max-height: 150px;overflow-y: scroll;"> 
                    <div class="checkboxes"> 
                      <?php  
					   
					 foreach($categories as $category){ 
					   echo "<label class='label_check'><input type='checkbox' value='{$category->id}' name='category[]'"; 
					   if(!empty($_GET["category"])){ 
							if(in_array($category->id, $cat_id)){ 
								echo "checked"; 
							}    
					   } 
					  echo ">{$category->name}</label>"; 
					 }?> 
                    </div> 
                  </div> 
                </div> 
                <div class="form-group"> 
                  <label class=" col-lg-3">Insert Date</label> 
                  <div class="col-lg-8"> 
                     <span class="list-group-item-text">&nbsp;&nbsp;<strong>From</strong></span> 
                      <input type="text" class="form-control " name="from"  id="start" value="<?php if(isset($_GET["from"])){ echo  $date_from;} ?>"> 
                      <br> 
                      <br> 
                      <span class="list-group-item-text">&nbsp;&nbsp;<strong>To</strong></span> 
                      <input type="text" class="form-control" name="to" value="<?php if(isset($_GET["from"])){ echo  $date_from;} ?>" id="end"> 
                     
                    <span class="help-block">Select date range</span> </div> 
                </div> 
                <div class="form-group"> 
                  <label  class="col-lg-2">Created By:</label> 
                  <div class="col-lg-3"> 
                    <?php  $users = Users::find_all();?> 
                    <select multiple="multiple" class="multi-select s" id="my_multi_select1" name="inserted_by[]"> 
                      <?php 
					    
					   
					       foreach ($users as $user) :  
						   if(!empty($inserted_by)){ ?> 
                      <?php  if(in_array($user->id , $inserted_by)){   
						        echo "<option value={$user->id} selected>{$user->user_name}</option>"; 
					      } 
						}else{ 
							echo "<option value={$user->id} >{$user->user_name}</option>"; 
						} 
					  
				  ?> 
                      </option> 
                      <?php endforeach; 
					    
					  ?> 
                    </select> 
                  </div> 
                </div> 
                <div class="form-group"> 
                  <div class="col-lg-offset-2 col-lg-10"> 
                    <button type="submit" class="btn btn-info" >Execute</button> 
                    <button type="reset" class="btn btn-default" onClick="window.location.href = 'view.php'">Clear</button> 
                  </div> 
                </div> 
              </form> 
            </div> 
          </div> 
      
    </div> 
  </div> 
  <!--main content end-->  
  <!--footer start--> 
  <?php require_once("../layout/footer.php");?> 

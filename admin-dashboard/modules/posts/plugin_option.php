<?php 
	require_once("../layout/initialize.php"); 
	if(!isset($_GET['id']) && !is_numeric($_GET['id'])||!isset($_GET['model']) && !is_numeric($_GET['model'])){ 
		redirect_to("view.php"); 
	} 
	$node_type = "post"; 
	$node_id = $_GET['id']; 
	$model_id = $_GET['model']; 
	//get all sidebar plugins for this module 
	$define_models_plugins = new ThemeLayoutModelPlugin(); 
	$define_models_plugins->enable_relation(); 
	$right_plugins = $define_models_plugins->get_model_plugin_data($model_id); 
	//check if sidebar plguin id already exist in node_plugins_options tbl 
	//define how many lang in localization 
	$langs = Localization::find_all(); 
	//if no add plguin id  
	foreach($right_plugins as $plugins){ 
		if($plugins->enable_translate == "yes"){ 
			foreach($langs as $lang){ 
				$get_plugin_option = NodesPluginsValues::get_plugin_values($plugins->plugin_id,$lang->id,$node_id,$node_type); 
				if(!$get_plugin_option){ 
					//insert 
					$add_plugin_record = new NodesPluginsValues(); 
					$add_plugin_record->type = $node_type; 
					$add_plugin_record->node_id = $node_id; 
					$add_plugin_record->lang_id = $lang->id; 
					$add_plugin_record->plugin_id = $plugins->plugin_id; 
					$add_plugin_record->insert(); 
				}				 
			}			 
		}else{ 
			$get_plugin_option = NodesPluginsValues::get_plugin_values($plugins->plugin_id,null,$node_id,$node_type); 
			if(!$get_plugin_option){ 
				//insert 
				$add_plugin_record = new NodesPluginsValues(); 
				$add_plugin_record->type = $node_type; 
				$add_plugin_record->node_id = $node_id; 
				$add_plugin_record->lang_id = 0; 
				$add_plugin_record->plugin_id = $plugins->plugin_id; 
				$add_plugin_record->insert(); 
			}	 
		} 
	} 
	//if save data button clicked save into db 
	if(isset($_POST["save_data"])){ 
		//update  
		foreach($right_plugins as $plugins){ 
			if($plugins->enable_translate == "yes"){ 
				foreach($langs as $lang){ 
					$get_node_plugin = NodesPluginsValues::get_plugin_values($plugins->plugin_id,$lang->id,$node_id,$node_type); 
					$update_plugin_record = NodesPluginsValues::find_by_id($get_node_plugin->id); 
					$text_value = $plugins->plugin_source."_".$lang->label; 
					$update_plugin_record->content = $_POST[$text_value]; 
					$update = $update_plugin_record->update();				 
				} 
			}else{ 
				$get_node_plugin = NodesPluginsValues::get_plugin_values($plugins->plugin_id,null,$node_id,$node_type); 
				$update_plugin_record = NodesPluginsValues::find_by_id($get_node_plugin->id); 
				$update_plugin_record->content = $_POST[$plugins->plugin_source]; 
				$update = $update_plugin_record->update();	 
			} 
		} 
		redirect_to('view.php'); 
	} 
	require_once("../layout/header.php"); 
	include("../../assets/texteditor4/head.php");  
	require_once("../layout/navigation.php"); 
  ?> 
<!--sidebar end-->  
<!--main content start--> 
<section id="main-content"> 
  <section class="wrapper site-min-height"> 
    <h4> Pages Module</h4> 
    <div class="row"> 
      <aside class="col-lg-10"> 
        <section> 
          <div class="panel"> <br/> 
            <div class="panel-heading"> Plugin Option</div> 
            <div class="panel-body"> 
              <form class="form-horizontal tasi-form" role="form" id="form_crud" method="post"> 
                <input type="hidden" id="process_type" value="options"> 
			    <?php 
                	foreach($right_plugins as $plugins){ 
						if($plugins->enable_translate == "yes"){ 
							foreach($langs as $lang){ 
								require("../../../plugins/{$plugins->plugin_source}/controller.php");  
							} 
						}else{ 
							require_once("../../../plugins/{$plugins->plugin_source}/controller.php");  
						} 
					} 
				?>               
                <div class="form-group"> 
                  <div class="col-lg-offset-2 col-lg-10"> 
                    <button type="submit" class="btn btn-info" id="submit" name="save_data">Save</button> 
                    <div id="loading_data"></div> 
                  </div> 
                </div> 
              </form> 
            </div> 
          </div> 
        </section> 
      </aside> 
    </div> 
    <!-- page end-->  
  </section> 
</section> 
<!--main content end-->  
<!--footer start--> 
<?php require_once("../layout/footer.php");?>
<?php  
	require_once("../layout/initialize.php"); 
	require_once("../layout/header.php"); 
?> 
  <script type="text/javascript" src="../../js-crud/crud_user_password.js"></script>  
  <!--header end-->  
  <!--sidebar start--> 
  <?php require_once("../layout/navigation.php");?> 
  <!--sidebar end-->  
  <!--main content start--> 
  <section id="main-content"> 
    <section class="wrapper site-min-height"> 
      <h4>Uesr Profile</h4> 
      <div class="row"> 
        <aside class="profile-info col-lg-8"> 
          <section> 
            <div class="panel panel-primary"> 
              <div class="panel-heading"> Update Password</div> 
              <div class="panel-body"> 
                <form class="form-horizontal" role="form" id="form_crud" action="data_model/update_pass.php"> 
                  <input type="hidden" id="process_type" value="update"> 
                  <div class="form-group"> 
                    <label  class="col-lg-3 ">Current Password:</label> 
                    <div class="col-lg-8"> 
                      <input type="password" class="form-control" id="current_pass" placeholder=" "> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                    <label  class="col-lg-3 ">New Password:</label> 
                    <div class="col-lg-8"> 
                      <input type="password" class="form-control" id="new_pass" placeholder=" "> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                    <label  class="col-lg-3 ">Re-type New Password:</label> 
                    <div class="col-lg-8"> 
                      <input type="password" class="form-control" id="retype_new_pass" placeholder=" "> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                    <div class="col-lg-offset-2 col-lg-10"> 
                      <button type="submit" id="submit" class="btn btn-info">Save</button> 
                      <button type="button" class="btn btn-default">Cancel</button> 
                      <div id="loading_data"></div> 
                    </div> 
                  </div> 
                </form> 
              </div> 
            </div> 
          </section> 
        </aside> 
      </div> 
       
      <!-- page end-->  
    </section> 
  </section> 
  <!--main content end-->  
  <!--footer start--> 
   
  <?php require_once("../layout/footer.php");?>
<?php
require_once('../initialize_classes_files.php'); 
if(!isset($front_session->user_id)){
	redirect_to('login.php') ; 
}
// get student id  from session  : 
$student_id = $front_session->user_id ; 
// student class new object to get student data && student information 
$student       = Students::find_by_id($student_id) ; 
$student_code  = $student->student_code ; 
$student_name  = $student->student_name ; 
$student_email = $student->student_email ; 
$student_stage = $student->student_stage ; 
$student_class = $student->student_class ; 
$seat_number   = $student->student_code ; 
$student_code  = $student->student_code ; 

################################################################################
#
#
#     -- Making Objects from all our classes to check students results 
#
#
#
################################################################################

$finalPrepFirstSecond          = FinalPrepFirstSecond::find_by_custom_filed('student_code' , $student_code); 
$finalPrimaryFourthFifth       = FinalPrimaryFourthFifth::find_by_custom_filed('student_code' , $student_code); 
$finalPrimarySecondThird       = FinalPrimarySecondThird::find_by_custom_filed('student_code' , $student_code); 
$finalSecondaryFirst           = FinalSecondaryFirst::find_by_custom_filed('student_code' , $student_code); 
$finalSecondarySecondArt       = FinalPrepFirstSecond::find_by_custom_filed('student_code' , $student_code); 
$finalSecondarySecondScience   = FinalSecondarySecondScience::find_by_custom_filed('student_code' , $student_code); 
$midTermPrepFirstSecond        = MidTermPrepFirstSecond::find_by_custom_filed('student_code' , $student_code); 
$midTermPrimaryFourthFifth     = MidTermPrimaryFourthFifth::find_by_custom_filed('student_code' , $student_code); 
$midTermPrimarySecondThird     = MidTermPrimarySecondThird::find_by_custom_filed('student_code' , $student_code); 
$midYearPrepFirstSecond        = MidYearPrepFirstSecond::find_by_custom_filed('student_code' , $student_code); 
$midYearPrimaryFourthFifth     = MidYearPrimaryFourthFifth::find_by_custom_filed('student_code' , $student_code); 
$midYearPrimarySecondThird     = MidYearPrimarySecondThird::find_by_custom_filed('student_code' , $student_code); 
$midYearSecondaryFirst         = MidYearSecondaryFirst::find_by_custom_filed('student_code' , $student_code); 
$midYearSecondarySecondArt     = MidYearSecondarySecondArt::find_by_custom_filed('student_code' , $student_code); 
$midYearSecondarySecondScience = MidYearSecondarySecondScience::find_by_custom_filed('student_code' , $student_code); 
$monthSecondaryFirst           = MonthSecondaryFirst::find_by_custom_filed('student_code' , $student_code); 
$monthSecondarySecondArt       = MonthSecondarySecondArt::find_by_custom_filed('student_code' , $student_code); 
$monthSecondarySecondScience   = MonthSecondarySecondScience::find_by_custom_filed('student_code' , $student_code); 


?>


<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title></title>

<link href='css/bootstrap.min.css' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="css/font-awesome.min.css">
<link href='css/google.font.css' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="index.css">
 

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />



<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
</head>
<body>
<!-- Where all the magic happens -->
<!-- LOGIN FORM -->
<div class="text-center" style="padding:50px 0">

    <div class="logo">MLS School's Grading System (Results)</div>
    <div class="welcome-student">Welcome : <?php echo $student_name ;  ?></div>

    <div class="available-results">
        <ul id="results">
  
       
        
        </ul>



<a href='#' data-toggle='modal' data-target='#myModal' >Secondary Final Report For Stage : ".$student_stage."</a>
            <!-- 
            
            modal immplementation : 
            i wish to use one modal changed with id of te result but as u know not the same stage  and not the same subject 
            
            -->

            

            <!-- Modal -->
            <div class="modal fade " id="myModalPrimaryPrep" role="dialog">
                <div class="modal-dialog modal-lg" >
                
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" style="color:#aaaaaa">Result in Details</h4>
                    </div>
                    <div class="modal-body">
                    <div class='table-responsive'>
                    <table class='table table-condensed'>
                    <thead>
                   

                    </thead>
                   
                    </table>
                    </div>
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
                
                </div>
            </div>


            <!-- end modal -->


    </div>

</div>	 
   
	<script src="js/jquery-1.11.3.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	 
	 
</body>
</html>
<?php 

require_once('../initialize_classes_files.php'); 


?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title></title>

<link href='css/bootstrap.min.css' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="css/font-awesome.min.css">
<link href='css/google.font.css' rel='stylesheet' type='text/css'>

 

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<link rel="stylesheet" href="index.css">


<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
</head>
<body>
<!-- Where all the magic happens -->
<!-- LOGIN FORM -->
<div class="text-center" style="padding:50px 0">
	<div class="logo">MLS School's Grading System</div>
	<!-- Main Form -->
	<div class="login-form-1">
		<form id="login-form" class="text-left" action="data_model/login_process.php" method="post">
			<div class="login-form-main-message"></div>
			<div class="main-login-form">
				<div class="login-group">
					<div class="form-group">
						<label for="lg_username" class="sr-only">Studentcode</label>
						<i class="fa fa-user fa-2x" aria-hidden="true" style="position: absolute;"></i>
						<input type="text" class="form-control" id="student_code" name="lg_username" placeholder="Student Code" style="padding-left: 35px;">
						 
					</div>
					<div class="form-group">
						<label for="lg_password" class="sr-only">Password</label>
						<i class="fa fa-key fa-2x" aria-hidden="true" style="position: absolute;"></i>
						<input type="password" class="form-control" id="student_password" name="lg_password" placeholder="Student Password" style="padding-left: 35px;">
					</div>
					
				</div>
				<button type="submit" class="login-button" id="login_btn"><i class="fa fa-chevron-right"></i></button>
			</div>
			<div class="etc-login-form">
				<p>forgot your password? <a href="#">click here</a></p>
				
			</div>
		</form>
	</div>
	<!-- end:Main Form -->
</div>



 
<!--<div class="text-center" style="padding:50px 0">
	<div class="logo">forgot password</div>
	 
	<div class="login-form-1">
		<form id="forgot-password-form" class="text-left">
			<div class="etc-login-form">
				<p>When you fill in your registered email address, you will be sent instructions on how to reset your password.</p>
			</div>
			<div class="login-form-main-message"></div>
			<div class="main-login-form">
				<div class="login-group">
					<div class="form-group">
						<label for="fp_email" class="sr-only">Email address</label>
						<input type="text" class="form-control" id="fp_email" name="fp_email" placeholder="email address">
					</div>
				</div>
				<button type="submit" class="login-button"><i class="fa fa-chevron-right"></i></button>
			</div>
			<div class="etc-login-form">
				<p>already have an account? <a href="#">login here</a></p>
				<p>new user? <a href="#">create new account</a></p>
			</div>
		</form>
	</div>
	 
</div>-->
	 
   
	<script src="js/jquery-1.11.3.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.validate.min.js"></script>
	 <script src="index.js"></script>

</body>
</html>
<?php 

require_once('../../classes/FrontSession.php'); 

require_once('../../classes/Students.php'); 

require_once("../../classes/MysqlDatabase.php"); 


header('Content-Type: application/json'); 

if(isset($_POST["task"]) && $_POST["task"] == "login"){ 

	if(isset($_POST["student_code"]) && isset($_POST["student_password"])){ 

		$student_code = $_POST["student_code"]; 

		$student_password = $_POST["student_password"]; 
		
		$find_student = Students::find_by_code_password($student_code, $student_password); 

		//send message 

		if($find_student){ 
		
			 $front_session->Login($find_student); 
             $_SESSION['student_code'] = $student_code ; 
             $_SESSION['student_password'] = $student_password ; 
			 $data  = array("status"=>"work"); 
			 echo json_encode($data); 

		}else{ 

			$data  = array("status"=>"error"); 

			echo json_encode($data); 

		}		 

	}

} 

//close connection 

if(isset($database)){ 

	$database->close_connection(); 

} 

?>	

//callback of the external plugins
$(document).ready(function() {
	if($('.various').length){
		$(".various").fancybox({
		maxWidth	: 800,
		maxHeight	: 470,
		fitToView	: false,
		width		: '70%',
		height		: '70%',
		autoSize	: true,
		closeClick	: false,
		openEffect	: 'fade',
		closeEffect	: 'fade'
	});
	}



	$('.main-features-carousel').owlCarousel({
		items: 5,
		itemsDesktop: [1199,5],
		navigation: true,
		navigationText: false,
		pagination: true , 
		autoPlay:true
	})

	$('.main-team-carousel').owlCarousel({
		items: 3,
		navigation: true,
		navigationText: false,
		itemsDesktop: [1199,3], 
		autoPlay:true 
	})


	$('.main-products-carousel').owlCarousel({
		items: 3,
		navigation: true,
		navigationText: false,
		itemsDesktop: [1199,3],
		autoPlay:true 
	})

	$('.owl-carousel').owlCarousel({
		items: 4,
		itemsDesktop: [1199,4],
		navigation: true,
		navigationText: false,
		pagination: true
	})

	



});


//call and cancel the search bar 

$('.header-search-btn').click(function(){
	$('.header-search').addClass('active')
})

$('.header-search-close').click(function(){
	$('.header-search').removeClass('active')
})

function openDropdownMenu(el,parent){
	el.onclick = function(){
		parent.classList.toggle('open')
	}
}

//audio

function togglePlay(){
	var el = document.getElementById('audio-player')
	if(el.paused){
		el.play()
		audioBtn.children[0].classList.add('fa-pause-circle-o')
	}else{
		el.pause()
		audioBtn.children[0].classList.remove('fa-pause-circle-o')
	}
}
var audioBtn = document.getElementById('audio-btn')
if(audioBtn){
	audioBtn.addEventListener('click',togglePlay)
}

//API

 rjq('#rjp-radiojar-player').radiojar('player', {
  "streamName": "uf6x8w5f81ac",
  "enableUpdates": true,
  "defaultImage": "http://radiojar.com/img/sample_images/Radio_Stations_Avatar_BLUE.png",
  "autoplay":false
 });
 rjq('#rjp-radiojar-player').off('rj-track-load-event');
 rjq('#rjp-radiojar-player').on('rj-track-load-event', function(event, data) {
   updateInfo(data);
   if (data.title != "" || data.artist != "") {
     rjq('.rjp-trackinfo-container').show();
     rjq('#trackInfo').html(data.artist + ' - "' + data.title + '"')
   } else {
     rjq('.rjp-trackinfo-container').hide();
   }
 });

 function updateInfo(data) {
   if (data.thumb) {
     rjq('#rj-cover').html('<a href="#"><img src="' + data.thumb + '" alt="" title="" /></a>')
   } else {
     rjq('#rj-cover').html('')
   }
 }

 function audioPlayer(){
 	var el = document.querySelector('.main-player')
 	var elOffset = el.offsetTop
 	var oldOffset = 231
 	if(window.pageYOffset >= elOffset){
 		el.classList.add('fixed')
 	}
 	 if(window.pageYOffset <= oldOffset){
 		el.classList.remove('fixed')
 		console.log(oldOffset)
 	}
 	return el
 }
var elementExist = audioPlayer()
if(elementExist){
window.addEventListener('scroll',audioPlayer)
}


//prepend the navbar button
$('.header-nav').prepend('<div class="navbar-header"><button class="navbar-toggle" data-toggle="collapse" data-target="#header-collapse"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button></div>')